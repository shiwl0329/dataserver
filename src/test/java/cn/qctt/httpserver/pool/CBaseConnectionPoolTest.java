package cn.qctt.httpserver.pool;

import org.junit.Test;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;

public class CBaseConnectionPoolTest {

    @Test
    public void test() throws Exception {
        new CBaseConnectionPool("182.92.78.154", 80000, 3600).afterPropertiesSet();
//        String key = "gyc11";
//        insert(key);
        System.out.println(get("20151120142722527789"));
//        CBaseConnectionPool.getBucket("default").replace(CBaseConnectionPool.getBucket("default").get(key));
//        Thread.sleep(2000);
//        System.out.println(CBaseConnectionPool.getBucket("default").get(key).expiry());
//        testTouch(key);
    }

    private static String get(String key){
    	return CBaseConnectionPool.getBucket("default").get(key).toString();
    }
    private static void insert(String key){
    	JsonObject content = JsonObject.empty().put(key, "Michael");
    	JsonDocument doc = JsonDocument.create(key,60, content);
    	CBaseConnectionPool.getBucket("default").upsert(doc);
    }
    
    private static void testTouch(String key){
    	CBaseConnectionPool.getBucket("").touch(key, 0);
    }
    
    private void testSqlTiming(){
    	  long s1 = System.currentTimeMillis();
          System.out.println(CBaseConnectionPool.getBucket("default")
                  .query(Query.simple("select * from default limit 10")).allRows());
          long s2 = System.currentTimeMillis();
          System.out.println(s2 - s1);
          System.out.println(CBaseConnectionPool.getBucket("default")
                  .query(Query.simple("select * from default where sClasses ='NBA' limit 10")).allRows());
          long s3 = System.currentTimeMillis();
          System.out.println(s3 - s2 + "==");
          System.out.println(CBaseConnectionPool.getBucket("default")
                  .query(Query.simple("select * from default UNNEST default.topN as topN where topN like '%互联网%' limit 10")).allRows());
          long s4 = System.currentTimeMillis();
          System.out.println((s4 - s3) + "----");
          System.out.println(CBaseConnectionPool.getBucket("default")
                  .query(Query.simple("select * from default UNNEST default.topN as topN where topN like '互联网%' limit 10")).allRows());
          long s5 = System.currentTimeMillis();
          System.out.println(s5 - s4);
          System.out.println(CBaseConnectionPool.getBucket("default").get("0135b5864db74a88a0ce5264e438993d"));
          System.out.println(System.currentTimeMillis() - s5);
    	
    }
}
