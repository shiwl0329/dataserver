package cn.qctt.httpserver.utils;

import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

public class HBaseUtilTest {

	@Test
	public void test() throws IOException, Exception {
		HTableInterface htable = getCon().getTable("news");
        HashMap<String, String> hm = new HashMap<String, String>();
        try {
            Get get = new Get("20151112121106228190".getBytes());
            Result result = htable.get(get);
            String col = null;
            String key = null;
            for (Cell cell : result.listCells()) {
                col = Bytes.toString(CellUtil.cloneQualifier(cell));
                key = (col == null ? Bytes.toString(CellUtil.cloneFamily(cell)) : col);
                hm.put(key, Bytes.toString(CellUtil.cloneValue(cell)));
            }
//            System.err.println(hm.size());
            System.err.println(hm.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                htable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

	
	private HConnection getCon() throws IOException{
		Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "182.92.116.37");
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        return HConnectionManager.createConnection(conf);
	}
}
