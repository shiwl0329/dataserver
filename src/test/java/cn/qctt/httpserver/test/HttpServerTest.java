/**
 * Wasu.com Inc.
 * Copyright (c) 2014-2015 All Rights Reserved.
 */

package cn.qctt.httpserver.test;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.handler.Handler;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Map;

/**
 * @author chenzehe
 * @description
 * @email chenzehe@wasu.com
 * @create 2015年3月17日 下午9:24:53
 */

@RequestMapping("/example")
public class HttpServerTest implements Handler {

    private static ClassPathXmlApplicationContext classPathXmlApplicationContext;

    public String invoke(Request request, Response response) {
        StringBuilder bulider = new StringBuilder();
        Map<String, List<String>> maps = request.getParams();
        for (String key : maps.keySet()) {
            bulider.append(key + ":" + request.getParam(key) + "\n");
        }
        bulider.append("cookie:" + request.getCookie("kk"));
        return bulider.toString();
    }

    public static void main(String[] args) {
        try {
            classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring/applicationContext.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        classPathXmlApplicationContext.close();
    }
}
