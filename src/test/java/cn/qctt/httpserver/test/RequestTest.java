package cn.qctt.httpserver.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.Utils;

public class RequestTest {

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		CountDownLatch count = new CountDownLatch(100); 
		MyThread th = null;
		ExecutorService pool = Utils.getThreadPool();
		for(int i = 0;i<100 ;i++){
			th = new MyThread(count, i);
			pool.execute(th);
		}
		System.out.println( start);
		 
	}

	
}
class MyThread implements Runnable{
	CountDownLatch count = null;
	int userId = 0;
	String[] types = new String[]{"up" ,"down"};
	public MyThread(CountDownLatch count ,int userId){
		this.count = count;
		this.userId = userId;
	}
	
	public void run() {
		int num = 0;
		while(num < 50){
			String url = "http://10.173.24.71:8090/dataServer/V3.0/news/getNewsById?token=NeXT&newsId=20160525162305803282&userId="+userId;
			HttpClientUtil.sendGetRequest(url);
			num++;
		}
		System.out.println(userId+"=="+System.currentTimeMillis());
//		count.countDown();
	}
	
}
