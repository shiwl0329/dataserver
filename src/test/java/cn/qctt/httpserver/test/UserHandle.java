/**
 * Wasu.com Inc.
 * Copyright (c) 2014-2015 All Rights Reserved.
 */

package cn.qctt.httpserver.test;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.handler.Handler;

import java.util.List;
import java.util.Map;

/**
 * @author chenzehe
 * @description
 * @email chenzehe@wasu.com
 * @create 2015年3月31日 下午9:57:33
 */
@RequestMapping("/user")
public class UserHandle implements Handler {

    @Override
    public String invoke(Request request, Response response) {
        StringBuilder bulider = new StringBuilder();
        Map<String, List<String>> maps = request.getParams();
        for (String key : maps.keySet()) {
            bulider.append(key + ":" + request.getParam(key) + "\n");
        }
        return bulider.toString();
    }

}
