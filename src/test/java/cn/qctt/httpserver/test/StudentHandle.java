/**
 * Wasu.com Inc.
 * Copyright (c) 2014-2015 All Rights Reserved.
 */

package cn.qctt.httpserver.test;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.handler.Handler;

import java.util.List;
import java.util.Map;

@RequestMapping("/student")
public class StudentHandle implements Handler {

    @Override
    public String invoke(Request request, Response response) {
        StringBuilder bulider = new StringBuilder();
        Map<String, List<String>> maps = request.getParams();
        for (String key : maps.keySet()) {
            bulider.append(key + ":" + request.getParam(key) + "\n");
        }
        System.out.println(bulider.toString() + "===========");
        return bulider.toString();
    }

}
