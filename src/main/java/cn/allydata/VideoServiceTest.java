package cn.allydata;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class VideoServiceTest {

	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring/video_client.xml");
		VideoProcessService service = (VideoProcessService) ctx.getBean("videoProcessService");
        System.err.println(service.getVideoStreamUrl("http://www.wasu.cn/Play/show/id/7203788?refer=video.baidu.com"));
	}

}
