package cn.allydata;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2016/5/17.
 */
public interface VideoProcessService {
    public List<Map<String, Object>> getVideoStreamUrl(String h5);
    public Set<String> getAllProcess();
}
