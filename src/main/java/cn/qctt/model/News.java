﻿package cn.qctt.model;

import java.io.Serializable;
import java.util.List;


/**
 * Created by Administrator on 15-6-3.
 */
public class News implements Serializable {
    private static final long serialVersionUID = -7178085449362200462L;
    private String news_title;//标题（必选）
    private String abs;//摘要
    private String pub_time;//发布时间（必选）2015-01-01 00:00:00
    private String create_time;//爬取时间（必选）2015-01-01 00:00:00
    private String url;//URL（必选）---http://*.*.*
    private String news_source;//来源网站名（必选）--- 搜狗
    private String site_name;
    private String list_images;//封面图片（必选）---a.png,b.png,c.png（至少3张）
    private int list_images_style;
    private String content;//内容（必选）
    private String channel;//分类（必选）
    private String tags;//新闻标签（可选）
    private String id;//评论要用的hash_id（必选）
    private String author;//作者（可选）
    private int hub_page_id;//种子页的id
    private long up_count;//点赞数
    private long down_count;//点踩数
    private long comment_count;//评论数
    private String path;//新闻类别路径
    private int is_hot;
    private String org_url;//原链接地址
    private String hub_site;///种子页类型
    private List<String> replyList;//评论列表
    private String link_type;//跳转类型
    private int duration;//视频播放时长
    /**
     * 新闻类型
     * @see News.TYPE
     */
    private TYPE type;

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    //    private String group;//新闻类别 可选值(news  weibo video)
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getHub_site() {
        return hub_site;
    }

    public void setHub_site(String hub_site) {
        this.hub_site = hub_site;
    }

    public String getOrg_url() {
        return org_url;
    }

    public void setOrg_url(String org_url) {
        this.org_url = org_url;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getAbs() {
        return abs;
    }

    public void setAbs(String abs) {
        this.abs = abs;
    }

    public String getPub_time() {
        return pub_time;
    }

    public void setPub_time(String pub_time) {
        this.pub_time = pub_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNews_source() {
        return news_source;
    }

    public void setNews_source(String news_source) {
        this.news_source = news_source;
    }

    public String getList_images() {
        return list_images;
    }

    public void setList_images(String list_images) {
        this.list_images = list_images;
    }

    public int getList_images_style() {
        return list_images_style;
    }

    public void setList_images_style(int list_images_style) {
        this.list_images_style = list_images_style;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getHub_page_id() {
        return hub_page_id;
    }

    public void setHub_page_id(int hub_page_id) {
        this.hub_page_id = hub_page_id;
    }

    public long getUp_count() {
        return up_count;
    }

    public void setUp_count(long up_count) {
        this.up_count = up_count;
    }

    public long getDown_count() {
        return down_count;
    }

    public void setDown_count(long down_count) {
        this.down_count = down_count;
    }

    public long getComment_count() {
        return comment_count;
    }

    public void setComment_count(long comment_count) {
        this.comment_count = comment_count;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(int is_hot) {
        this.is_hot = is_hot;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public List<String> getReplyList() {
        return replyList;
    }

    public void setReplyList(List<String> replyList) {
        this.replyList = replyList;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public static enum TYPE{
        /**
         * 新闻
         */
        NEWS{
            public int getType() {
                return 1;
            }
        },
        /**
         * 微博
         */
        NEWS_WEIBO{
            public int getType() {
                return 2;
            }
        },
        /**
         * 视频
         */
        NEWS_VIDEO{
            public int getType() {
                return 3;
            }
        },
        /**
         * 贴吧
         */
        POST_BAR{
            public int getType() {
                return 4;
            }
        },
        /**
         * 明星
         */
        STARS{
            public int getType() {
                return 7;
            }
        };

        public abstract int getType();
    }
}


