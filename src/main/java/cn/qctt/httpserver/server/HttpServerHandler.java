package cn.qctt.httpserver.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.handler.Handler;
import cn.qctt.httpserver.handler.HandlerMap;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.manager.TokenManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * ClassName:HttpServerInboundHandler <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2014年10月23日 上午10:03:10 <br/>
 *
 * @author Max dou
 * @version V1.0
 * @update yanchao_guo
 * @see
 * @since JDK 1.6
 */
public class HttpServerHandler extends SimpleChannelInboundHandler<Object> {

	private static Logger logger = LoggerFactory.getLogger(HttpServerHandler.class);

	private Request request;
	private Response response;
	private static String[] bips = ConfigInit.getValue("reqBlackIps").split(",");
	private static String[] burls = ConfigInit.getValue("reqBlackUrls").split(",");
	/*
	 * int DEFAULT_MAX_THREAD_NUM = 10; private ExecutorService processorPool =
	 * Executors .newFixedThreadPool(DEFAULT_MAX_THREAD_NUM); private static
	 * final HttpDataFactory factory = new DefaultHttpDataFactory(
	 * DefaultHttpDataFactory.MINSIZE); // Disk private HttpPostRequestDecoder
	 * decoder;
	 */
	/**
	 * 是否设置为长连接
	 */
	private boolean keepalive;
	/**
	 * 返回值类型
	 */
	private String contentType;
	/**
	 * 返回字符值
	 */
	private String charset;

	public HttpServerHandler(boolean keepalive, String contentType, String charset) {
		this.keepalive = keepalive;
		this.contentType = contentType;
		this.charset = charset;
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		// super.channelReadComplete(ctx);
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		logger.error("error:", cause);
		ResultMsg res = new ResultMsg();
		res.addReturnDesc(cause.getMessage());
		res.addReturnCode(CodeManager.CODE_500);
		ctx.write(res.toJson());
	}

	@Override
	protected void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {
		if (msg instanceof Request) {
			request = (Request) msg;
			if (!filterReqBlackIps(request.getRequestIp())&&!filterReqBlackUrls(request.getPath())) {
				logger.info("request begin -> " + request.getRequestIp());
				logger.info(request.toString());
				String jsonResult = "";
				ResultMsg res = new ResultMsg();
				response = new Response(contentType, charset);
				String token = request.getParam("token");
				try {
					String requestPath = request.getPath();
					String key = "";
					try {
						if (requestPath != null && requestPath.startsWith(BasicConstants.URL_PREFIX) && requestPath.split("/").length > 3) {
							key = BasicConstants.URL_PREFIX + "/" + requestPath.split("/")[3];
						} else {
							key = BasicConstants.URL_PREFIX + "/notFound";
						}
					} catch (Exception e) {
						logger.error("", e);
						key = BasicConstants.URL_PREFIX + "/notFound";
					}
					Handler handle = HandlerMap.getHandle(key);

					if (requestPath == null || requestPath.length() == 0) {
						logger.warn("request uri path is empty ");
						res.addReturnCode(CodeManager.MISSING_PARAMETERS);
						res.addReturnDesc("request uri path is empty ");
						jsonResult = res.toJson();
					} else if (null == token && !requestPath.startsWith(BasicConstants.URL_PREFIX+"/token")) {
						logger.warn("token is empty ");
						res.addReturnCode(CodeManager.MISSING_PARAMETERS);
						res.addReturnDesc("token is empty ");
						jsonResult = res.toJson();
					} else if (!requestPath.startsWith(BasicConstants.URL_PREFIX+"/token") && !TokenManager.checkToken(token)) {
						logger.warn("token is invalid ");
						res.addReturnCode(CodeManager.CODE_400);
						res.addReturnDesc("token is invalid ");
						jsonResult = res.toJson();
					} else if (handle != null) {
						jsonResult = handle.invoke(request, response);
					} else {
						res.addReturnCode(CodeManager.CODE_404);
						res.addReturnDesc("404 Not Found");
						jsonResult = res.toJson();
					}

				} catch (Exception e) {
					e.printStackTrace();
					logger.error("error message is :" + e);
					res.addReturnCode(CodeManager.CODE_500);
					res.addReturnDesc(e.getMessage());
					jsonResult = res.toJson();
					response.setContent(jsonResult.getBytes());
				} finally {
					response.setKeepAlive(keepalive);
					response.setContent(jsonResult);
					response.setRequestTime(request.getRequestTime());
					logger.debug("response content :" + jsonResult);
					ctx.write(response);
				}
				
			}else{
				ctx.write(request);
			}
		} else {
			ctx.write(msg);
		}
	}

	private boolean filterReqBlackUrls(String path) {
		if(path == null){
			return true;
		}
		for (String v : burls) {
			if (path.equals(v)) {
				logger.debug("filter "+v);
				return true;
			}
		}
		return false;
	}

	private boolean filterReqBlackIps(String ip) {
		for (String v : bips) {
			if (ip.equals(v)) {
				logger.debug("filter "+v);
				return true;
			}
		}
		return false;
	}

}
