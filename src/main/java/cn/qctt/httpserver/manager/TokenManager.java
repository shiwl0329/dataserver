package cn.qctt.httpserver.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.util.StringUtils;

import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import redis.clients.jedis.Jedis;

public class TokenManager {
    private static Logger logger = LoggerFactory.getLogger(TokenManager.class);
    private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
    private static String token_pre = "tkn";
//    private static Jedis cluster = RedisConnectionPool.getCluster();
    private static final String TokenUserTab = "tokens";

    public static TokenManager getInstance() {
        return new TokenManager();
    }

    // token失效返回false
    public static boolean checkToken(String token) {
    	Jedis cluster = RedisConnectionPool.getCluster();
        String ADMIN_TOKEN = BasicConstants.ADMIN_TOKEN;
        if (!token.equals(ADMIN_TOKEN) && null == cluster.get(token)) {
            return false;
        }
        cluster.close();
        return true;
    }

    public static String getToken(String tuId, String expire) {
    	Jedis cluster = RedisConnectionPool.getCluster();
        String uidKey = token_pre + "_" + tuId;
        String token = cluster.get(uidKey);
        if (!StringUtils.isEmpty(token)) {
            return token;
        } else {
            token = UUID.randomUUID().toString().replaceAll("-", "");
            cluster.set(uidKey, token);
            logger.info("set uidKey ="+ uidKey +", token = " +token);
            cluster.set(token, tuId);
            logger.info("set token ="+ token +", token = " +tuId);
            if(Integer.parseInt(expire) > 0){
            	cluster.expire(uidKey, Integer.parseInt(expire));
            	logger.info("expire uidKey ="+ uidKey +", expire = " +expire);
            	cluster.expire(token, Integer.parseInt(expire));
            	logger.info("expire token ="+ token +", expire = " +expire);
            }

        }
        cluster.close();
        return token;
    }

    public static String getExpire(String name, String pwd) {
        String sql = "select expire from " + TokenUserTab + " where token_user = '" + name + "' and token_pwd = '" + pwd + "'";
        String expire = connectionUtil.query(sql, new ResultSetExtractor<String>() {
            @Override
            public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                while (resultSet.next()) {
                	return resultSet.getString("expire");
                }
                return null;
            }
        });
        logger.info("get expire in sql [" + sql + "]");
        if (expire != null) {
            return expire;
        } else {
            return null;
        }
    }
}
