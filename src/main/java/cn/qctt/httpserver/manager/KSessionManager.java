package cn.qctt.httpserver.manager;

import java.io.File;
import java.net.MalformedURLException;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.core.BasicConstants;

public class KSessionManager {
	private static Logger logger = LoggerFactory.getLogger(KSessionManager.class);
	private static StatefulKnowledgeSession ksession = null;
	private static KnowledgeBase kbase = readKnowledgeBase();
	
	public static StatefulKnowledgeSession getKSession() {
		if(null == ksession){
			ksession = kbase.newStatefulKnowledgeSession();
		}
		return ksession;
	}
	/** 执行规则 
	private static void executeRule(Person p) throws Exception {
		KnowledgeBase kbase = readKnowledgeBase();
		StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
		KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory
				.newFileLogger(ksession, "test");
		// 将fact实例P放入WorkMemory中
		ksession.insert(p);
		// 执行规则
		ksession.fireAllRules();
		logger.close();
	}
*/
	/** 加载规则包 */
	@SuppressWarnings("deprecation")
	private static KnowledgeBase readKnowledgeBase()  {
		// 加载规则文件
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		File f = new File(DroolsMonitorTimer.class.getResource("/"+BasicConstants.RULENAME).getPath());
		try {
			kbuilder.add(ResourceFactory.newUrlResource(f.toURL()), ResourceType.DRL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 查找规则文件错误提示
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		// 将规则包加入规则库当中
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

		return kbase;

	}

	/** 规则刷新 */
	public static StatefulKnowledgeSession refreshKSession() throws Exception {
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		// 遍历规则包
		for (KnowledgePackage kpackage : kbase.getKnowledgePackages()) {
			// 删除规则包
			kbase.removeKnowledgePackage(kpackage.getName());
		}
		// 添加规则包
		kbase = readKnowledgeBase();
		ksession = kbase.newStatefulKnowledgeSession();
		logger.info("refresh ksession");
		return ksession;
	}
	
	public static void main(String[] args) {
//		File f = new File(DroolsMonitorTimer.class.getResource("/Sample.drl").getPath());
	}
}
