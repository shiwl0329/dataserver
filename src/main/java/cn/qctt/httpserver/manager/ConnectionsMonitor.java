package cn.qctt.httpserver.manager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.EmailUtil;
import cn.qctt.httpserver.utils.HBaseUtil;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

public class ConnectionsMonitor extends TimerTask {
	private static final Logger logger = LoggerFactory.getLogger(ConnectionsMonitor.class);
	
	public void run() {
		String info = "";
		try {
			info = testRedis();
			logger.info("redis is ok...");
			String key = testCBaseGet();
			logger.info("couchbase is ok...");
			if(testHbaseGet(key).size() == 0){
				info += " || not found content in hbase , id is "+key;
			}else{
				logger.info("hbase is ok...");
			}
		} catch (Throwable e) {
			EmailUtil.send("warn", info +"==="+ExceptionUtils.getFullStackTrace(e));
		}

	}

	private String testRedis() throws Exception{
		String key = "test:"+Utils.randomInt(100);
		String val = "test";
		Jedis redis = null;
		try {
			redis = RedisConnectionPool.getCluster();
			logger.info("redis key is "+key);
			redis.set(key, val);
			String v = redis.get(key);
			if(!v.equals(val)){
				return RedisConnectionPool.getClusterInfo();
			}
			redis.del(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("redis",e);
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		return "";
	}
	
	private String testCBaseGet() {
		String sql = "select id from "+BasicConstants.SOURCE_BUCKET+" limit 1";
		QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.SOURCE_BUCKET, sql);
		String id = null;
		if (queryResult.finalSuccess()) {
			JsonObject val = null;
			for (QueryRow row : queryResult.allRows()) {
				val = row.value();
				id = val.getString("id");
			}
		}
		return id;
	}
	
	private Map<String,String> testHbaseGet(String rowKey) throws IOException, Exception{
		if(null == rowKey){
			return new HashMap<>();
		}
		return HBaseUtil.getByRowKey(BasicConstants.NEWS_HTABLE ,rowKey);
	}
}
