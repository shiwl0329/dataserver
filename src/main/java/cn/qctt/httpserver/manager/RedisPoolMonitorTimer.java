package cn.qctt.httpserver.manager;

import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.pool.RedisConnectionPool;

public class RedisPoolMonitorTimer extends TimerTask{
	Logger logger = LoggerFactory.getLogger(RedisPoolMonitorTimer.class);
	@Override
	public void run() {
		RedisConnectionPool.init();
		logger.info("reset redis pool");
	}

}
