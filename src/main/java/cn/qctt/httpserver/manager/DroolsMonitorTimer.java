package cn.qctt.httpserver.manager;

import java.io.File;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.core.BasicConstants;

public class DroolsMonitorTimer extends TimerTask{
	private static Logger logger = LoggerFactory.getLogger(DroolsMonitorTimer.class);
	
	private static final File drlFile = new File(DroolsMonitorTimer.class.getResource("/"+BasicConstants.RULENAME).getPath());
	private static long lastModified = 0;
	@Override
	public void run() {
		if(lastModified != drlFile.lastModified()){
			lastModified = drlFile.lastModified();
			try {
				KSessionManager.refreshKSession();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("更新drools 规则失败");
			}
		}
		
	}

	public static void main(String[] args) {
		System.out.println(DroolsMonitorTimer.class.getResource("/Sample.drl"));
		System.out.println(new File(DroolsMonitorTimer.class.getResource("/Sample.drl").getPath()).lastModified());
	}
}
