package cn.qctt.httpserver.manager;

public class CodeManager {
		public static final String CODE_500 = "500";  //网络超时
		public static final String CODE_200 = "200";
		public static final String CODE_404 = "404";
		public static final String CODE_400 = "400";  //没有权限
		public static final String CODE_401 = "401";  //操作非法
		public static final String CODE_402 = "402";  //操作异常

		public static final String MISSING_PARAMETERS = "40101";
		
		public static final String BM_UPSERTCONFIG = "40201";
		public static final String BM_GETCONFIGURLBYID = "40202";
		public static final String BM_COMMENTSWITCH = "40203";
		public static final String BM_PUSHMSGTOREADLIST = "40204";
		public static final String BM_SENDFORNEWUSER = "40205";
		public static final String BM_STARHOME = "40206";
		public static final String BM_ADDCARD = "40207";
		public static final String BM_DELCARD = "40208";
		public static final String BM_TOPNSTARS = "40209";
		public static final String BM_UPCONFIG = "40210";
		
		public static final String COMMENT_PUTCOMMENT = "40301";
		public static final String COMMENT_PUTREPLY = "40302";
		public static final String COMMENT_GETCOMMENTANDREPLYS = "40303";
		public static final String COMMENT_DELCOMMENT = "40304";
		public static final String COMMENT_DELREPLY = "40305";
		public static final String COMMENT_UPCOMMENT = "40306";
		public static final String COMMENT_UPREPLY = "40307";
		public static final String COMMENT_GETALL = "40308";
		public static final String COMMENT_GETCOMMENTSBYID = "40309";
		public static final String COMMENT_NEWS_DISABLED = "40310";
		
		public static final String FLOWTEST = "40401";
		
		public static final String HQL = "40501";
		
		public static final String NEWS_GETNEWSBYID = "40601";
		public static final String NEWS_GETCONTENTBYID = "40602";
		public static final String NEWS_GETNEWSBYIDINHBASE= "40603";
		public static final String NEWS_GETNEWSPROBYID = "40604";
		public static final String NEWS_IS_DELETED = "40605";

		public static final String NOTICE = "40701";
		
		public static final String SIMHASHTEST = "40801";

		public static final String USERREFRESH_UPANDDOWNREFRESH = "40901";
		
		public static final String USERACTION_INCRE = "41001";
		public static final String USERACTION_SEARCH = "41002";
		public static final String USERACTION_UPSERTUSERATTR = "41003";
		public static final String USERACTION_GETUSERATTRBYID = "41004";
		public static final String USERACTION_GETRECOMMENTSTARS = "41005";
		public static final String USERACTION_GETNOREADCOUNT = "41006";
		
		public static final String RELATIONHANDLER_ADDTAKE = "42001";
		public static final String RELATIONHANDLER_ISTAKE = "42002";
		public static final String RELATIONHANDLER_DELTAKE = "42003";
		public static final String RELATIONHANDLER_GETTAKEBYID= "42004";
		public static final String RELATIONHANDLER_GETFOLLOWBYUSERID= "42005";
		public static final String RELATIONHANDLER_UPDATERELATION= "42006";
		public static final String RELATIONHANDLER_PINSTAR= "42007";
		public static final String RELATIONHANDLER_PINSTAR_42008= "42008";
		
		public static final String ESINDEXHANDLER_SEARCH= "42101";
		public static final String ESINDEXHANDLER_ADDINDEX= "42102";
		
		/**
		 * couchbase
		 */
		public static final String COUCHBASE_NOT_FOUND_KEY= "51001";
		
}
