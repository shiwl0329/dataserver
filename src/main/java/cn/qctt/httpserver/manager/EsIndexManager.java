package cn.qctt.httpserver.manager;

import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;

import cn.qctt.httpserver.bean.CurlResponse;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.utils.DateUtil;
import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.HttpUrlUtil;
import cn.qctt.httpserver.utils.Utils;

public class EsIndexManager extends TimerTask {
	private static final Logger logger = LoggerFactory.getLogger(EsIndexManager.class);

	public void run() {
		String isDelIndex = ConfigInit.getValue("esIndexDel");
		int day = Integer.parseInt(DateUtil.format(new Date(), "dd"));
		if (day != 28) {
			logger.info("EsIndexManager monitor...");
			return;
		}
		if (isDelIndex.equals("true")) {
			String url = ConfigInit.getValue("esUrl");
			long time = DateUtil.getBeforeMonthTime(1);
			String myurl = url + "/suggest_" + DateUtil.format(new Date(time), "yyMM");
			CurlResponse rep = HttpUrlUtil.get4Rep(myurl);
			logger.info("code is " + rep.getCode());
			if (rep.getCode() == 200) {
				try {
					time = DateUtil.getAfterMonthTime(1);
					myurl = url + "/suggest_" + DateUtil.format(new Date(time), "yyMM");
					create(myurl, "suggest_"+DateUtil.format(new Date(), "yyMM"));
					time = DateUtil.getBeforeMonthTime(1);
					myurl = url + "/suggest_" + DateUtil.format(new Date(time), "yyMM");
					delete(myurl);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				logger.info("not fount this db");
			}
			time = DateUtil.getBeforeMonthTime(2); //删除三个月前的
			myurl = url + "/nm_" + DateUtil.format(new Date(time), "yyMM");
			rep = HttpUrlUtil.get4Rep(myurl);
			if (rep.getCode() == 200) {
				try {
					time = DateUtil.getAfterMonthTime(1);
					myurl = url + "/nm_" + DateUtil.format(new Date(time), "yyMM");
					create(myurl, "nm_"+DateUtil.format(new Date(), "yyMM"));
					time = DateUtil.getBeforeMonthTime(2);
					myurl = url + "/nm_" + DateUtil.format(new Date(time), "yyMM");
					delete(myurl);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				logger.info("not fount this db");
			}
			int currentMonthInt = Integer.parseInt(DateUtil.format(new Date(), "MM"));
			if (currentMonthInt % 2 == 1) {
				myurl = url + "/nm_uneven";
			} else {
				myurl = url + "/nm_even";
			}
			rep = HttpUrlUtil.get4Rep(myurl);
			if (rep.getCode() == 200) {
				try {
					delete(myurl);
					create(myurl, "nm_"+DateUtil.format(new Date(), "yyMM"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				logger.info("not fount this db");
			}
		} else {
			String url = ConfigInit.getValue("esUrl");

			long time;
			String myurl;
			try {
				time = DateUtil.getAfterMonthTime(1);
				myurl = url + "/suggest_" + DateUtil.format(new Date(time), "yyMM");
				create(myurl, "suggest_"+DateUtil.format(new Date(), "yyMM"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				time = DateUtil.getAfterMonthTime(1);
				myurl = url + "/nm_" + DateUtil.format(new Date(time), "yyMM");
				create(myurl, "nm_"+DateUtil.format(new Date(), "yyMM"));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private void delete(String url) {
		try {
			logger.info("url:" + url);
			CurlResponse re = HttpUrlUtil.Delete4Curl(url);
			if (re.getCode() == 200) {
				String data = new String(re.getContent());
				logger.info(data);
			}
		} catch (Exception e) {
			logger.error("", e);
			e.printStackTrace();
		}
	}

	private void create(String url, String fileName) throws Exception {
		String json = null;
		logger.info("url:" + url);
//		json = new String(Utils.readStream(Utils.getConfigFileStream(fileName)));
		json = getCreatedIndexJson(fileName);
		String re = HttpClientUtil.sendPostRequest(url, json, "utf-8");
		logger.info(re);
	}
	
	private static String getCreatedIndexJson(String indexName){
		String url =  ConfigInit.getValue("esUrl") + "/" + indexName;
		logger.info("fetch index info using ["+url+"]");
		String re = HttpClientUtil.sendGetRequest(url);
		List<String> mappings = JsonPath.read(re, "$..mappings");
		List<String> settings = JsonPath.read(re, "$..settings");
		return "{"+"\"mappings\":"+Utils.getGson().toJson(mappings.get(0))+",\"settings\":"+Utils.getGson().toJson(settings.get(0))+"}";
	}
	
	public static void main(String[] args) {
		System.out.println(getCreatedIndexJson("nm_1511"));
	}
}
