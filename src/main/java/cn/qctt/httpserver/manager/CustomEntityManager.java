package cn.qctt.httpserver.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.util.StringUtils;

import cn.qctt.httpserver.bean.OtherEntity;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import cn.qctt.httpserver.utils.DateUtil;
import cn.qctt.httpserver.utils.EmailUtil;
import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

public class CustomEntityManager extends TimerTask {
	private static final Logger logger = LoggerFactory.getLogger(CustomEntityManager.class);
//	private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
	public void run() {
		String mysql = "SELECT entity.name ,entity.badge,GROUP_CONCAT(DISTINCT entity_class.class_first) as first_class_ids, entity.id,img,entity.intro,entity.name,entity.nicknames,entity.rank FROM entity LEFT JOIN entity_class on entity.id = entity_class.entity_id  where entity.updated_time> DATE_SUB(NOW(), INTERVAL 7 DAY) and `status`=2 GROUP BY entity.id";
		logger.info("mysql is "+mysql);
		List<OtherEntity> ls = ConnectionUtil.getMysqlConnect().query(mysql, new ResultSetExtractor<List<OtherEntity>>() {
			public List<OtherEntity> extractData(ResultSet resultSet)
					throws SQLException, DataAccessException {
				List<OtherEntity> ls = new ArrayList<>();
				while (resultSet.next()) {
					OtherEntity entity = new OtherEntity();
					entity.setId(resultSet.getString("id"));
					entity.setBadge(resultSet.getString("badge"));
					entity.setFirst_class_ids(resultSet.getString("first_class_ids"));
					entity.setImg(resultSet.getString("img"));
					entity.setIntro(resultSet.getString("intro"));
					entity.setName(resultSet.getString("name"));
					entity.setNicknames(resultSet.getString("nicknames"));
					entity.setRank(resultSet.getDouble("rank"));
					
					ls.add(entity);
				}
				return ls;
			}
		});
		Jedis redis = null;
		try {
			redis = RedisConnectionPool.getCluster();
			mysql = "update entity set `status` = 1 ,updated_time=NOW() where id in ($ids)";
			StringBuffer sb = new StringBuffer();
			StringBuffer msg = new StringBuffer();
			Map<String, Object> doc = null;
			Map<String, Object> suMap = null;
			StringBuffer docs = new StringBuffer();
			for(OtherEntity entity:ls){
//				String redisKey = BasicConstants.ENTITY_HOME_LEN_PREFIX + ":" + DateUtil.getDateFormat("MMdd").format(Utils.getTimeMillisBefore(1))+":"+entity.getId();
				String redisKey = BasicConstants.ENTITY_HOME_PREFIX + ":" + entity.getId();
				long count =  redis.zcard(redisKey);
				if(count < BasicConstants.ENTITY_MAX_PASS_NEWS_SIZE){
					redisKey = BasicConstants.ENTITY_HOME_LEN_PREFIX + ":" + DateUtil.getCurrentTime("MMdd")+":"+entity.getId();
					String val = redis.get(redisKey);
					if(null != val){
						count =  Integer.parseInt(val);
					}
				}
				
				if(count >= BasicConstants.ENTITY_MAX_PASS_NEWS_SIZE){
					sb.append(entity.getId() + ",");
					msg.append(entity.getName()+" ,");
					
					doc = new HashMap<>();
					suMap = new HashMap<>();
					List<String> inputs = new ArrayList<>();
					inputs.add(entity.getName());
					if(!StringUtils.isEmpty(entity.getNicknames())){
						inputs.addAll(Arrays.asList(entity.getNicknames().split(",")));
					}
					suMap.put("output", entity.getName());
					suMap.put("input", inputs.toArray(new String[]{}));
					doc.put("_index", BasicConstants.ENTITY_INDEX_DB);
					doc.put("_type", BasicConstants.ENTITY_INDEX_TABLE_OTHER);
					doc.put("_id", entity.getId());
					entity.setName_su(suMap);
//					jsonObj.add(BasicConstants.ENTITY_SUGGEST_FIELD, Utils.getJsonParser().parse(suMap.toString()));
					docs.append("{\"index\":" + Utils.getGson().toJson(doc) + "} \n");
					docs.append(Utils.getGson().toJson(entity) +"\n");
				}
			}
			if(sb.length()>1){
				mysql = mysql.replace("$ids", sb.toString()+"-1");
				logger.info("mysql is "+mysql);
				ConnectionUtil.getMysqlConnect().execute(mysql);
				String esUrl = ConfigInit.getValue("esUrl"); 
				logger.info("url=" + esUrl +" , body="+docs);
				String re = HttpClientUtil.sendPostRequest(esUrl + "/_bulk", docs.toString(), "utf-8");
				logger.info(re);
				EmailUtil.send("新增实体 "+DateUtil.getCurrentDate() ,msg.toString());
			}
		} catch (Exception e) {
			logger.error("" ,e);
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		
	}
 
	public static void main(String[] args) {
		 
	}
}
