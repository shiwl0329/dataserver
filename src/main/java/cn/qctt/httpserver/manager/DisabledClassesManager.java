package cn.qctt.httpserver.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import redis.clients.jedis.Jedis;

public class DisabledClassesManager extends TimerTask {
	private static final Logger logger = LoggerFactory.getLogger(DisabledClassesManager.class);
//	private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
	public void run() {
		String mysql = "SELECT id FROM entity_class_tree WHERE `level` = 1 AND `status` = 0";
		logger.info("mysql is "+mysql);
		List<String> ls = ConnectionUtil.getMysqlConnect().query(mysql, new ResultSetExtractor<List<String>>() {
			public List<String> extractData(ResultSet resultSet)
					throws SQLException, DataAccessException {
				List<String> ls = new ArrayList<>();
				while (resultSet.next()) {
					ls.add(resultSet.getString("id"));
				}
				return ls;
			}
		});
		Jedis redis = null;
		try {
			if(ls!=null && ls.size()>0){
				redis = RedisConnectionPool.getCluster();
				redis.sadd(BasicConstants.DISABLED_CLASS_KEY, ls.toArray(new String[]{}));
			}
		} catch (Exception e) {
			logger.error("" ,e);
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		
	}
 
	public static void main(String[] args) {
		 
	}
}
