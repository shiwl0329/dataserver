package cn.qctt.httpserver.pool;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

public class HbaseConnectionPool implements InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(HbaseConnectionPool.class);
    private String zkHosts = "";
    private static Configuration conf = null;
    private static HConnection conn = null;

    public HbaseConnectionPool() {
    }

    public static Configuration getConfig(){
    	return conf;
    }
    public HbaseConnectionPool(String zkHosts) {
        this.zkHosts = zkHosts;
    }

    public static HConnection getConnection() throws Exception {
        if (conn == null || conn.isClosed()) {
            new HbaseConnectionPool().afterPropertiesSet();
        }
        return conn;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", zkHosts);
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        conn = HConnectionManager.createConnection(conf);
        logger.info("zkHosts is [" + zkHosts + "]");
    }

    public void setZkHosts(String zkHosts) {
        this.zkHosts = zkHosts;
    }

}
