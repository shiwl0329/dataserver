package cn.qctt.httpserver.pool;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.sql.Connection;
import java.sql.SQLException;

public class MySqlConnectionPool implements InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(MySqlConnectionPool.class);
    private static BoneCP connectionPool = null;
    private String jdbcUrl;
    private String userName;
    private String pwd;
    private int minConnections;
    private int maxConnections;
    private int partitionCount;

    public static void setConnectionPool(BoneCP connectionPool) {
        MySqlConnectionPool.connectionPool = connectionPool;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setMinConnections(int minConnections) {
        this.minConnections = minConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public void setPartitionCount(int partitionCount) {
        this.partitionCount = partitionCount;
    }

    public MySqlConnectionPool() {
    }

    public MySqlConnectionPool(String jdbcUrl, String userName, String pwd, int minConnections, int maxConnections, int partitionCount) {
        this.jdbcUrl = jdbcUrl;
        this.userName = userName;
        this.pwd = pwd;
        this.minConnections = minConnections;
        this.maxConnections = maxConnections;
        this.partitionCount = partitionCount;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
        logger.info("init jdbc");
    }

    public void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            BoneCPConfig config = new BoneCPConfig();
            config.setJdbcUrl(jdbcUrl);
            config.setUsername(userName);
            config.setPassword(pwd);
            config.setMinConnectionsPerPartition(minConnections);
            config.setMaxConnectionsPerPartition(maxConnections);
            config.setPartitionCount(partitionCount);
            config.setCloseConnectionWatch(true);
            config.setLogStatementsEnabled(true);
            connectionPool = new BoneCP(config);
        } catch (Exception e) {
            logger.error("init:" + e);
        }
    }

    public static Connection getConnection() throws SQLException {
        if (connectionPool != null) {
            return connectionPool.getConnection();
        } else {
            logger.info("mysql connection pool is null...");
        }
        return null;
    }

    public static void closeConnection(Connection connection) throws SQLException {
        if (null != connection && !connection.isClosed()) {
            connection.close();
        }
    }

}
