package cn.qctt.httpserver.pool;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisConnectionPool implements InitializingBean {
//	private static final Logger logger = LoggerFactory.getLogger(RedisConnectionPool.class);

	private static JedisPool pool ; 
    private static String hostsAndPorts = null;
    private static String pwd = null;
    private static int maxWaitMillis;
    private static int maxTotal;
    private static int minIdle;
    private static int maxIdle;


    public RedisConnectionPool() {
    }

    public RedisConnectionPool(String hostsAndPorts, int maxWaitMillis, int maxTotal, int minIdle, int maxIdle,String pwd) {
    	RedisConnectionPool.hostsAndPorts = hostsAndPorts;
    	RedisConnectionPool.maxWaitMillis = maxWaitMillis;
    	RedisConnectionPool.maxTotal = maxTotal;
    	RedisConnectionPool.minIdle = minIdle;
    	RedisConnectionPool.maxIdle = maxIdle;
    	RedisConnectionPool.pwd = pwd;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    public static void init() {
    	JedisPoolConfig config = new JedisPoolConfig();  
        //控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；  
        //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。  
        config.setMaxTotal(maxTotal);  
        //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。  
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
        //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；  
//        config.setMaxWaitMillis(maxWaitMillis);  
        //在获取连接的时候检查有效性, 默认false
//        config.setTestOnBorrow( true );    
        /*//返回一个jedis实例给连接池时，是否检查连接可用性（ping()）
        config.setTestOnReturn( true );
        //在空闲时检查有效性, 默认false
        config.setTestWhileIdle( true );     
        config.setMaxWaitMillis(3000);
        //逐出连接的最小空闲时间 默认1800000毫秒(30分钟)
        config.setMinEvictableIdleTimeMillis(1000L * 60L * 1L);
        //对象空闲多久后逐出, 当空闲时间>该值 ，且 空闲连接>最大空闲数 时直接逐出,不再根据MinEvictableIdleTimeMillis判断  (默认逐出策略)，默认30m   
        config.setSoftMinEvictableIdleTimeMillis(1000L * 60L * 1L);                        
        //逐出扫描的时间间隔(毫秒) 如果为负数,则不运行逐出线程, 默认-1
        config.setTimeBetweenEvictionRunsMillis(60000); //1m
        //每次逐出检查时 逐出的最大数目 如果为负数就是 : 1/abs(n), 默认3
        config.setNumTestsPerEvictionRun(10);*/
        String[] array= hostsAndPorts.split(":");
        /*if(pool !=null){
        	logger.info("pool.getNumIdle() = "+pool.getNumIdle()+" ,maxIdle = "+maxIdle+" ,pool.getNumActive() = " + pool.getNumActive());
//	        if(pool.getNumIdle() >= maxIdle ){
	        	logger.info("destroy redis pool...");
	        	pool.destroy();
//	        }
        }*/
        if(StringUtils.isEmpty(pwd)){
        	pool = new JedisPool(config,  array[0], Integer.parseInt(array[1])) ;
        }else{
        	pool = new JedisPool(config, array[0], Integer.parseInt(array[1]) ,maxWaitMillis ,pwd);  
        }
    }

    public synchronized static Jedis getCluster() {
    	Jedis jedis = pool.getResource();
        return jedis;
    }
    public static String getClusterInfo() {
    	return "pool.getNumIdle() = "+pool.getNumIdle()+" ,maxIdle = "+maxIdle+" ,pool.getNumActive() = " + pool.getNumActive();
    }

    public void setHostsAndPorts(String hostsAndPorts) {
    	RedisConnectionPool.hostsAndPorts = hostsAndPorts;
    }

    public void setMaxWaitMillis(int maxWaitMillis) {
    	RedisConnectionPool.maxWaitMillis = maxWaitMillis;
    }

    public void setMaxTotal(int maxTotal) {
    	RedisConnectionPool.maxTotal = maxTotal;
    }

    public void setMinIdle(int minIdle) {
    	RedisConnectionPool.minIdle = minIdle;
    }

    public void setMaxIdle(int maxIdle) {
    	RedisConnectionPool.maxIdle = maxIdle;
    }

	public static void setPool(JedisPool pool) {
		RedisConnectionPool.pool = pool;
	}

	public void setPwd(String pwd) {
		RedisConnectionPool.pwd = pwd;
	}

}
