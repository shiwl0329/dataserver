package cn.qctt.httpserver.pool;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.PersistTo;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

import cn.qctt.httpserver.core.BasicConstants;

public class CBaseConnectionPool implements InitializingBean {
    private String nodes = "";
    private int timeOut;
    private int keepAliveInterval;
    private int autoreleaseAfter = 5000;
    private int computationPoolSize = 500;
    private int ioPoolSize = 50;
    private static Cluster cluster = null;
    private static Bucket bucket = null;
    private static Map<String, Bucket> bucketMap = new HashMap<String, Bucket>();

    public CBaseConnectionPool() {
    }

    public CBaseConnectionPool(String nodes, int timeOut, int keepAliveInterval) {
        this.nodes = nodes;
        this.timeOut = timeOut;
        this.keepAliveInterval = keepAliveInterval;
    }

    public static Cluster getConnection() {
        return cluster;
    }

    public static Bucket getBucket(String bucketName, String pwd) {
        Bucket bt = bucketMap.get(bucketName);
        if (null == bt) {
        	synchronized (CBaseConnectionPool.class) {
        		if (null == bt) {
	        		  bt = open(bucketName, pwd);
	                  bucketMap.put(bucketName, bt);
        		}
			}
        }
        return bt;
    }

    private static synchronized Bucket open(String bucketName, String pwd){
    	return cluster.openBucket(bucketName, pwd);
//    	return cluster.openBucket(bucketName, pwd,60 ,TimeUnit.SECONDS);
    }
    
    public static Bucket getBucket(String bucketName) {
    	if(bucketName == null || bucketName == ""){
    		bucketName = BasicConstants.SOURCE_BUCKET;
    	}
        return getBucket(bucketName, "");
    }

    public static QueryResult sql(String bk ,String sql){
    	QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
    	return queryResult;
    }
    /**
     * 根据key 删除文档
     * @param bk
     * @param key
     */
    public static void deleteByKey(String bk ,String key){
    	getBucket(bk).remove(key);
    }
    /**
     * 根据key 获取文档
     * @param bk
     * @param key
     */
    public static JsonDocument get(String bk ,String key){
    	return getBucket(bk).get(key);
    }
    /**
     * 添加或更新文档
     * @param bk
     * @param doc
     */
    public static void upsert(String bk, JsonDocument doc){
    	getBucket(bk).upsert(doc ,PersistTo.MASTER);
    }
    
    
    
    
    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    public void setKeepAliveInterval(int keepAliveInterval) {
        this.keepAliveInterval = keepAliveInterval;
    }

    public void setAutoreleaseAfter(int autoreleaseAfter) {
        this.autoreleaseAfter = autoreleaseAfter;
    }

    
    public void setComputationPoolSize(int computationPoolSize) {
		this.computationPoolSize = computationPoolSize;
	}

	public void setIoPoolSize(int ioPoolSize) {
		this.ioPoolSize = ioPoolSize;
	}

	public static void setCluster(Cluster cluster) {
		CBaseConnectionPool.cluster = cluster;
	}

	public static void setBucket(Bucket bucket) {
		CBaseConnectionPool.bucket = bucket;
	}

	public static void setBucketMap(Map<String, Bucket> bucketMap) {
		CBaseConnectionPool.bucketMap = bucketMap;
	}

	@Override
    public void afterPropertiesSet() throws Exception {
        DefaultCouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
//                .connectTimeout(timeOut)
                .queryEnabled(true)
                .queryTimeout(5500)
                .computationPoolSize(computationPoolSize)
                .ioPoolSize(ioPoolSize)
//                .autoreleaseAfter(autoreleaseAfter)
//                .bootstrapCarrierEnabled(false)
                .build();
        cluster = CouchbaseCluster.create(env, nodes.split(","));
    }

    public static void main(String[] args) throws InterruptedException {
    	long s1 = System.currentTimeMillis();
    	DefaultCouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
                .queryEnabled(true)
                .build();
        cluster = CouchbaseCluster.create(env, "10.162.223.45");
        bucket = cluster.openBucket("default");
        QueryResult queryResult = bucket.query(Query.simple("select * from user_news tb1 left join default tb2 on keys tb1.end_id where tb2.type = 3 and tb1.collect_relation='1' and tb1.start_id='16' order by tb1.update_time desc limit 11"));
        for (QueryRow row : queryResult.allRows()) {
    		System.out.println(row.value().toMap());
    	}
       Thread.currentThread().sleep(3000);
        bucket.close();
        long s2 = System.currentTimeMillis();
        System.out.println(s2-s1);
         
	}

}
