package cn.qctt.httpserver.handler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;

@RequestMapping(BasicConstants.URL_PREFIX+"/notFound")
public class NotFoundHandler implements Handler{
	private static Logger logger = LoggerFactory.getLogger(NotFoundHandler.class);
	public String invoke(Request request, Response response) throws IOException {
		ResultMsg resultObject = new ResultMsg();
		resultObject.addReturnCode(CodeManager.CODE_404);
		resultObject.addReturnDesc("404 Not Found");
		logger.warn("not found this url ["+request.getUri()+"]");
		return resultObject.toJson();
	}

}
