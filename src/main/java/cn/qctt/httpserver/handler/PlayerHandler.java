package cn.qctt.httpserver.handler;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.rpc.server.VideoServer;

@RequestMapping(BasicConstants.URL_PREFIX + "/player")
public class PlayerHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(PlayerHandler.class);

	private static String getVideoStreamUrl = BasicConstants.URL_PREFIX + "/player/getVideoStreamUrl";
	private static String getAllProcessUrl = BasicConstants.URL_PREFIX + "/player/getAllProcess";

	@Override
	public String invoke(Request request, Response response) throws IOException {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		try {
			if (urlPath.startsWith(getVideoStreamUrl)) {
				returnStr = getVideoStream(request);
			} else if (urlPath.startsWith(getAllProcessUrl)) {
				returnStr = getAllProcess(request);
			} else {
				resultObject.addReturnCode(CodeManager.CODE_404);
				resultObject.addReturnDesc("not found :" + urlPath);
				returnStr = resultObject.toJson();
				logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return returnStr;
	}

	private String getAllProcess(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		try {
		 
			Set<String> allProcess = VideoServer.getAllProcess();
			logger.info("allProcess = "+ allProcess);
			resultObject.addResultData(allProcess);
			if(null != null){
				resultObject.putCount(allProcess.size());
			}
		} catch (Throwable e) {
			logger.error("getAllProcess:", e);
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getVideoStream(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String h5 = request.getParam("h5");
		byte[] body = request.getContent();
		try {
			if (body != null) {
				h5 = new String(body);
			}
			List<Map<String, Object>> vodeoObj = VideoServer.getVideoStream(h5);
			logger.info("h5 = "+ h5);
			if (null == vodeoObj || vodeoObj.size() == 0) {
				resultObject.addReturnCode(CodeManager.CODE_402);
				resultObject.addReturnDesc("don't support");
			} else {
				resultObject.addResultData(vodeoObj);
				resultObject.putCount(vodeoObj.size());
			}
			logger.info(" ", vodeoObj);
		} catch (Throwable e) {
			logger.error("getVideoStream:", e);
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("error  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

}
