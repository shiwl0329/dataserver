package cn.qctt.httpserver.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;

@RequestMapping(BasicConstants.URL_PREFIX + "/me")
public class MeHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(MeHandler.class);

	private static String getMyCollectedListUrl = BasicConstants.URL_PREFIX + "/me/getMyCollectedList";

	@Override
	public String invoke(Request request, Response response) throws IOException {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		try {
			if (urlPath.startsWith(getMyCollectedListUrl)) {
				returnStr = getMyCollectedList(request);
			} else {
				resultObject.addReturnCode(CodeManager.CODE_404);
				resultObject.addReturnDesc("not found :" + urlPath);
				returnStr = resultObject.toJson();
				logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return returnStr;
	}

	private String getMyCollectedList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String type = request.getParam("type" ,"1");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			int limit = Integer.parseInt(size);
			int from = (Integer.parseInt(page) - 1) * limit;
			List<Object> list = new ArrayList<>();
			String sql = "select * from user_news tb1 left join default tb2 on keys tb1.end_id where tb2.type = "+type+" and tb1.collect_relation='1' and tb1.start_id='"+userId+"' order by tb1.update_time desc offset "+from+" limit "+limit;
			logger.info("sql is "+sql);
			QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.SOURCE_BUCKET, sql);
			for (QueryRow row : queryResult.allRows()) {
				list.add(row.value().toMap().get("tb2"));
	    	}
			resultObject.putCount(list.size());
			resultObject.addResultData(list);
			 
		} catch (Throwable e) {
			logger.error("getMyCollectedList:", e);
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
 

}
