package cn.qctt.httpserver.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import cn.qctt.httpserver.utils.StringUtils;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX+"/userAction")
public class UserClickActionHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(UserClickActionHandler.class);
//	private static String putUrl = BasicConstants.URL_PREFIX+"/userAction/put"; // 记录用户点击行为
//	private static String getByUserIdUrl = BasicConstants.URL_PREFIX+"/userAction/getByUserId"; // 根据用户Id
	// 获取用户点击行为

	private static String increUrl = BasicConstants.URL_PREFIX+"/userAction/incre";
	/*
	 * private static String increDownUrl = "/userAction/increDown"; private
	 * static String increcommentUrl = "/userAction/increcomment"; private
	 * static String increForwardUrl = "/userAction/increForward"; private
	 * static String increCollectionUrl = "/userAction/increCollection"; private
	 * static String increClickUrl = "/userAction/increClick";
	 */

//	private static String getByHql = BasicConstants.URL_PREFIX+"/userAction/getByHql";
	private static String searchUrl = BasicConstants.URL_PREFIX+"/userAction/search";
	private static String upSertUserAttrUrl = BasicConstants.URL_PREFIX+"/userAction/upSertUserAttr";
	private static String getUserAttrByIdUrl = BasicConstants.URL_PREFIX+"/userAction/getUserAttrById";
	private static String getRecommentStarsUrl = BasicConstants.URL_PREFIX+"/userAction/getRecommentStars";
	private static String getNoReadCountUrl = BasicConstants.URL_PREFIX+"/userAction/getNoReadCount";
	
//	private static JsonParser jsonParser = new JsonParser();
//	private static String bucketName = "user_action";
//	private static String bucketPwd = "";
	private static String userAttributeBucket = "user_attribute";
	private static String defaultName = BasicConstants.SOURCE_BUCKET;
	private static String sourceOtherName = BasicConstants.SOURCE_OTHER_BUCKET;
	private static String sourceOtherPwd = "";
	private static int serachMax = 20;

	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		
		/*if (urlPath.startsWith(getByUserIdUrl)) {
			returnStr = getByUserId(request);
		} else if (urlPath.startsWith(putUrl)) {
			returnStr = putDoc(request);
		} else if (urlPath.startsWith(searchUrl)) {
			returnStr = search(request);
		} else if (urlPath.startsWith(getByHql)) {
			returnStr = getByHql(request);
		} else*/ if (urlPath.startsWith(increUrl)) {
			String type = request.getParam("type");
			returnStr = incre(request, type);
		} else if (urlPath.startsWith(searchUrl)) {
			returnStr = search(request);
		} else if (urlPath.startsWith(upSertUserAttrUrl)) {
			returnStr = upSertUserAttr(request);
		} else if (urlPath.startsWith(getUserAttrByIdUrl)) {
//			returnStr = getUserAttrById(request);
		} else if (urlPath.startsWith(getRecommentStarsUrl)) {
			returnStr = getRecommentStars(request);
		} else if (urlPath.startsWith(getNoReadCountUrl)) {
			returnStr = getNoReadCount(request);
		} else {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}


	private String getNoReadCount(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("userId");
		String prefix = request.getParam("type" ,BasicConstants.READ_LIST_MAIN_CHANNEL);
		Jedis js = RedisConnectionPool.getCluster();
		try {
			if (StringUtils.isBlank(id)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else {
				String key = prefix + ":" + id;
//				resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
				Long count = js.zcard(key);
				resultObject.addResultData((Long)count * 0.85);
				resultObject.putHead("count", 1);
				logger.info("获取未读数据 ("+count+" * 0.85) for "+id);
			}
		} catch (Exception e) {
			logger.error("getNoReadCount:", e);
			logger.error("获取未读数据失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("获取未读数据失败  ,params is [" + request.getParams() + "]");
		}finally{
			js.close();
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getRecommentStars(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String page = request.getParam("page" ,"0");
		String size = request.getParam("size" ,"20");
		Jedis js = RedisConnectionPool.getCluster();
		try {
			if (StringUtils.isBlank(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数userId = "+userId);
			} else {
				int c = Integer.parseInt(size);
				long begin =  (Long.parseLong(page) -1)*c;
				long end = begin +  c - 1;
				Set<String> stars = js.zrevrange(BasicConstants.STAR_CARD_PREFIX + ":" + userId , begin, end);
//				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				Set<Map<String, Object>> ls = Utils.setJson2Map(stars);
				StringBuffer sb = new StringBuffer();
				for(Map<String,Object> m:ls){
					sb.append("'"+m.get("id").toString()+"' ,");
				}
				String sql = "select count(1) as count,end_id as id from "+BasicConstants.USER_STAR_BUCKET+" where end_id in ["+sb.toString()+"''] and type is missing group by end_id";
				QueryResult queryResult = CBaseConnectionPool.getBucket(sourceOtherName)
						.query(Query.simple(sql));
				logger.info("sql is "+sql);
				String id = "";
				for(Map<String,Object> m:ls){
					id =  m.get("id").toString();
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						if (null != val) {
							if (id.equals(val.getString("id"))) {
								m.put("count", val.getInt("count")+"");
							}
						}
					}
				}
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				logger.info("获取推荐明星成功 size["+stars.size()+"] ,params is [" + request.getParams() + "]");
			}
		} catch (Throwable e) {
			logger.error("getRecommentStars:", e);
			logger.error("获取推荐明星失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETRECOMMENTSTARS);
			resultObject.addReturnDesc("获取推荐明星失败 ,params is [" + request.getParams() + "]");
		}finally{
			js.close();
		}
		
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
/*	
	private String getUserAttrById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("userId");
		try {
			if (null == id) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数userId");
			} else {
				JsonDocument doc = CBaseConnectionPool.getBucket(BasicConstants.USER_ATTRIBUTE_BUCKET).get(id);
//				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				if(null == doc){
					resultObject.addReturnDesc("not found...");
				}else{
					resultObject.addResultData(doc.content().toMap());
					resultObject.putHead("count", 1);
				}
				logger.info("获取用户动作成功  ,params is [" + request.getParams() + "]");
			}
		} catch (Exception e) {
			logger.error("getByUserId:", e);
			logger.error("获取用户动作失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETUSERATTRBYID);
			resultObject.addReturnDesc("获取用户动作失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/

	private String upSertUserAttr(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		if (StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("userId is empty...");
		}
		try {
			byte[] content = request.getContent();
			if (null == content) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("error ,body is empty");
			} else {
				JsonObject job = JsonObject.fromJson(new String(content));
				JsonDocument doc = JsonDocument.create(userId, job);
				CBaseConnectionPool.getBucket(userAttributeBucket).upsert(doc);
//				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				resultObject.addReturnDesc("更新成功");
				logger.info("更新成功  ,userId is [" + userId + "]");
			}
		} catch (Exception e) {
			logger.error("upSertUserAttr:", e);
			logger.error("更新失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_UPSERTUSERATTR);
			resultObject.addReturnDesc("更新动作失败 ,error is [" + e.getMessage() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
		
	}

	private String search(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String sql = "select  default.id,  default.insert_date,  default.post_title,  default.post_from from " + sourceOtherName + " as other join " + defaultName + " ON KEYS other.id";
		String search = request.getParam("search");
		String limit = request.getParam("limit" ,serachMax+"");
		String offset = request.getParam("offset" ,"0");
		try {
			if (null == search) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数serach");
			} else {

					String sql1 = sql + " where other.tl like '" + search.trim() + "%' offset "+offset+" limit "+limit;
					QueryResult queryResult = CBaseConnectionPool.getBucket(sourceOtherName)
							.query(Query.simple(sql1));
					/*if (!queryResult.finalSuccess() || queryResult.allRows().size() == 0) {
						logger.info("result is empty for [" + sql1 + "]");
						sql += " where";
						for (String str : tokens) {
							sql += " '"+str+"'  in other.k_w[*].k and";
						}
						sql = sql.substring(0, sql.lastIndexOf("and"));
						sql += " offset "+offset+" limit "+limit;
						queryResult = CBaseConnectionPool.getBucket(sourceOtherName)
								.query(Query.simple(sql),10,TimeUnit.SECONDS);
						if (queryResult.finalSuccess()) {
//							resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
							Set<Map<String,Object>> ls = CouchbaseUtil.resultToSet(queryResult,sourceOtherName);
							resultObject.addResultData(ls);
							resultObject.putHead("count", ls.size());
							logger.info("查询成功 ,sql is [" + sql + "]");
						} else {
							resultObject.addReturnCode(CodeManager.USERACTION_SEARCH);
							resultObject.addReturnDesc("查询失败  ,sql is [" + sql + "]");
							logger.info("查询失败  ,sql is [" + sql + "] , error is " + queryResult.info());
						}
					} else {*/
				if (queryResult.finalSuccess()){
//						resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
						resultObject.addResultData(CouchbaseUtil.resultToList(queryResult,sourceOtherName));
						logger.info("查询成功 ,sql is [" + sql1 + "]");
//					}
				} else {
					resultObject.addReturnCode(CodeManager.USERACTION_SEARCH);
					resultObject.addReturnDesc("查询失败  ,search is [" + search + "]");
					logger.warn("查询失败  ,search is [" + search + "]");
				}
			}
		} catch (Exception e) {
			logger.error("search:", e);
			logger.error("获取动作 失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_SEARCH);
			resultObject.addReturnDesc("获取动作失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String incre(Request request, String key) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String incr = request.getParam("incr" ,"true");
//		String putSource = request.getParam("putSource"); // true:插入default 内容
		Bucket bk = CBaseConnectionPool.getBucket(sourceOtherName, sourceOtherPwd);
		Jedis jedis = null;
		try {
			if (null == newsId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else {
				jedis = RedisConnectionPool.getCluster();
				JsonDocument doc = bk.getAndLock(newsId,1);
				if (doc != null) {
					if("block".equals( doc.content().get("state"))){
						logger.info(newsId + " 禁止操作");
						resultObject.addReturnCode(CodeManager.USERACTION_INCRE);
						resultObject.addReturnDesc(newsId + " 禁止操作");
					}else if("deleted".equals( doc.content().get("state"))){
						logger.info(newsId + " 已被删除");
//						resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
						resultObject.addReturnDesc(newsId + " 已被删除");
					}else{
						int count = 0;
						if(incr.equals("true")){
							count = doc.content().getInt(MappingClass.get(key)) + 1;
							/*if("true".equals(ConfigInit.getValue("countFlag"))){
								if("click".equals(key)){
									jedis.zadd(BasicConstants.REDIS_CLICK_COUNT, count, newsId);
								}else if("comment".equals(key)){
									jedis.zadd(BasicConstants.REDIS_COMMENT_COUNT, count, newsId);
								}
							}*/
						}else{
							count = doc.content().getInt(MappingClass.get(key)) - 1;
						}
						doc.content().put(MappingClass.get(key), count);
						CBaseConnectionPool.getBucket(defaultName).touch(newsId, 0);
						/*if (null != putSource && StringUtils.toBoolean(putSource)) {
							if(doc.content().get("source") == null){
								doc.content().put(BasicConstants.SOURCE_COLUMN_NAME , CBaseConnectionPool.getBucket(defaultName).get(newsId).content());
							}
						}*/
						bk.replace(doc);
						// down：点踩 ,up:点赞 ，comment：评论 ，forward:转载 ，collection：收藏 ，click:点击  都不过期
					/*	if (!"comment".equals(key) && !"collection".equals(key) && !"forward".equals(key)) { // 除了 评论 、转载 、收藏
							logger.info("touch "+newsId +" " +Utils.getExpiry()+"seconds in source_other for [" + key+"]");
							bk.touch(newsId, Utils.getExpiry());
						}*/
//						resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
						resultObject.addReturnDesc(newsId + " " + key + " incre 1");
						logger.info(newsId + " " + key + " incre 1");
					}
				} else {
					resultObject.addReturnCode(CodeManager.USERACTION_INCRE);
					resultObject.addReturnDesc("无法检索到对应的文档  ，newsId is " + newsId);
				}
			}
		} catch (Exception e) {
			logger.error("incre:", e);
			logger.error("incre ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_INCRE);
			resultObject.addReturnDesc("incre  ,params is [" + request.getParams() + "]");
		}finally{
			if(null != jedis){
				jedis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
/*
	private String getByHql(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String sql = request.getParam("sql");
		try {
			if (null == sql) {
				resultObject.addReturnCode(CodeManager._CODE_ERROR);
				resultObject.addReturnDesc("缺失参数sql");
			} else {
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				if (queryResult.finalSuccess()) {
					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(CouchbaseUtil.resultToList(queryResult));
					logger.info("获取用户动作 ,sql is [" + sql + "]");
				} else {
					resultObject.addReturnCode(CodeManager._CODE_ERROR);
					resultObject.addReturnDesc("查询失败 ：" + queryResult.info());
				}
			}
		} catch (Exception e) {
			logger.error("getByHql:", e);
			logger.error("获取用户动作 失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager._CODE_ERROR);
			resultObject.addReturnDesc("获取用户动作失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/

/*	private String getByUserId(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("user_id");
		try {
			if (null == id) {
				resultObject.addReturnCode(CodeManager._CODE_ERROR);
				resultObject.addReturnDesc("缺失参数user_id");
			} else {
				JsonDocument doc = CBaseConnectionPool.getBucket(bucketName, bucketPwd).get(id);
				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				if(null == doc){
					resultObject.addReturnDesc("not found...");
				}else{
					resultObject.addResultData(doc.content());
					resultObject.putHead("count", 1);
				}
				logger.info("获取用户动作成功  ,params is [" + request.getParams() + "]");
			}
		} catch (Exception e) {
			logger.error("getByUserId:", e);
			logger.error("获取用户动作失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager._CODE_ERROR);
			resultObject.addReturnDesc("获取用户动作失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
/*
	private String putDoc(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userAction = request.getParam("userAction");
		if (StringUtils.isEmpty(userAction)) {
			resultObject.addReturnCode(CodeManager._CODE_ERROR);
			resultObject.addReturnDesc("configInfo is empty...");
		}
		try {
			String id = jsonParser.parse(userAction).getAsJsonObject().get("user_id").getAsString();
			if (null == id) {
				resultObject.addReturnCode(CodeManager._CODE_ERROR);
				resultObject.addReturnDesc("缺失参数Id");
			} else {
				JsonObject job = JsonObject.fromJson(userAction);
				JsonDocument doc = JsonDocument.create(id, job);
				CBaseConnectionPool.getBucket(bucketName, bucketPwd).insert(doc);
				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				if(null == doc){
					resultObject.addReturnDesc("not found...");
				}else{
					resultObject.addResultData(doc.content());
					resultObject.putHead("count", 1);
				}
				logger.info("获取用户动作  ,userAction is [" + userAction + "]");
			}
		} catch (Exception e) {
			logger.error("putDoc:", e);
			logger.error("获取用户动作失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager._CODE_ERROR);
			resultObject.addReturnDesc("获取用户动作失败 ,userAction is [" + userAction + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
	
	
	/*
	 *  
	private long u_c;//点赞数
    private long d_c;//点踩数
    private long c_c;//评论数
    private long f_c;//转发数
    private long m_c;//收藏数
    private long cl_c;//点击数
	 */
	static class MappingClass{
		static Map<String,String> map = new HashMap<String,String>();
		static{
			map.put("up", "u_c");
			map.put("down", "d_c");
			map.put("comment", "c_c");
			map.put("forward", "f_c");
			map.put("collection", "m_c");
			map.put("click", "cl_c");
		}
		static String get(String key){
			return map.get(key);
		}
	}

}

