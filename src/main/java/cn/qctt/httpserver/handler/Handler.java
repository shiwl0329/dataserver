/**
 * Wasu.com Inc.
 * Copyright (c) 2014-2015 All Rights Reserved.
 */

package cn.qctt.httpserver.handler;

import com.google.gson.Gson;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;

import java.io.IOException;

/**
 * @author chenzehe
 * @description 暴露给用户的接口，用户自定义类实现该接口即可被Server调用，
 * 返回用户响应结果，注意该类的对象为单例
 * @email chenzehe@wasu.com
 * @create 2015年3月16日 上午8:02:46
 */

public interface Handler {
    public Gson gson = new Gson();

    String invoke(Request request, Response response) throws IOException;
}
