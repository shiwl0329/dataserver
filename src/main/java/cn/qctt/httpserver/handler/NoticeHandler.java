package cn.qctt.httpserver.handler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.StringUtils;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX+"/notice")
public class NoticeHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(NoticeHandler.class);
	private static String sendUrl = BasicConstants.URL_PREFIX+"/notice/send";
	
	@Override
	public String invoke(Request request, Response response) throws IOException {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(sendUrl)) {
			returnStr = sendNotice(request);
		} else {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}
	
	private String sendNotice(Request request){
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String channel = request.getParam("channel");
		String msg = request.getParam("msg");
		Jedis js = null;
		try {
			js = RedisConnectionPool.getCluster();
			if (StringUtils.isBlank(channel) ) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 ,channel="+channel+" ,msg="+msg);
			} else {
				if(null == msg){
					msg = new String(request.getContent());
				}
				logger.info("send msg is ["+msg+"]");
				if( !StringUtils.isBlank(msg)){
					js.publish(channel, msg);
					resultObject.addReturnDesc("success");
					logger.info("send channel ["+channel+"] " + msg);
				}else{
					resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
					resultObject.addReturnDesc("缺失参数 ,channel="+channel+" ,body="+msg);
				}
			}
		} catch (Exception e) {
			logger.error("sendNotice:", e);
			logger.error("send notice error ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.NOTICE);
			resultObject.addReturnDesc("send notice error  ,params is [" + request.getParams() + "]");
		}finally{
			if(null != js){
				js.close();
			}
		}
		
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start)+" timing");
		return resultObject.toJson();
	}
}
