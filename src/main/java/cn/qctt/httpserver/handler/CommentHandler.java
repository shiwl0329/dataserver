package cn.qctt.httpserver.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX+"/comment")
public class CommentHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(CommentHandler.class);
	private static String putCommentUrl = BasicConstants.URL_PREFIX+"/comment/putComment";
	private static String putReplyUrl = BasicConstants.URL_PREFIX+"/comment/putReply";
	private static String getCommentAndReplysUrl = BasicConstants.URL_PREFIX+"/comment/getCommentAndReplys";
	private static String delCommentUrl = BasicConstants.URL_PREFIX+"/comment/delComment";
	private static String delReplyUrl = "/comment/delReply";
	private static String upCommentUrl = BasicConstants.URL_PREFIX+"/comment/upComment";
	private static String downCommentUrl = BasicConstants.URL_PREFIX+"/comment/downComment";
	private static String upReplyUrl = BasicConstants.URL_PREFIX+"/comment/upReply";
	private static String downReplyUrl = BasicConstants.URL_PREFIX+"/comment/downReply";
	private static String getAllUrl = BasicConstants.URL_PREFIX+"/comment/getAll";
	private static String getCommentsByIdUrl = BasicConstants.URL_PREFIX+"/comment/getCommentsById";
	private static String getCommentsByUserIdUrl = BasicConstants.URL_PREFIX+"/comment/getCommentsByUserId";
	private static String getCommentsCountByIdUrl = BasicConstants.URL_PREFIX+"/comment/getCommentsCountById";
//	private static String getByHql = BasicConstants.URL_PREFIX+"/comment/getByHql";
//	private static JsonParser jsonParser = new JsonParser();
	private static String bucketName = BasicConstants.COMMENTS_BUCKET;
	private static String bucketPwd = "";

	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(putCommentUrl)) {
			returnStr = putComment(request);
		} else if (urlPath.startsWith(putReplyUrl)) {
			returnStr = putReply(request);
		} else if (urlPath.startsWith(getCommentAndReplysUrl)) {
			returnStr = getCommentAndReplys(request);
		} else if (urlPath.startsWith(getAllUrl)) {
			returnStr = getAll(request);
		} else if (urlPath.startsWith(delCommentUrl)) {
			returnStr = delComment(request);
		} else if (urlPath.startsWith(delReplyUrl)) {
			returnStr = delReply(request);
		} else if (urlPath.startsWith(upReplyUrl)) {
			returnStr = upReply(request);
		} else if (urlPath.startsWith(upCommentUrl)) {
			returnStr = upComment(request);
		} else if (urlPath.startsWith(downCommentUrl)) {
			returnStr = downComment(request);
		} else if (urlPath.startsWith(downReplyUrl)) {
			returnStr = downReply(request);
		} else if (urlPath.startsWith(getCommentsByIdUrl)) {
			returnStr = getCommentsById(request);
		}  else if (urlPath.startsWith(getCommentsByUserIdUrl)) {
			returnStr = getCommentsByUserId(request);
		}  else if (urlPath.startsWith(getCommentsCountByIdUrl)) {
			returnStr = getCommentsCountById(request);
		} else {
			resultObject.addReturnCode(CodeManager.CODE_404);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}

	private String getCommentsByUserId(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "16");
		String type = request.getParam("type", "1");
		String text = request.getParam("text");
		String order = request.getParam("order", "post_time");
		try {
			if (StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 userId = "+userId);
				logger.warn("missing arguments: userId = "+userId);
			} else {
				String sql = "select comments.*   from comments left outer join default on keys comments.news_id  where  default.type= "+type+" and comments.userId='"+userId+"' " ;
				if(text != null && text.length() != 0){
					sql += " and comments.comment like '%" +text+"%' ";
				}
				if("up_count".equals(order)){
					sql += " and comments.up_count > 5 ";
				}
				/*if("up_count".equals(order)){
					size = (Integer.parseInt(size) - 2)+"";
				}*/
				int offset = (Integer.parseInt(page) - 1) * Integer.parseInt(size);
				if (offset < 0) {
					offset = 0;
				}
				sql += " order by comments."+order +" desc  offset "+offset+"  limit "+size;
				logger.info("sql is  " + sql  );
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,"comments");
					/*else{
						String upIds = js.hget(key, field);
						String[] upIdArray = upIds.split(",");
						String tId = null;
						List<Map<String,Object>> lsTmp = new ArrayList<>();
						for(String id :upIdArray){
							for(Map<String, Object> m: ls){
								tId = m.get("id").toString();
								if(id.equals(tId)){
									lsTmp.add(m);
								}
							}
						}
						ls.removeAll(lsTmp);
					}*/
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
					resultObject.addReturnDesc("获取新闻评论失败 ：" + queryResult.info());
				}
				
			}
		} catch (Throwable e) {
			logger.error("getCommentsById:", e);
			String info = "获取新闻评论失败 ,params is [" + request.getParams() + "]";
			logger.error(info);
			resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
			resultObject.addReturnDesc(info);
		} 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getCommentsCountById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		try {
			if (StringUtils.isEmpty(newsId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 newsId = "+newsId+" ;userId = "+newsId);
				logger.warn("missing arguments: newsId = "+newsId );
			} else {
				
				JsonDocument map = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).get(newsId);
				List<Map<String,Integer>> ls = new ArrayList<>();
				Map<String, Integer> m = new HashMap<>();
				if(null != map){
					m.put("count", map.content().getInt("c_c"));
					ls.add(m);
				}
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				/*String sql = "select count(1) as count from comments where comments.news_id = '" + newsId + "' and (comments.del is missing or comments.del != '1') ";
				logger.info("sql is  " + sql  );
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,BasicConstants.COMMENTS_BUCKET);
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
					resultObject.addReturnDesc("获取新闻评论数失败 ：" + queryResult.info());
				}*/
				
			}
		} catch (Throwable e) {
			logger.error("getCommentsById:", e);
			String info = "获取新闻评论数失败 ,params is [" + request.getParams() + "]";
			logger.error(info);
			resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
			resultObject.addReturnDesc(info);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String downReply(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		String replyId = request.getParam("replyId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		String up = "up_reply";
		JsonDocument doc = null;
		try {
			int count = 0;
			if (null == commentId || null == replyId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.getAndLock(commentId, 1))) {
				resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
//					JsonObject replyObj = doc.content().getObject(replyId);
					JsonArray replys = doc.content().getArray("replys");
					for(int i =0 ; i< replys.size() ;i++){
						JsonObject replyObj = replys.getObject(i);
						if(replyObj.containsKey(replyId)){
							Integer c =  replyObj.getInt(up);
							if(null == c){
								c=0;
							}
							count = c - 1;
							replyObj.put(up, count);
							logger.info("decrease reply ["+replyId+"]");
							break;
						}
					}
					doc.content().put("replys", replys);
					bk.replace(doc);
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(count);
					resultObject.putHead("count", 1);
					logger.info(commentId + " decrease reply ["+replyId+"]");
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("downReply:", e);
			logger.error("downReply ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
			resultObject.addReturnDesc("downReply  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String downComment(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		String up = "up_count";
		JsonDocument doc = null;
		try {
			if (null == commentId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.getAndLock(commentId, 1))) {
				resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
					Integer c = doc.content().getInt(up);
					int count = c - 1;
					doc.content().put(up, count);
					bk.replace(doc);
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(count);
					resultObject.putHead("count", 1);
					logger.info(commentId + " " + up + " decrease to " + count);
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("downComment:", e);
			resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
			resultObject.addReturnDesc("downComment error ,"+e.getMessage());
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getCommentsById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String userId = request.getParam("userId");
		String cursor = request.getParam("cursor", "0");
		String size = request.getParam("size", "16");
		String order = request.getParam("order", "post_time");
		Jedis js = null;
		try {
			if (StringUtils.isEmpty(newsId) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 newsId = "+newsId+" ;userId = "+newsId);
				logger.warn("missing arguments: newsId = "+newsId+" ;userId = "+userId);
			} else {
				js= RedisConnectionPool.getCluster();
				String key = BasicConstants.USER_STATE_PREFIX+":"+userId;
				
				String field = "upCommentIdsStr";
				String sql = "select comments.* ,user_news.up_comment from comments left outer join user_news on keys '"+userId+"_'|| comments.id where comments.news_id = '" + newsId + "' and (comments.del is missing or comments.del != '1') ";
				if("up_count".equals(order)){
					sql += " and comments.up_count > 5 ";
				}else{
					sql += " and comments.id not in [ "+js.hget(key, field)+"] ";
				}
				/*if("up_count".equals(order)){
					size = (Integer.parseInt(size) - 2)+"";
				}*/
				sql += " order by comments."+order +" desc  offset "+cursor+"  limit "+size;
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				logger.info("sql is  " + sql  );
				StringBuffer upCommentIds = new StringBuffer();
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,"comments");
					if("up_count".equals(order)){
						for(Map<String, Object> m: ls){
							upCommentIds.append("'"+m.get("id")+"',");
//							upCommentIds.append(m.get("id")+",");
						}
						upCommentIds.append("''");
						js.hset(key, field, upCommentIds.toString());
						js.expire(key, 3600 * 60 * 24 * 30); //过期时间一个月
					}/*else{
						String upIds = js.hget(key, field);
						String[] upIdArray = upIds.split(",");
						String tId = null;
						List<Map<String,Object>> lsTmp = new ArrayList<>();
						for(String id :upIdArray){
							for(Map<String, Object> m: ls){
								tId = m.get("id").toString();
								if(id.equals(tId)){
									lsTmp.add(m);
								}
							}
						}
						ls.removeAll(lsTmp);
					}*/
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
					resultObject.addReturnDesc("获取新闻评论失败 ：" + queryResult.info());
				}
				
			}
		} catch (Throwable e) {
			logger.error("getCommentsById:", e);
			String info = "获取新闻评论失败 ,params is [" + request.getParams() + "]";
			logger.error(info);
			resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
			resultObject.addReturnDesc(info);
		}finally{
			if(js!=null){
				js.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/*
	private String getAllComments(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String userId = request.getParam("userId");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "16");
//		String order = request.getParam("order", "post_time");
		try {
			if (StringUtils.isEmpty(newsId) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 newsId = "+newsId+" ;userId = "+newsId);
				logger.warn("missing arguments: newsId = "+newsId+" ;userId = "+userId);
			} else {
				int limit = Integer.parseInt(size);
				int offset = (Integer.parseInt(page) - 1) * limit;
				if (offset < 0) {
					offset = 0;
				}
				String sqlUp = "select comments.* ,user_news.up_comment from "+BasicConstants.COMMENTS_BUCKET+" left outer join "+BasicConstants.USER_NEWS_BUCKET+" on keys '"+userId+"_'|| comments.id where comments.news_id = '" + newsId + "' and (comments.del is missing or comments.del != '1')  and comments.up_count > 5 order by comments.up_count desc   limit 20";
				String sqlTime = "select comments.* ,user_news.up_comment from "+BasicConstants.COMMENTS_BUCKET+" left outer join  "+BasicConstants.USER_NEWS_BUCKET+" on keys '"+userId+"_'|| comments.id where comments.news_id = '" + newsId + "' and (comments.del is missing or comments.del != '1')   and comments.id not in( ";
				StringBuffer sb = new StringBuffer();
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sqlUp));
				logger.info("sqlUp is  " + sqlUp  );
				Map<String, Object> map = new HashMap<>();
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,BasicConstants.COMMENTS_BUCKET);
					for(Map<String,Object> m:ls){
						sb.append("'"+m.get("id")+"',");
					}
					sb.append("'')");
					sqlTime+=sb.toString()+" order by comments.post_time desc  offset "+offset+"  limit "+size;
					map.put("hotComments", ls);
					queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sqlTime));
					map.put("newComments", CouchbaseUtil.resultToList(queryResult ,BasicConstants.COMMENTS_BUCKET));
					resultObject.addResultData(map);
					resultObject.putHead("count", map.size());
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
					resultObject.addReturnDesc("获取新闻评论失败 ：" + queryResult.info());
				}
				
			}
		} catch (Throwable e) {
			logger.error("getCommentsById:", e);
			String info = "获取新闻评论失败 ,params is [" + request.getParams() + "]";
			logger.error(info);
			resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTSBYID);
			resultObject.addReturnDesc(info);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
*/

	private String upComment(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		String up = "up_count";
		JsonDocument doc = null;
		try {
			if (null == commentId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.getAndLock(commentId, 1))) {
				resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
					Integer c = doc.content().getInt(up);
					int count = c + 1;
					doc.content().put(up, count);
					bk.replace(doc);
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(count);
					resultObject.putHead("count", 1);
					logger.info(commentId + " " + up + " incre to " + count);
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("upComment:", e);
			resultObject.addReturnCode(CodeManager.COMMENT_UPCOMMENT);
			resultObject.addReturnDesc("upComment error ,"+e.getMessage());
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String upReply(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		String replyId = request.getParam("replyId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		String up = "up_reply";
		JsonDocument doc = null;
		try {
			int count = 0;
			if (null == commentId || null == replyId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.getAndLock(commentId, 1))) {
				resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
//					JsonObject replyObj = doc.content().getObject(replyId);
					JsonArray replys = doc.content().getArray("replys");
					for(int i =0 ; i< replys.size() ;i++){
						JsonObject replyObj = replys.getObject(i);
						if(replyObj.containsKey(replyId)){
							Integer c =  replyObj.getInt(up);
							if(null == c){
								c=0;
							}
							count = c + 1;
							replyObj.put(up, count);
							logger.info("up reply ["+replyId+"]");
							break;
						}
					}
					doc.content().put("replys", replys);
					bk.replace(doc);
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(count);
					resultObject.putHead("count", 1);
					logger.info(commentId + " up reply ["+replyId+"]");
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("incre:", e);
			logger.error("incre ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_UPREPLY);
			resultObject.addReturnDesc("incre  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String delReply(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		String replyId = request.getParam("replyId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		JsonDocument doc = null;
		try {
			if (null == commentId || null == replyId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.getAndLock(commentId,1))) {
				resultObject.addReturnCode(CodeManager.COMMENT_DELREPLY);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
//					JsonObject replyObj = doc.content().getObject(replyId);
					JsonArray replys = doc.content().getArray("replys");
					for(int i =0 ; i< replys.size() ;i++){
						JsonObject job = replys.getObject(i);
						if(job.containsKey(replyId)){
							job.put("del", "1");
							logger.info("删除评论 ["+replyId+"]");
							break;
						}
					}
//					replyObj.put("del", "1");
					doc.content().put("replys", replys);
					bk.replace(doc);
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(replyId);
					resultObject.putHead("count", 1);
					logger.info(commentId + " deleted" );
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_DELREPLY);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("incre:", e);
			logger.error("incre ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_DELREPLY);
			resultObject.addReturnDesc("incre  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	//删除评论 评论数 -1
	private String delComment(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		Bucket bk = CBaseConnectionPool.getBucket(bucketName);
		JsonDocument doc = null;
		try {
			if (null == commentId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else if (commentIsDel(doc = bk.get(commentId))) {
				resultObject.addReturnCode(CodeManager.COMMENT_DELCOMMENT);
				resultObject.addReturnDesc("此评论已删除 操作失败");
				logger.info("此评论已删除 操作失败  ,params is [" + request.getParams() + "]");
			} else {
				if (doc != null) {
					doc.content().put("del", "1");
					bk.replace(doc);
					String newsId = doc.content().getString("news_id");
					JsonDocument news = CBaseConnectionPool.get(BasicConstants.SOURCE_OTHER_BUCKET, newsId);
					if(null != news){
						int c_c = news.content().getInt("c_c");
						c_c -= 1;
						if(c_c < 0){
							c_c = 0;
						}
						 news.content().put("c_c", c_c);
						CBaseConnectionPool.upsert(BasicConstants.SOURCE_OTHER_BUCKET, news);
					}
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addResultData(commentId);
					resultObject.putHead("count", 1);
					logger.info(commentId + " deleted" );
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_DELCOMMENT);
					resultObject.addReturnDesc("无法检索到对应的文档  ，commentId is " + commentId);
				}
			}
		} catch (Exception e) {
			logger.error("incre:", e);
			logger.error("incre ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_DELCOMMENT);
			resultObject.addReturnDesc("incre  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getAll(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String order = request.getParam("order" ,"post_time");
		try {
			if (newsId == null) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 ");
			} else {
				String sql = "select * from comments where news_id = '" + newsId + "' and (del is missing or del != '1') order by "+order + " desc";
				QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,"comments");
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
					logger.info("获取新闻评论 ,sql is [" + sql + "]");
				} else {
					resultObject.addReturnCode(CodeManager.COMMENT_GETALL);
					resultObject.addReturnDesc("获取新闻评论失败 ：" + queryResult.info());
				}
				logger.info("getAll ["+sql+"]");
			}
		} catch (Exception e) {
			logger.error("getByHql:", e);
			String info = "获取新闻评论失败 ,params is [" + request.getParams() + "]";
			logger.error(info);
			resultObject.addReturnCode(CodeManager.COMMENT_GETALL);
			resultObject.addReturnDesc(info);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getCommentAndReplys(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String commentId = request.getParam("commentId");
		if (null == commentId) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数 ");
		}
		// String key = newsId + "_" + commentId;
		try {
			JsonDocument doc = CBaseConnectionPool.getBucket(bucketName, bucketPwd).get(commentId);
			if (commentIsDel(doc)) {
				resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTANDREPLYS);
				resultObject.addReturnDesc("此评论已删除 查看失败");
				logger.info("此评论已删除 查看失败  ,params is [" + request.getParams() + "]");
			} else {
//				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				if(doc == null){
					resultObject.addReturnDesc("not found this comment ,doc is ["+doc+"]");;
					logger.warn("not found this comment ,doc is ["+doc+"]");
				}else{
					resultObject.addResultData(doc.content().toMap());
					resultObject.putHead("count", 1);
					logger.info("获取评论及回复  ,params is [" + request.getParams() + "]");
				}
			}
		} catch (Exception e) {
			logger.error("getCommentAndReplys:", e);
			logger.error("获取评论及回复失败  ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_GETCOMMENTANDREPLYS);
			resultObject.addReturnDesc("获取评论及回复 失败 ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}


	private String putReply(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		byte[] replyByte = request.getContent();
		String commentId = request.getParam("commentId");
		// String newsId = request.getParam("newsId");
		if (null == replyByte) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("reply is empty...");
		} else if (null == commentId) {
			resultObject.addReturnCode(CodeManager.COMMENT_PUTREPLY);
			resultObject.addReturnDesc("缺失参数 ");
		}
		String reply = new String(replyByte);
		// String key = newsId + "_" + commentId;
		try {
			String reply_id = Utils.getRandomId_6();
			// String reply_id =
			// jsonParser.parse(reply).getAsJsonObject().get("reply_id").getAsString();
			/*
			 * if (null == reply_id) {
			 * resultObject.addReturnCode(CodeManager._CODE_ERROR);
			 * resultObject.addReturnDesc("缺失参数 reply_id"); } else {
			 */
			JsonDocument doc = CBaseConnectionPool.getBucket(bucketName, bucketPwd).get(commentId);
			// String del = doc.content().getString("del");
			if (commentIsDel(doc)) {
				resultObject.addReturnCode(CodeManager.COMMENT_PUTREPLY);
				resultObject.addReturnDesc("此评论已删除 回复失败");
				logger.info("此评论已删除 回复失败  ,params is [" + request.getParams() + "]");
			} else {
				JsonObject replyObj = JsonObject.fromJson(reply);
				replyObj.put("reply_id", reply_id);
				JsonArray array = doc.content().getArray("replys");
				if(null == array){
					array = JsonArray.create();
				}
				array.add(replyObj);
				doc.content().put("replys", array);
				CBaseConnectionPool.getBucket(bucketName, bucketPwd).replace(doc);
//				resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				resultObject.addResultData(reply_id);
				resultObject.putHead("count", 1);
				logger.info("添加回复成功  ,params is [" + request.getParams() + "]");
			}
			// }
		} catch (Exception e) {
			logger.error("putReply:", e);
			logger.error("添加回复失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_PUTREPLY);
			resultObject.addReturnDesc("添加回复失败 ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	//添加评论 comment +1
	private String putComment(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		byte[] comment_byte = request.getContent();
		String newsId = request.getParam("newsId");
		if (StringUtils.isEmpty(comment_byte)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("comment is empty...");
		}
		if ( StringUtils.isEmpty(newsId)) {
			resultObject.addReturnCode(CodeManager.COMMENT_PUTCOMMENT);
			resultObject.addReturnDesc("缺失参数 newsId");
		}
		String comment = new String(comment_byte);
		try {
			JsonDocument news = CBaseConnectionPool.get(BasicConstants.SOURCE_OTHER_BUCKET, newsId);
			if(news != null && !"1".equals(news.content().get("disable"))){
				String commentId = Utils.getRandomId_6();
				String id = newsId + "_" + commentId;
				JsonObject commentObj = JsonObject.fromJson(comment);
				commentObj.put("id", id);
				commentObj.put("news_id", newsId);
				JsonDocument doc = JsonDocument.create(id, commentObj);
				CBaseConnectionPool.getBucket(bucketName, bucketPwd).insert(doc);
				//对指定新闻评论数+1
				JsonDocument newsOther = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).getAndLock(newsId,1);
				if(newsOther == null){
					logger.warn("not found news["+newsId+"]");
				}else{
					String key = UserClickActionHandler.MappingClass.get("comment");
					int count = newsOther.content().getInt(key)+1;
					newsOther.content().put(key, count);
					CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).replace(newsOther);
				}
	//			resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				resultObject.addResultData(id);
				resultObject.putHead("count", 1);
				logger.info("添加评论成功["+id+"]  ,params is [" + request.getParams() + "]");
			}else{
				resultObject.addReturnCode(CodeManager.COMMENT_NEWS_DISABLED);
				resultObject.addReturnDesc("此新闻被禁用，无法评论");
			}
		} catch (Exception e) {
			logger.error("putComment:", e);
			logger.error("添加评论失败,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.COMMENT_PUTCOMMENT);
			resultObject.addReturnDesc("添加评论失败 ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private boolean commentIsDel(JsonDocument doc) {
		if(doc == null) {
			logger.warn("not found this comment ,doc is "+doc);
			return false;
		}
		String del = doc.content().getString("del");
		if (del != null && del.equals("1")) {
			return true;
		}
		return false;
	}

}
