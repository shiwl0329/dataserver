package cn.qctt.httpserver.handler;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.google.gson.JsonElement;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Tuple;

@RequestMapping(BasicConstants.URL_PREFIX+"/shortProfile")
public class UserProfileHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(UserProfileHandler.class);

	private static String addURL = BasicConstants.URL_PREFIX+"/shortProfile/add";

	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		
		if (urlPath.startsWith(addURL)) {
			returnStr = add(request);
		} else {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}
 
	private String add(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		
		byte[] body = request.getContent();
		int profileShortTimeLen = 7;
		double penaltyFactor = 1;
		Jedis js = null;
		String content = "";
		try {
			if (StringUtils.isEmpty(userId) || body == null) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失字段userId="+userId);
				logger.info("缺失字段userId="+userId);
			} else {
				js = RedisConnectionPool.getCluster();
				Pipeline pp = js.pipelined();
				String key = BasicConstants.USER_SHORT_PROFILE + ":" + userId;
				content = new String(body);
				//{"tag":"","weight":1,"minScore":0.5,"pinTime":11111}
				JsonElement json = Utils.getJsonParser().parse(content);
				if(null != json){
					Set<Tuple> profiles = js.zrevrangeWithScores(key, 0, -1);
					com.google.gson.JsonObject obj = json.getAsJsonObject();
					String tag = obj.get("tag").getAsString();
					int weight = obj.get("weight").getAsInt();
					double minScore = obj.get("minScore").getAsDouble();
					if(minScore>1){
						minScore = 0.5;
					}
					com.google.gson.JsonObject profile = null;
					String itemTag = null;
					double weightTotal = weight;
					int itemWeight = 0;
					long itemPinTime = 0;
					double itemMinScore = 0;
					
					Map<String, Double> userProfilesMap = new HashMap<>();
					long berforDayTimeStamp = Utils.getTimeMillisBefore(profileShortTimeLen);
					Set<Tuple> filteredProfile = new LinkedHashSet<>(); 
					for(Tuple item:profiles){
						profile = Utils.getJsonParser().parse(item.getElement()).getAsJsonObject();
//						itemTag = profile.get("tag").getAsString();
						itemWeight = profile.get("weight").getAsInt();
						itemPinTime = profile.get("pinTime").getAsLong();
//						itemScore = item.getScore();
						if(itemPinTime<berforDayTimeStamp){  //滤掉太久的标签 7日
							continue;
						}
						weightTotal += itemWeight;
						filteredProfile.add(item);
					}
					for(Tuple item:filteredProfile){
						profile = Utils.getJsonParser().parse(item.getElement()).getAsJsonObject();
						itemTag = profile.get("tag").getAsString();
						itemWeight = profile.get("weight").getAsInt();
						itemMinScore = profile.get("minScore").getAsDouble();
						
						profile.addProperty("pinTime", System.currentTimeMillis());
						
						if(tag.equals(itemTag)){
							obj.addProperty("weight", weight+itemWeight);
							weight+= itemWeight*penaltyFactor;
						}else{
							double score = itemWeight/weightTotal;
							if(score < itemMinScore){
								score = itemMinScore;
							}
							userProfilesMap.put(item.getElement(), score);
						}
					}
					double score = weight/weightTotal;
					if(score<minScore){
						score = minScore;
					}
					logger.info("score is "+score+" for "+obj.toString());
//					double s = Utils.formatDouble(score, 2);
					userProfilesMap.put(obj.toString(),score );
					pp.del(key);
					pp.zadd(key, userProfilesMap);
					pp.expire(key, profileShortTimeLen*3600*24);
					pp.sync();
				}
			}
		} catch (Exception e) {
			logger.error("add:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("add is error  content = "+content);
			logger.info(content);
		}finally{
			if(null != js){
				js.close();
			}
		} 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	static class MappingClass{
		static Map<String,String> map = new HashMap<String,String>();
		static{
			map.put("up", "u_c");
			map.put("down", "d_c");
			map.put("comment", "c_c");
			map.put("forward", "f_c");
			map.put("collection", "m_c");
			map.put("click", "cl_c");
		}
		static String get(String key){
			return map.get(key);
		}
	}

}

