package cn.qctt.httpserver.handler;
import java.util.concurrent.TimeUnit;

import org.jboss.netty.handler.codec.rtsp.RtspHeaders.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.manager.CodeManager;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpHeaders.Names;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.CharsetUtil;
public class HttpRequestTimeOutHandler extends ReadTimeoutHandler {
	private static final Logger logger = LoggerFactory.getLogger(HttpRequestTimeOutHandler.class);
	private boolean				isClosed;
	private final static int	TIMEOUTSECONDS	= 8;

	public HttpRequestTimeOutHandler() {
		this(TIMEOUTSECONDS);
	}

	public HttpRequestTimeOutHandler(int timeoutSeconds) {
		super(timeoutSeconds, TimeUnit.SECONDS);
	}

	protected void readTimedOut(ChannelHandlerContext ctx) throws Exception {
		if (!isClosed) {
			ResultMsg result = new ResultMsg();
			result.addReturnDesc("Connection timed out, please try again later.");
			result.addReturnCode(CodeManager.CODE_500);
			ByteBuf bf = Unpooled.copiedBuffer(result.toJson(), CharsetUtil.UTF_8);
			FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, bf);
			response.headers().set(Names.CONTENT_TYPE, "text/plain; charset=UTF-8");
			response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, bf.readableBytes());
			response.headers().set(Names.CONNECTION, Values.CLOSE);
			isClosed = true;
			logger.warn("====================request time out==============================");
			ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
		}
	}

}
