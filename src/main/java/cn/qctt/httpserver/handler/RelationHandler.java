package cn.qctt.httpserver.handler;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.util.StringUtils;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.RCodeManager;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX + "/relation")
public class RelationHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(RelationHandler.class);
	private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
	private static String addTakeUrl = BasicConstants.URL_PREFIX + "/relation/addTake";
//	private static String upsertStarRelationUrl = BasicConstants.URL_PREFIX + "/relation/upsertStarRelation";
//	private static String delStarRelationUrl = BasicConstants.URL_PREFIX + "/relation/delStarRelation";
	private static String delTakeUrl = BasicConstants.URL_PREFIX + "/relation/delTake";
	private static String isTakeUrl = BasicConstants.URL_PREFIX + "/relation/isTake";
	private static String getTakeByIdUrl = BasicConstants.URL_PREFIX + "/relation/getTakeById";
	private static String getFollowByUserIdUrl = BasicConstants.URL_PREFIX + "/relation/getFollowByUserId";
//	private static String getRelationByStarIdUrl = BasicConstants.URL_PREFIX + "/relation/getRelationByStarId";
	private static String getFansCountByStarIdsUrl = BasicConstants.URL_PREFIX + "/relation/1getFansCountByStarIds";
	private static String getFlowingCountByIdUrl = BasicConstants.URL_PREFIX + "/relation/getFlowingCountById";
	private static String getRelationByUserIdAndEntityNamesUrl = BasicConstants.URL_PREFIX
			+ "/relation/getRelationByUserIdAndEntityNames";
	private static String getRelationByUserIdAndEntityNamesOrIdsUrl = BasicConstants.URL_PREFIX
			+ "/relation/getRelationByUserIdAndEntityNamesOrIds";
	private static String getStartIdByEndIdsUrl = BasicConstants.URL_PREFIX + "/relation/getStartIdByEndIds";
	private static String updateRelationUrl = BasicConstants.URL_PREFIX + "/relation/updateRelation";
	private static String pinStarsUrl = BasicConstants.URL_PREFIX + "/relation/pinStars";
	private static String cancelPinUrl = BasicConstants.URL_PREFIX + "/relation/cancelPin";
	private static String saveCurrentEpisodeUrl = BasicConstants.URL_PREFIX + "/relation/saveCurrentEpisode";
	private static String getLastEpisodeTitleUrl = BasicConstants.URL_PREFIX + "/relation/getLastEpisodeTitle";
//	private static String getLastEpisodeUrl = BasicConstants.URL_PREFIX + "/relation/getLastEpisode";
	private static String checkLookInIdsUrl = BasicConstants.URL_PREFIX + "/relation/checkLookInIds";
	private static String isLookURL = BasicConstants.URL_PREFIX + "/relation/isLook";
	
	/**
	 *新版自定义关键词统一接口 
	 */
	private static String subEntityURL = BasicConstants.URL_PREFIX + "/relation/subEntity";
	private static String unSubscribeURL = BasicConstants.URL_PREFIX + "/relation/unSubscribe";
	private static String unIterestedURL = BasicConstants.URL_PREFIX + "/relation/unInterested";
	private static String getRelationForEntityIdsURL = BasicConstants.URL_PREFIX + "/relation/getRelationForEntityIds";
	private static String findSubEntitysURL = BasicConstants.URL_PREFIX + "/relation/findSubEntitys";
	private static String getSubEntitysByUserIdAndFirstClassIdsURL = BasicConstants.URL_PREFIX + "/relation/getSubEntitysByUserIdAndFirstClassIds";
	private static String pinEntityURL = BasicConstants.URL_PREFIX + "/relation/pinEntity";
	private static String getUserSubFirstClassesURL = BasicConstants.URL_PREFIX + "/relation/getUserSubFirstClasses";
	private static String getFansCountByEntityIdsURL = BasicConstants.URL_PREFIX + "/relation/getFansCountByEntityIds";
//	private static String cancelPinEntityURL = BasicConstants.URL_PREFIX + "/relation/cancelPinEntity";

	@Override
	public String invoke(Request request, Response response) throws IOException {
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(addTakeUrl)) {
			returnStr = addTake(request);
		} else if (urlPath.startsWith(delTakeUrl)) {
			returnStr = delTake(request);
		} else if (urlPath.startsWith(isLookURL)) {
			returnStr = isLook(request);
		} else if (urlPath.startsWith(isTakeUrl)) {
			returnStr = isTake(request);
		} else if (urlPath.startsWith(saveCurrentEpisodeUrl)) {
			returnStr = saveCurrentEpisode(request);
		} else if (urlPath.startsWith(getLastEpisodeTitleUrl)) {
			returnStr = getLastEpisodeTitle(request);
		} else if (urlPath.startsWith(getTakeByIdUrl)) {
			returnStr = getTakeById(request);
		} else if (urlPath.startsWith(getFollowByUserIdUrl)) {
			returnStr = getFollowByUserId(request);
		} /*else if (urlPath.startsWith(getRelationByStarIdUrl)) {
			returnStr = getRelationByStarId(request);
		} */else if (urlPath.startsWith(getRelationByUserIdAndEntityNamesOrIdsUrl)) {
			returnStr = getRelationByUserIdAndEntityNamesOrIds(request);
		} else if (urlPath.startsWith(getFansCountByStarIdsUrl)) {
			returnStr = getFansCountByStarIds(request);
		} else if (urlPath.startsWith(getFlowingCountByIdUrl)) {
			returnStr = getFlowingCountById(request);
		} else if (urlPath.startsWith(getRelationByUserIdAndEntityNamesUrl)) {
			returnStr = getRelationByUserIdAndEntityName(request);
		} else if (urlPath.startsWith(checkLookInIdsUrl)) {
			returnStr = checkLookInIds(request);
		} else if (urlPath.startsWith(getFansCountByEntityIdsURL)) {
			returnStr = getFansCountByEntityIds(request);
		}
		/*else if (urlPath.startsWith(upsertStarRelationUrl)) {
			returnStr = upsertStarRelation(request);
		} else if (urlPath.startsWith(delStarRelationUrl)) {
			returnStr = delStarRelation(request);
		} */
		else if (urlPath.startsWith(getStartIdByEndIdsUrl)) {
			returnStr = getStartIdByEndIds(request);
		} else if (urlPath.startsWith(updateRelationUrl)) {
			returnStr = updateRelation(request);
		} else if (urlPath.startsWith(pinStarsUrl)) {
			returnStr = pinStars(request);
		} else if (urlPath.startsWith(subEntityURL)) {
			returnStr = subEntity(request);
		} else if (urlPath.startsWith(unSubscribeURL)) {
			returnStr = unSubscribe(request);
		} else if (urlPath.startsWith(unIterestedURL)) {
			returnStr = unInterested(request);
		} else if (urlPath.startsWith(findSubEntitysURL)) {
			returnStr = findSubEntitys(request);
		} else if (urlPath.startsWith(getRelationForEntityIdsURL)) {
			returnStr = getRelationForEntityIds(request);
		} else if (urlPath.startsWith(getSubEntitysByUserIdAndFirstClassIdsURL)) {
			returnStr = getSubEntitysByUserIdAndFirstClassIds(request);
		} else if (urlPath.startsWith(pinEntityURL)) {
			returnStr = pinEntity(request);
		} else if (urlPath.startsWith(cancelPinUrl)) {
			returnStr = cancelPin(request);
		} else if (urlPath.startsWith(getUserSubFirstClassesURL)) {
			returnStr = getUserSubFirstClasses(request);
		}
		return returnStr;
	}

	private String getFlowingCountById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String firstClassId = request.getParam("firstClassId" );
		logger.info("getFlowingCountById -> userId=" + userId);
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(firstClassId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> userId=" + userId +" ,firstClassId = "+firstClassId);
			logger.warn("missing parameter -> userId=" + userId +" ,firstClassId = "+firstClassId);
		} else {
			String sql = "select count(1) as count from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id = '"+userId+"' and  relation='11' and first_class_id='"+firstClassId+"' ";
			 
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
					.query(Query.simple(sql));
			Map<String, Integer> dataTmp = new HashMap<>();
			if (queryResult.finalSuccess()) {
				for (QueryRow row : queryResult.allRows()) {
					JsonObject val = row.value();
					if (null != val) {
						dataTmp.put("count", val.getInt("count"));
					}
				}
				resultObject.putHead("count", dataTmp.size());
				resultObject.addResultData(dataTmp);
			} else {
				resultObject.addReturnDesc(queryResult.errors());
				logger.warn("query fail " + queryResult.info());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String isLook(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String ids = request.getParam("ids");
		logger.info("isLook -> userId=" + userId + " ;ids=" + ids );
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(ids)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("userId=" + userId + " ;ids=" + ids);
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_NEWS_BUCKET;
				String sql = "select * from "+BasicConstants.USER_NEWS_BUCKET+" use keys [";
				
				String[] idArray = ids.split(",");
				for (String id:idArray) {
					sql+="'"+userId+"_"+id+"',";
				}
				sql += "''] where look_relation = '1'";
				logger.info("sql is "+sql);
				QueryResult queryResult = CBaseConnectionPool.getBucket(bk)
						.query(Query.simple(sql));
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult ,bk);
				resultObject.putHead("count", ls.size()); 
				resultObject.addResultData(ls); 
			} catch (Exception e) {
				logger.error("isLook:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ISTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String checkLookInIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String episodeIds =  request.getParam("episodeIds");
		try {
			if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(episodeIds)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失userId");
				logger.info("缺失 userId");
			} else {
				String sql = "select * from "+BasicConstants.USER_NEWS_BUCKET+" use keys [";
				
				String[] ids = episodeIds.split(",");
				for (String id:ids) {
					sql+="'"+id+"',";
				}
				sql+="'']";
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET).query(Query.simple(sql));
				logger.info("sql is "+sql);
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult);
				resultObject.putHead("count", ls.size());
				resultObject.addReturnDesc(ls);
			}
		} catch (Exception e) {
			logger.error("starHomeDynamic:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("starHomeDynamic is error");
		} 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getLastEpisodeTitle(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String programId = request.getParam("programId");
		if (StringUtils.isEmpty(userId) ||StringUtils.isEmpty(programId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_ENTITY_BUCKET;
					String key = userId + "_" + programId;
					JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
					if (null != data) {
						String episode = data.content().getString("last_episode");
						/*String mysql = "select episode from  v_video v where v.id =  "+id;
						String episode = connectionUtil.query(mysql, new ResultSetExtractor<String>() {
							@Override
							public String extractData(ResultSet resultSet)
									throws SQLException, DataAccessException {
								String episode = "";
								while (resultSet.next()) {
									episode = resultSet.getString("episode");
								}
								return episode;
							}
						});*/
						resultObject.addResultData(episode);
						resultObject.putHead("count", 1);
					}else{
						logger.info("not fount episode for "+key);
					}
			} catch (Exception e) {
				logger.error("getLastEpisodeTitle:", e);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	

	private String saveCurrentEpisode(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String programId = request.getParam("programId");
		String episodeTitle = request.getParam("episodeTitle");
//		String episodeId = request.getParam("episodeId");
//		String playTime = request.getParam("playTime");
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(programId) ||StringUtils.isEmpty(episodeTitle)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_ENTITY_BUCKET;
					String key = userId + "_" + programId;
					JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
					Map<String, Object> tmpMap = null;
					if (null != data) {
						tmpMap = data.content().toMap();
						
					}else{
						tmpMap = new HashMap<>();
						tmpMap.put("start_id", userId);
						tmpMap.put("end_id", programId);
						tmpMap.put("relation", "41");
					}
					tmpMap.put("update_time", System.currentTimeMillis());
					
					tmpMap.put("first_class_id", "2");
					tmpMap.put("last_episode", episodeTitle);
//					tmpMap.put("last_episode_id", episodeId);
//					tmpMap.put("play_time", playTime);
					JsonObject job = JsonObject.from(tmpMap);
					JsonDocument doc = JsonDocument.create(key, job);
					if (null != data) {
						CBaseConnectionPool.getBucket(bk).upsert(doc);
					}else{
						CBaseConnectionPool.getBucket(bk).upsert(doc, 7, TimeUnit.DAYS);
					}
			} catch (Exception e) {
				logger.error("pinStars:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
/*
	private String cancelPin(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("userId");
		String starId = request.getParam("starId");
		if (StringUtils.isEmpty(startId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_ENTITY_BUCKET;
				String key = startId + "_" + starId;
				JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
				if (null != data) {
					Map<String, Object> tmpMap = data.content().toMap();
					tmpMap.remove("order");
					tmpMap.remove("pin_time");
					JsonObject job = JsonObject.from(tmpMap);
					JsonDocument doc = JsonDocument.create(key, job);
					CBaseConnectionPool.getBucket(bk).upsert(doc);
				}
			} catch (Exception e) {
				logger.error("cancelPin:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/

	/**
	 * 取消对自定义关键词的置顶
	 * @param request
	 * @return
	 */
	private String cancelPin(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityId = request.getParam("entityId");
		if (StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_ENTITY_BUCKET;
				String key = userId + "_" + entityId;
				JsonDocument data = CBaseConnectionPool.get(bk ,key);
				if (null != data) {
					Map<String, Object> tmpMap = data.content().toMap();
					tmpMap.remove("order");
					tmpMap.remove("pin_time");
					JsonObject job = JsonObject.from(tmpMap);
					JsonDocument doc = JsonDocument.create(key, job);
					CBaseConnectionPool.upsert(bk, doc);
				}
			} catch (Exception e) {
				logger.error("cancelPin:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	
	/**
	 * 获取用户已订阅实体的一级类别分布
	 * @param request
	 * @return
	 */
	private String getUserSubFirstClasses(Request request){
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
//		byte[] orderJson = request.getContent();
		if (StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			try {
					String sql = "select distinct first_class_id  from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id='"+userId+"' and relation = '11' ";
					logger.info(sql);
					QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET, sql);
					List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.USER_ENTITY_BUCKET);
					resultObject.addReturnDesc(ls.size());
					resultObject.addResultData(ls);
			} catch (Exception e) {
				logger.error("getUserSubFirstClasses:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 对自定义关键词进行置顶功能
	 * @param request
	 * @return
	 */
	private String pinEntity(Request request){
			long start = System.currentTimeMillis();
			ResultMsg resultObject = new ResultMsg();
			String userId = request.getParam("userId");
			String entityId = request.getParam("entityId");
			String firstClassId = request.getParam("firstClassId");
//			byte[] orderJson = request.getContent();
			if (StringUtils.isEmpty(userId) || entityId == null || StringUtils.isEmpty(firstClassId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺少字段");
				logger.warn("execute fail and missing arguments");
			} else {
				Map<String, Object> tmp = new HashMap<String, Object>();
				try {
						String key = userId + "_" + entityId;
						tmp = new HashMap<String, Object>();
						tmp.put("order",  "1");
						tmp.put("pin_time",  System.currentTimeMillis());
						String sql = "select count(1) as count from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id='"+userId+"' and `order` = '1' and first_class_id ='"+firstClassId+"'";
						logger.info(sql);
						QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET, sql);
						int count = 0 ;
						List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.USER_ENTITY_BUCKET);
						for(Map<String, Object> m:ls){
							count = Integer.parseInt(m.get("count").toString());
						}
						JsonDocument data = CBaseConnectionPool.get(BasicConstants.USER_ENTITY_BUCKET ,key);
						if (null != data && count< 10) {
							Map<String, Object> tmpMap = data.content().toMap();
							tmpMap.putAll(tmp);
							tmp = tmpMap;
							JsonObject job = JsonObject.from(tmp);
							JsonDocument doc = JsonDocument.create(key, job);
							CBaseConnectionPool.upsert(BasicConstants.USER_ENTITY_BUCKET ,doc);
						}else if(data != null){
							resultObject.addReturnCode(CodeManager.RELATIONHANDLER_PINSTAR);
							resultObject.addReturnDesc(count);
						}else{
							resultObject.addReturnCode(CodeManager.RELATIONHANDLER_PINSTAR_42008);
						}
				} catch (Exception e) {
					logger.error("pinEntity:", e);
					// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
					resultObject.addReturnDesc(e.getMessage());
					resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
				}
			}
			resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
			logger.info((System.currentTimeMillis() - start) + " timing");
			return resultObject.toJson();
	}
	
	@Deprecated
	private String pinStars(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("userId");
		String endId = request.getParam("starId");
//		byte[] orderJson = request.getContent();
		if (StringUtils.isEmpty(startId) || endId == null) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺少字段");
			logger.warn("execute fail and missing arguments");
		} else {
			Map<String, Object> tmp = new HashMap<String, Object>();
			try {
					String key = startId + "_" + endId;
					tmp = new HashMap<String, Object>();
					tmp.put("order",  "1");
					tmp.put("pin_time",  System.currentTimeMillis());
					String sql = "select count(1) as count from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id='"+startId+"' and `order` = '1'";
					logger.info(sql);
					QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
							.query(Query.simple(sql));
					int count = 0 ;
					List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.USER_ENTITY_BUCKET);
					for(Map<String, Object> m:ls){
						count = Integer.parseInt(m.get("count").toString());
					}
					JsonDocument data = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET).get(key);
					if (null != data && count< 10) {
						Map<String, Object> tmpMap = data.content().toMap();
						tmpMap.putAll(tmp);
						tmp = tmpMap;
						JsonObject job = JsonObject.from(tmp);
						JsonDocument doc = JsonDocument.create(key, job);
						CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET).upsert(doc);
					}else if(data != null){
						resultObject.addReturnCode(CodeManager.RELATIONHANDLER_PINSTAR);
						resultObject.addReturnDesc(count);
					}else{
						resultObject.addReturnCode(CodeManager.RELATIONHANDLER_PINSTAR_42008);
					}
			} catch (Exception e) {
				logger.error("pinStars:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String updateRelation(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String fromUser = request.getParam("fromUser");
		String toUser = request.getParam("toUser");
		String type = request.getParam("type","all");
		logger.info("updateRelation -> fromUser=" + fromUser + " ;toUser=" + toUser + " ;type=" + type);
		if (StringUtils.isEmpty(fromUser) || StringUtils.isEmpty(toUser) || StringUtils.isEmpty(type)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("fromUser=" + fromUser + " ;toUser=" + toUser + " ;type=" + type);
		} else {
			try {
				String bk = BasicConstants.USER_NEWS_BUCKET;
				String sql = null;
				QueryResult  queryResult = null;
				List<Map<String, Object>> ls = null;
				List<Map<String, Object>> ls2 = null;
				if("all".equals(type) || "news".equals(type)){
					sql = "select * from "+bk+" where start_id='"+fromUser+"'";
					logger.info(sql);
					queryResult = CBaseConnectionPool.getBucket(bk)
							.query(Query.simple(sql));
					ls = CouchbaseUtil.resultToList(queryResult ,bk);
					sql = "select * from "+bk+" where start_id='"+toUser+"'";
					logger.info(sql);
					queryResult = CBaseConnectionPool.getBucket(bk)
							.query(Query.simple(sql));
					ls2 = CouchbaseUtil.resultToList(queryResult ,bk);
					String endId = null;
					for(Map<String, Object> m  : ls){
						endId = m.get("end_id").toString();
						for(Map<String, Object> m2  : ls2){
							if(endId.equals(m2.get("end_id").toString())){
								m.putAll(m2);
								break;
							}
						}
						m.put("start_id", toUser);
						JsonObject job = JsonObject.from(m);
						JsonDocument doc = JsonDocument.create(toUser+"_"+endId, job);
						try {
							CBaseConnectionPool.getBucket(bk).upsert(doc);
							CBaseConnectionPool.getBucket(bk).remove(fromUser+"_"+endId);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("",e);
						}
					}
					if(ls2!=null){
						ls2.clear();
						ls2 = null;
					}
				}
				if("all".equals(type) || "star".equals(type)){
					bk = BasicConstants.USER_ENTITY_BUCKET;
					sql = "select * from "+bk+" where start_id='"+fromUser+"' and type is missing";
					logger.info(sql);
					queryResult = CBaseConnectionPool.getBucket(bk)
							.query(Query.simple(sql));
					ls = CouchbaseUtil.resultToList(queryResult ,bk);
					String endId = null;
					for(Map<String, Object> m  : ls){
						endId = m.get("end_id").toString();
						m.put("start_id", toUser);
						JsonObject job = JsonObject.from(m);
						JsonDocument doc = JsonDocument.create(toUser+"_"+endId, job);
						try {
							CBaseConnectionPool.getBucket(bk).upsert(doc);
							CBaseConnectionPool.getBucket(bk).remove(fromUser+"_"+endId);
						} catch (Exception e) {
							e.printStackTrace();
							logger.error("",e);
						}
					}
				}
				if (queryResult.finalSuccess()) {
					resultObject.addReturnDesc("update success ");
				} else {
					resultObject.addReturnCode(CodeManager.RELATIONHANDLER_UPDATERELATION);
					resultObject.addReturnDesc("update error ");
				}
			} catch (Exception e) {
				logger.error("updateRelation:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getStartIdByEndIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String endIds = request.getParam("endIds");
		String flag = request.getParam("flag");
		String page = request.getParam("page");
		String size = request.getParam("size", "100");
		logger.info("getStartIdByEndIds -> endIds=" + endIds + " ;flag=" + flag + " ;page=" + page + " ;size=" + size);
		if (StringUtils.isEmpty(endIds) || StringUtils.isEmpty(flag) || StringUtils.isEmpty(page)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc(
					"getStartIdByEndIds -> startId=" + endIds + " ;flag=" + flag + " ;page=" + page + " ;size=" + size);
			logger.warn("execute fail and missing arguments");
		} else {
			String[] endIdArray = endIds.split(",");
			StringBuffer sb = new StringBuffer();
			for (String id : endIdArray) {
				sb.append("'" + id + "',");
			}
			sb.append("''");
			try {
				String bk = BasicConstants.USER_NEWS_BUCKET;
				String colum = "";
				String typeWhere = "";
				int limit = Integer.parseInt(size);
				int offset = (Integer.parseInt(page) - 1) * limit;
				if (offset < 0) {
					offset = 0;
				}
				String sql = "";
				if (RCodeManager.userStarFollow.equals(flag) || RCodeManager.userSubPlayer.equals(flag) || RCodeManager.userStarSpecial.equals(flag)) {
					bk = BasicConstants.USER_ENTITY_BUCKET;
					String type = "type is missing";
					if(RCodeManager.userSubPlayer.equals(flag)){
						type = "type = '1'";
					}
					sql = "select end_id ,start_id from " + bk
							+ " where  relation='11'  and end_id in [" + sb.toString()
							+ "] and "+type+" order by update_time desc offset " + offset + " limit " + limit;
					QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
					List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult);
					resultObject.putHead("count", ls.size());
					resultObject.addResultData(ls);
				} else {
					if (RCodeManager.userStarReco.equals(flag)) { // 推荐明星
						colum = "relation  = '1' ";
						bk = BasicConstants.USER_ENTITY_BUCKET;
					} else if (RCodeManager.userNewsCollect.equals(flag)) {
						colum = "collect_relation='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsShare.equals(flag)) {
						colum = "share_relation='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsComment.equals(flag)) {
						colum = "comment_relation='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsLook.equals(flag)) {
						colum = "look_relation='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsThumb.equals(flag)) {
						colum = "up_relation='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userBlogFollow.equals(flag)) {
						colum = "collect_relation='1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogShare.equals(flag)) {
						colum = "share_relation='1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogThumb.equals(flag)) {
						colum = "up_relation='1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogComment.equals(flag)) {
						colum = "comment_relation='1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userVideoCollect.equals(flag)) {
						colum = "collect_relation='1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userVideoShare.equals(flag)) {
						colum = "share_relation='1'";
						typeWhere = "type = '3' and ";
					} else if (RCodeManager.userNewsCommentThumb.equals(flag)) {
						colum = "up_comment='1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userBlogCommentThumb.equals(flag)) {
						colum = "up_comment='1'";
						typeWhere = "type = '2' and ";
					}
					sql = "select end_id ,start_id from " + bk + " where " + typeWhere + " " + colum
							+ "  and end_id in [" + sb.toString() + "] order by update_time desc offset "
							+ offset + " limit " + limit;
					QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
					if (queryResult.finalSuccess()) {
						List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, bk);
						resultObject.putHead("count", ls.size());
						resultObject.addResultData(ls);
					}
				}
				logger.info("sql is " + sql);
			} catch (Exception e) {
				logger.error("getStartIdByEndIds", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_GETTAKEBYID);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();

	}

	public String delStarRelation(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String endIds = request.getParam("endIds");
		String relation = request.getParam("relation");
		logger.info("delStarRelation -> startId=" + startId + " ;endId=" + endIds + " ;relation=" + relation);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(endIds) || StringUtils.isEmpty(relation)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("startId=" + startId + " ;endId=" + endIds + " ;relation=" + relation);
		} else {
			try {
				String bk = BasicConstants.STAR_STAR_BUCKET;
				String[] ids = endIds.split(",");
				StringBuffer sb = new StringBuffer();
				for (String id : ids) {
					sb.append("'" + startId + "_" + id + "',");
				}
				String sql = "delete from " + bk + " use keys[" + sb.toString() + "'']";
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
						.query(Query.simple(sql));
				logger.info("sql is " + sql);
				if (queryResult.finalSuccess()) {
					resultObject.addReturnDesc("delete success ");
				} else {
					resultObject.addReturnDesc("delete error ");
				}
			} catch (Exception e) {
				logger.error("delStarRelation:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public String upsertStarRelation(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String endId = request.getParam("endId");
		String relation = request.getParam("relation");
		logger.info("addStarRelation -> startId=" + startId + " ;endId=" + endId + " ;relation=" + relation);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(endId) || StringUtils.isEmpty(relation)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("startId=" + startId + " ;endId=" + endId + " ;relation=" + relation);
		} else {
			try {
				String bk = BasicConstants.STAR_STAR_BUCKET;
				JsonObject json = JsonObject.empty();
				json.put("start_id", startId);
				json.put("end_id", endId);
				json.put("relation", relation);
				json.put("update_time", System.currentTimeMillis());
				JsonDocument doc = JsonDocument.create(startId + "_" + endId, json);
				CBaseConnectionPool.getBucket(bk).upsert(doc);
			} catch (Exception e) {
				logger.error("addStarRelation:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getRelationByUserIdAndEntityName(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityIds = request.getParam("entityIds");
		String entityNames = request.getParam("entityNames");
		logger.info("getRelationByUserIdAndEntityName -> startId=" + userId + " ;entityIds=" + entityIds
				+ " ;starNames=" + entityNames);
		if (StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> userId=" + userId + " ;entityIds=" + entityIds);
			logger.warn("missing parameter -> userId=" + userId + " ;entityIds=" + entityIds);
		} else {
			if (StringUtils.isEmpty(entityIds) && !StringUtils.isEmpty(entityNames)) {
				String[] array = entityNames.split(",");
				StringBuffer sb = new StringBuffer(" ");
				for (String name : array) {
					sb.append("'" + name + "',");
				}
				String mysql = "SELECT s.id from entity as s where s.name in(" + sb.toString() + "'')";
				logger.info("mysql is " + mysql);
				entityIds = connectionUtil.query(mysql, new ResultSetExtractor<String>() {
					@Override
					public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						StringBuffer sb = new StringBuffer(" ");
						while (resultSet.next()) {
							sb.append(resultSet.getString("id") + ",");
						}
						return sb.toString();
					}
				});
			}
			String[] sIds = entityIds.split(",");
			StringBuffer ssb = new StringBuffer(" ");
			for (String sid : sIds) {
				ssb.append("'" + userId + "_" + sid.trim() + "',");
			}
			String sql = "select end_id as id from "+BasicConstants.USER_ENTITY_BUCKET+" use keys[" + ssb.toString()
					+ "'']  where  relation='11' ";
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				String ids = "";
				StringBuffer idsb = new StringBuffer(" ");
				String id = "";
				for (String sid : sIds) {
					boolean flag = false;
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						if (null != val) {
							id = val.getString("id");
							if (id.equals(sid.trim())) {
								flag = true;
								break;
							}
						}
					}
					if(!flag){
						idsb.append(sid + ",");
					}
				}
				ids = idsb.toString();
				String mysql = "SELECT s.id,s.name,s.img,s.badge  FROM entity AS s "
						+ " WHERE s.status=1 and s.id in(" + ids + " -1)" + " GROUP BY s.id ";
				logger.info("mysql is " + mysql);
				List<Map<String, Object>> tmp = connectionUtil.query(mysql,
						new ResultSetExtractor<List<Map<String, Object>>>() {

							@Override
							public List<Map<String, Object>> extractData(ResultSet resultSet)
									throws SQLException, DataAccessException {
								Map<String, Object> map = null;
								List<Map<String, Object>> ls = new ArrayList<>();
								while (resultSet.next()) {
									map = new HashMap<>();
									map.put("id", resultSet.getInt("id"));
									map.put("name", resultSet.getString("name"));
									map.put("img", resultSet.getString("img"));
									map.put("badge", resultSet.getString("badge"));
//									String info_badge = resultSet.getString("info_badge");
//									map.put("info_badge", StringUtils.isEmpty(info_badge) ? "" : info_badge.split(","));
									ls.add(map);
								}
								return ls;
							}

						});
				resultObject.putHead("count", tmp.size());
				resultObject.addResultData(tmp);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	/**
	 * 根据实体Id 获取实体被订阅的数量
	 * @param request
	 * @return
	 */
	private String getFansCountByEntityIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String entityIds = request.getParam("entityIds");
		logger.info("getFansCountByEntityIds -> entityIds=" + entityIds);
		if (StringUtils.isEmpty(entityIds)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> entityIds=" + entityIds);
			logger.warn("missing parameter -> entityIds=" + entityIds);
		} else {
			String bk = BasicConstants.USER_ENTITY_BUCKET;
			String[] sIds = entityIds.split(",");
			StringBuffer ssb = new StringBuffer(" ");
			for (String sid : sIds) {
				ssb.append("'" + sid + "',");
			}
			String sql = "select end_id,count(1) as count from "+bk+" where end_id in [" + ssb.toString()
					+ "''] and  relation = '11' group by end_id";
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.sql(bk, sql);
			Map<String, Integer> dataTmp = new HashMap<>();
			if (queryResult.finalSuccess()) {
				for (QueryRow row : queryResult.allRows()) {
					JsonObject val = row.value();
					if (null != val) {
						dataTmp.put(val.getString("end_id") + "", val.getInt("count"));
					}
				}
				resultObject.putHead("count", dataTmp.size());
				resultObject.addResultData(dataTmp);
			} else {
				resultObject.addReturnDesc(queryResult.errors());
				logger.warn("query fail " + queryResult.info());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	private String getFansCountByStarIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String starIds = request.getParam("starIds");
		logger.info("getFansCountByStarIds -> starIds=" + starIds);
		if (StringUtils.isEmpty(starIds)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> starIds=" + starIds);
			logger.warn("missing parameter -> starIds=" + starIds);
		} else {
			String[] sIds = starIds.split(",");
			StringBuffer ssb = new StringBuffer(" ");
			for (String sid : sIds) {
				ssb.append("'" + sid + "',");
			}
			String sql = "select end_id,count(1) as count from "+BasicConstants.USER_ENTITY_BUCKET+" where end_id in [" + ssb.toString()
					+ "''] and ( relation = '11' or  relation = '21')  group by end_id";
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
					.query(Query.simple(sql));
			Map<String, Integer> dataTmp = new HashMap<>();
			if (queryResult.finalSuccess()) {
				for (QueryRow row : queryResult.allRows()) {
					JsonObject val = row.value();
					if (null != val) {
						dataTmp.put(val.getString("end_id") + "", val.getInt("count"));
					}
				}
				resultObject.putHead("count", dataTmp.size());
				resultObject.addResultData(dataTmp);
			} else {
				resultObject.addReturnDesc(queryResult.errors());
				logger.warn("query fail " + queryResult.info());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getRelationByUserIdAndEntityNamesOrIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityIds = request.getParam("entityIds");
		String entityNames = request.getParam("entityNames");
		// String page = request.getParam("page");
		// String size = request.getParam("size", "100");
		logger.info("getRelationByUserIdAndStarNamesOrIds -> startId=" + userId + " ;entityIds=" + entityIds);
		if (StringUtils.isEmpty(userId) || (StringUtils.isEmpty(entityIds) && StringUtils.isEmpty(entityNames))) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter  -> userId=" + userId + " ;entityIds=" + entityIds);
			logger.warn("missing parameter -> userId=" + userId + " ;entityIds=" + entityIds);
		} else {
			// int limit = Integer.parseInt(size);
			// int offset = (Integer.parseInt(page) - 1) * limit;
			if (StringUtils.isEmpty(entityIds) && !StringUtils.isEmpty(entityNames)) {
				String[] array = entityNames.split(",");
				StringBuffer sb = new StringBuffer(" ");
				for (String name : array) {
					sb.append("'" + name + "',");
				}
				String mysql = "SELECT s.id from entity as s where s.name in(" + sb.toString() + "'')";
				logger.info("mysql is " + mysql);
				entityIds = connectionUtil.query(mysql, new ResultSetExtractor<String>() {
					@Override
					public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						StringBuffer sb = new StringBuffer(" ");
						while (resultSet.next()) {
							sb.append(resultSet.getString("id") + ",");
						}
						return sb.toString();
					}
				});
			}
			String[] sIds = entityIds.split(",");
			StringBuffer ssb = new StringBuffer(" ");
			for (String sid : sIds) {
				ssb.append("'" + userId + "_" + sid + "',");
			}
			String sql = "select end_id as id ,relation as isTake from "+BasicConstants.USER_ENTITY_BUCKET+" use keys[" + ssb.toString()
					+ "''] order by update_time desc ";
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.USER_ENTITY_BUCKET);
				resultObject.putHead("count", ls.size());
				resultObject.addResultData(ls);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public String getRelationByStarId(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String starId = request.getParam("starId");
		String userId = request.getParam("userId");
		logger.info("getRelationByStarId -> starId=" + starId + " ;userId=" + userId);
		if (StringUtils.isEmpty(starId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> starId=" + starId + " ;userId=" + userId);
			logger.warn("missing parameter -> starId=" + starId + " ;userId=" + userId);
		} else {
			String sql = "select star_star.end_id,star_star.relation from star_star  where star_star.start_id = '"
					+ starId + "'";
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.STAR_STAR_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				Map<String, Map<String, Object>> data = new HashMap<>();
				Map<String, Object> dataTmp = new HashMap<>();
				StringBuffer sb = new StringBuffer(" ");
				List<Map<String, Object>> tmp = new ArrayList<>();
				sql = "select end_id from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id = '" + userId
						+ "' and  relation='11'   and type is missing";
				QueryResult queryResult2 = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
						.query(Query.simple(sql));
				logger.info("sql is " + sql);
				JsonObject val = null;
				for (QueryRow row : queryResult.allRows()) {
					JsonObject val2 = null;
					boolean flag = true;
					val = row.value();
					for (QueryRow row2 : queryResult2.allRows()) {
						val2 = row2.value();
						if (val2.getString("end_id").equals(val.getString("end_id"))) {// 已订阅的除外
							flag = false;
							break;
						}
					}
					if (flag) {
						dataTmp = new HashMap<>();
						sb.append(val.getString("end_id") + ",");
						dataTmp.put("relation", val.getString("relation"));
						data.put(val.getString("end_id") + "", dataTmp);
					}
				}
				final Map<String, Map<String, Object>> data2 = data;
				String ids = sb.toString();
				if (ids.length() > 0) {
					String mysql = "SELECT s.id,s.name,s.intro,s.img,s.badge,s.rank,GROUP_CONCAT(DISTINCT c.name) AS info_badge,GROUP_CONCAT(DISTINCT c2.name) AS classify_one FROM star AS s "
							+ "LEFT JOIN   star_classify AS sc ON s.id=sc.id LEFT JOIN classify  AS c ON sc.classify_id=c.id LEFT JOIN classify AS c2 ON c2.id=c.parent_id"
							+ " WHERE s.status=1 and s.id in(" + ids + "-1)" + " GROUP BY s.id ";
					logger.info("mysql is " + mysql);
					tmp = connectionUtil.query(mysql, new ResultSetExtractor<List<Map<String, Object>>>() {
						@Override
						public List<Map<String, Object>> extractData(ResultSet resultSet)
								throws SQLException, DataAccessException {
							Map<String, Object> map = null;
							List<Map<String, Object>> ls = new ArrayList<>();
							while (resultSet.next()) {
								map = data2.get(resultSet.getString("id"));
								map.put("id", resultSet.getInt("id"));
								map.put("rank", resultSet.getInt("rank"));
								map.put("name", resultSet.getString("name"));
								map.put("img", resultSet.getString("img"));
								map.put("intro", resultSet.getString("intro"));
								map.put("badge", resultSet.getString("badge"));
								String info_badge = resultSet.getString("info_badge");
								map.put("info_badge", StringUtils.isEmpty(info_badge) ? "" : info_badge.split(","));
								String classify_one = resultSet.getString("classify_one");
								map.put("classify_one",
										StringUtils.isEmpty(classify_one) ? "" : classify_one.split(","));
								ls.add(map);
							}
							return ls;
						}
					});

				}
				resultObject.putHead("count", tmp.size());
				resultObject.addResultData(tmp);
			} else {
				resultObject.addReturnDesc(queryResult.info());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	// 根据userId 获取关注特别关注的明星id
	private String getFollowByUserId(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("userId");
		String page = request.getParam("page");
		String size = request.getParam("size", "100");
		String firstClassId = request.getParam("firstClassId");
		logger.info("getFollowByStarId -> startId=" + startId + " ;page=" + page + " ;size=" + size);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(page)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("missing parameter -> startId=" + startId + " ;page=" + page + " ;size=" + size);
			logger.warn("execute fail and missing arguments");
		} else {
			int limit = Integer.parseInt(size);
			int offset = (Integer.parseInt(page) - 1) * limit;
			if (offset < 0) {
				offset = 0;
			}
			String typeWhere = " ";
			if(!StringUtils.isEmpty(firstClassId)){
				typeWhere=" and first_class_id = '"+firstClassId+"'";
			}
			String sql = "select end_id as entity_id from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id = '" + startId
					+ "' and relation = '11'  "+typeWhere+"  order by update_time desc  offset " + offset
					+ " limit " + limit;
			logger.info("sql is " + sql);
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.USER_ENTITY_BUCKET);
				resultObject.putHead("count", ls.size());
				resultObject.addResultData(ls);
			} else {
				resultObject.addReturnDesc("" + queryResult.info().asJsonObject());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_GETFOLLOWBYUSERID);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getTakeById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String flag = request.getParam("flag");
		String page = request.getParam("page");
		String size = request.getParam("size", "100");
		logger.info("getTakeById -> startId=" + startId + " ;flag=" + flag + " ;page=" + page + " ;size=" + size);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(flag) || StringUtils.isEmpty(page)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc(
					"getTakeById -> startId=" + startId + " ;flag=" + flag + " ;page=" + page + " ;size=" + size);
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_NEWS_BUCKET;
				String colum = "";
				String typeWhere = "";
				int limit = Integer.parseInt(size);
				int offset = (Integer.parseInt(page) - 1) * limit;
				if (offset < 0) {
					offset = 0;
				}
				String sql = "";
				/*if (RCodeManager.userStarFollow.equals(flag) || RCodeManager.userStarSpecial.equals(flag)) {
					bk = BasicConstants.USER_ENTITY_BUCKET;
					sql = "select end_id as id from " + bk + " where  (relation='11' or relation='21') and start_id = '"
							+ startId + "' and type is missing order by update_time desc offset " + offset + " limit " + limit;
					logger.info("sql is "+sql);
					QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
					if (queryResult.finalSuccess()) {
						StringBuffer sb = new StringBuffer(" ");
						List<Map<String, Object>> tmp = new ArrayList<>();
						for (QueryRow row : queryResult.allRows()) {
							JsonObject val = row.value();
							if (null != val) {
								sb.append(val.getString("id") + ",");
							}
						}
						String ids = sb.toString();
						if (ids.length() > 0) {
							String mysql = "SELECT s.id,s.name,s.intro,s.img,s.badge,s.rank,GROUP_CONCAT(DISTINCT c.name) AS info_badge,GROUP_CONCAT(DISTINCT c2.name) AS classify_one FROM star AS s "
									+ "LEFT JOIN   star_classify AS sc ON s.id=sc.id LEFT JOIN classify  AS c ON sc.classify_id=c.id LEFT JOIN classify AS c2 ON c2.id=c.parent_id"
									+ " WHERE s.status=1 and s.id in(" + ids + " -1)  " + " GROUP BY s.id ";
							logger.info("mysql is " + mysql);
							Map<String ,Map<String,Object>> tmpMap = connectionUtil.query(mysql, new ResultSetExtractor<Map<String ,Map<String,Object>>>() {
								@Override
								public Map<String ,Map<String,Object>> extractData(ResultSet resultSet)
										throws SQLException, DataAccessException {
									Map<String, Object> map = null;
									Map<String ,Map<String,Object>> ls = new HashMap<>();
									while (resultSet.next()) {
										map = new HashMap<>();
										map.put("id", resultSet.getInt("id"));
										map.put("rank", resultSet.getInt("rank"));
										map.put("name", resultSet.getString("name"));
										map.put("img", resultSet.getString("img"));
										map.put("intro", resultSet.getString("intro"));
										map.put("badge", resultSet.getString("badge"));
										String info_badge = resultSet.getString("info_badge");
										map.put("info_badge",
												StringUtils.isEmpty(info_badge) ? "" : info_badge.split(","));
										String classify_one = resultSet.getString("classify_one");
										map.put("classify_one",
												StringUtils.isEmpty(classify_one) ? "" : classify_one.split(","));
										ls.put(resultSet.getInt("id")+"", map);
									}
									return ls;
								}
							});
							String[] idsArray = ids.split(",");
							Map<String,Object> map = null;
							for(String starId :idsArray){
								map = tmpMap.get(starId.trim());
								if(map != null){
									tmp.add(map);
								}
							}
						}
						resultObject.putHead("count", tmp.size());
						resultObject.addResultData(tmp);
					}
				} else*/ if(RCodeManager.userSubPlayer.equals(flag)){
						bk = BasicConstants.USER_ENTITY_BUCKET;
						sql = "select end_id as id from " + bk + " where  relation='11' first_class_id = '2' and start_id = '"
								+ startId + "' order by update_time desc offset " + offset + " limit " + limit;
						QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
						if (queryResult.finalSuccess()) {
							List<String> tmp = new ArrayList<>();
							for (QueryRow row : queryResult.allRows()) {
								JsonObject val = row.value();
								if (null != val) {
									tmp.add(val.getString("id"));
								}
							}
							resultObject.putHead("count", tmp.size());
							resultObject.addResultData(tmp);
						}
				} else {
					if (RCodeManager.userStarReco.equals(flag)) { // 推荐明星
						colum = "relation  = '1'";
						bk = BasicConstants.USER_ENTITY_BUCKET;
					} else if (RCodeManager.userNewsCollect.equals(flag)) {
						colum = "collect_relation = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsShare.equals(flag)) {
						colum = "share_relation = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsComment.equals(flag)) {
						colum = "comment_relation = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsLook.equals(flag)) {
						colum = "look_relation = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userNewsThumb.equals(flag)) {
						colum = "up_relation = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userBlogFollow.equals(flag)) {
						colum = "collect_relation = '1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogShare.equals(flag)) {
						colum = "share_relation = '1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogThumb.equals(flag)) {
						colum = "up_relation = '1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userBlogComment.equals(flag)) {
						colum = "comment_relation = '1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userVideoCollect.equals(flag)) {
						colum = "collect_relation = '1'";
						typeWhere = "type = '3' and ";
					} else if (RCodeManager.userVideoShare.equals(flag)) {
						colum = "share_relation = '1'";
						typeWhere = "type = '3' and ";
					} else if (RCodeManager.userNewsCommentThumb.equals(flag)) {
						colum = "up_comment = '1'";
						typeWhere = "type = '1' and ";
					} else if (RCodeManager.userBlogCommentThumb.equals(flag)) {
						colum = "up_comment = '1'";
						typeWhere = "type = '2' and ";
					} else if (RCodeManager.userMusicCollect.equals(flag)) {
						colum = "collect_relation = '1'";
						typeWhere = "type = '4' and ";
					} else if (RCodeManager.userMusicShare.equals(flag)) {
						colum = "share_relation = '1'";
						typeWhere = "type = '4' and ";
					} else if (RCodeManager.userPlayCollect.equals(flag)) {
						colum = "collect_relation = '1' ";
						typeWhere = "type = '5' and ";
					} else if (RCodeManager.userPlayShare.equals(flag)) {
						colum = "share_relation = '1' ";
						typeWhere = "type = '5' and ";
					} else {
						resultObject.addReturnDesc("not fount this flag");
						resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ISTAKE);
					}
					sql = "select end_id as id from " + bk + " where  " + typeWhere + " " + colum
							+ "  and start_id = '" + startId + "' order by update_time desc offset "
							+ offset + " limit " + limit;
					QueryResult queryResult = CBaseConnectionPool.getBucket(bk).query(Query.simple(sql));
					if (queryResult.finalSuccess()) {
						List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, bk);
						resultObject.putHead("count", ls.size());
						resultObject.addResultData(ls);
					}
				}
				logger.info("sql is " + sql);
			} catch (Exception e) {
				logger.error("getTakeById", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_GETTAKEBYID);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String isTake(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String endId = request.getParam("endId");
		String flag = request.getParam("flag");
		logger.info("isTake -> startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(endId) || StringUtils.isEmpty(flag)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
			logger.warn("execute fail and missing arguments");
		} else {
			try {
				String bk = BasicConstants.USER_NEWS_BUCKET;
				String key = startId + "_" + endId;
				/*if (RCodeManager.userStarFollow.equals(flag) || RCodeManager.userStarSpecial.equals(flag)) {
					bk = BasicConstants.USER_ENTITY_BUCKET;
					JsonDocument doc = CBaseConnectionPool.getBucket(bk).get(key);
					if (null != doc) {
						String relation = doc.content().getString("relation");
						if (relation != null && (relation.equals("11") || relation.equals("21"))) {
							resultObject.putHead("count", 1);
							resultObject.addResultData(flag);
						}
					}
				} else if (RCodeManager.userStarInterest.equals(flag)) {
					bk = BasicConstants.USER_ENTITY_BUCKET;
					JsonDocument doc = CBaseConnectionPool.getBucket(bk).get(key);
					if (null != doc) {
						String interest_relation = doc.content().getString("interest_relation");
						if (interest_relation != null && interest_relation.equals("1")) {
							resultObject.putHead("count", 1);
							resultObject.addResultData(flag);
						}
					}
				} else if (RCodeManager.userSubPlayer.equals(flag)) {
					bk = BasicConstants.USER_ENTITY_BUCKET;
					JsonDocument doc = CBaseConnectionPool.getBucket(bk).get(key);
					if (null != doc) {
						String relation = doc.content().getString("relation");
						String type = doc.content().getString("type");
						if (relation != null && (relation.equals("11") && "1".equals(type))) {
							resultObject.putHead("count", 1);
							resultObject.addResultData(flag);
						}
					}
				} else if (RCodeManager.userStarReco.equals(flag)) { // 推荐明星
					bk = BasicConstants.USER_ENTITY_BUCKET;
					checkRelation(bk, key, "relation", flag, resultObject);
				} else*/ if (RCodeManager.userNewsCollect.equals(flag)) {
					checkRelation(bk, key, "collect_relation", flag, resultObject);
				} else if (RCodeManager.userNewsShare.equals(flag)) {
					checkRelation(bk, key, "share_relation", flag, resultObject);
				} else if (RCodeManager.userNewsComment.equals(flag)) {
					checkRelation(bk, key, "comment_relation", flag, resultObject);
				} else if (RCodeManager.userNewsLook.equals(flag)) {
					checkRelation(bk, key, "look_relation", flag, resultObject);
				} else if (RCodeManager.userNewsThumb.equals(flag)) {
					checkRelation(bk, key, "up_relation", flag, resultObject);
				} else if (RCodeManager.userBlogFollow.equals(flag)) {
					checkRelation(bk, key, "collect_relation", flag, resultObject);
				} else if (RCodeManager.userBlogShare.equals(flag)) {
					checkRelation(bk, key, "share_relation", flag, resultObject);
				} else if (RCodeManager.userBlogThumb.equals(flag)) {
					checkRelation(bk, key, "up_relation", flag, resultObject);
				} else if (RCodeManager.userBlogComment.equals(flag)) {
					checkRelation(bk, key, "comment_relation", flag, resultObject);
				} else if (RCodeManager.userVideoCollect.equals(flag)) {
					checkRelation(bk, key, "collect_relation", flag, resultObject);
				} else if (RCodeManager.userVideoShare.equals(flag)) {
					checkRelation(bk, key, "share_relation", flag, resultObject);
				} else if (RCodeManager.userNewsCommentThumb.equals(flag)) {
					checkRelation(bk, key, "up_comment", flag, resultObject);
				} else if (RCodeManager.userBlogCommentThumb.equals(flag)) {
					checkRelation(bk, key, "up_comment", flag, resultObject);
				} else if (RCodeManager.userMusicCollect.equals(flag)) {
					checkRelation(bk, key, "collect_relation", flag, resultObject);
				} else if (RCodeManager.userMusicShare.equals(flag)) {
					checkRelation(bk, key, "share_relation", flag, resultObject);
				} else if (RCodeManager.userPlayCollect.equals(flag)) {
					checkRelation(bk, key, "collect_relation", flag, resultObject);
				} else if (RCodeManager.userPlayShare.equals(flag)) {
					checkRelation(bk, key, "share_relation", flag, resultObject);
				} else if (RCodeManager.userVideoThumb.equals(flag)) {
					checkRelation(bk, key, "up_relation", flag, resultObject);
				} else if (RCodeManager.userMusicThumb.equals(flag)) {
					checkRelation(bk, key, "up_relation", flag, resultObject);
				} else if (RCodeManager.userPlayThumb.equals(flag)) {
					checkRelation(bk, key, "up_relation", flag, resultObject);
				} else if (RCodeManager.userPlayerLook.equals(flag)) {
					checkRelation(bk, key, "look_relation", flag, resultObject);
				} else {
					resultObject.addReturnDesc("not fount this flag");
					resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ISTAKE);
				}
			} catch (Exception e) {
				logger.error("isTake:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ISTAKE);
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private void checkRelation(String bk, String key, String colum, String flag, ResultMsg resultObject) {
		JsonDocument doc = CBaseConnectionPool.getBucket(bk).get(key);
		if (null != doc) {
			String relation = doc.content().getString(colum);
			if (relation != null) {
				resultObject.putHead("count", 1);
				resultObject.addResultData(flag);
			}
		}
	}

	private String delTake(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String endId = request.getParam("endId");
		String flag = request.getParam("flag");
		logger.info("delTake -> startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(flag)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
			logger.warn("execute fail and missing arguments");
		} else {
			Map<String, Object> tmp = null;
			Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				if (StringUtils.isEmpty(endId)) {
					String bk = BasicConstants.USER_ENTITY_BUCKET;
					String relationWhere = "";
					/*
					 * if(RCodeManager.userStarFollow.equals(flag) ||
					 * RCodeManager.userStarSpecial.equals(flag) ||
					 * RCodeManager.userStarReco.equals(flag)){ bk =
					 * BasicConstants.USER_ENTITY_BUCKET; }else
					 * if(RCodeManager.userStarRelation.equals(flag)){ bk =
					 * BasicConstants.STAR_STAR_BUCKET; }
					 */
					
					if (RCodeManager.userStarFollow.equals(flag)) {
						relationWhere = " (relation = '10' or relation = '11') and type is missing ";
					} else if (RCodeManager.userStarSpecial.equals(flag)) {
						relationWhere = " (relation = '20' or relation = '21')  and type is missing ";
					} else if (RCodeManager.userStarReco.equals(flag)) {
						relationWhere = " relation='31'  and type is missing ";
					} else if (RCodeManager.userSubPlayer.equals(flag)) {
						relationWhere = " relation='11' and type = '1' ";
					}
					String sql = "delete from " + bk + " where " + relationWhere + " and start_id='" + startId + "'";
					QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_ENTITY_BUCKET)
							.query(Query.simple(sql));
					logger.info("sql is " + sql);
					if (queryResult.finalSuccess()) {
						resultObject.addReturnDesc("delete success for [" + sql + "]");
//						logger.info("delete success "+startId+" for "+(BasicConstants.STAR_USERS_PREFIX+endId)+" in redis");
					}
					
				} else {
					String[] endIds = endId.split(",");
					String bk = BasicConstants.USER_NEWS_BUCKET;
					String mapKey = "";
					for (String id : endIds) {
						String key = startId + "_" + id;
						/*if (RCodeManager.userStarFollow.equals(flag)) {
							bk = BasicConstants.USER_ENTITY_BUCKET;
							mapKey = "10";
							js.zrem(BasicConstants.STAR_USERS_PREFIX+":"+endId, startId);
							js.zrem(BasicConstants.USER_STARS_PREFIX+":"+startId, endId);
							logger.info(BasicConstants.STAR_USERS_PREFIX+":"+endId+" ,startId:"+ startId);
						} else if (RCodeManager.userSubPlayer.equals(flag)) {
							mapKey = "10";
							bk = BasicConstants.USER_ENTITY_BUCKET;
							js.zrem(BasicConstants.PLAYER_USERS_PREFIX+":"+endId, startId);
							js.zrem(BasicConstants.USER_PLAYERS_PREFIX+":"+startId, endId);
							logger.info(BasicConstants.USER_PLAYERS_PREFIX+":"+endId+" ,startId:"+ startId);
						} else if (RCodeManager.userStarSpecial.equals(flag)) {
							mapKey = "20";
							bk = BasicConstants.USER_ENTITY_BUCKET;
							js.zrem(BasicConstants.STAR_USERS_PREFIX+":"+endId, startId);
							js.zrem(BasicConstants.USER_STARS_PREFIX+":"+startId, endId);
							logger.info(BasicConstants.STAR_USERS_PREFIX+":"+endId+" ,startId:"+ startId);
						} else if (RCodeManager.userStarReco.equals(flag)) { // 推荐明星
							mapKey = "30";
							bk = BasicConstants.USER_ENTITY_BUCKET;
						} else if (RCodeManager.userStarInterest.equals(flag)) { // 推荐明星
							mapKey = "0";
							bk = BasicConstants.USER_ENTITY_BUCKET;
						} else*/ 
						if (RCodeManager.userNewsCollect.equals(flag)) {
							mapKey = "collect_relation";
						} else if (RCodeManager.userNewsShare.equals(flag)) {
							mapKey = "share_relation";
						} else if (RCodeManager.userNewsComment.equals(flag)) {
							mapKey = "comment_relation";
						} else if (RCodeManager.userNewsLook.equals(flag)) {
							mapKey = "look_relation";
						} else if (RCodeManager.userNewsThumb.equals(flag)) {
							mapKey = "up_relation";
						} else if (RCodeManager.userBlogFollow.equals(flag)) {
							mapKey = "collect_relation";
						} else if (RCodeManager.userBlogShare.equals(flag)) {
							mapKey = "share_relation";
						} else if (RCodeManager.userBlogThumb.equals(flag)) {
							mapKey = "up_relation";
						} else if (RCodeManager.userBlogComment.equals(flag)) {
							mapKey = "comment_relation";
						} else if (RCodeManager.userVideoCollect.equals(flag)) {
							mapKey = "collect_relation";
						} else if (RCodeManager.userVideoShare.equals(flag)) {
							mapKey = "share_relation";
						} else if (RCodeManager.userNewsCommentThumb.equals(flag)) {
							mapKey = "up_comment";
						} else if (RCodeManager.userBlogCommentThumb.equals(flag)) {
							mapKey = "up_comment";
						} else if (RCodeManager.userMusicCollect.equals(flag)) {
							mapKey = "collect_relation";
						} else if (RCodeManager.userMusicShare.equals(flag)) {
							mapKey = "share_relation";
						}  else if (RCodeManager.userPlayCollect.equals(flag)) {
							mapKey = "collect_relation";
						}  else if (RCodeManager.userPlayShare.equals(flag)) {
							mapKey = "share_relation";
						}  else if (RCodeManager.userPlayThumb.equals(flag)) {
							mapKey = "up_relation";
						}  else if (RCodeManager.userVideoThumb.equals(flag)) {
							mapKey = "up_relation";
						}  else if (RCodeManager.userMusicThumb.equals(flag)) {
							mapKey = "up_relation";
						}  else if (RCodeManager.userPlayerLook.equals(flag)) {
							mapKey = "look_relation";
						}  else {
							resultObject.addReturnDesc("not fount this flag");
							resultObject.addReturnCode(CodeManager.RELATIONHANDLER_DELTAKE);
						}
						JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
						if (null != data) {
							tmp = data.content().toMap();
							tmp.put("update_time", System.currentTimeMillis());
							if (mapKey.equals("10") || mapKey.equals("20")) {
								tmp.put("relation", mapKey);
							} /*else if (RCodeManager.userStarInterest.equals(flag)) {
								tmp.put("interest_relation", mapKey);
							}*/ else {
								tmp.remove(mapKey);
							}
							JsonObject job = JsonObject.from(tmp);
							JsonDocument doc = JsonDocument.create(key, job);
							CBaseConnectionPool.getBucket(bk).upsert(doc);
						}
					}
				}
			} catch (Exception e) {
				logger.error("delTake:", e);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_DELTAKE);
			}finally{
				if(null != js){
					js.close();
				}
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	/**
	 * 分页获取某个用户在指定分类下订阅的实体，结果按照订阅时间倒序
	 * @param request
	 * @return
	 */
	private String getSubEntitysByUserIdAndFirstClassIds(Request request){
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String firstClassIds = request.getParam("firstClassIds");
		String firstClassId = request.getParam("firstClassId");
		int page = Integer.parseInt(request.getParam("page","1"));
		int size = Integer.parseInt(request.getParam("size","10"));
		
		if (StringUtils.isEmpty(userId) ) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("getSubEntitysByUserIdAndFirstClassId -> userId=" + userId );
			logger.warn("execute fail and missing arguments");
		} else {
			int offset = (page - 1)*size;
			String where = "";
			if(!StringUtils.isEmpty(firstClassId)){
				firstClassIds += firstClassId;
			}
			if(!StringUtils.isEmpty(firstClassIds)){
				where += " and first_class_id in [";
				String [] ids = firstClassIds.split(",");
				for(String id : ids){
					where += "'"+id+"',";
				}
				where += "'']";
			}
			String sql = "select end_id as entity_id from "+BasicConstants.USER_ENTITY_BUCKET+" where start_id ='"+userId+"' "+where+" and relation = '11' order by update_time desc offset "+offset+" limit "+size+" ";
			logger.info("sql is "+sql);
			List<Map<String, Object>> ls = CouchbaseUtil.resultToList(CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET ,sql) ,BasicConstants.USER_ENTITY_BUCKET);
			resultObject.putCount(ls.size());
			resultObject.addResultData(ls);
			
			logger.info("getSubEntitysByUserIdAndFirstClassId-> userId=" + userId );
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	
	
	/**
	 * 判断当前用户是否订阅这些实体
	 * @param request
	 * @return
	 */
	private String findSubEntitys(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityIds = request.getParam("entityIds");
		
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(entityIds)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("isSubEntitys -> userId=" + userId + " ;entityIds=" +entityIds);
			logger.warn("execute fail and missing arguments");
		} else {
			String sql = "select end_id as entity_id from "+BasicConstants.USER_ENTITY_BUCKET+" use keys [";
			String[] entityIdsArray = entityIds.split(",");
			StringBuffer sb = new StringBuffer();
			for(String entityId:entityIdsArray){
				sb.append("'"+userId+"_"+entityId+"',");
			}
			sql+= sb.toString()+"''] where relation = '11'";
			logger.info("sql is "+sql);
			List<Map<String, Object>> ls = CouchbaseUtil.resultToList(CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET ,sql) ,BasicConstants.USER_ENTITY_BUCKET);
			List<String> ids = new ArrayList<>();
			for(Map<String, Object> map : ls){
				ids.add(map.get("entity_id").toString());
			}
			resultObject.putCount(ids.size());
			resultObject.addResultData(ids);
			
			logger.info("isSubEntitys-> userId=" + userId + " ;entityId=" +entityIds);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 获取用户与实体之间的关系
	 * @param request
	 * @return
	 */
	private String getRelationForEntityIds(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityIds = request.getParam("entityIds");
		
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(entityIds)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("isSubEntitys -> userId=" + userId + " ;entityIds=" +entityIds);
			logger.warn("execute fail and missing arguments");
		} else {
			String sql = "select end_id as entity_id,relation from "+BasicConstants.USER_ENTITY_BUCKET+" use keys [";
			String[] entityIdsArray = entityIds.split(",");
			StringBuffer sb = new StringBuffer();
			for(String entityId:entityIdsArray){
				sb.append("'"+userId+"_"+entityId+"',");
			}
			sql+= sb.toString()+"'']";
			List<Map<String, Object>> ls = CouchbaseUtil.resultToList(CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET ,sql) ,BasicConstants.USER_ENTITY_BUCKET);
			resultObject.putCount(ls.size());
			resultObject.addResultData(ls);
			
			logger.info("isSubEntitys-> userId=" + userId + " ;entityId=" +entityIds);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 不感兴趣
	 * @param request
	 * @return
	 */
	private String unInterested(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityId = request.getParam("entityId");
		String firstClassId = request.getParam("firstClassId");
//		Jedis js = null;
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(entityId) ) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("unInterested -> userId=" + userId + " ;entityId=" +entityId + " ;firstClassId=" + firstClassId);
			logger.warn("execute fail and missing arguments");
		} else {
			Map<String, Object> tmp = null;
			String key = userId + "_" + entityId;
			
			
			String bk = BasicConstants.USER_ENTITY_BUCKET;
			JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
			if (null != data) {
				tmp = data.content().toMap();
			}else{
				tmp = new HashMap<>();
				tmp.put("start_id", userId);
				tmp.put("end_id", entityId);
			}
			tmp.put("relation", BasicConstants.UNINTERESTED_ENTITY_RELATION_CODE);
			tmp.put("update_time", System.currentTimeMillis());
			
			JsonObject job = JsonObject.from(tmp);
			JsonDocument doc = JsonDocument.create(key, job);
			CBaseConnectionPool.upsert(BasicConstants.USER_ENTITY_BUCKET ,doc);
			/*try {
				js = RedisConnectionPool.getCluster();
				js.zrem(BasicConstants.SUB_ENTITY_RELATION_CODE+":"+userId, entityId);
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if(null != js){
					js.close();
				}
			}*/
			
//			logger.info("unInterested-> userId=" + userId + " ;entityId=" +entityId + " ;firstClassId=" + firstClassId);
			logger.info(BasicConstants.MY_LOG_PREFIX+"unInterested"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+entityId);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 取消自定义关键词对象的订阅关系 (删除 user-entity 关系链)
	 * @param request
	 * @return
	 */
	private String unSubscribe(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityId = request.getParam("entityId");
		
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(entityId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("unSubscribe -> userId=" + userId + " ;entityId=" +entityId );
			logger.warn("execute fail and missing arguments");
		} else {
			String key = userId + "_" + entityId;
			try {
				CBaseConnectionPool.deleteByKey(BasicConstants.USER_ENTITY_BUCKET ,key);
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("not fount key ["+key+"]");
				resultObject.addReturnCode(CodeManager.COUCHBASE_NOT_FOUND_KEY);
				resultObject.addReturnDesc("not fount key ["+key+"]");
			}
			/*Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				js.zadd(BasicConstants.REDIS_USER_ENTITIES_PREFIX +":"+userId, Double.parseDouble(BasicConstants.UNINTERESTED_ENTITY_RELATION_CODE), entityId);
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if(null != js){
					js.close();
				}
			}*/
//			logger.info("unSubscribe-> userId=" + userId + " ;entityId=" +entityId  );
			logger.info(BasicConstants.MY_LOG_PREFIX+"unSubscribe"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+entityId);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 自定义关键词订阅接口
	 * @param request
	 * @return
	 */
	private String subEntity(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityId = request.getParam("entityId");
		String firstClassId = request.getParam("firstClassId");
		
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(entityId) || StringUtils.isEmpty(firstClassId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("subEntity -> userId=" + userId + " ;entityId=" +entityId + " ;firstClassId=" + firstClassId);
			logger.warn("execute fail and missing arguments");
		} else {
//			Jedis js = null;
			try {
				
				Integer.parseInt(firstClassId);
				Map<String, Object> tmp = new HashMap<String, Object>();
				String key = userId + "_" + entityId;
				tmp.put("start_id", userId);
				tmp.put("end_id", entityId);
				tmp.put("relation", BasicConstants.SUB_ENTITY_RELATION_CODE);
				tmp.put("first_class_id", firstClassId);
				tmp.put("update_time", System.currentTimeMillis());
				JsonObject job = JsonObject.from(tmp);
				JsonDocument doc = JsonDocument.create(key, job);
				CBaseConnectionPool.upsert(BasicConstants.USER_ENTITY_BUCKET ,doc);
				
//				js = RedisConnectionPool.getCluster();
//				js.zadd(BasicConstants.REDIS_USER_ENTITIES_PREFIX +":"+userId, Double.parseDouble(BasicConstants.SUB_ENTITY_RELATION_CODE), entityId);
				resultObject.putCount(1);
			} catch (Exception e) {
				logger.error("",e);
				resultObject.addReturnCode(CodeManager.CODE_401);
				resultObject.addReturnDesc("firstClassId 不能转化为数字 "+e.getMessage());
				e.printStackTrace();
			} /*finally{
				if(null != js){
					js.close();
				}
			}*/
			
//			logger.info("subEntity-> userId=" + userId + " ;entityId=" +entityId + " ;firstClassId=" + firstClassId);
			logger.info(BasicConstants.MY_LOG_PREFIX+"subEntity"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+entityId+BasicConstants.MY_LOG_SPLIT+firstClassId);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/*
	 * flag-01代表用户订阅明星，flag-02代表用户特别关注明星；11代表用户收藏内容，12代表用户分享内容，13代表用户评论内容；
	 * 14代表用户查看内容；21代表用户收藏帖子；22代表用户分享帖子，23代表用户评论帖子；31代表用户收藏视频，32代表用户分享视频
	 */
	private String addTake(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String startId = request.getParam("startId");
		String endId = request.getParam("endId");
		String flag = request.getParam("flag");
		
		if (StringUtils.isEmpty(startId) || StringUtils.isEmpty(endId) || StringUtils.isEmpty(flag)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
			logger.warn("execute fail and missing arguments");
		} else {
			Map<String, Object> tmp = new HashMap<String, Object>();
			Jedis js = null;
			try {
//				logger.info("addTake -> startId=" + startId + " ;endId=" + endId + " ;flag=" + flag);
				logger.info(BasicConstants.MY_LOG_PREFIX+"addTake"+BasicConstants.MY_LOG_SPLIT+startId+BasicConstants.MY_LOG_SPLIT+endId+BasicConstants.MY_LOG_SPLIT+flag);
				js = RedisConnectionPool.getCluster();
				String[] endIds = endId.split(",");
				String bk = BasicConstants.USER_NEWS_BUCKET;
				for (String id : endIds) {
					String key = startId + "_" + id;
					tmp.put("start_id", startId);
					tmp.put("end_id", id);
					tmp.put("update_time", System.currentTimeMillis());
					boolean exist = true;
					/*	if (RCodeManager.userStarFollow.equals(flag)) {
						js.zadd(BasicConstants.STAR_USERS_PREFIX +":"+endId, 11, startId);
						js.zadd(BasicConstants.USER_STARS_PREFIX +":"+startId, 11,endId);
						logger.info("add success "+startId+" for "+(BasicConstants.STAR_USERS_PREFIX+endId)+" in redis");
						tmp.put("relation", "11");
						bk = BasicConstants.USER_ENTITY_BUCKET;
					} else if (RCodeManager.userStarSpecial.equals(flag)) {
						js.zadd(BasicConstants.STAR_USERS_PREFIX+":"+endId, 21, startId);
						js.zadd(BasicConstants.USER_STARS_PREFIX +":"+startId, 21,endId);
						logger.info("add success "+startId+" for "+(BasicConstants.STAR_USERS_PREFIX+endId)+" in redis");
						tmp.put("relation", "21");
						bk = BasicConstants.USER_ENTITY_BUCKET;
					} else*/
					if (RCodeManager.userSubPlayer.equals(flag)) {
						/*js.zadd(BasicConstants.PLAYER_USERS_PREFIX+":"+endId, 11, startId);
						js.zadd(BasicConstants.USER_PLAYERS_PREFIX +":"+startId, 11,endId);
						logger.info("add success "+startId+" for "+(BasicConstants.STAR_USERS_PREFIX+endId)+" in redis");*/
						tmp.put("relation", "11");
						tmp.put("type", "1");
						bk = BasicConstants.USER_ENTITY_BUCKET;
					} else if (RCodeManager.userStarReco.equals(flag)) { // 推荐明星
					/*	js.zadd(BasicConstants.STAR_USERS_PREFIX+":"+endId, 31, startId);
						logger.info("add success "+startId+" for "+(BasicConstants.STAR_USERS_PREFIX+endId)+" in redis");*/
						tmp.put("relation", "31");
						bk = BasicConstants.USER_ENTITY_BUCKET;
					}/* else if (RCodeManager.userStarInterest.equals(flag)) { // 感兴趣
						tmp.put("interest_relation", "1");
						bk = BasicConstants.USER_ENTITY_BUCKET;
					}*/ else if (RCodeManager.userNewsCollect.equals(flag)) {
						tmp.put("collect_relation", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userNewsShare.equals(flag)) {
						tmp.put("share_relation", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userNewsComment.equals(flag)) {
						tmp.put("comment_relation", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userNewsLook.equals(flag)) {
						tmp.put("look_relation", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userNewsThumb.equals(flag)) {
						tmp.put("up_relation", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userBlogFollow.equals(flag)) {
						tmp.put("collect_relation", "1");
						tmp.put("type", "2");
					} else if (RCodeManager.userBlogShare.equals(flag)) {
						tmp.put("share_relation", "1");
						tmp.put("type", "2");
					} else if (RCodeManager.userBlogThumb.equals(flag)) {
						tmp.put("up_relation", "1");
						tmp.put("type", "2");
					} else if (RCodeManager.userBlogComment.equals(flag)) {
						tmp.put("comment_relation", "1");
						tmp.put("type", "2");
					} else if (RCodeManager.userVideoCollect.equals(flag)) {
						tmp.put("collect_relation", "1");
						tmp.put("type", "3");
					} else if (RCodeManager.userVideoShare.equals(flag)) {
						tmp.put("share_relation", "1");
						tmp.put("type", "3");
					} else if (RCodeManager.userNewsCommentThumb.equals(flag)) {
						tmp.put("up_comment", "1");
						tmp.put("type", "1");
					} else if (RCodeManager.userBlogCommentThumb.equals(flag)) {
						tmp.put("up_comment", "1");
						tmp.put("type", "2");
					} else if (RCodeManager.userMusicCollect.equals(flag)) {
						tmp.put("collect_relation", "1");
						tmp.put("type", "4");
					} else if (RCodeManager.userMusicShare.equals(flag)) {
						tmp.put("share_relation", "1");
						tmp.put("type", "4");
					} else if (RCodeManager.userPlayCollect.equals(flag)) {
						tmp.put("collect_relation", "1");
						tmp.put("type", "5");
					} else if (RCodeManager.userPlayShare.equals(flag)) {
						tmp.put("share_relation", "1");
						tmp.put("type", "5");
					} else if (RCodeManager.userPlayThumb.equals(flag)) {
						tmp.put("up_relation", "1");
						tmp.put("type", "5");
					} else if (RCodeManager.userVideoThumb.equals(flag)) {
						tmp.put("up_relation", "1");
						tmp.put("type", "3");
					} else if (RCodeManager.userMusicThumb.equals(flag)) {
						tmp.put("up_relation", "1");
						tmp.put("type", "4");
					} else if (RCodeManager.userPlayerLook.equals(flag)) {
						tmp.put("look_relation", "1");
						tmp.put("type", "6");
					} else {
						resultObject.addReturnDesc("not fount this flag");
						resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
						exist = false;
					}
					if(exist){
						JsonDocument data = CBaseConnectionPool.getBucket(bk).get(key);
						if (null != data) {
							Map<String, Object> tmpMap = data.content().toMap();
							tmpMap.putAll(tmp);
							tmp = tmpMap;
						}
						JsonObject job = JsonObject.from(tmp);
						JsonDocument doc = JsonDocument.create(key, job);
						if(tmp.containsKey("collect_relation") || tmp.containsKey("up_relation")  || tmp.containsKey("share_relation") ){
							CBaseConnectionPool.getBucket(bk).upsert(doc );
						}else{
							CBaseConnectionPool.getBucket(bk).upsert(doc ,15 ,TimeUnit.DAYS);
						}
					}
				}
			} catch (Exception e) {
				logger.error("addTake:", e);
				// resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
				resultObject.addReturnDesc(e.getMessage());
				resultObject.addReturnCode(CodeManager.RELATIONHANDLER_ADDTAKE);
			}finally{
				if(null != js){
					js.close();
				}
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

}
