package cn.qctt.httpserver.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.CurlResponse;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.bean.TypeCode;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.core.RCodeManager;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import cn.qctt.httpserver.utils.DateUtil;
import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.HttpUrlUtil;
import cn.qctt.httpserver.utils.KafkaProducer;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX + "/index")
public class EsIndexHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(EsIndexHandler.class);
	private static String autoCompletedUrl = BasicConstants.URL_PREFIX + "/index/autoCompleted";
	private static String addIndexUrl = BasicConstants.URL_PREFIX + "/index/addIndex";
	private static String addAsyncIndexUrl = BasicConstants.URL_PREFIX + "/index/addAsyncIndex";
	private static String createDBUrl = BasicConstants.URL_PREFIX + "/index/createDB";
	private static String delDBUrl = BasicConstants.URL_PREFIX + "/index/delDB";
	private static String delIndexUrl = BasicConstants.URL_PREFIX + "/index/delIndex";
	private static String upIndex4FlagUrl = BasicConstants.URL_PREFIX + "/index/upIndex4Flag";
	private static String upIndexUrl = BasicConstants.URL_PREFIX + "/index/upIndex";
	private static String searchUrl = BasicConstants.URL_PREFIX + "/index/search";
	private static String mainSearchUrl = BasicConstants.URL_PREFIX + "/index/mainSearch";
	private static String queryAllNewsUrl = BasicConstants.URL_PREFIX + "/index/queryAllNews";
	private static String queryEntityInfoByNamesUrl = BasicConstants.URL_PREFIX + "/index/queryEntityInfoByNames";
	private static String queryNewsWeiBoUrl = BasicConstants.URL_PREFIX + "/index/1queryNewsWeiBo";
	private static String queryNewsVideoUrl = BasicConstants.URL_PREFIX + "/index/queryNewsVideo";
	private static String queryPostBarUrl = BasicConstants.URL_PREFIX + "/index/queryPostBar";
	private static String queryPlayerUrl = BasicConstants.URL_PREFIX + "/index/queryPlayer";
	private static String queryStarsUrl = BasicConstants.URL_PREFIX + "/index/queryStars";
	private static String queryCollectionsUrl = BasicConstants.URL_PREFIX + "/index/1queryCollections";
	private static JdbcTemplate connectionUtil2 = ConnectionUtil.getMysqlConnect();
	// private static int SEARCH_INFO_SIZE_DEFAULT = 50;
	private static int SEARCH_PLAYER_SIZE_DEFAULT = 1;
	private static String QUERY_STAR_PREFIX = "queryStars";
	private static String QUERY_TAG = "5566";

	/**
	 * 3.0 api
	 *
	 */
	private static String mainQueryForEntitysUrl = BasicConstants.URL_PREFIX + "/index/mainQueryForEntitys";
	private static String queryEntitysInFirstClassUrl = BasicConstants.URL_PREFIX + "/index/queryEntitysInFirstClass";
	private static String termQueryUrl = BasicConstants.URL_PREFIX + "/index/termQuery";

	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(addIndexUrl)) {
			returnStr = addIndex(request);
		} else if (urlPath.startsWith(mainQueryForEntitysUrl)) {
			returnStr = mainQueryForEntitys(request);
		} else if (urlPath.startsWith(queryEntitysInFirstClassUrl)) {
			returnStr = queryEntitysInFirstClass(request);
		} else if (urlPath.startsWith(queryEntityInfoByNamesUrl)) {
			returnStr = queryEntityInfoByNames(request);
		} else if (urlPath.startsWith(upIndex4FlagUrl)) {
			returnStr = upIndex4Flag(request);
		} else if (urlPath.startsWith(addAsyncIndexUrl)) {
			returnStr = addIndex4Kafka(request);
		} else if (urlPath.startsWith(searchUrl)) {
			returnStr = search(request);
		} else if (urlPath.startsWith(mainSearchUrl)) {
			returnStr = mainSearch(request);
		} else if (urlPath.startsWith(createDBUrl)) {
			returnStr = createDB(request);
		} else if (urlPath.startsWith(queryPlayerUrl)) {
			returnStr = queryPlayer(request);
		} else if (urlPath.startsWith(delDBUrl)) {
			returnStr = delDB(request);
		} else if (urlPath.startsWith(queryNewsWeiBoUrl)) {
			returnStr = queryNewsWeiBo(request);
		} else if (urlPath.startsWith(queryNewsVideoUrl)) {
			returnStr = queryNewsVideo(request);
		} else if (urlPath.startsWith(queryAllNewsUrl)) {
			returnStr = queryAllNews(request);
		} else if (urlPath.startsWith(queryPostBarUrl)) {
			returnStr = queryPostBar(request);
		} else if (urlPath.startsWith(queryStarsUrl)) {
			returnStr = queryStars(request);
		} else if (urlPath.startsWith(queryCollectionsUrl)) {
			returnStr = queryCollections(request);
		} else if (urlPath.startsWith(delIndexUrl)) {
			returnStr = delIndex(request);
		} else if (urlPath.startsWith(autoCompletedUrl)) {
			returnStr = autoCompleted(request);
		} else if (urlPath.startsWith(upIndexUrl)) {
			returnStr = upIndex(request);
		} else if (urlPath.startsWith(termQueryUrl)) {
			returnStr = termQuery(request);
		} else {
			resultObject.addReturnCode(CodeManager.CODE_404);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}

	private String termQuery(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String type = request.getParam("type");
		String firstClassIds = request.getParam("firstClassIds");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			 
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				String machAll = "{  \"query\": {    \"match_all\": {}  },  \"sort\": [    {      \"pub_time\": {        \"order\": \"desc\"      }    }  ],   \"size\": $size,  \"from\": $from} ";
				String filteredByFirstClass = " {  \"query\": {    \"bool\": {      \"filter\": {        \"terms\": {          \"first_class_ids\": [            $firstClassIds          ]        }      }    }  }, \"sort\": [    {      \"pub_time\": {        \"order\": \"desc\"      }    }  ],   \"size\": $size,  \"from\": $from} ";
				String url = ConfigInit.getValue("esUrl")+"/"+ BasicConstants.INDEX_PREFIX +  "*/_search";
				if(null != type){
					 url = ConfigInit.getValue("esUrl") +"/"+ BasicConstants.INDEX_PREFIX + "*/" + type +  "/_search";
				}
				String query = null;
				if(firstClassIds != null){
					StringBuffer sb = new StringBuffer();
					String [] ids = firstClassIds.split(",");
					for(String id : ids){
						sb.append("\"" + id + "\" ,");
					}
					sb.append("\"\"");
					query = filteredByFirstClass.replace("$size", limit+"").replace("$from", from+"").replace("$firstClassIds", sb.toString());
				}else{
					query = machAll.replace("$size", limit+"").replace("$from", from+"");
				}
				
				String jsonStr = HttpClientUtil.sendPostRequest(url, query,	"utf-8");
				JsonParser paser = new JsonParser();
				JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
				if (obj != null) {
					JsonObject hits = obj.get("hits").getAsJsonObject();
					JsonArray array = hits.get("hits").getAsJsonArray(); 
					resultObject.addResultData(array);
					resultObject.putCount(array.size());;
				}
		} catch (Exception e) {
			logger.error("", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("query is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private List<JsonObject> queryEntitysInFirstClass(String text, String firstClassId, int from, int size) {
		boolean isContinue = false;
		List<JsonObject> ls = new ArrayList<>();
//		Map<String, Object> map = null;
//		String query = "{  \"query\": {    \"bool\": {      \"filter\": {        \"term\": {          \"first_class_ids\": \"$firstClassId\"        }      },      \"should\": [        {          \"multi_match\": {            \"query\": \"$text\",            \"fields\": [\"name^3\",\"nicknames\", \"title\",\"entity_names\",\"player_type\"]          }        }      ]    }  },  \"from\": $from,  \"size\": $size}";
		String query = " {  \"query\": {    \"function_score\": {      \"query\": {        \"bool\": {          \"filter\": {            \"term\": {              \"first_class_ids\": \"$firstClassId\"            }          },          \"should\": [            {              \"multi_match\": {                \"query\": \"$text\",                \"fields\": [                  \"name^3\",                  \"nicknames\",                  \"title\",                  \"entity_names\",                  \"player_type\"                ]              }            }          ]        }      },      \"field_value_factor\": {        \"field\": \"rank\",        \"modifier\": \"log1p\",        \"factor\": 20      },      \"boost_mode\": \"sum\",      \"max_boost\":  3    }  },  \"from\": $from,  \"size\": $size} ";
		query = query.replace("$text", text);
		query = query.replace("$firstClassId", firstClassId);
		query = query.replace("$from", from + "");
		query = query.replace("$size", size + "");
		String url = ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB +  "/_search";
		 
		String jsonStr = HttpClientUtil.sendPostRequest(url, query,	"utf-8");
		double minScore =1.4;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (!text.contains(QUERY_TAG)) {
			text += " " + QUERY_TAG;
			isContinue = true;
		}
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			double tagScore = 0;
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
//				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
//					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore ) {
						break;
					} else if (job.get("_score").getAsDouble() > 0.5) {
						JsonObject source = job.get("_source").getAsJsonObject();
						JsonElement name = source.get("name");
						if (isContinue && name!=null && name.getAsString().contains(QUERY_TAG)) {
							tagScore = job.get("_score").getAsDouble() * 0.8;
							continue;
						}
						if (job.get("_score").getAsDouble() < tagScore) {
							break;
						}
						/*map.put("name", name);
						map.put("entity_id", source.get("id").getAsString());
						map.put("intro", source.get("intro").getAsString());
						map.put("img", source.get("img").getAsString());
						map.put("badge", source.get("badge").getAsString());
						map.put("rank", source.get("rank"));
						JsonElement info = source.get("info_badge");
						if (info != null && !info.isJsonNull()) {
							map.put("info_badge", info.getAsString());
						} else {
							map.put("info_badge", "");
						}
*/
						source.remove("content");
						ls.add(source);
					}
				}
			}
		}
		return ls;
	}

	/**
	 * 根据firstClassId进行垂直类别搜索
	 * 
	 * @param request
	 * @return
	 */
	private String queryEntitysInFirstClass(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String firstClassId = request.getParam("firstClassId");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				List<JsonObject> ls = new ArrayList<>();
				ls = queryEntitysInFirstClass(text, firstClassId, from, limit);
				addFlowStatAndfollowedCount4Document(ls ,userId);
				if(ls.size()==1){
					resultObject.addResultData(fullStarInfo4Document(ls,userId));
					resultObject.putHead("count", 2);
				}else{
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				}
				/*
				 * if(redisData.size()>0){ jedis.del(QUERY_STAR_PREFIX + ":" +
				 * userId); logger.info(
				 * "clear es query cache for stars ,and redis size is "
				 * +redisData); }
				 */
				Map<String, String> kw = new HashMap<>();
				kw.put("time", DateUtil.getCurrentDate());
				kw.put("query", text);
				kw.put("user", userId);
				kw.put("size", ls.size() + "");
				kw.put("type", "queryEntitysInFirstClass");
				kw.put("firstClassId", firstClassId);
				kw.put("timing", (System.currentTimeMillis() - start) + "");
				String kwJson = new Gson().toJson(kw);
				logger.info(kwJson);
				logger.info(BasicConstants.MY_LOG_PREFIX+"queryEntitysInFirstClass"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+text+BasicConstants.MY_LOG_SPLIT+DateUtil.getCurrentDate()+BasicConstants.MY_LOG_SPLIT+firstClassId);
				KafkaProducer.send("kw", kwJson);
			}
		} catch (Exception e) {
			logger.error("", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("query is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	/**
	 * 实体查询综合接口
	 * 
	 * @param request
	 * @return
	 */
	private String mainQueryForEntitys(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		Jedis redis = null;
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				redis = RedisConnectionPool.getCluster();
				Set<String> classIds = redis.smembers(BasicConstants.DISABLED_CLASS_KEY);
				StringBuffer sb = new StringBuffer();
				for(String s: classIds){
					sb.append(" \""+s+"\" ,");
				}
				sb.append("\"\"");
//				String query = "{    \"query\": {        \"function_score\": {            \"query\": {                \"multi_match\": {                    \"query\": \"$text\",                    \"fields\": [                        \"name\",                        \"nicknames\",                        \"title\",                        \"stars\"                    ],                    \"type\": \"cross_fields\"                }            },            \"functions\": [                {                    \"field_value_factor\": {                        \"field\": \"rank\",                        \"modifier\": \"log1p\",                        \"factor\": 0.05                    }                }            ],            \"boost_mode\": \"sum\",            \"max_boost\": 2        }    },    \"from\": $from,    \"size\": $size}";
				String query = "{    \"query\": {        \"function_score\": {     \"filter\": {        \"not\": {          \"filter\": {            \"terms\": {              \"first_class_ids\": [                $firstClassIds              ]            }          }        }      },        \"query\": {                \"multi_match\": {                    \"query\": \"$text\",                    \"fields\": [                        \"name\",                        \"nicknames\",                        \"title\",                        \"entity_names\"                    ],                    \"type\": \"cross_fields\"                }            },                    \"boost_mode\": \"sum\",            \"max_boost\": 2        }    },    \"from\": $from,    \"size\": $size}";
				String url = ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/_search";
				query = query.replace("$firstClassIds", sb.toString());
				query = query.replace("$text", text);
				query = query.replace("$from", from + "");
				query = query.replace("$size", limit + "");
				JsonArray jsonArray = Utils.fetchDataInES(url, query);
				List<JsonObject> ls = new ArrayList<>();
				String type = "";
				for (JsonElement doc : jsonArray) {
					JsonElement source = doc.getAsJsonObject().get("_source");
					type = doc.getAsJsonObject().get("_type").getAsString();
					source.getAsJsonObject().addProperty("type", type);
					ls.add(source.getAsJsonObject());
				}
				addFlowStatAndfollowedCount4Document(ls ,userId);
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				/*
				 * if(redisData.size()>0){ jedis.del(QUERY_STAR_PREFIX + ":" +
				 * userId); logger.info(
				 * "clear es query cache for stars ,and redis size is "
				 * +redisData); }
				 */
				Map<String, String> kw = new HashMap<>();
				kw.put("time", DateUtil.getCurrentDate());
				kw.put("query", text);
				kw.put("user", userId);
				kw.put("size", ls.size() + "");
				kw.put("type", "mainQueryForEntitys");
				kw.put("timing", (System.currentTimeMillis() - start) + "");
				String kwJson = new Gson().toJson(kw);
//				logger.info(kwJson);
				logger.info(BasicConstants.MY_LOG_PREFIX+"mainQueryForEntitys"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+text+BasicConstants.MY_LOG_SPLIT+DateUtil.getCurrentDate());
				KafkaProducer.send("kw", kwJson);
			}
		} catch (Exception e) {
			logger.error(QUERY_STAR_PREFIX + ":", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc(QUERY_STAR_PREFIX + " is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	
	/**
	 * 为实体添加订阅状态和订阅数
	 * @param request
	 * @return
	 */
	private void addFlowStatAndfollowedCount4Document(List<JsonObject> items, String userId) {
		if (items.size() == 0) {
			return;
		}
		String bk = BasicConstants.USER_ENTITY_BUCKET;
		String sql = "select end_id  from " + bk + " use keys[";
		String sql2 = "select end_id,count(end_id) count from " + bk
				+ " where end_id in [";
		String ids = "";
		String endIds = "";
		for (JsonElement element : items) {
			ids += "'" + userId + "_" + element.getAsJsonObject().get("id").getAsString() + "',";
			endIds += "'" + element.getAsJsonObject().get("id").getAsString() + "',";
		}
		sql += ids + "''] where relation = '11'";
		sql2 += endIds + " '']  and  relation='11'  ";
		/*if (type != null) {
			sql2 += " and type='" + type + "' ";
		} else {
			sql2 += " and type is missing ";
		}*/
		sql2 += "group by end_id";
		QueryResult queryResult = CBaseConnectionPool.sql(bk, sql);
		logger.info("sql is " + sql);
		QueryResult queryResult2 = CBaseConnectionPool.sql(bk,sql2);
		logger.info("sql is " + sql2);
		if (queryResult.finalSuccess()) {
			String endId = "";
			JsonObject jsonObj = null;
			for (JsonElement element  : items) {
				jsonObj = element.getAsJsonObject();
				com.couchbase.client.java.document.json.JsonObject val = null;
				jsonObj.addProperty("is_followed", "0");
				jsonObj.addProperty("fans_count", "0");
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (jsonObj.get("id").getAsString().equals(endId)) {
							jsonObj.addProperty("is_followed", "1");
							break;
						}
					}
				}
				for (QueryRow row : queryResult2.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (jsonObj.get("id").getAsString().equals(endId)) {
							jsonObj.addProperty("fans_count", val.getInt("count") + "");
							break;
						}
					}
				}
			}
		}

	}

	private String queryEntityInfoByNames(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("names");
		List<Map<String, Object>> ls = new ArrayList<>();
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 names = " + text + " ,userId = " + userId);
				logger.info("缺失 names = " + text + " ,userId = " + userId);
			} else {
				String bodyJson = " {  \"query\": {    \"bool\": {      \"should\": [        {          \"terms\": {            \"id\": [              $names            ]          }        },         {          \"terms\": {            \"name\": [              $names            ]          }        }      ]    }  },  \"sort\": [    {      \"rank\": {        \"order\": \"desc\"      }    }  ]}";
				StringBuffer sb = new StringBuffer();
				String[] stars = text.split(",");
				List<String> starLs = new ArrayList<>();
				for (String s : stars) {
					starLs.add(s.trim());
					sb.append("\"" + s.trim() + "\",");
				}
				sb.append("\"\"");
				bodyJson = bodyJson.replace("$names", sb.toString());
				String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/"+BasicConstants.ENTITY_INDEX_DB+"/"+BasicConstants.ENTITY_INDEX_TABLE_OTHER+"/_search",
						bodyJson, "utf-8");
				JsonParser paser = new JsonParser();
				JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
				Map<String, Object> map = null;
				if (obj != null) {
					JsonObject hits = obj.get("hits").getAsJsonObject();
					for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
						JsonObject job = starObj.getAsJsonObject();
						map = new HashMap<>();
						JsonObject source = job.get("_source").getAsJsonObject();
						String name = source.get("name").getAsString();
						String id = source.get("id").getAsString();
						if (!starLs.contains(name) && !starLs.contains(id)) {
							continue;
						}
						map.put("name", name);
						map.put("entity_id", id);
						map.put("intro", source.get("intro").getAsString());
						map.put("img", source.get("img").getAsString());
						map.put("badge", source.get("badge").getAsString());
						map.put("rank", source.get("rank").getAsDouble());
						map.put("first_class_ids", source.get("first_class_ids").getAsString());
						/*JsonElement info = source.get("info_badge");
						if (info != null && !info.isJsonNull()) {
							map.put("info_badge", info.getAsString());
						} else {
							map.put("info_badge", "");
						}*/

						ls.add(map);
					}
				}
				addFlowStatAndfollowedCount(ls, userId);
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
			}
		} catch (Exception e) {
			logger.error(QUERY_STAR_PREFIX + ":", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc(QUERY_STAR_PREFIX + " is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private int getType(String flag) {
		if (RCodeManager.userStarFollow.equals(flag) || RCodeManager.userStarSpecial.equals(flag)) {
			return TypeCode.STARS.getType();
		} else if (RCodeManager.userSubPlayer.equals(flag)) {
			return TypeCode.PLAYER.getType();
		} else if (RCodeManager.userNewsLook.equals(flag) || RCodeManager.userNewsComment.equals(flag)) {
			return TypeCode.NEWS.getType();
		}
		return -1;
	}

	private int getFllowedCount(String id) {
		String sql = "select end_id,count(1) as count from " + BasicConstants.USER_STAR_BUCKET + " where end_id in ["
				+ id + "''] and relation = '11'  group by end_id";
		QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET)
				.query(Query.simple(sql));
		logger.info("sql is " + sql);
		if (queryResult.finalSuccess()) {
			for (QueryRow row : queryResult.allRows()) {
				com.couchbase.client.java.document.json.JsonObject val = row.value();
				if (null != val) {
					return val.getInt("count");
				}
			}
		}
		return 0;
	}

	private Map<String, Object> getNewsCountState(String newsId) {
		Map<String, Object> data = new HashMap<>();
		String sql = "select cl_c ,c_c as count from " + BasicConstants.SOURCE_OTHER_BUCKET + " use keys ['" + newsId
				+ "']";
		QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET)
				.query(Query.simple(sql));
		logger.info("sql is " + sql);
		if (queryResult.finalSuccess()) {
			for (QueryRow row : queryResult.allRows()) {
				com.couchbase.client.java.document.json.JsonObject val = row.value();
				if (null != val) {
					data.put("click_count", val.getInt("cl_c"));
					data.put("comment_count", val.getInt("c_c"));
				}
			}
		}
		return data;
	}

	private String upIndex4Flag(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		// String db = request.getParam("db");
		String flag = request.getParam("flag");
		String id = request.getParam("id");
		// String userId = request.getParam("userId");
		String pubTime = request.getParam("pubTime");
		try {
			if (StringUtils.isEmpty(flag) || StringUtils.isEmpty(id)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 flag = " + flag + " ,id = " + id + " ,pubTime=" + pubTime);
				logger.info("缺失 flag = " + flag + " ,id = " + id + " ,pubTime=" + pubTime);
			} else {
				int type = getType(flag);
				if (type == -1) {
					resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
					resultObject.addReturnDesc("not support");
					return resultObject.toJson();
				}
				Map<String, Object> data = new HashMap<>();
				String json = null;
				if (TypeCode.STARS.getType() == type) {
					data.put("flows", getFllowedCount(id));
					data.put("id", id);
					data.put("type", type);
				} else if (TypeCode.PLAYER.getType() == type) {
					data.put("flows", getFllowedCount(id));
					data.put("id", id);
					data.put("type", type);
				} else if (TypeCode.NEWS.getType() == type) {
					data = getNewsCountState(id);
					data.put("id", id);
					data.put("type", type);
					data.put("pub_time", Long.parseLong(pubTime));
				}
				json = Utils.getGson().toJson(data);
				KafkaProducer.send(BasicConstants.ES_KAFKA_TOPIC, json);
				/*
				 * logger.info("url="+esUrl+" ,body="+json); re =
				 * HttpClientUtil.sendPostRequest(esUrl+"/_bulk",
				 * docs.toString(), "utf-8"); logger.info(re);
				 * if(Utils.getEsResultState(re)){
				 * resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS); }
				 * resultObject.addReturnDesc(re);
				 */
			}
		} catch (Exception e) {
			logger.error("upIndex4Flag:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("upIndex4Flag is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String queryAllNews(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		String tables = request.getParam("newsTypes" ,BasicConstants.NEWS_WEIBO_VIDEO);
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				/*
				 * List<Map<String, Object>> starsMap = getStars(text,
				 * BasicConstants.ES_QUERY_STARS, 0, 5, QUERY_TAG, true);
				 */
				// Set<String> stars = getStarsNames(starsMap);
				// Set<String> starsClasses = getStarsClass(starsMap);
				List<Map<String, Object>> news = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				news = queryAllNews(text, null, from, limit ,tables);
				logger.info(BasicConstants.MY_LOG_PREFIX+"queryAllNews"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+text+BasicConstants.MY_LOG_SPLIT+DateUtil.getCurrentDate());
				resultObject.addResultData(news);
				resultObject.putHead("count", news.size());
			}
		} catch (Exception e) {
			logger.error("queryAllNews:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryAllNews is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String queryPlayer(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				List<Map<String, Object>> starsMap = getEntityOther(text, BasicConstants.ES_QUERY_STARS, 0, 5,
						QUERY_TAG, true);
				Set<String> stars = getEntityNames(starsMap);
				// Set<String> starsClasses = getStarsClass(starsMap);
				List<Map<String, Object>> players = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				players = queryPlayers(text, stars, from, limit);
				addFlowStatAndfollowedCount(players, userId);
				resultObject.addResultData(players);
				resultObject.putHead("count", players.size());
			}
		} catch (Exception e) {
			logger.error("queryPostBar:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryPostBar is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String createDB(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String db = request.getParam("db");
		byte[] body = request.getContent();
		try {
			if (StringUtils.isEmpty(db)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 db = " + db);
				logger.info("缺失 db = " + db);
			} else {
				String url = ConfigInit.getValue("esUrl");
				String json = null;
				if (body == null) {
					String fileName = "";
					if (db.startsWith("suggest")) {
						fileName = "suggest";
					} else if (db.startsWith("nm_")) {
						fileName = "nm";
						url += "/" + db;
					}
					json = new String(Utils.readStream(Utils.getConfigFileStream(fileName)));
				}
				logger.info("url=" + url + " ,body=" + json);
				String re = HttpClientUtil.sendPostRequest(url, json, "utf-8");
				logger.info(re);
				resultObject.addReturnDesc(re);
				;
			}
		} catch (Exception e) {
			logger.error("createDB:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("createDB is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String delDB(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String db = request.getParam("db");
		try {
			if (StringUtils.isEmpty(db)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 db = " + db);
				logger.info("缺失 db = " + db);
			} else {
				String url = ConfigInit.getValue("esUrl");
				url += "/" + db;
				logger.info("url=" + url);
				CurlResponse re = HttpUrlUtil.Delete4Curl(url);
				if (re.getCode() != 200) {
					resultObject.addReturnCode(re.getCode());
				}
				String data = new String(re.getContent());
				logger.info(data);
				resultObject.addReturnDesc(data);
			}
		} catch (Exception e) {
			logger.error("delDB:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("delDB is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String upIndex(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		// String db = request.getParam("db");
		String typeStr = request.getParam("type");
		String id = request.getParam("id");
		// String userId = request.getParam("userId");
		String pubTime = request.getParam("pubTime");
		String upDateType = request.getParam("upDateType" ,"index");
		byte[] conetnt = request.getContent();
		StringBuffer docs = new StringBuffer();

		try {
			if (StringUtils.isEmpty(typeStr) || StringUtils.isEmpty(id) || StringUtils.isEmpty(pubTime)
					|| conetnt == null) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
				logger.info("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
			} else {
				String db = "";
				String table = "";
				String re = "";
				String esUrl = ConfigInit.getValue("esUrl");
				int type = Integer.parseInt(typeStr);
				Map<String, Object> doc = new HashMap<>();
				if (TypeCode.STARS.getType() == type) {
					db = BasicConstants.ENTITY_INDEX_DB;
					doc.put("_index",db);
					doc.put("_type", "other");
					doc.put("_id", id);
				} else if (TypeCode.PLAYER.getType() == type) {
					db = BasicConstants.ENTITY_INDEX_DB;
					doc.put("_index", db);
					doc.put("_type", "player");
					doc.put("_id", id);
				} else if (TypeCode.NEWS.getType() == type || TypeCode.NEWS_WEIBO.getType() == type
						|| TypeCode.NEWS_VIDEO.getType() == type || TypeCode.POST_BAR.getType() == type) {
					db = getIndex(Long.parseLong(pubTime));

					if (TypeCode.NEWS.getType() == type) {
						table = BasicConstants.NM_NEWS;
					} else if (TypeCode.NEWS_VIDEO.getType() == type) {
						table = BasicConstants.NM_WEIBO;
					} else if (TypeCode.NEWS_VIDEO.getType() == type) {
						table = BasicConstants.NM_VIDEO;
					} else if (TypeCode.POST_BAR.getType() == type) {
						table = BasicConstants.NM_POST;
					}
					doc.put("_index", db);
					doc.put("_type", table);
					doc.put("_id", id);
				} else if (TypeCode.AD.getType() == type) {
					db = BasicConstants.INDEX_OTHER_CARD;
					doc.put("_index", db);
					doc.put("_type", BasicConstants.CARD_AD);
					doc.put("_id", id);
				} else if (TypeCode.THEME.getType() == type) {
					db = BasicConstants.INDEX_OTHER_CARD;
					doc.put("_index", db);
					doc.put("_type", BasicConstants.CARD_THEME);
					doc.put("_id", id);
				} else if (TypeCode.TOPIC_MODEL.getType() == type) {
					db = BasicConstants.INDEX_OTHER_CARD;
					doc.put("_index", db);
					doc.put("_type", BasicConstants.CARD_TOPIC);
					doc.put("_id", id);
				} else if (TypeCode.ACTIVITY.getType() == type) {
					db = BasicConstants.INDEX_OTHER_CARD;
					doc.put("_index", db);
					doc.put("_type", BasicConstants.CARD_ACTIVITY);
					doc.put("_id", id);
				} else if (TypeCode.ARDENLY.getType() == type) {
					db = BasicConstants.INDEX_OTHER_CARD;
					doc.put("_index", db);
					doc.put("_type", BasicConstants.CARD_ARDENLY);
					doc.put("_id", id);
				}

				String json = new String(conetnt);
				docs.append("{\""+upDateType+"\":" + Utils.getGson().toJson(doc) + "} \n");
				json.replaceAll("\n", "").replaceAll("\r\n", "");
				if("update".equals(upDateType)){
					json = "{ \"doc\" :"+json+"}";
				}
				docs.append(json + "\n");
				logger.info("url=" + esUrl + " ,body=" + json);
				re = HttpClientUtil.sendPostRequest(esUrl + "/_bulk", docs.toString(), "utf-8");
				logger.info(re);
				HttpClientUtil.sendGetRequest(esUrl + "/" + db +"/_flush");
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String autoCompleted(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String text = request.getParam("text");
		String type = request.getParam("type", "all");
		String sizeStr = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text);
				logger.info("缺失 text = " + text);
			} else {
				int size = Integer.parseInt(sizeStr);
				text = text.trim();
				String query = " {  \"su-1\":{    \"text\":\"" + text
						+ "\",    \"completion\": {            \"field\" : \"name_su\" ,\"size\":" + sizeStr
						+ "    }  }}'";
				Set<String> data = new HashSet<>();
				if ("all".equals(type) || "star".equals(type)) {
					Set<String> stars = getSuggest(
							ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/_suggest", query,
							"su-1");
					if (text.length() == 1) {
						for (String star : stars) {
							if (star.contains(text)) {
								data.add(star);
							}
						}
					} else {
						data.addAll(stars);
					}
				}
				if ("all".equals(type) || "news".equals(type)) {
					query = " {    \"su-2\":{    \"text\":\"" + text
							+ "\",    \"completion\": {            \"field\" : \"kw_su\",            \"size\":"
							+ (size - data.size() - 1) + "     }       }}'";
					data.addAll(getSuggest(ConfigInit.getValue("esUrl") + "/suggest_*/_suggest", query, "su-2"));
				}
				if (size - data.size() > 0 && ("all".equals(type) || "player".equals(type))) {
					query = " {    \"su-3\":{    \"text\":\"" + text
							+ "\",    \"completion\": {            \"field\" : \"title_su\",            \"size\":"
							+ (size - data.size()) + "     }       }}'";
					data.addAll(getSuggest(
							ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/_suggest", query,
							"su-3"));
				}
				resultObject.addResultData(data);
				resultObject.putHead("count", data.size());
				;
			}
		} catch (Exception e) {
			logger.error("autoCompleted:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("autoCompleted is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private Set<String> getSuggest(String url, String query, String suggestName) {
		Set<String> data = new HashSet<>();
		JsonParser paser = new JsonParser();
		String re = HttpClientUtil.sendPostRequest(url, query, "utf-8");
		JsonElement jsonObj = paser.parse(re);
		if (null != jsonObj) {
			String title = "";
			JsonElement su = jsonObj.getAsJsonObject().get(suggestName);
			if (null == su || su.isJsonNull()) {
				return data;
			}
			JsonArray options = su.getAsJsonArray().get(0).getAsJsonObject().get("options").getAsJsonArray();
			for (JsonElement el : options) {
				title = el.getAsJsonObject().get("text").getAsString();
				if (title.length() > 20) {
					title = title.substring(0, 20);
					int len = title.lastIndexOf(" ");
					if (title.length() - len < 3)
						title = title.substring(0, title.lastIndexOf(" "));
				}
				data.add(title);
			}
		}
		return data;
	}

	private String delIndex(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		// String db = request.getParam("db");
		String typeStr = request.getParam("type");
		String id = request.getParam("id");
		// String userId = request.getParam("userId");
		String pubTime = request.getParam("pubTime");
		try {
			if (StringUtils.isEmpty(typeStr) || StringUtils.isEmpty(id) || StringUtils.isEmpty(pubTime)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
				logger.info("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
			} else {
				String db = "";
				String table = "";
				String re = "";
				int type = Integer.parseInt(typeStr);
				String url = ConfigInit.getValue("esUrl");
				if (TypeCode.STARS.getType() == type) {
					db = BasicConstants.ENTITY_INDEX_DB;
					url += "/" + db + "/other/" + id;
				} else if (TypeCode.PLAYER.getType() == type) {
					db = BasicConstants.INDEX_PLAYERS;
					url += "/" + db + "/player/" + id;
				} else if (TypeCode.NEWS.getType() == type || TypeCode.NEWS_WEIBO.getType() == type
						|| TypeCode.NEWS_VIDEO.getType() == type || TypeCode.POST_BAR.getType() == type) {
					db = getIndex(Long.parseLong(pubTime));
					if (TypeCode.NEWS.getType() == type) {
						table = BasicConstants.NM_NEWS;
					} else if (TypeCode.NEWS_WEIBO.getType() == type) {
						table = BasicConstants.NM_WEIBO;
					} else if (TypeCode.NEWS_VIDEO.getType() == type) {
						table = BasicConstants.NM_VIDEO;
					} else if (TypeCode.POST_BAR.getType() == type) {
						table = BasicConstants.NM_POST;
					}
					url += "/" + db + "/" + table + "/" + id;
					// re = HttpUrlUtil.Delete(url);
				} else if (TypeCode.AD.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_AD + "/" + id;
				} else if (TypeCode.THEME.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_THEME + "/" + id;
				} else if (TypeCode.TOPIC_MODEL.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_TOPIC + "/" + id;
				} else if (TypeCode.ACTIVITY.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_ACTIVITY + "/" + id;
				} else if (TypeCode.ARDENLY.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_ARDENLY + "/" + id;
				}
				re = HttpUrlUtil.Delete(url);
				logger.info("es index del :" + re);
				resultObject.addReturnDesc(re);
				;
			}
		} catch (Exception e) {
			logger.error("delIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("delIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public String getIndex(long pubTime) {
		String currentMonth = DateUtil.format(new Date(), "yyMM");
		String beforeMonth1 = DateUtil.format(new Date(DateUtil.getBeforeMonthTime(1)), "yyMM");
		String beforeMonth2 = DateUtil.format(new Date(DateUtil.getBeforeMonthTime(2)), "yyMM");
		String pubMonth = DateUtil.format(new Date(pubTime), "yyMM");
		String index = BasicConstants.INDEX_PREFIX;
		if (currentMonth.equals(pubMonth)) {
			index += currentMonth;
		} else if (beforeMonth1.equals(pubMonth)) {
			index += beforeMonth1;
		} else if (beforeMonth2.equals(pubMonth)) {
			index += beforeMonth2;
		} else {
			int currentMonthInt = Integer.parseInt(DateUtil.format(new Date(pubTime), "MM"));
			if (currentMonthInt % 2 == 1) {
				index += "uneven";
			} else {
				index += "even";
			}
		}
		return index;
	}

	private String queryCollections(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				// Map<String,Object> data = new HashMap<>();
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				List<Map<String, Object>> starsMap = getEntityOther(text, BasicConstants.ES_QUERY_STARS, 0, 5,
						QUERY_TAG, true);
				Set<String> stars = getEntityNames(starsMap);
				Set<String> starsClasses = getStarsClass(starsMap);
				List<Map<String, Object>> collections = null;
				collections = subList(queryCollection(text, stars, starsClasses, userId, from, limit));
				// data.put("collections", collections);
				resultObject.addResultData(collections);
				resultObject.putHead("count", collections.size());
			}
		} catch (Exception e) {
			logger.error("queryCollections:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryCollections is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private List<Map<String, Object>> getEntitysByNames(Set<String> names, int limit) {
		if (names == null) {
			return new ArrayList<>();
		}
		String ids = set2String(names, ",", "'", limit);
		if (ids.length() == 0) {
			return new ArrayList<>();
		}
		String sql = "SELECT entity.badge,GROUP_CONCAT(DISTINCT entity_class.class_first) as first_class_ids, entity.id,img,entity.intro,entity.name,entity.nicknames,entity.rank FROM " + BasicConstants.MYSQL_ENTITY_TABLE+" LEFT JOIN entity_class on entity.id = entity_class.entity_id  "
				+ " WHERE entity.status=1 and entity.`name` in (" + set2String(names, ",", "'", limit) + ") GROUP BY entity.`name`  "
				+ " limit " + limit;
		logger.info("mysql is " + sql);
		Map<String, Map<String, Object>> data = connectionUtil2.query(sql,
				new ResultSetExtractor<Map<String, Map<String, Object>>>() {
					@Override
					public Map<String, Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						Map<String, Map<String, Object>> tmp = new HashMap<>();
						while (resultSet.next()) {
							Map<String, Object> m = new HashMap<String, Object>();
							m.put("img", resultSet.getString("img"));
							// m.put("info_badge",
							// resultSet.getString("info_badge"));
							m.put("badge", resultSet.getString("badge"));
							m.put("name", resultSet.getString("name"));
							m.put("intro", resultSet.getString("intro"));
							m.put("entity_id", resultSet.getString("id"));
							m.put("first_class_ids", resultSet.getString("first_class_ids"));
							m.put("rank", resultSet.getDouble("rank"));
							tmp.put(resultSet.getString("name"), m);
						}
						return tmp;
					}
				});
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> m = null;
		for (String nm : names) {
			m = data.get(nm);
			if (null != m) {
				ls.add(m);
			}
		}
		return ls;
	}

	private String queryStars(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String firstClassId = request.getParam("firstClassId");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "4");
		// String noCache = request.getParam("noCache", "false");
		Jedis jedis = null;
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId) || StringUtils.isEmpty(firstClassId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				jedis = RedisConnectionPool.getCluster();
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				// if(redisData.size()==0){
				Set<String> redisData = new LinkedHashSet<>();
				List<JsonObject> esls = queryEntitysInFirstClass(text, firstClassId, from, limit);
				for (JsonObject star : esls) {
					redisData.add(star.get("name").toString());
				}
//				redisData = getEntityNames(esls);
				/*
				 * if("true".equals(noCache)){ List<Map<String, Object>> stars =
				 * getEntityOther(text, BasicConstants.ES_QUERY_STARS2, 0,
				 * 3,QUERY_TAG , true); redisData = getEntityNames(stars);
				 * 
				 * }else{ redisData = jedis.zrevrange(QUERY_STAR_PREFIX + ":" +
				 * userId, from, from + limit -1); }
				 */

				// }
				List<Map<String, Object>> returnls = getEntitysByNames(redisData, redisData.size());
				addFlowStatAndfollowedCount(returnls, userId);
				List<Map<String, Object>> ls = null;
				if (returnls.size() == 1) {
					ls = fullStarInfo(returnls, redisData, userId);
				} else {
					ls = new ArrayList<>();
					Map<String, Object> map = null;
					for (Map<String, Object> temp : returnls) {
						map = new HashMap<>();
						map.put("result", temp);
						ls.add(map);
					}
				}
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				/*
				 * if(redisData.size()>0){ jedis.del(QUERY_STAR_PREFIX + ":" +
				 * userId); logger.info(
				 * "clear es query cache for stars ,and redis size is "
				 * +redisData); }
				 */
				Map<String, String> kw = new HashMap<>();
				kw.put("time", DateUtil.getCurrentDate());
				kw.put("query", text);
				kw.put("user", userId);
				kw.put("size", ls.size() + "");
				kw.put("type", "queryStars");
				kw.put("timing", (System.currentTimeMillis() - start) + "");
				String kwJson = new Gson().toJson(kw);
				logger.info(kwJson);
				KafkaProducer.send("kw", kwJson);
			}
		} catch (Exception e) {
			logger.error(QUERY_STAR_PREFIX + ":", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc(QUERY_STAR_PREFIX + " is error");
		} finally {
			if (null != jedis) {
				jedis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/*
	 * private String queryStars(Request request) { long start =
	 * System.currentTimeMillis(); ResultMsg resultObject = new ResultMsg();
	 * String userId = request.getParam("userId"); String text =
	 * request.getParam("text"); String page = request.getParam("page", "1");
	 * String size = request.getParam("size", "4"); String isCache =
	 * request.getParam("isCache", "false"); Jedis jedis = null; try { if
	 * (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
	 * resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
	 * resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
	 * logger.info("缺失 text = " + text + " ,userId = " + userId); } else { jedis
	 * = RedisConnectionPool.getCluster(); int limit = Integer.parseInt(size);
	 * int from = (Integer.parseInt(page) - 1) * limit; //
	 * if(redisData.size()==0){ Set<String> esData = new LinkedHashSet<>();
	 * if(page.equals("1")){ List<Map<String, Object>> stars = getStars(text,
	 * BasicConstants.ES_QUERY_STARS2, 0, 3,QUERY_TAG , true); esData =
	 * getStarsNames(stars); if(esData.contains(text.trim())){ esData.clear();
	 * esData.add(text.trim()); } } Set<String> redisData =
	 * jedis.zrevrange(QUERY_STAR_PREFIX + ":" + userId, from, from + limit -1);
	 * boolean b = false; Set<String> temRedis = Utils.subSet(redisData, 3);
	 * for(String esStar:esData){ if(temRedis.contains(esStar)){ b= true; break;
	 * } } if(!b && page.equals("1") && esData.size()==1){ redisData.clear();
	 * jedis.del(QUERY_STAR_PREFIX + ":" + userId); logger.info(
	 * "clear es query cache for stars"); }
	 * 
	 * esData.addAll(redisData); redisData = esData; // } List<Map<String,
	 * Object>> returnls = getStarsByNames(redisData, redisData.size());
	 * addFlowStatAndFlows(returnls, userId, null); List<Map<String, Object>> ls
	 * = null; if (returnls.size() == 1) { ls = fullStarInfo(returnls,
	 * redisData, userId); } else { ls = new ArrayList<>(); Map<String, Object>
	 * map = null; for (Map<String, Object> temp : returnls) { map = new
	 * HashMap<>(); map.put("result", temp); ls.add(map); } }
	 * resultObject.addResultData(ls); resultObject.putHead("count", ls.size());
	 * if(redisData.size()>0){ jedis.del(QUERY_STAR_PREFIX + ":" + userId);
	 * logger.info("clear es query cache for stars ,and redis size is "
	 * +redisData); } Map<String, String> kw = new HashMap<>(); kw.put("time",
	 * DateUtil.getCurrentDate()); kw.put("query", text); kw.put("user",
	 * userId); kw.put("size", ls.size() + ""); kw.put("type", "queryStars");
	 * kw.put("timing", (System.currentTimeMillis() - start) + ""); String
	 * kwJson = new Gson().toJson(kw); logger.info(kwJson);
	 * KafkaProducer.send("kw", kwJson); } } catch (Exception e) {
	 * logger.error(QUERY_STAR_PREFIX + ":", e);
	 * resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
	 * resultObject.addReturnDesc(QUERY_STAR_PREFIX + " is error"); } finally {
	 * if (null != jedis) { jedis.close(); } }
	 * resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() -
	 * start); logger.info((System.currentTimeMillis() - start) + " timing");
	 * return resultObject.toJson(); }
	 */

	private List<Map<String, Object>> getRelationInfoWithStar(String name, int limit) {
		String mysql = "SELECT 	star.id, 	star.rank, star.intro,	star.name,star.badge, 	star.img  , 	s.type AS relation ,GROUP_CONCAT(DISTINCT entity_class.class_first) as first_class_ids  FROM "+BasicConstants.MYSQL_ENTITY_TABLE+"  as star 	LEFT JOIN "+BasicConstants.MYSQL_ENTITY_RELAS_TABLE+" as s ON s.end_id=star.id	left join "+BasicConstants.MYSQL_ENTITY_CODE_TABLE+" as star_code on star_code.type=s.type LEFT JOIN "+BasicConstants.MYSQL_ENTITY_CLASS_TABLE+" on star.id = entity_class.entity_id  	WHERE s.start_name='"
				+ ""+name+"' and star.status=1 GROUP BY star.id order by star_code.code desc limit " + limit;
		logger.info("mysql is " + mysql);
		List<Map<String, Object>> data = connectionUtil2.query(mysql,
				new ResultSetExtractor<List<Map<String, Object>>>() {
					@Override
					public List<Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						List<Map<String, Object>> tmp = new ArrayList<>();
						while (resultSet.next()) {
							Map<String, Object> m = new HashMap<String, Object>();
							m.put("img", resultSet.getString("img"));
//							m.put("info_badge", resultSet.getString("info_badge"));
							m.put("badge", resultSet.getString("badge"));
							m.put("name", resultSet.getString("name"));
							m.put("intro", resultSet.getString("intro"));
							m.put("entity_id", resultSet.getString("id"));
							m.put("rank", resultSet.getDouble("rank"));
							m.put("first_class_ids", resultSet.getString("first_class_ids"));
							m.put("relation", resultSet.getString("relation"));
							// m.put("flows",
							// resultSet.getString("flows"));
							tmp.add(m);
						}
						return tmp;
					}
				});
		return data;
	}

	private List<Map<String, Object>> fullStarInfo(List<Map<String, Object>> starsMap, Set<String> stars1,
			String userId) {
		Map<String, Object> re = null;
		List<Map<String, Object>> ls = new ArrayList<>();
		Set<String> readTmp = new HashSet<>();
		// String key = "";
		try {
			String mysql = " SELECT 	star.id, 	star.rank, star.intro,	star.name,star.badge, 	star.img,  	s.type AS relation   FROM "+BasicConstants.MYSQL_ENTITY_TABLE+" as star 	LEFT JOIN "+BasicConstants.MYSQL_ENTITY_RELAS_TABLE+" as s ON s.end_id=star.id	left join "+BasicConstants.MYSQL_ENTITY_CODE_TABLE+" as star_code on star_code.type=s.type	  WHERE s.start_name=\"$text\" and star.status=1 GROUP BY star.id order by star_code.code desc limit 3";
			for (Map<String, Object> map : starsMap) {
				String sql = mysql.replace("$text", map.get("name").toString());
				readTmp.add(map.get("entity_id").toString());
				// key = userId+"_"+map.get("star_id");
				// map.put("is_flow", isTake(key)+"");
				logger.info("mysql is " + sql);
				re = new HashMap<String, Object>();
				List<Map<String, Object>> data = connectionUtil2.query(sql,
						new ResultSetExtractor<List<Map<String, Object>>>() {
							@Override
							public List<Map<String, Object>> extractData(ResultSet resultSet)
									throws SQLException, DataAccessException {
								List<Map<String, Object>> tmp = new ArrayList<>();
								while (resultSet.next()) {
									Map<String, Object> m = new HashMap<String, Object>();
									m.put("img", resultSet.getString("img"));
									// m.put("info_badge",
									// resultSet.getString("info_badge"));
									m.put("badge", resultSet.getString("badge"));
									m.put("name", resultSet.getString("name"));
									m.put("intro", resultSet.getString("intro"));
									m.put("entity_id", resultSet.getString("id"));
									m.put("rank", resultSet.getDouble("rank"));
									m.put("relation", resultSet.getString("relation"));
									// m.put("flows",
									// resultSet.getString("flows"));
									tmp.add(m);
								}
								return tmp;
							}
						});
				// List<Map<String, String>> rmTemp = new ArrayList<>();
				int count = 0;
				Map<String, Object> entityMap = null;
				Map<String, Object> entityTemp = new HashMap<>();
				for (int i = 0; i < data.size(); i++) {
					entityMap = data.get(i);
					Object entityId = entityMap.get("entity_id");
					if (entityId != null && !readTmp.contains(entityId)) {
						// starMap.put("is_flow", isTake(userId+"_"+starId)+"");
						readTmp.add(entityId.toString());
						count++;
					} else {
						entityTemp.putAll(entityMap);
					}
				}
				data.remove(entityTemp); // 移除 result 中重复的部分
				re.put("result", map);
				if (count == 0) {
					data.clear();
				}
				addFlowStatAndfollowedCount(data, userId);
				re.put("suggest", data);
				ls.add(re);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("", e);
		}
		return ls;
	}
	private Object fullStarInfo4Document(List<JsonObject> entitys ,
			String userId) {
		Map<String, Object> re = null;
		List<Map<String, Object>> ls = new ArrayList<>();
		Set<String> readTmp = new HashSet<>();
		// String key = "";
		try {
			String mysql = "SELECT 	star.id, 	star.rank, star.intro,	star.name,star.badge, 	star.img,  	s.type AS relation,GROUP_CONCAT(DISTINCT entity_class.class_first) as first_class_ids   FROM "+BasicConstants.MYSQL_ENTITY_TABLE+" as star 	LEFT JOIN "+BasicConstants.MYSQL_ENTITY_RELAS_TABLE+" as s ON s.end_id=star.id	left join "+BasicConstants.MYSQL_ENTITY_CODE_TABLE+"  as star_code on star_code.type=s.type	 LEFT JOIN entity_class on star.id = entity_class.entity_id  WHERE s.start_name=\"$text\" and star.status=1 GROUP BY star.id order by star_code.code desc limit 3";
			for (JsonObject jsonObj : entitys) {
				String sql = mysql.replace("$text", jsonObj.get("name").getAsString());
				readTmp.add(jsonObj.get("id").getAsString());
				// key = userId+"_"+map.get("star_id");
				// map.put("is_flow", isTake(key)+"");
				logger.info("mysql is " + sql);
				re = new HashMap<String, Object>();
				List<Map<String, Object>> data = connectionUtil2.query(sql,
						new ResultSetExtractor<List<Map<String, Object>>>() {
							@Override
							public List<Map<String, Object>> extractData(ResultSet resultSet)
									throws SQLException, DataAccessException {
								List<Map<String, Object>> tmp = new ArrayList<>();
								while (resultSet.next()) {
									Map<String, Object> m = new HashMap<String, Object>();
									m.put("img", resultSet.getString("img"));
									// m.put("info_badge",
									// resultSet.getString("info_badge"));
									m.put("badge", resultSet.getString("badge"));
									m.put("name", resultSet.getString("name"));
									m.put("intro", resultSet.getString("intro"));
									m.put("id", resultSet.getString("id"));
									m.put("entity_id", resultSet.getString("id"));
									m.put("rank", resultSet.getDouble("rank"));
									m.put("relation", resultSet.getString("relation"));
									// m.put("flows",
									// resultSet.getString("flows"));
									tmp.add(m);
								}
								return tmp;
							}
						});
				if(data.size()==0){
					return entitys;
				}
				re.put("result", jsonObj);
				addFlowStatAndfollowedCount(data, userId);
				re.put("suggest", data);
				ls.add(re);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("", e);
		}
		return ls;
	}

	private void starNames2Redis(Set<String> starNames, String userId) {
		if (starNames == null || starNames.size() == 0) {
			return;
		}
		Jedis jedis = null;
		try {
			jedis = RedisConnectionPool.getCluster();
			Map<String, Double> rels = new HashMap<>();
			Iterator<String> it = starNames.iterator();
			int i = starNames.size();
			while (it.hasNext()) {
				String name = it.next().toString();
				rels.put(name, Double.parseDouble(i + ""));
				i--;
			}
			jedis.del(QUERY_STAR_PREFIX + ":" + userId);
			jedis.zadd(QUERY_STAR_PREFIX + ":" + userId, rels);
			jedis.expire(QUERY_STAR_PREFIX + ":" + userId, 1000 * 120);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("", e);
		} finally {
			if (null != jedis) {
				jedis.close();
			}
		}
	}

	private void starMap2Redis(List<Map<String, Object>> starsMap, Set<String> stars, String userId) {
		int topCount = BasicConstants.STAR_MAP_TOP_NUM;
		if (starsMap.size() <= topCount) {
			topCount = starsMap.size() - 1;
		}
		List<Map<String, Object>> top5 = starsMap.subList(0, topCount);
		Map<String, Object> re = new HashMap<String, Object>();
		Set<String> readTmp = new HashSet<>();
		Map<String, Double> rels = new HashMap<>();
		String key = "";
		Jedis jedis = null;
		try {
			jedis = RedisConnectionPool.getCluster();
			String mysql = "SELECT 	star.id, 	star.rank, star.intro,	star.name,star.badge, 	star.img,	s.start_id AS stid, 	s.end_id AS enid , 	s.type AS relation,	star_code.type as startype,	star_code.code as starcode ,      GROUP_CONCAT(DISTINCT c.name) AS info_badge	FROM star 	LEFT JOIN star_relas as s ON s.end_id=star.id	left join star_code on star_code.type=s.type	   	LEFT JOIN   star_classify AS sc ON star.id=sc.id       LEFT JOIN classify  AS c ON sc.classify_id=c.id 	WHERE s.start_name=\"$text\" and star.status=1 GROUP BY star.id order by star_code.code desc limit 3";
			for (Map<String, Object> map : starsMap) {
				String sql = mysql.replace("$text", map.get("name").toString());
				readTmp.add(map.get("star_id").toString());
				key = userId + "_" + map.get("star_id");
				map.put("is_flow", isTake(key) + "");
				logger.info("mysql is " + sql);
				List<Map<String, String>> data = connectionUtil2.query(sql,
						new ResultSetExtractor<List<Map<String, String>>>() {
							@Override
							public List<Map<String, String>> extractData(ResultSet resultSet)
									throws SQLException, DataAccessException {
								List<Map<String, String>> tmp = new ArrayList<>();
								while (resultSet.next()) {
									Map<String, String> m = new HashMap<String, String>();
									m.put("img", resultSet.getString("img"));
									m.put("info_badge", resultSet.getString("info_badge"));
									m.put("badge", resultSet.getString("badge"));
									m.put("name", resultSet.getString("name"));
									m.put("intro", resultSet.getString("intro"));
									m.put("relation", resultSet.getString("relation"));
									m.put("star_id", resultSet.getString("id"));
									m.put("rank", resultSet.getString("rank"));
									// m.put("flows",
									// resultSet.getString("flows"));
									tmp.add(m);
								}
								return tmp;
							}
						});
				// List<Map<String, String>> rmTemp = new ArrayList<>();
				double db = 0;
				int count = 0;
				Map<String, String> starMap = null;
				Map<String, String> starTemp = new HashMap<>();

				for (int i = 0; i < data.size(); i++) {
					starMap = data.get(i);

					// String starName = starMap.get("name");
					String starId = starMap.get("star_id");
					if (!readTmp.contains(starId)) {
						starMap.put("is_flow", isTake(userId + "_" + starId) + "");
						db += Double.parseDouble(data.get(i).get("rank"));
						readTmp.add(starId);
						count++;
					} else {
						starTemp.putAll(starMap);
					}
				}
				if (count != 0) {
					db = db / count;
				}
				data.remove(starTemp);
				re.put("result", map);
				if (count == 0) {
					data.clear();
				}
				re.put("suggest", data);
				for (Map<String, Object> k : top5) {
					if (k.get("name").toString().equals(map.get("name").toString())) {
						db = db * 5;
						break;
					}
				}
				if (stars.contains(map.get("name").toString())) {
					db = db * 2;
				}
				rels.put(Utils.getGson().toJson(re), db);
			}
			jedis.del(QUERY_STAR_PREFIX + ":" + userId);
			jedis.zadd(QUERY_STAR_PREFIX + ":" + userId, rels);
			jedis.expire(QUERY_STAR_PREFIX + ":" + userId, 3600 * 48);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("", e);
		} finally {
			if (null != jedis) {
				jedis.close();
			}
		}
	}

	private boolean isTake(String key) {
		String bk = BasicConstants.USER_STAR_BUCKET;
		JsonDocument doc = CBaseConnectionPool.getBucket(bk).get(key);
		if (null != doc) {
			String relation = doc.content().getString("relation");
			if (relation != null && (relation.equals("11") || relation.equals("21"))) {
				return true;
			}
		}
		return false;
	}

	private String queryPostBar(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				// Map<String,Object> data = new HashMap<>();
				/*
				 * List<Map<String, Object>> starsMap = getStars(text,
				 * BasicConstants.ES_QUERY_STARS, 0, 5, QUERY_TAG, true);
				 */
				// Set<String> stars = getStarsNames(starsMap);
				// Set<String> starsClasses = getStarsClass(starsMap);
				List<Map<String, Object>> post_bar = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				post_bar = queryPostBar(text, null, null, from, limit);
				addUserInfo2Bar(post_bar);
				// data.put("post_bar", post_bar);
				resultObject.addResultData(post_bar);
				resultObject.putHead("count", post_bar.size());
				Map<String, String> kw = new HashMap<>();
				kw.put("query", text);
				kw.put("user", userId);
				kw.put("size", post_bar.size() + "");
				kw.put("type", "queryPostBar");
				kw.put("timing", (System.currentTimeMillis() - start) + "");
				logger.info(Utils.getGson().toJson(kw));
				logger.info(BasicConstants.MY_LOG_PREFIX+"queryPostBar"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+text+BasicConstants.MY_LOG_SPLIT+DateUtil.getCurrentDate());
				sendKafka(kw);
			}
		} catch (Exception e) {
			logger.error("queryPostBar:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryPostBar is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String queryNewsVideo(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				// Map<String,Object> data = new HashMap<>();
				List<Map<String, Object>> starsMap = getEntityOther(text, BasicConstants.ES_QUERY_STARS, 0, 5,
						QUERY_TAG, true);
				Set<String> stars = getEntityNames(starsMap);
				List<Map<String, Object>> news_video = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				news_video = queryNewsVideo(text, stars, from, limit, null);
				// data.put("news_video", news_video);
				resultObject.addResultData(news_video);
				resultObject.putHead("count", news_video.size());
			}
		} catch (Exception e) {
			logger.error("queryNewsVideo:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryNewsVideo is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String queryNewsWeiBo(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				// Map<String,Object> data = new HashMap<>();
				List<Map<String, Object>> starsMap = getEntityOther(text, BasicConstants.ES_QUERY_STARS, 0, 5,
						QUERY_TAG, true);
				Set<String> stars = getEntityNames(starsMap);
				List<Map<String, Object>> news_weibo = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				news_weibo = queryNewsWeiBo(text, stars, from, limit, null);
				resultObject.addResultData(news_weibo);
				resultObject.putHead("count", news_weibo.size());
			}
		} catch (Exception e) {
			logger.error("queryNewsWeiBo:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryNewsWeiBo is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	/**
	 * 3.0版
	 * 
	 * @param request
	 * @return
	 */
	private String mainSearch(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		
		Jedis redis = null;
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				Map<String, Object> data = new HashMap<>();
				List<Map<String, Object>> entityTop6 = getEntityOther(text, BasicConstants.ES_QUERY_STARS, 0, 5,
						"", true);
				// System.out.println(starsTop3);
				Set<String> entityNamesOnQuery = getEntityNames(entityTop6);
				Set<String> entityNames = new LinkedHashSet<>();
				// Set<String> entityClasses = getStarsClass(entityTop6);
				if (page.equals("1")) {
					redis = RedisConnectionPool.getCluster();
					Set<String> disabledClassIds = redis.smembers(BasicConstants.DISABLED_CLASS_KEY);
					
					entityNames.addAll(entityNamesOnQuery);
					List<Map<String, Object>> post_bar = null;
					// List<Map<String, Object>> extendStars = extendStars(text,
					// starNamesOnQuery, starsClasses, 0,
					// SEARCH_INFO_SIZE_DEFAULT);
					List<Map<String, Object>> extendStars = null;
					for (String star : entityNames) {
						extendStars = getRelationInfoWithStar(star, 1);
						break;
					}
					if (extendStars != null && extendStars.size() == 1) {
						entityNames.add(extendStars.get(0).get("name").toString());
					}
					List<Map<String, Object>> players = queryPlayers(text, entityNamesOnQuery, 0,
							SEARCH_PLAYER_SIZE_DEFAULT);
					entityNames.addAll(findStarNames(players));
					if(disabledClassIds == null || !disabledClassIds.contains("2")){
						addFlowStatAndfollowedCount(players, userId);
						data.put("player", players);
					}
					// data.put("player", players.subList(0, players.size() > 3
					// ? 3 : players.size()));
					List<Map<String, Object>> news = queryAllNews(text, null, 0, limit ,BasicConstants.NEWS_AND_WEIBO);
					data.put("info", news);
//					List<Map<String, Object>> videos = queryAllNews(text, null, 0, 2 ,BasicConstants.NM_VIDEO);
					data.put("video", queryAllNews(text, null, 0, 2 ,BasicConstants.NM_VIDEO));
					entityNames.addAll(findStarNames(news));
					post_bar = queryPostBar(text, null, null, 0, 2);
					entityNames.addAll(findStarNames(post_bar));
					addUserInfo2Bar(post_bar); // 添加发帖人信息
					if (post_bar.size() >= 2) {
						data.put("post_bar", post_bar.subList(0, 2));
					} else {
						data.put("post_bar", post_bar);
					}
					entityNames.removeAll(entityNamesOnQuery);
					if (entityTop6.size() < BasicConstants.STAR_MAP_TOP_NUM && entityNames.size() > 0) {
						entityTop6.addAll(
								getEntitysByNames(entityNames, BasicConstants.STAR_MAP_TOP_NUM - entityTop6.size()));
					}
					entityNamesOnQuery.addAll(entityNames);
					addFlowStatAndfollowedCount(entityTop6, userId); // 给明星添加关注关系
					List<Map<String, Object>> tmp = new ArrayList<>();
					for(Map<String, Object> map:entityTop6){
						if(null != disabledClassIds && disabledClassIds.contains(map.get("first_class_ids").toString())){
							tmp.add(map);
						}
					}
					entityTop6.removeAll(tmp);
					
					if(entityTop6.size() > 2){
						data.put("entitys", entityTop6);
					}
					if (entityTop6.size() == 0) {
						logger.info("not found stars for [" + text + "]");
					} else {
						starNames2Redis(entityNamesOnQuery, userId);// 将明星name保存到redis
					}
					Map<String, String> kw = new HashMap<>();
					kw.put("time", DateUtil.getCurrentDate());
					kw.put("query", text);
					kw.put("user", userId);
					kw.put("size", data.size() + "");
					kw.put("type", "mainSearch");
					kw.put("timing", (System.currentTimeMillis() - start) + "");
					String kwJson = new Gson().toJson(kw);
					logger.info(BasicConstants.MY_LOG_PREFIX+"mainSearch"+BasicConstants.MY_LOG_SPLIT+userId+BasicConstants.MY_LOG_SPLIT+text+BasicConstants.MY_LOG_SPLIT+DateUtil.getCurrentDate());
					logger.info(kwJson);
					KafkaProducer.send("kw", kwJson);
					resultObject.addResultData(data);
					resultObject.putHead("count", data.size());
				} else {
					List<Map<String, Object>> news = queryAllNews(text, entityNamesOnQuery, from, limit ,BasicConstants.NEWS_AND_WEIBO);
					resultObject.addResultData(news);
					resultObject.putHead("count", news.size());
				}

			}
		} catch (Exception e) {
			logger.error("search:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("search is error");
		}finally{
			if(redis != null){
				redis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/*
	 * private String mainSearch(Request request) { long start =
	 * System.currentTimeMillis(); ResultMsg resultObject = new ResultMsg();
	 * String userId = request.getParam("userId"); String text =
	 * request.getParam("text"); String page = request.getParam("page", "1");
	 * String size = request.getParam("size", "10"); Jedis jedis = null; try {
	 * jedis = RedisConnectionPool.getCluster(); if (StringUtils.isEmpty(text)
	 * || StringUtils.isEmpty(userId)) {
	 * resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
	 * resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
	 * logger.info("缺失 text = " + text + " ,userId = " + userId); } else { int
	 * limit = Integer.parseInt(size); int from = (Integer.parseInt(page) - 1) *
	 * limit; Map<String, Object> data = new HashMap<>(); List<Map<String,
	 * Object>> starsTop3 = getStars(text, BasicConstants.ES_QUERY_STARS, 0, 5,
	 * QUERY_TAG, true); // System.out.println(starsTop3); Set<String>
	 * starNamesOnQuery = getStarsNames(starsTop3); Set<String> starNames = new
	 * LinkedHashSet<>(); Set<String> starsClasses = getStarsClass(starsTop3);
	 * if (page.equals("1")) { starNames.addAll(starNamesOnQuery);
	 * List<Map<String, Object>> post_bar = null; // List<Map<String, Object>>
	 * extendStars = extendStars(text, starNamesOnQuery, starsClasses, 0,
	 * SEARCH_INFO_SIZE_DEFAULT); List<Map<String, Object>> extendStars = null;
	 * for(String star:starNames){ extendStars =
	 * getRelationInfoWithStar(star,1); break; } if(extendStars!=null &&
	 * extendStars.size()==1){
	 * starNames.add(extendStars.get(0).get("name").toString()); }
	 * List<Map<String, Object>> players = queryPlayers(text, starNamesOnQuery,
	 * starsClasses, 0,SEARCH_PLAYER_SIZE_DEFAULT);
	 * starNames.addAll(findStarNames(players)); addFlowStatAndFlows(players,
	 * userId, "1"); data.put("player", players.subList(0, players.size() > 3 ?
	 * 3 : players.size())); List<Map<String, Object>> news = queryAllNews(text,
	 * null, null, 0, limit); data.put("info", news);
	 * starNames.addAll(findStarNames(news)); post_bar = queryPostBar(text,
	 * null, null, 0, 2);
	 * 
	 * addUserInfo2Bar(post_bar); // 添加发帖人信息 if (post_bar.size() >= 2) {
	 * data.put("post_bar", post_bar.subList(0, 2)); } else {
	 * data.put("post_bar", post_bar); } starNames.removeAll(starNamesOnQuery);
	 * if (starsTop3.size() < BasicConstants.STAR_MAP_TOP_NUM &&
	 * starNames.size() > 0) { starsTop3 .addAll(getStarsByNames(starNames,
	 * BasicConstants.STAR_MAP_TOP_NUM - starsTop3.size())); }
	 * starNamesOnQuery.addAll(starNames); addFlowStatAndFlows(starsTop3,
	 * userId, null); // 给明星添加关注关系 data.put("stars", starsTop3);
	 * if(starsTop3.size()==0){ jedis.del(QUERY_STAR_PREFIX + ":" + userId);
	 * logger.info("not found stars for ["+text+"]"); }else{
	 * starNames2Redis(starNamesOnQuery, userId);// 将明星name保存到redis }
	 * Map<String, String> kw = new HashMap<>(); kw.put("time",
	 * DateUtil.getCurrentDate()); kw.put("query", text); kw.put("user",
	 * userId); kw.put("size", data.size() + ""); kw.put("type", "mainSearch");
	 * kw.put("timing", (System.currentTimeMillis() - start) + ""); String
	 * kwJson = new Gson().toJson(kw); logger.info(kwJson);
	 * KafkaProducer.send("kw", kwJson); resultObject.addResultData(data);
	 * resultObject.putHead("count", data.size()); } else { List<Map<String,
	 * Object>> news = queryAllNews(text, starNamesOnQuery, starsClasses, from,
	 * limit); resultObject.addResultData(news); resultObject.putHead("count",
	 * news.size()); }
	 * 
	 * } } catch (Exception e) { logger.error("search:", e);
	 * resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
	 * resultObject.addReturnDesc("search is error"); }finally{ if(jedis!=null){
	 * jedis.close(); } } resultObject.putHead(BasicConstants.TIMING,
	 * System.currentTimeMillis() - start);
	 * logger.info((System.currentTimeMillis() - start) + " timing"); return
	 * resultObject.toJson(); }
	 */

	private void sendKafka(Map<String, String> kw) {
		kw.put("time", DateUtil.getCurrentDate());
		String kwJson = new Gson().toJson(kw);
		logger.info(kwJson);
		KafkaProducer.send("kw", kwJson);
	}

	public List<Map<String, Object>> extendStars(String text, Set<String> stars, Set<String> starsClasses, int from,
			int size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("extendStars");
		String filter = getStarFilter2(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$class", set2String(starsClasses));
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(
				ConfigInit.getValue("esUrl") + "/nm_*/news,news_video,news_weibo/_search", bodyJson, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return ls;
			}
			JsonObject hits = hitsEle.getAsJsonObject();
			/*
			 * String type = ""; JsonElement orgUrl = null; JsonElement
			 * commentCount = null;
			 */
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
				JsonObject job = starObj.getAsJsonObject();
				map = new HashMap<>();
				if (job.get("_score").getAsDouble() < minScore) {
					break;
				} else {
					// type = job.get("_type").getAsString();
					JsonObject source = job.get("_source").getAsJsonObject();
					/*
					 * orgUrl = source.get("org_url"); if(null != orgUrl){
					 * map.put("org_url", orgUrl.getAsString()); } commentCount
					 * = source.get("comment_count"); if(null != commentCount){
					 * map.put("comment_count", commentCount.getAsInt()); }
					 */
					/*
					 * map.put("type", type); map.put("title",
					 * source.get("title").getAsString()); map.put("id",
					 * source.get("id").getAsString()); map.put("pub_time",
					 * source.get("pub_time").getAsLong());
					 * map.put("news_source",
					 * source.get("news_source").getAsString());
					 * map.put("list_images",
					 * source.get("list_images").getAsString());
					 * map.put("list_images_style",
					 * source.get("list_images_style").getAsInt());
					 */
					map.put("stars", source.get("stars").getAsString());

					ls.add(map);
				}
			}
		}
		return ls;
	}

	private Set<String> findStarNames(List<Map<String, Object>> items) {
		Set<String> names = new LinkedHashSet<>();
		String[] stars = null;
		for (Map<String, Object> map : items) {
			Object entityNames = map.get("entity_names");
			if(null != entityNames){
				stars = map.get("entity_names").toString().replace("\"", "").split(", ");
				for (String s : stars) {
					s = s.trim();
					if (s.length() > 0) {
						names.add(s);
					}
				}
			}
		}
		return names;
	}

	private void addFlowStatAndfollowedCount(List<Map<String, Object>> items, String userId) {
		if (items.size() == 0) {
			return;
		}
		String bk = BasicConstants.USER_ENTITY_BUCKET;
		String sql = "select end_id  from " + bk + " use keys[";
		String sql2 = "select end_id,count(end_id) count from " + bk
				+ " where end_id in [";
		String ids = "";
		String endIds = "";
		for (Map<String, Object> map : items) {
			ids += "'" + userId + "_" + map.get("entity_id") + "',";
			endIds += "'" + map.get("entity_id") + "',";
		}
		sql += ids + "''] where relation = '11'";
		sql2 += endIds + " '']  and  relation='11'  ";
		/*if (type != null) {
			sql2 += " and type='" + type + "' ";
		} else {
			sql2 += " and type is missing ";
		}*/
		sql2 += "group by end_id";
		QueryResult queryResult = CBaseConnectionPool.sql(bk ,sql);
		logger.info("sql is " + sql);
		QueryResult queryResult2 = CBaseConnectionPool.sql(bk ,sql2);
		logger.info("sql is " + sql2);
		if (queryResult.finalSuccess()) {
			String endId = "";
			for (Map<String, Object> map : items) {
				com.couchbase.client.java.document.json.JsonObject val = null;
				map.put("is_followed", "0");
				map.put("fans_count", "0");
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (map.get("entity_id").toString().equals(endId)) {
							map.put("is_followed", "1");
							break;
						}
					}
				}
				for (QueryRow row : queryResult2.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (map.get("entity_id").toString().equals(endId)) {
							map.put("fans_count", val.getInt("count") + "");
							break;
						}
					}
				}
			}
		}

	}

	public void addfollowedStat(List<Map<String, Object>> items, String userId, String type) {
		if (items.size() == 0) {
			return;
		}
		String sql = "select end_id  from " + BasicConstants.USER_STAR_BUCKET + " use keys[";
		// String sql2 = "select end_id,count(end_id) count from user_star where
		// end_id in [";
		String ids = "";
		for (Map<String, Object> map : items) {
			ids += "'" + userId + "_" + map.get("entity_id") + "',";
		}
		sql += ids + "'']";
		/*
		 * sql2 += ids+" ''] and relation='11' "; if(type != null){ sql2+=
		 * " and type='"+type+"' "; } sql2+= "group by end_id";
		 */
		QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.USER_ENTITY_BUCKET ,sql);
		logger.info("sql is " + sql);
		/*
		 * QueryResult queryResult2 =
		 * CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET).query(
		 * Query.simple(sql2)); logger.info("sql is "+sql2);
		 */
		if (queryResult.finalSuccess()) {
			String endId = "";
			for (Map<String, Object> map : items) {
				com.couchbase.client.java.document.json.JsonObject val = null;
				map.put("isFollowed", "false");
				// map.put("flows", "0");
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (map.get("entity_id").toString().equals(endId)) {
							map.put("isFollowed", "true");
							break;
						}
					}
				}
				/*
				 * for (QueryRow row : queryResult2.allRows()) { val =
				 * row.value(); if (null != val) { endId =
				 * val.getString("end_id");
				 * if(map.get("star_id").toString().equals(endId)){
				 * map.put("is_flow", val.getInt("count")+""); break; } } }
				 */
			}
		}

	}

	private void addUserInfo2Bar(List<Map<String, Object>> post_bars) {
		if (post_bars.size() == 0) {
			return;
		}
		String mysql = "select * from  users_crawler u where u.user_id in (";
		String ids = "";
		for (Map<String, Object> bar : post_bars) {
			if (bar.get("post_from") == null || bar.get("post_from").toString().equals("1")) {
				ids += "'" + bar.get("owners") + "',";
			}
		}
		mysql += ids + "'')";
		logger.info("mysql is " + mysql);
		List<Map<String, String>> userInfos = connectionUtil2.query(mysql,
				new ResultSetExtractor<List<Map<String, String>>>() {
					@Override
					public List<Map<String, String>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						List<Map<String, String>> userInfos = new ArrayList<>();
						while (resultSet.next()) {
							Map<String, String> m = new HashMap<String, String>();
							m.put("img", resultSet.getString("portrait"));
							m.put("user_id", resultSet.getString("user_id"));
							m.put("nick_name", resultSet.getString("nick_name"));
							userInfos.add(m);
						}
						return userInfos;
					}
				});
		if (userInfos.size() < post_bars.size()) {
			mysql = "select * from  users u where u.user_id in (";
			ids = "";
			for (Map<String, Object> bar : post_bars) {
				if (bar.get("post_from") != null && bar.get("post_from").toString().equals("0")) {
					ids += "'" + bar.get("owners") + "',";
				}
			}
			mysql += ids + "'')";
			logger.info("mysql is " + mysql);
			userInfos.addAll(connectionUtil2.query(mysql, new ResultSetExtractor<List<Map<String, String>>>() {
				@Override
				public List<Map<String, String>> extractData(ResultSet resultSet)
						throws SQLException, DataAccessException {
					List<Map<String, String>> userInfos = new ArrayList<>();
					while (resultSet.next()) {
						Map<String, String> m = new HashMap<String, String>();
						m.put("img", resultSet.getString("portrait"));
						m.put("user_id", resultSet.getString("user_id"));
						m.put("nick_name", resultSet.getString("nick_name"));
						userInfos.add(m);
					}
					return userInfos;
				}
			}));
		}
		for (Map<String, Object> bar : post_bars) {
			Object owner = bar.get("owners");
			if (owner == null) {
				continue;
			}
			String userId = "";
			for (Map<String, String> map : userInfos) {
				userId = map.get("user_id");
				if (userId.equals(owner.toString())) {
					bar.put("author_img", map.get("img"));
					bar.put("post_author_name", map.get("nick_name"));
					break;
				}
			}
		}
	}

	private String search(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String text = request.getParam("text");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		// Jedis jedis = null;
		try {
			if (StringUtils.isEmpty(text) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = " + text + " ,userId = " + userId);
				logger.info("缺失 text = " + text + " ,userId = " + userId);
			} else {
				// jedis = RedisConnectionPool.getCluster();
				Map<String, Object> data = new HashMap<>();
				List<Map<String, Object>> starsMap = getEntityOther(text, BasicConstants.ES_QUERY_STARS2, 0, 5,
						QUERY_TAG, false);
				List<Map<String, Object>> redisStarsMap = new ArrayList<>();
				Set<String> stars = getEntityNames(starsMap);
				Set<String> starsClasses = getStarsClass(starsMap);
				Set<String> reatedStars = new TreeSet<>();
				Set<String> otherStars = new TreeSet<>();
				reatedStars.addAll(stars);
				// List<Map<String, Object>> news_weibo = null;
				// List<Map<String, Object>> news_video = null;
				List<Map<String, Object>> post_bar = null;
				// List<Map<String, Object>> collections = null;
				int maxQuery = 300;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				// List<Map<String, Object>> news =
				// queryNews(text,stars,starsClasses,from,limit,reatedStars);
				List<Map<String, Object>> news = queryNews(text, stars, starsClasses, from, limit, reatedStars);
				data.put("info", news);
				if (page.equals("1")) {
					// reatedStars.removeAll(stars);
					// starsMap.addAll(getStars(set2String(reatedStars),BasicConstants.ES_QUERY_STARS,0,5-starsMap.size()));

					/*
					 * news_weibo = subList(queryNewsWeiBo(text,stars,0,1
					 * ,otherStars)); data.put("news_weibo", news_weibo);
					 * news_video = subList(queryNewsVideo(text,stars,0,1
					 * ,otherStars)); data.put("news_video", news_video);
					 */
					post_bar = subList(queryPostBar(text, stars, starsClasses, 0, 1));
					data.put("post_bar", post_bar);
					// collections =
					// subList(queryCollection(text,stars,starsClasses,userId
					// ,0,1));
					// data.put("collections", collections);

					reatedStars.removeAll(stars);
					if (starsMap.size() < BasicConstants.STAR_MAP_TOP_NUM && reatedStars.size() > 0) {
						starsMap.addAll(
								getEntitysByNames(reatedStars, BasicConstants.STAR_MAP_TOP_NUM - starsMap.size()));
						otherStars.removeAll(reatedStars);
					}
					otherStars.removeAll(stars);
					if (starsMap.size() < BasicConstants.STAR_MAP_TOP_NUM && otherStars.size() > 0) {
						starsMap.addAll(
								getEntitysByNames(otherStars, BasicConstants.STAR_MAP_TOP_NUM - starsMap.size()));
					}
					data.put("stars", starsMap);

					redisStarsMap.addAll(starsMap);
					queryNews(text, stars, starsClasses, from + limit, maxQuery, reatedStars);
					reatedStars.removeAll(otherStars);
					reatedStars.removeAll(stars);
					if (reatedStars.size() > 0) {
						redisStarsMap.addAll(getEntitysByNames(reatedStars, maxQuery));
						otherStars.addAll(stars);
						starMap2Redis(redisStarsMap, otherStars, userId);
					}

				}
				// jedis.zadd("k_w", Double.parseDouble(userId), text);
				Map<String, String> kw = new HashMap<>();
				kw.put("time", DateUtil.getCurrentDate());
				kw.put("text", text);
				kw.put("user", userId);
				String kwJson = new Gson().toJson(kw);
				logger.info(kwJson);
				KafkaProducer.send("kw", kwJson);
				resultObject.addResultData(data);
				resultObject.putHead("count", data.size());
			}
		} catch (Exception e) {
			logger.error("search:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("search is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String set2String(Set<String> set) {
		return set2String(set, " ", "");
	}

	private String set2String(Set<String> set, String delim, String warp) {
		String s = "";
		if (null == set || set.size() == 0) {
			return s;
		}
		for (String str : set) {
			s += warp + str + warp + delim;
		}
		return s.substring(0, s.length() - 1);
	}

	private String set2String(Set<String> set, String delim, String warp, int limit) {
		String s = "";
		if (null == set || set.size() == 0) {
			return s;
		}
		int count = 1;
		for (String str : set) {
			s += warp + str + warp + delim;
			if (limit == count) {
				break;
			}
			count++;
		}
		return s.substring(0, s.length() - 1);
	}

	private List<Map<String, Object>> subList(List<Map<String, Object>> ls) {
		if (null != ls && ls.size() > 0) {
			return ls.subList(0, 1);
		}
		return ls;
	}

	private List<Map<String, Object>> queryCollection(String text, Set<String> stars, Set<String> starsClasses,
			String userId, Integer from, Integer size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		String filter = getStarFilter4User(userId);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$class", set2String(starsClasses));
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/relation_*/_search", bodyJson,
				"utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						JsonObject source = job.get("_source").getAsJsonObject();
						JsonObject table = job.get("_type").getAsJsonObject();
						map.put("title", source.get("title").getAsString());
						map.put("type", table.getAsString());
						map.put("id", source.get("id").getAsString());
						map.put("pub_time", source.get("pub_time").getAsLong());
						JsonElement newsSourceObj = source.get("news_source");
						if (newsSourceObj != null) {
							map.put("news_source", newsSourceObj.getAsString());
						}
						map.put("news_source", source.get("news_source").getAsString());
						map.put("list_images", source.get("list_images").getAsString());
						JsonElement styleObj = source.get("list_images_style");
						if (starObj != null) {
							map.put("list_images_style", styleObj.getAsInt());
						}
						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryPostBar(String text, Set<String> stars, Set<String> starsClasses,
			Integer from, Integer size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		String filter = getStarFilter(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$class", set2String(starsClasses));
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/post_bar/_search",
				bodyJson, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			JsonElement postFrom = null;
			JsonElement commentCount = null;
			JsonElement upCount = null;
			JsonElement owners = null;
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						try {
							JsonObject source = job.get("_source").getAsJsonObject();
							map.put("title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsLong());
							map.put("list_images", source.get("list_images").getAsString());
							map.put("list_images_style", source.get("list_images_style").getAsInt());
							map.put("entity_names", source.get("entity_names"));
							map.put("intro", source.get("intro"));
							// map.put("owners",
							// source.get("owners").getAsString());
							owners = (JsonElement) source.get("owners");
							if (null != owners) {
								map.put("owners", owners.getAsString());
							}
							commentCount = (JsonElement) source.get("comment_count");
							if (null != commentCount) {
								map.put("comment_count", commentCount.getAsInt());
							} else {
								map.put("comment_count", 0);
							}
							upCount = (JsonElement) source.get("up_count");
							if (null != upCount) {
								map.put("up_count", upCount.getAsInt());
							} else {
								map.put("up_count", 0);
							}
							postFrom = source.get("post_from");
							if (null == source.get("post_from")) {
								map.put("post_from", "1");
							} else {
								map.put("post_from", postFrom.getAsInt() + "");
							}

							/*
							 * JsonElement starObjs = source.get("stars");
							 * if(null != starObjs && !starObjs.isJsonNull() &&
							 * relatedStars !=null){ for(String
							 * name:starObjs.getAsString().split(",")){
							 * relatedStars.add(name.trim()); } }
							 */
							ls.add(map);
						} catch (Exception e) {
							logger.error("", e);
						}
					}
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryNewsVideo(String text, Set<String> stars, Integer from, Integer size,
			Set<String> relatedStars) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsWeiBoAndVideoQuery");
		String filter = getStarFilter4NewTab(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$title", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/news_video/_search",
				bodyJson, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						try {
							JsonObject source = job.get("_source").getAsJsonObject();
							map.put("title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsLong());
							map.put("news_source", source.get("news_source").getAsString());
							map.put("list_images", source.get("list_images").getAsString());
							map.put("list_images_style", source.get("list_images_style").getAsInt());
							map.put("org_url", source.get("org_url").getAsString());
							JsonElement starObjs = source.get("stars");
							if (null != starObj && relatedStars != null) {
								for (String name : starObjs.getAsString().split(",")) {
									relatedStars.add(name.trim());
								}
							}
							ls.add(map);
						} catch (Exception e) {
							logger.error("", e);
						}
					}
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryNewsWeiBo(String text, Set<String> stars, Integer from, Integer size,
			Set<String> relatedStars) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsWeiBoAndVideoQuery");
		String filter = getStarFilter4NewTab(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$title", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");

		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/news_weibo/_search",
				bodyJson, "utf-8");
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
				JsonObject job = starObj.getAsJsonObject();
				map = new HashMap<>();
				try {
					JsonObject source = job.get("_source").getAsJsonObject();
					map.put("title", source.get("title").getAsString());
					map.put("id", source.get("id").getAsString());
					map.put("pub_time", source.get("pub_time").getAsLong());
					map.put("news_source", source.get("news_source").getAsString());
					map.put("list_images", source.get("list_images").getAsString());
					map.put("list_images_style", source.get("list_images_style").getAsInt());
					JsonElement org = source.get("org_url");
					map.put("org_url", org == null ? "" : org.getAsString());
					JsonElement starObjs = source.get("stars");
					if (null != starObj && relatedStars != null) {
						for (String name : starObjs.getAsString().split(",")) {
							relatedStars.add(name.trim());
						}
					}
					ls.add(map);
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("", e);
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryPlayers(String text, Set<String> stars, int from, int size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetPlayerQuery");
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$pubTime", DateUtil.getCurrentTime("yyyy") + "");
		String jsonStr = HttpClientUtil.sendPostRequest(
				ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/player/_search", bodyJson,
				"utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return ls;
			}
			JsonObject hits = hitsEle.getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * 0.4;
//				JsonElement flow = null;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						JsonObject source = job.get("_source").getAsJsonObject();
						map.put("title", source.get("title").getAsString());
						map.put("id", source.get("id").getAsString());
						map.put("entity_id", source.get("id").getAsString());
						map.put("pub_time", source.get("pub_time").getAsLong());
						map.put("news_source", source.get("news_source").getAsString());
						map.put("list_images", source.get("list_images"));
						map.put("list_images_v", source.get("list_images_v"));
						map.put("img_v", source.get("img_v"));
						map.put("img", source.get("img"));
						map.put("intro", source.get("intro"));
						map.put("first_class_ids", source.get("first_class_ids"));
						map.put("list_images_style", source.get("list_images_style").getAsInt());
						map.put("entity_names", source.get("entity_names").getAsString());
						map.put("tv_type", source.get("tv_type"));
						map.put("rank", source.get("rank").getAsDouble());
						/*flow = source.get("followed_count");
						if (flow != null) {
							map.put("followed_count", flow.getAsInt());
						} else {
							map.put("followed_count", 0);
						}*/
						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryAllNews(String text, Set<String> stars, int from, int size ,String tables) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		String filter = getStarFilter(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$class", "");
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(
				ConfigInit.getValue("esUrl") + "/nm_*/"+tables+"/_search", bodyJson, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return ls;
			}
			JsonObject hits = hitsEle.getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * 0.4;
				String tableType = "";
				JsonElement orgUrl = null;
//				JsonElement commentCount = null;
				JsonElement duration = null;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						tableType = job.get("_type").getAsString();
						JsonObject source = job.get("_source").getAsJsonObject();
						orgUrl = source.get("org_url");
						if (null != orgUrl) {
							map.put("landing_param", orgUrl.getAsString());
//							map.put("org_url", orgUrl.getAsString());
						}
					/*	commentCount = source.get("comment_count");
						if (null != commentCount) {
							map.put("comment_count", commentCount.getAsInt());
						}*/
						duration = source.get("duration");
						if (null != duration && tableType.equals(BasicConstants.NM_VIDEO)) {
							map.put("duration", duration.getAsInt());
						}
						if (tableType.equals(BasicConstants.NM_VIDEO)) {
							map.put("type", "video");
							map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
						} else if(tableType.equals(BasicConstants.NM_WEIBO)){
							map.put("type", "weibo");
							map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
						} else {
						map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
						map.put("type", "news");
						}
//						map.put("type", tableType);
						map.put("title", source.get("title").getAsString());
						map.put("id", source.get("id").getAsString());
						map.put("pub_time", source.get("pub_time").getAsLong());
						map.put("news_source", source.get("news_source").getAsString());
						map.put("list_images", source.get("list_images"));
						map.put("list_images_style", source.get("list_images_style"));
						map.put("entity_names", source.get("entity_names"));

						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private List<Map<String, Object>> queryNews(String text, Set<String> stars, Set<String> starsClasses, int from,
			int size, Set<String> relatedStars) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		String filter = getStarFilter(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		bodyJson = bodyJson.replace("$class", set2String(starsClasses));
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/news/_search", bodyJson,
				"utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return ls;
			}
			JsonObject hits = hitsEle.getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * 0.4;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else {
						JsonObject source = job.get("_source").getAsJsonObject();
						map.put("title", source.get("title").getAsString());
						map.put("id", source.get("id").getAsString());
						map.put("pub_time", source.get("pub_time").getAsLong());
						map.put("news_source", source.get("news_source").getAsString());
						map.put("list_images", source.get("list_images").getAsString());
						map.put("list_images_style", source.get("list_images_style").getAsInt());
						JsonElement starObjs = source.get("stars");
						if (null != starObj) {
							for (String name : starObjs.getAsString().split(",")) {
								relatedStars.add(name.trim());
							}
						}
						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private String getStarFilter4User(String userId) {
		String filter = "\"filter\": {        \"terms\": {          \"user_id\": [        $userId     ]        }      } ,";
		if (null == userId) {
			return "";
		}
		filter = filter.replace("$userId", userId);
		return filter;
	}

	private String getStarFilter(Set<String> stars) {
		String filter = "\"filter\": {        \"query\": {          \"multi_match\": {            \"query\": \"$query\",            \"fields\": [\"title\",\"content\"]          }       } } ,";
		String query = "";
		query = set2String(stars);
		if (query.length() == 0) {
			return "";
		}
		filter = filter.replace("$query", query);
		return filter;
	}

	private String getStarFilter2(Set<String> stars) {
		String filter = "\"filter\": {        \"query\": {          \"multi_match\": {            \"query\": \"$query\",            \"fields\": [\"title\"]          }       } } ,";
		String query = "";
		query = set2String(stars);
		if (query.length() == 0) {
			return "";
		}
		filter = filter.replace("$query", query);
		return filter;
	}

	private Set<String> getEntityNames(List<Map<String, Object>> stars) {
		Set<String> starNames = new LinkedHashSet<>();
		for (Map<String, Object> star : stars) {
			starNames.add(star.get("name").toString());
		}
		return starNames;
	}

	private Set<String> getStarsClass(List<Map<String, Object>> stars) {
		Set<String> starClasses = new HashSet<>();
		for (Map<String, Object> star : stars) {
			starClasses.add(star.get("info_badge").toString());
		}
		return starClasses;
	}

	private String getStarFilter4NewTab(Set<String> stars) {
		String filter = "\"filter\": {        \"query\": {          \"multi_match\": {            \"query\": \"$query\",            \"fields\": [\"title\",\"stars\"]          }   }     } ,";
		String query = set2String(stars);
		if (query.length() == 0) {
			return "";
		}
		filter = filter.replace("$query", query);
		return filter;
	}

	private static List<Map<String, Object>> getEntityOther(String text, String queryKey, Integer from, Integer size,
			String tag, boolean accurate) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		boolean isContinue = false;
		if (!text.contains(tag)) {
			text += " " + tag;
			isContinue = true;
		}
		String bodyJson = ConfigInit.getValue(queryKey).replace("$text", text);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/"
				+ BasicConstants.ENTITY_INDEX_DB + "/" + BasicConstants.ENTITY_INDEX_TABLE_OTHER + "/_search", bodyJson,
				"utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			double tagScore = 0;
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else if (job.get("_score").getAsDouble() > 0.5) {
						JsonObject source = job.get("_source").getAsJsonObject();
						String name = source.get("name").getAsString();
						if (isContinue && name.contains(tag)) {
							tagScore = job.get("_score").getAsDouble() * 0.8;
							continue;
						}
						if (accurate && job.get("_score").getAsDouble() < tagScore) {
							break;
						}
						map.put("name", name);
						map.put("entity_id", source.get("id").getAsString());
						map.put("intro", source.get("intro").getAsString());
						map.put("img", source.get("img").getAsString());
						map.put("badge", source.get("badge").getAsString());
						map.put("rank", source.get("rank"));
						map.put("first_class_ids", source.get("first_class_ids"));
						/*JsonElement info = source.get("info_badge");
						if (info != null && !info.isJsonNull()) {
							map.put("info_badge", info.getAsString());
						} else {
							map.put("info_badge", "");
						}
*/
						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private String addIndex(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		// String db = request.getParam("db");
		String typeStr = request.getParam("type");
		String id = request.getParam("id");
		// String userId = request.getParam("userId");
		String pubTime = request.getParam("pubTime");
		byte[] conetnt = request.getContent();
		try {
			if (StringUtils.isEmpty(typeStr) || StringUtils.isEmpty(id) || StringUtils.isEmpty(pubTime)
					|| conetnt == null) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
				logger.info("缺失 type = " + typeStr + " ,id = " + id + " ,pubTime=" + pubTime);
			} else {
				String db = "other_card";
				String table = "";
				String re = "";
				String url = ConfigInit.getValue("esUrl");
				int type = Integer.parseInt(typeStr);
				if (TypeCode.STARS.getType() == type) {
					db = BasicConstants.INDEX_STARS;
					url += "/" + db + "/star/" + id;
					// re = HttpUrlUtil.Delete(url);
				} else if (TypeCode.PLAYER.getType() == type) {
					db = BasicConstants.INDEX_PLAYERS;
					url += "/" + db + "/player/" + id;
				} else if (TypeCode.NEWS.getType() == type || TypeCode.NEWS_WEIBO.getType() == type
						|| TypeCode.NEWS_VIDEO.getType() == type || TypeCode.POST_BAR.getType() == type) {
					db = getIndex(Long.parseLong(pubTime));
					if (TypeCode.NEWS.getType() == type) {
						table = BasicConstants.NM_NEWS;
					} else if (TypeCode.NEWS_VIDEO.getType() == type) {
						table = BasicConstants.NM_WEIBO;
					} else if (TypeCode.NEWS_VIDEO.getType() == type) {
						table = BasicConstants.NM_VIDEO;
					} else if (TypeCode.POST_BAR.getType() == type) {
						table = BasicConstants.NM_POST;
					}
					url += "/" + db + "/" + table + "/" + id;
				} else if (TypeCode.AD.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_AD + "/" + id;
				} else if (TypeCode.THEME.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_THEME + "/" + id;
				} else if (TypeCode.TOPIC_MODEL.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_TOPIC + "/" + id;
				} else if (TypeCode.ACTIVITY.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_ACTIVITY + "/" + id;
				} else if (TypeCode.ARDENLY.getType() == type) {
					url += "/other_card/" + BasicConstants.CARD_ARDENLY + "/" + id;
				}
				String json = new String(conetnt);
				logger.info("url=" + url + " ,body=" + json);
				re = HttpClientUtil.sendPostRequest(url, json, "utf-8");
				logger.info(re);
				HttpClientUtil.sendGetRequest(ConfigInit.getValue("esUrl")+ "/" + db + "/_flush");
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("delIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("delIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String addIndex4Kafka(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		byte[] content = request.getContent();
		try {
			if (content != null) {
				KafkaProducer.send(BasicConstants.ES_KAFKA_TOPIC, content);
			} else {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 body");
				logger.info("缺失 body");
			}
		} catch (Exception e) {
			logger.error("addIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_ADDINDEX);
			resultObject.addReturnDesc("addIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

}
