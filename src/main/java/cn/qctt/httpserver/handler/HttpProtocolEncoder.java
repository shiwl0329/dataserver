package cn.qctt.httpserver.handler;

import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.core.BasicConstants;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders.Names;
import io.netty.handler.codec.http.HttpHeaders.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * @author yanchao_guo
 */
public class HttpProtocolEncoder extends ChannelHandlerAdapter {

    private FullHttpResponse fullHttpResponse = null;
    private static Logger logger = LoggerFactory.getLogger(HttpProtocolEncoder.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof Response) {
            Response response = (Response) msg;
            fullHttpResponse = response.toFullHttpResponse();
            if (response.isKeepAlive()) {
                fullHttpResponse.headers().set(Names.CONNECTION, Values.KEEP_ALIVE);
                ctx.writeAndFlush(fullHttpResponse);
                logger.info("ChannelFuture {} is keep-alive , not close......", this);
            } else {
                ctx.writeAndFlush(fullHttpResponse).addListener(ChannelFutureListener.CLOSE);
                logger.info("ChannelFuture {} is closed......", this);
            }
        } else {
            fullHttpResponse = getHttpResponse(msg.toString().getBytes());
            ctx.writeAndFlush(fullHttpResponse).addListener(ChannelFutureListener.CLOSE);
        }
    }

    private FullHttpResponse getHttpResponse(byte[] rep) {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(rep));
        response.headers().set(Names.CONTENT_TYPE, "text/plain; charset=" + BasicConstants.HTTP_SERVER_URL_CHARCODE_UTF_8);
        response.headers().set(Names.CONTENT_LENGTH, response.content().readableBytes());
        response.headers().set(Names.CONNECTION, Values.KEEP_ALIVE);
        return response;
    }
}
