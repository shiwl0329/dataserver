package cn.qctt.httpserver.handler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonSyntaxException;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Regular;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.manager.KSessionManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import cn.qctt.httpserver.utils.DateUtil;
import cn.qctt.httpserver.utils.DisplyTime;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

@RequestMapping(BasicConstants.URL_PREFIX + "/userRefresh")
public class UpAndDownRefreshHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(UpAndDownRefreshHandler.class);

	private static SimpleDateFormat sdf = new SimpleDateFormat("mmss");

//	private static String bucketName = BasicConstants.BK_CONFIG_BUCKET;
	private static String bucketPwd = "";
	// private static String upRefreshUrl = BasicConstants.URL_PREFIX +
	// "/userRefresh/upRefresh";
	private static String upAndDownRefreshUrl = BasicConstants.URL_PREFIX + "/userRefresh/upAndDownRefresh";
	private static String upAndDownRefresh4PageUrl = BasicConstants.URL_PREFIX + "/userRefresh/upAndDownRefresh4Page";

	private static String getRefreshCountUrl = BasicConstants.URL_PREFIX + "/userRefresh/getRefreshCount";
	// 话题 、广告、任务组 保留时间
	private static final int days = Integer.parseInt(ConfigInit.getValue("cardLifeAge"));
	// 卡片长度 每3 条新闻一个卡片
	private static final int newsLenthInCard = Integer.parseInt(ConfigInit.getValue("newsCardsLength"));
	// 获取明星的top Num
	private static final int starsTopSize = Integer.parseInt(ConfigInit.getValue("starsTopSize"));
	// 热门卡片长度 5个位一组
	private static final int hotCardsSize = Integer.parseInt(ConfigInit.getValue("hotCardsSize"));
	// 混合排序时支持的最大长度 300 = readList.length + cache.length
	private static final int maxLengthInGroup = Integer.parseInt(ConfigInit.getValue("maxLengthInGroup"));;

	private static final int themPosition = ConfigInit.getValue("themPosition") == null ? BasicConstants.THEME_POSITION
			: Integer.parseInt(ConfigInit.getValue("themPosition"));
	private static final int topicPosition = ConfigInit.getValue("topicPosition") == null
			? BasicConstants.THEME_POSITION : Integer.parseInt(ConfigInit.getValue("topicPosition"));
	private static final int adPosition = ConfigInit.getValue("adPosition") == null ? BasicConstants.THEME_POSITION
			: Integer.parseInt(ConfigInit.getValue("adPosition"));
	private static final int starPosition = ConfigInit.getValue("starPosition") == null ? BasicConstants.THEME_POSITION
			: Integer.parseInt(ConfigInit.getValue("starPosition"));
	private static final int hotPosition = ConfigInit.getValue("hotPosition") == null ? BasicConstants.THEME_POSITION
			: Integer.parseInt(ConfigInit.getValue("hotPosition"));

	private int dataLen = 0;

	@Override
	public String invoke(Request request, Response response) {
		String urlPath = request.getPath();
		String returnStr = "";
		try {
			if (urlPath.startsWith(upAndDownRefresh4PageUrl)) {
				returnStr = upAndDownRefresh4Page(request);
			} else if (urlPath.startsWith(upAndDownRefreshUrl)) {
				returnStr = upAndDownRefresh(request);
			} else if (urlPath.startsWith(getRefreshCountUrl)) {
				returnStr = getRefreshCount(request);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return returnStr;
	}

	private String getRefreshCount(Request request) {
		long start = System.currentTimeMillis();
		Jedis redisCluster = null;
		String id = request.getParam("userId");
		String lastCursor = request.getParam("lastCursor");
		String prefix = request.getParam("type", BasicConstants.READ_LIST_MAIN_CHANNEL);
		ResultMsg resultObject = new ResultMsg();
		try {
			redisCluster = RedisConnectionPool.getCluster();
			if (StringUtils.isBlank(id) || StringUtils.isBlank(lastCursor)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数 ,userId=" + id + " ,lastCursor=" + lastCursor);
			} else {
				String key = prefix + ":" + id;
				Long count = redisCluster.zcard(key);
				count = (long) (count * 0.85);
				key = BasicConstants.APP_CACHE_CHANNEL + ":" + id;
				Set<Tuple> cardCache = redisCluster.zrevrangeWithScores(key, Integer.parseInt(lastCursor), -1);
				if (count > 0) {
					count += flatCard(cardCache).size();
				}
				resultObject.addResultData(count);
				resultObject.putHead("count", 1);
				logger.info("刷新数 (" + count + " * 0.85) for " + id);
			}
		} catch (Throwable e) {
			logger.error("getNoReadCount:", e);
			logger.error("获取未读数据失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("获取未读数据失败  ,params is [" + request.getParams() + "]");
		} finally {
			if (null != redisCluster) {
				redisCluster.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String upAndDownRefresh4Page(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		// int page = Integer.parseInt(request.getParam("page", "1"));
		int size = Integer.parseInt(request.getParam("size", "10")) - 1;
		String type = request.getParam("type", "up");
		// String lastCursor = request.getParam("lastCursor", "0");
		String channel = BasicConstants.APP_CACHE_CHANNEL;
		if (StringUtils.isBlank(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数:" + request.getParams());
		} else {
			Jedis redisCluster = null;
			// resultObject.addReturnCode(CodeManager.USERREFRESH_UPANDDOWNREFRESH);
			try {
				redisCluster = RedisConnectionPool.getCluster();
				List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
				String key = channel + ":" + userId;
				boolean b = true;
				// long lastCursor = page * size ;
				if ("down".equals(type)) {
					Map<String, Object> msg = new HashMap<>();
					msg.put("userId", "" + userId);
					redisCluster.publish(BasicConstants.USER_FEED, Utils.getGson().toJson(msg));
					logger.info("send " + msg + " to distribute");
					b = copyAndRegroup4Page(userId);
					logger.info("down refresh for copyAndRegroup " + (System.currentTimeMillis() - start) + " timing");
					Long cacheNum = redisCluster.zcard(key);
					if (!b && cacheNum <= 30) {
						resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
						resultObject.addResultData(tmp);
						return resultObject.toJson();
					}
				}

				// long startNum = page * size;
				// long endNum = startNum + size - 1;

				// 1 、返回查询数据
				Set<String> appCacheData = redisCluster.zrevrange(key, 0, size);

				// logger.info("key:" + key + " ,startNum:" + startNum + "
				// ,endNum:" + endNum + " ,size:"
				// + appCacheData.size());
				// 刷新消息中的评论数 疑似耗时操作
				// appCacheData = ;
				tmp.addAll(refreshCacheData(Utils.setJson2Map(appCacheData), userId, type));
				// 进行插队操作
				int zc = tmp.size();
//				tmp = jumpQueue_3(tmp, userId);
				// 清除已读cache数据
				if (size > 1) {
					redisCluster.zremrangeByRank(key, -(size + 1), -1);
					logger.info("remove " + (size + 1) + " for down");
				}

				resultObject.addResultData(tmp);
				resultObject.putHead("count", dataLen);
				dataLen = 0;

				logger.info("获取卡片组" + tmp.size() + "组 ,其中资讯数是 " + zc + "组  ,params is [" + request.getParams() + "]");
				// 3、设置过期时间 长期在一段时间不操作 清空用户app cache 、各个readed channel

				redisCluster.expire(key, BasicConstants.REDIS_EXPIRE);
			} catch (Throwable e) {
				e.printStackTrace();
				logger.error("upAndDownRefresh", e);
			} finally {
				if (null != redisCluster) {
					redisCluster.close();
				}
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing for upAndDownRefresh");
		return resultObject.toJson();
	}

	private String upAndDownRefresh(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String cursor = request.getParam("cursor", "0");
		String size = request.getParam("size", "10");
		String type = request.getParam("type", "up");
		String lastCursor = request.getParam("lastCursor", "0");
		String channel = BasicConstants.APP_CACHE_CHANNEL;
		if (StringUtils.isBlank(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数:" + request.getParams());
		} else {
			Jedis redisCluster = null;
			// resultObject.addReturnCode(CodeManager.USERREFRESH_UPANDDOWNREFRESH);
			try {
				redisCluster = RedisConnectionPool.getCluster();
				List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
				String key = channel + ":" + userId;
				boolean b = true;
				if ("down".equals(type)) {
					b = copyAndRegroup(userId, Long.parseLong(lastCursor));
					logger.info("down refresh for copyAndRegroup " + (System.currentTimeMillis() - start) + " timing");
					Long cacheNum = redisCluster.zcard(key);
					if (!b && cacheNum <= 30) {
						resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
						resultObject.addResultData(tmp);
						return resultObject.toJson();
					} else if (!b) {
						redisCluster.zremrangeByRank(key, -Long.parseLong(lastCursor), -1);
						logger.info("remove " + Long.parseLong(lastCursor) + " for down");
					}
					/*
					 * if (!b) { // false down刷新为空 //
					 * resultObject.putHead(BasicConstants.TIMING,
					 * System.currentTimeMillis() - start); //
					 * resultObject.addResultData(tmp); // return
					 * resultObject.toJson();
					 * 
					 * redisCluster.zremrangeByRank(key,
					 * -Long.parseLong(lastCursor), -1); }
					 */
				}

				long startNum = Long.parseLong(cursor);
				long endNum = Long.parseLong(cursor) + Long.parseLong(size) - 1;

				// 1 、返回查询数据
				Set<String> appCacheData = redisCluster.zrevrange(key, startNum, endNum);
				logger.info("key:" + key + " ,startNum:" + startNum + " ,endNum:" + endNum + " ,size:"
						+ appCacheData.size());
				// 刷新消息中的评论数 疑似耗时操作
				// appCacheData = ;
				tmp.addAll(refreshCacheData(Utils.setJson2Map(appCacheData), userId, type));

				// 进行插队操作
				int zc = tmp.size();
				tmp = jumpQueue_3(tmp, userId);

				resultObject.addResultData(tmp);
				resultObject.putHead("count", tmp.size());

				logger.info("获取卡片组" + tmp.size() + "组 ,其中资讯数是 " + zc + "组  ,params is [" + request.getParams() + "]");
				/*
				 * // 2、清理掉已读取的数据 String[] cacheArray = appCacheData.toArray(new
				 * String[0]); if (cacheArray.length > 0) { logger.debug(
				 * "清理掉appCacheData已读取的数据 [" + appCacheData + "]");
				 * RedisConnectionPool.getCluster().zrem(key,
				 * appCacheData.toArray(new String[0]));
				 * 
				 * logger.info("start thread removing cacheAndReadList"); }
				 */
				// 3、设置过期时间 长期在一段时间不操作 清空用户app cache 、各个readed channel

				redisCluster.expire(key, BasicConstants.REDIS_EXPIRE);
				/*
				 * RedisConnectionPool.getCluster().expire(BasicConstants.
				 * AD_CARD_READ, BasicConstants.REDIS_EXPIRE);
				 * RedisConnectionPool.getCluster().expire(BasicConstants.
				 * THEME_CARD_READ, BasicConstants.REDIS_EXPIRE);
				 * RedisConnectionPool.getCluster().expire(BasicConstants.
				 * AD_CARD_READ, BasicConstants.REDIS_EXPIRE);
				 * RedisConnectionPool.getCluster().expire(BasicConstants.
				 * STAR_CARD_READ, BasicConstants.REDIS_EXPIRE);
				 * RedisConnectionPool.getCluster().expire(BasicConstants.
				 * TOPIC_CARD_READ, BasicConstants.REDIS_EXPIRE);
				 */
			} catch (Throwable e) {
				e.printStackTrace();
				logger.error("upAndDownRefresh", e);
			} finally {
				if (null != redisCluster) {
					redisCluster.close();
				}
			}
		}
		/*
		 * try { if (null != redisCluster) { redisCluster.close(); } } catch
		 * (Exception e) { redisCluster.disconnect(); logger.error(
		 * "jedis close fail :" +e.getMessage()); }
		 */
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing for upAndDownRefresh");
		return resultObject.toJson();
	}

	private long max(List<Long> ls) {
		long max = 0;
		for (long a : ls) {
			if (max < a) {
				max = a;
			}
		}
		return max;
	}

	private List<String> lastRefreshAndEarliestDisplayTime(String userId) {
		Jedis redis = null;
		List<String> times = new ArrayList<>();
		try {
			long lastRefresh = System.currentTimeMillis();
			long earliestDisplay = DateUtil.getBeforeMonthTime(1);
			redis = RedisConnectionPool.getCluster();
			String timeStr = redis.hget(BasicConstants.USER_STATE_PREFIX + ":" + userId, "feed");
			// redis.hset("last_refresh:" + userId, "feed", re + "");
			logger.info("lastRefreshAndEarliestDisplayTime " + timeStr);
			if (!StringUtils.isEmpty(timeStr)) {
				String[] timeArray = timeStr.split(":");
				lastRefresh = Long.parseLong(timeArray[0]);
				earliestDisplay = Long.parseLong(timeArray[1]);
			}
			times.add(lastRefresh + "");
			times.add(earliestDisplay + "");

		} catch (Exception e) {
			logger.error("", e);
		} finally {
			if (null != redis) {
				redis.close();
			}
		}
		return times;
	}

	// 疑似耗时操作
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> refreshCacheData(Set<Map<String, Object>> appCacheData, String userId,
			String type) {
		// Set<Map<String, Object>> tmp = new LinkedHashSet<Map<String,
		// Object>>();
		// Set<Map<String, Object>> appCacheDataTmp = new LinkedHashSet<>();
		List<Map<String, Object>> ls = null;
		List<Map<String, Object>> ls2 = null;
		StringBuffer sb = new StringBuffer();
		Set<String> starIds = new HashSet<String>();
		List<String> times = lastRefreshAndEarliestDisplayTime(userId);
		logger.info("appCacheData size is " + appCacheData.size());
		long minDisPlayTime = System.currentTimeMillis();
		List<Map<String, Object>> appCacheList = new ArrayList<>(appCacheData);
		appCacheData.clear();
		appCacheData = null;
		for (Map<String, Object> m : appCacheList) {
			// m = mapJson2Map(json);
			ls = (List<Map<String, Object>>) m.get("data");
			dataLen += ls.size();
			String starId = m.get("star_id").toString();
			m.put("is_follow", "0");
			String sql = "select relation from "+BasicConstants.USER_STAR_BUCKET+" where start_id = '" + userId + "' and end_id='" + starId + "'";
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET, bucketPwd).query(Query.simple(sql));
			logger.debug("refreshCacheData [" + sql + "]");
			if (queryResult.finalSuccess()) {
				ls2 = CouchbaseUtil.resultToList(queryResult, "source_other");
				if (ls2.size() > 0) {
					m.put("is_follow", ls2.get(0).get("relation"));
				}
			}
			String linkType = "";
			Object linkTypeObj = "";
			List<Long> displayTimes = new ArrayList<>();
			long disPlayTime = 0;
			//组内排序
			Collections.sort(ls, new Comparator<Map<String, Object>>() {
				public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
					// 从大到小排序
					return arg1.get("pub_time").toString().compareTo(arg0.get("pub_time").toString());
				}
			});
			for (Map<String, Object> item : ls) {
				String id = item.get("id").toString();
				if (type.equals("up")) {
					disPlayTime = DisplyTime.upRefresh(Utils.toPlainLong(item.get("pub_time").toString()),
							Long.parseLong(times.get(1)));
				} else {
					disPlayTime = DisplyTime.downRefresh(Utils.toPlainLong(item.get("pub_time").toString()),
							Long.parseLong(times.get(0)));
				}
				displayTimes.add(disPlayTime);
				if (minDisPlayTime > disPlayTime) {
					minDisPlayTime = disPlayTime;
				}
				item.put("pub_time", disPlayTime);

				linkTypeObj = item.get("link_type");
				if (null != linkTypeObj) {
					linkType = linkTypeObj.toString();
				}
				starIds.add(item.get("star_id").toString());
				if (!BasicConstants.NEWS_LINKED_TYPE.equals(linkType)) {
					logger.debug("discard " + id + ",link_type is [" + linkType + "]");
					continue;
				}
				sb.append("'" + id + "',");
			}
			// 按照display_time 排序
			m.put("sort_time", max(displayTimes));
			
		}

		Jedis redis = null;
		try {
			redis = RedisConnectionPool.getCluster();
			redis.hset(BasicConstants.USER_STATE_PREFIX + ":" + userId, "feed",
					System.currentTimeMillis() + ":" + minDisPlayTime);
		} catch (Exception e) {
			logger.error("", e);
		} finally {
			if (redis != null) {
				redis.close();
			}
		}

		if (sb.length() > 0) {
			String sql = "select id,c_c as count from "+BasicConstants.SOURCE_OTHER_BUCKET+" use keys [" + sb.toString() + " '']";
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET, bucketPwd).query(Query.simple(sql));
			logger.debug("refreshCacheData [" + sql + "]");

			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> tmpLs = CouchbaseUtil.resultToList(queryResult, "source_other");
				for (Map<String, Object> m : appCacheList) {
					ls = (List<Map<String, Object>>) m.get("data");
					for (Map<String, Object> item : ls) {
						String id2 = item.get("id").toString();
						for (Map<String, Object> jsonMap : tmpLs) {
							String id1 = jsonMap.get("id").toString();
							if (id1.equals(id2)) {
								item.put("count", jsonMap.get("count"));
							}
						}
					}
					// appCacheDataTmp.add(m);
				}
				// logger.info("sql is [" + sql + "]");
			} else {
				logger.warn("查询失败 " + queryResult.errors());
			}
		}
		// appCacheData.removeAll(appCacheDataTmp);
		// logger.info("appCacheData size is "+appCacheData.size());
		// 对卡片排序
		Collections.sort(appCacheList, new Comparator<Map<String, Object>>() {
			public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
				// 从大到小排序
				return arg1.get("sort_time").toString().compareTo(arg0.get("sort_time").toString());
			}
		});
		return appCacheList;
		// return tmp;
	}

	/*
	 * // 疑似耗时操作
	 * 
	 * @SuppressWarnings("unchecked") private Set<Map<String, Object>>
	 * refreshCacheData(Set<Map<String, Object>> appCacheData, String userId) {
	 * // Set<Map<String, Object>> tmp = new LinkedHashSet<Map<String, //
	 * Object>>(); Set<Map<String, Object>> appCacheDataTmp = new
	 * LinkedHashSet<>(); List<Map<String, Object>> ls = null; StringBuffer sb =
	 * new StringBuffer(); Set<String> starIds = new HashSet<String>(); for
	 * (Map<String, Object> m : appCacheData) { // m = mapJson2Map(json); ls =
	 * (List<Map<String, Object>>) m.get("data"); String linkType = ""; for
	 * (Map<String, Object> item : ls) { String id = item.get("id").toString();
	 * linkType = item.get("link_type").toString();
	 * starIds.add(item.get("star_id").toString()); if
	 * (!BasicConstants.NEWS_LINKED_TYPE.equals(linkType)) { logger.debug(
	 * "discard " + id + ",link_type is [" + linkType + "]"); continue; }
	 * sb.append("'" + id + "',"); }
	 * 
	 * m.put("data", ls); tmp.add(m);
	 * 
	 * } if (sb.length() > 0) { String sql =
	 * "select id,c_c as count from source_other use keys [" + sb.toString() +
	 * " '']"; QueryResult queryResult =
	 * CBaseConnectionPool.getBucket(bucketName,
	 * bucketPwd).query(Query.simple(sql)); logger.debug("refreshCacheData [" +
	 * sql + "]"); String url = "http://" + ConfigInit.getValue("neo4jHost") +
	 * "/" + ConfigInit.getValue("getRelationByUserIdAndStarNames") + "?userId="
	 * + userId + "&starIds=" + Utils.makeStr(starIds, ",");// JsonObject flow =
	 * Utils.getByUrl(url);
	 * 
	 * if (queryResult.finalSuccess()) { List<Map<String, Object>> tmpLs =
	 * CouchbaseUtil.resultToList(queryResult, "source_other"); for (Map<String,
	 * Object> m : appCacheData) { ls = (List<Map<String, Object>>)
	 * m.get("data"); String id = m.get("star_id").toString(); String isTake =
	 * "0"; if (null == flow) { logger.debug("get flow state for user[" + userId
	 * + "] but flow is null"); } else { JsonArray data =
	 * flow.get("response").getAsJsonObject().getAsJsonArray("data"); for
	 * (JsonElement ob : data) { if
	 * (id.equals(ob.getAsJsonObject().get("id").getAsString())) { isTake =
	 * ob.getAsJsonObject().get("isTake").getAsString(); m.put("is_follow",
	 * isTake); break; } } } if ("00".equals(isTake) || "10".equals(isTake) ||
	 * "20".equals(isTake)) { appCacheDataTmp.add(m); logger.debug(
	 * "discard ,star[" + id + "] flow state is [" + isTake + "] for user[" +
	 * userId + "]"); continue; }
	 * 
	 * for (Map<String, Object> item : ls) { String id2 =
	 * item.get("id").toString(); for (Map<String, Object> jsonMap : tmpLs) {
	 * String id1 = jsonMap.get("id").toString(); if (id1.equals(id2)) {
	 * item.put("count", jsonMap.get("count")); } } } appCacheDataTmp.add(m); }
	 * // logger.info("sql is [" + sql + "]"); } else { logger.warn("查询失败 " +
	 * queryResult.info()); } } appCacheData.removeAll(appCacheDataTmp); return
	 * appCacheData; // return tmp; }
	 */
	// 疑似耗时操作
	@SuppressWarnings("unchecked")
	private Map<String, Object> refreshCacheData(Map<String, Object> card, String userId) {
		StringBuffer sb = new StringBuffer();
		List<Map<String, Object>> list = (List<Map<String, Object>>) card.get("data");
		// Set<String> starIds = new HashSet<String>();
		String starId = "";
		if (null == list) {
			logger.warn("items is null and card is [" + card + "]");
			return card;
		}
		for (Map<String, Object> item : list) {
			Object id = item.get("id");
			// starIds.add(item.get("star_id").toString());
			starId = item.get("star_id").toString();
			if (null == id) {
				logger.debug("discard ,not found id for [" + item + "]");
				continue;
			}
			sb.append("'" + id + "',");
			// JsonDocument doc =
			// CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).get(id);
			/*
			 * if(doc != null){ int count = doc.content().getInt("m_c");
			 * item.put("count", count); }else{ logger.warn("not fount doc for "
			 * +id); }
			 */
		}
		if (sb.length() > 0) {
			/*
			 * String url = "http://" + ConfigInit.getValue("neo4jHost") + "/" +
			 * ConfigInit.getValue("getRelationByUserIdAndStarNames") +
			 * "?userId=" + userId+"&starIds="+Utils.makeStr(starIds, ",");//
			 * JsonObject flow = Utils.getByUrl(url); if(null != flow){
			 * JsonArray data =
			 * flow.get("response").getAsJsonObject().getAsJsonArray("data");
			 * if(data.size() > 0){ card.put("is_follow",
			 * data.get(0).getAsJsonObject().get("isTake")); } }
			 */
			card.put("is_follow", "0");
			String sql = "select relation from "+BasicConstants.USER_STAR_BUCKET+" where start_id = '" + userId + "' and end_id='" + starId + "'";
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET, bucketPwd).query(Query.simple(sql));
			logger.info("refreshCacheData [" + sql + "]");
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, "source_other");
				if (ls.size() > 0) {
					card.put("is_follow", ls.get(0).get("relation"));
				}
			}

			sql = "select id,c_c as count from "+BasicConstants.SOURCE_OTHER_BUCKET+" use keys [" + sb.toString() + " '']";
			queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET, bucketPwd).query(Query.simple(sql));
			logger.info("refreshCacheData count [" + sql + "]");
			if (queryResult.finalSuccess()) {
				// List<Map<String, Object>> ls =
				// CouchbaseUtil.resultToList(queryResult);
				for (QueryRow row : queryResult.allRows()) {
					String id1 = row.value().getString("id");
					for (Map<String, Object> item : list) {
						String id2 = item.get("id").toString();
						if (id1.equals(id2)) {
							item.put("count", row.value().getInt("count"));
						}
					}
				}
				// logger.info("sql is [" + sql + "]");
			} else {
				logger.warn("查询失败 " + queryResult.info());
			}
		}
		return card;
	}

	private boolean copyAndRegroup4Page(String userId) {
		long start = System.currentTimeMillis();
		Jedis redisCluster = null;
		try {
			redisCluster = RedisConnectionPool.getCluster();
			// 1 、从用户readList 中获取数据
			String key = BasicConstants.READ_LIST_MAIN_CHANNEL + ":" + userId;
			// 首页数据获取策略 - begin
			Long totalCount = redisCluster.zcard(key);
			// logger.info("key is "+key +" val is "+totalCount);
			Long endNum = (long) (totalCount * 0.85);
			if (endNum < 7) {
				endNum = totalCount;
			}
			logger.info("totalCount :" + totalCount + " ,endNum :" + endNum);
			// 首页数据获取策略 - end
			Set<Tuple> data = redisCluster.zrevrangeWithScores(key, 0, endNum);
			if (data.size() == 0) // 如果无法从readList 中获取数据 那么不进行洗牌动作
				return false;
			redisCluster.zremrangeByRank(key, -(endNum + 1), -1);
			logger.info("清空 readList for " + key + " ,endNum:" + (endNum + 1));
			String cacheKey = BasicConstants.APP_CACHE_CHANNEL + ":" + userId;
			int end = -1;
			if (data.size() < maxLengthInGroup) {
				end = maxLengthInGroup - data.size();
			}
			Set<Tuple> cardCache = redisCluster.zrevrangeWithScores(cacheKey, 0, end);
			logger.info("get data from cache [0," + end + "] ,size is " + cardCache.size());
			if (cardCache.size() > 0) {
				redisCluster.zremrangeByRank(cacheKey, 0, -1);
				logger.info("清空 appCache for " + cacheKey);
			}
			// 5、对两批数据+主题卡片组数据+热门卡片组数据 进行重组 、组内排序、组间排序
			List<Map<String, Object>> newCache = compute(data, cardCache);
			// 6、将分组完的数据插到cache中 自动根据score 做排序
			Map<String, Double> tmp = new HashMap<String, Double>();
			/*
			 * if (null == newCache) { logger.info(
			 * "not fount data in readlist for " + userId); return ; }
			 */
			for (Map<String, Object> m : newCache) {
				tmp.put(Utils.getGson().toJson(m), Double.parseDouble(m.get("create_time").toString()));
			}
			// 将新数据添加到appcache 前清除掉以前的数据
			redisCluster.zadd(cacheKey, tmp);
			logger.info("add cache[" + cacheKey + "] ,size is " + tmp.size());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("copyAndRegroup", e);
		} finally {
			if (null != redisCluster) {
				redisCluster.close();
			}
		}
		logger.info((System.currentTimeMillis() - start) + " timing for copyAndRegroup");
		return true;
	}

	private boolean copyAndRegroup(String userId, long lastCursor) {
		long start = System.currentTimeMillis();
		Jedis redisCluster = null;
		try {
			redisCluster = RedisConnectionPool.getCluster();
			// 1 、从用户readList 中获取数据
			String key = BasicConstants.READ_LIST_MAIN_CHANNEL + ":" + userId;
			// 首页数据获取策略 - begin
			Long totalCount = redisCluster.zcard(key);
			Long endNum = (long) (totalCount * 0.85);
			if (endNum < 7) {
				endNum = totalCount;
			}
			logger.info("totalCount :" + totalCount + " ,endNum :" + endNum);
			// 首页数据获取策略 - end
			Set<Tuple> data = redisCluster.zrevrangeWithScores(key, 0, endNum);
			if (data.size() == 0) // 如果无法从readList 中获取数据 那么不进行洗牌动作
				return false;
			redisCluster.zremrangeByRank(key, 0, endNum);
			logger.info("清空 readList for " + key);
			// 3、从app cache中读取最多X组数据
			long end = lastCursor + (maxLengthInGroup - (data.size() / 2));
			if (end < lastCursor) {
				end = lastCursor;
			}
			String cacheKey = BasicConstants.APP_CACHE_CHANNEL + ":" + userId;
			Set<Tuple> cardCache = redisCluster.zrevrangeWithScores(cacheKey, lastCursor, end);
			// Utils.getThreadPool().execute(new RemoveCacheAndReadList(key,
			// cacheKey));
			redisCluster.zremrangeByRank(cacheKey, 0, -1);
			logger.info("清空 appCache for " + cacheKey);

			// 5、对两批数据+主题卡片组数据+热门卡片组数据 进行重组 、组内排序、组间排序
			List<Map<String, Object>> newCache = compute(data, cardCache);
			// 6、将分组完的数据插到cache中 自动根据score 做排序
			Map<String, Double> tmp = new HashMap<String, Double>();
			/*
			 * if (null == newCache) { logger.info(
			 * "not fount data in readlist for " + userId); return ; }
			 */
			for (Map<String, Object> m : newCache) {
				tmp.put(Utils.getGson().toJson(m), Double.parseDouble(m.get("create_time").toString()));
			}
			// 将新数据添加到appcache 前清除掉以前的数据

			redisCluster.zadd(cacheKey, tmp);
			logger.info("add cache[" + cacheKey + "] ,size is " + tmp.size());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("copyAndRegroup", e);
		} finally {
			if (null != redisCluster) {
				redisCluster.close();
			}
		}
		logger.info((System.currentTimeMillis() - start) + " timing for copyAndRegroup");
		return true;
	}

	// 获取指定数量的未读信息
	private List<Map<String, Object>> getNoReadItemsFromCardPool(String userId, String channel, Set<Tuple> pool,
			int noReadSize) {
		List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
		if (null == pool) {
			return tmp;
		}
		int count = 0;
		String topicCardReadJson = null;
		Jedis js = null;
		try {
			js = RedisConnectionPool.getCluster();
			topicCardReadJson = js.hget(channel, userId);

			List<Map<String, Long>> readMap = readJson2ReadMap(topicCardReadJson);
			for (Tuple t : pool) {
				String valueJson = t.getElement();
				Map<String, Object> map = mapJson2Map(valueJson);
				if (!isReaded(map, readMap)) {
					tmp.add(map);
					count++;
				}
				if (noReadSize == count) {
					break;
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("", e);
		} finally {
			if (null != js) {
				js.close();
			}
		}
		return tmp;
	}

	private List<Map<String, Object>> compute(Set<Tuple> data, Set<Tuple> cardCache) {
		List<Map<String, Object>> readList = new ArrayList<Map<String, Object>>();
		Map<String, Object> m = null;
		for (Tuple t : data) {
			m = mapJson2Map(t.getElement());
			readList.add(m);
		}
		List<Map<String, Object>> cacheList = flatCard(cardCache); // 卡片组
		readList.addAll(cacheList);
		List<Map<String, Object>> cards = groupAndTransform(readList);
		return cards;
	}

	/*
	 * //六小时以内的与非native的全过，有评论的全过 并保证最终通过率为30% private List<Map<String, Object>>
	 * computeAndFilter(Set<Tuple> data, Set<Tuple> cardCache ,String key) {
	 * List<Map<String, Object>> readList = new ArrayList<Map<String,
	 * Object>>(); List<Map<String, Object>> readListTmp = new
	 * ArrayList<Map<String, Object>>(); Map<String, Object> m = null; float
	 * failCount = 0; int totalCount = data.size(); Map<String, Double> tmpData
	 * = new HashMap<>(); StringBuffer sb = new StringBuffer(); for (Tuple t :
	 * data) { m = mapJson2Map(t.getElement()); readList.add(m);
	 * sb.append("'"+m.get("id").toString()+"',"); }
	 * 
	 * String sql = "select c_c as count,id from source_other use keys["
	 * +sb.toString()+" '']"; QueryResult queryResult =
	 * CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).query(
	 * Query.simple(sql)); logger.info(sql); if (queryResult.finalSuccess()) {
	 * int count = 0; String id = "";
	 * 
	 * for(Map<String, Object> map: readList){ for(QueryRow r :
	 * queryResult.allRows()){ count = r.value().getInt("count"); id =
	 * r.value().getString("id"); if(id.equals(map.get("id"))){ map.put("count",
	 * count); if((failCount / totalCount) < 0.7 && ! commentsFilter(map)){
	 * failCount ++; readListTmp.add(map); break; } } } }
	 * readList.removeAll(readListTmp); } if(count > 0){ return true; }
	 * 
	 * 
	 * logger.info("failCount : "+failCount+" ; totalCount : "+totalCount);
	 * if(tmpData.size()>0){ Jedis redisCluster =
	 * RedisConnectionPool.getCluster(); try { redisCluster.zadd(key, tmpData);
	 * } catch (Exception e) { e.printStackTrace(); logger.error("",e);
	 * }finally{ redisCluster.close(); } }
	 * 
	 * List<Map<String, Object>> cacheList = flatCard(cardCache); // 卡片组
	 * readList.addAll(cacheList); List<Map<String, Object>> cards =
	 * groupAndTransform(readList); return cards; } //返回true 通过 private boolean
	 * commentsFilter(Map<String, Object> m){ long before_6H =
	 * Utils.getTimeMillisBeforeH(6); long pubTime =
	 * Long.parseLong(m.get("pub_time").toString()); Object linkType =
	 * m.get("link_type"); int count = Integer.parseInt(
	 * m.get("count").toString() ); // Object Id = m.get("id"); if(linkType !=
	 * null && !BasicConstants.NEWS_LINKED_TYPE.equals(linkType.toString())){
	 * return true; }else if(pubTime > before_6H){ return true; }else if(count >
	 * 0){ return true; } return false; }
	 */
	private List<Map<String, Object>> groupAndTransform(List<Map<String, Object>> newsItems) {
		long start = System.currentTimeMillis();
		// transform to card list model => List<Map<String, Object>>
		List<Map<String, Object>> cardLs = null;
		List<String> filterLs = new ArrayList<>();
		try {
			Map<String, List<Map<String, Object>>> groups = new HashMap<String, List<Map<String, Object>>>();
			List<Map<String, Object>> tmpLs = null;
			Object title = null;
			for (Map<String, Object> m : newsItems) {
				// logger.info("info message is "+m);
				// 过滤掉 title 重复的数据
				title = m.get("news_title");
				if (null != title && filterLs.contains(title.toString().trim())) {
					logger.warn("fount repeated data ,title is " + title);
					continue;
				}
				filterLs.add(title.toString().trim());
				Object starName = m.get("name");
				Object starObj = m.get("star");
				if (null == starName && null == starObj) {
					continue;
				}
				String g = (starName == null ? starObj.toString() : starName.toString());
				if (filterByName(g)) {
					continue;
				}
				if (groups.containsKey(g)) {
					List<Map<String, Object>> ls = (List<Map<String, Object>>) groups.get(g);
					ls.add(m);
				} else {
					tmpLs = new ArrayList<Map<String, Object>>();
					tmpLs.add(m);
					groups.put(g, tmpLs);
				}
			}
			cardLs = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> items = null;
			Map<String, Object> card = null;
			for (String key : groups.keySet()) {
				// 拿到当前组的所有数据
				List<Map<String, Object>> ls = groups.get(key);
				// 组内排序
				/*
				 * // 排序改在刷新dispalyTime 后做 Collections.sort(ls, new
				 * Comparator<Map<String, Object>>() { public int
				 * compare(Map<String, Object> arg0, Map<String, Object> arg1) {
				 * // 从大到小排序 return
				 * arg1.get("pub_time").toString().compareTo(arg0.get("pub_time"
				 * ).toString()); } });
				 */
				items = new ArrayList<Map<String, Object>>();
				card = new HashMap<String, Object>();
				// Long max = 0L;
				int count = 0;
				for (Map<String, Object> m : ls) {
					items.add(m);
					// Long score =
					// Long.parseLong(m.get("pub_time").toString());
					/*
					 * if (score > max) { max = score; }
					 */
					count++;
					if (count == newsLenthInCard) {
						card = transformCard(items, null);
						// max = 0L;
						count = 0;
						cardLs.add(card);
						items = new ArrayList<Map<String, Object>>();
						card = new HashMap<String, Object>();
					}
				}
				if (0 < items.size()) {
					card = transformCard(items, null);
					cardLs.add(card);
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("groupAndTransform", e);
		}
		logger.info(System.currentTimeMillis() - start + " timing for groupAndTransform");
		return cardLs;
	}

	private boolean filterByName(String g) {
		g = g.trim();
		if (g.length() < 2 || g.matches("\\d+")) {
			logger.info("filter " + g);
			return true;
		}
		return false;
	}

	// 将卡片组数据打撒 方便再排序
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> flatCard(Set<Tuple> cards) {
		long start = System.currentTimeMillis();
		List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();

		for (Tuple t : cards) {
			Map<String, Object> card = mapJson2Map(t.getElement());
			List<Map<String, Object>> list = (List<Map<String, Object>>) card.get("data");
			ls.addAll(list);
		}
		logger.info(System.currentTimeMillis() - start + " timing for transform");
		return ls;
	}

	private void removeRecordsBeforeInReadForUser(String cardReadJson, String userId, String channel, int days) {
		long start = System.currentTimeMillis();
		List<Map<String, Long>> readLs = null; // 用户已读列表模型 map_key = newsId
												// value = time
		if (null == cardReadJson) {
			readLs = new ArrayList<Map<String, Long>>();
		} else {
			readLs = Utils.getGson().fromJson(cardReadJson, new TypeToken<List<Map<String, Long>>>() {
				private static final long serialVersionUID = 1L;
			}.getType());
		}
		long day_3 = Utils.getTimeMillisBefore(days);
		List<Map<String, Long>> tmp = new ArrayList<>();
		for (Map<String, Long> m : readLs) {
			for (String k : m.keySet()) {
				if (m.get(k) < day_3) {
					tmp.add(m);
				}
			}
		}
		if (tmp.size() == 0) {
			return;
		}
		readLs.removeAll(tmp);
		// 从readLs 中移除已过期的数据
		String json = Utils.getGson().toJson(readLs);
		Jedis js = null;
		try {
			js = RedisConnectionPool.getCluster();
			js.hset(channel, userId, json);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("", e);
		} finally {
			if (null != null) {
				js.close();
			}
		}

		logger.debug("remove [" + json + "] for " + userId);
		logger.info(System.currentTimeMillis() - start + " timing for removeRecordsBeforeInReadForUser ");
	}

	// 话题 (3天的 每天一个)、广告（每天一个未下线的，下线的从redis删除）、人物组（top3）
	// 注 每一个 卡片 的信息必须有对应的 id 字段
	// 每张卡片 在redis 中都是一个json str
	private List<Map<String, Object>> jumpQueue_3(List<Map<String, Object>> newsData, String userId) {
		// 刷新 count recoment_count
		long start = System.currentTimeMillis();
		// RemoveRecordsThread removeThread = new RemoveRecordsThread();
		Map<String, Object> userAttr = null;
		/*JsonDocument userDoc = CBaseConnectionPool.getBucket(BasicConstants.USER_ATTRIBUTE_BUCKET).get(userId);
		if (null != userDoc) {
			userAttr = userDoc.content().toMap();
		} else {
			logger.info("not found user[" + userId + "] other attribute...");
		}*/

		Jedis redisCluster = null;
		List<Map<String, Object>> rels = new ArrayList<>();
		try {
			redisCluster = RedisConnectionPool.getCluster();
			long before_3 = Utils.getTimeMillisBefore(days);
			long before_2 = Utils.getTimeMillisBefore(days - 1);// theme card
			Set<String> topicCardJson = redisCluster.zrevrangeByScore(BasicConstants.TOPIC_CARD_CHANNEL, Long.MAX_VALUE,
					before_3);
			String topicCardReadJson = redisCluster.hget(BasicConstants.TOPIC_CARD_READ, userId);
			List<Map<String, Object>> topicQueue = getJumpData(topicCardJson, topicCardReadJson, 1, userAttr, true);
			Map<String, Object> topicCard = null;
			if (topicQueue.size() > 0) {
				// 此处可改为异步处理
				topicCard = new HashMap<String, Object>();
				// removeThread.add(new RemoveRecords(topicCardReadJson, userId,
				// BasicConstants.TOPIC_CARD_READ, 3));
				removeRecordsBeforeInReadForUser(topicCardReadJson, userId, BasicConstants.TOPIC_CARD_READ, 3);
				// removeRecordsBeforeInReadForUser(topicCardReadJson, userId,
				// BasicConstants.TOPIC_CARD_READ, 3);
				// todo 组内排序
				dataLen += topicQueue.size();
				topicCard = transformCard(topicQueue, null);
			}

			/*
			 * Set<String> adCardJson =
			 * redisCluster.zrevrangeByScore(BasicConstants.AD_CARD_CHANNEL,
			 * Long.MAX_VALUE, before_3);
			 */
			Set<String> adCardJson = redisCluster.zrevrange(BasicConstants.AD_CARD_CHANNEL, 0, -1);
			String adCardReadJson = redisCluster.hget(BasicConstants.AD_CARD_READ, userId);
			List<Map<String, Object>> adQueue = getJumpData(adCardJson, adCardReadJson, 1, userAttr, true);
			Map<String, Object> adCard = null;
			if (adQueue.size() > 0) {
				adCard = new HashMap<String, Object>();
				// removeThread.add(new RemoveRecords(adCardReadJson, userId,
				// BasicConstants.AD_CARD_READ, 3));
				removeRecordsBeforeInReadForUser(adCardReadJson, userId, BasicConstants.AD_CARD_READ, 3);
				dataLen += adQueue.size();
				adCard = transformCard(adQueue, null);
			}

			// 每天最多推20位
			Set<String> starCardJson = redisCluster.zrevrange(BasicConstants.STAR_CARD_PREFIX + ":" + userId, 0, 100);
			String starCardReadJson = redisCluster.hget(BasicConstants.STAR_CARD_READ, userId);
			List<Map<String, Object>> starQueue = getJumpDataForStarCard(starCardJson, starCardReadJson, starsTopSize,
					userAttr, false);
			Map<String, Object> starCard = null;
			if (starQueue.size() > 0) {
				starCard = new HashMap<String, Object>();
				
				// refreshCacheData(starQueue);
				dataLen += starQueue.size();
				starCard = transformStarCard(starQueue);
			}
			 removeRecordsBeforeInReadForUser(starCardReadJson, userId,
					 BasicConstants.STAR_CARD_READ, 1);
			// 获取3天内未读 主题卡片与 3天内未读的 热门卡片 每6小时读取一次
			String themeCardKey = BasicConstants.THEME_CARD_CHANNEL;
			Set<String> themeJson = redisCluster.zrevrangeByScore(themeCardKey, Long.MAX_VALUE, before_2);
			String themeCardReadJson = redisCluster.hget(BasicConstants.THEME_CARD_READ, userId);
			List<Map<String, Object>> themeQueue = getJumpData(themeJson, themeCardReadJson, 1, userAttr, true);
			Map<String, Object> themeCard = null;
			if (themeQueue.size() > 0) {
				themeCard = new HashMap<String, Object>();
				// removeThread.add(new RemoveRecords(themeCardReadJson, userId,
				// BasicConstants.THEME_CARD_READ, 3));
				removeRecordsBeforeInReadForUser(themeCardReadJson, userId, BasicConstants.THEME_CARD_READ, 3);
				// removeRecordsBeforeInReadForUser(themeCardReadJson, userId,
				// BasicConstants.THEME_CARD_READ, 3);
				// -refreshCacheData(themeQueue);
				dataLen += themeQueue.size();
				themeCard = transformCard(themeQueue, null);
			}
			// 获取5条 未读的热门信息
			String hotCardKey = "";
			Set<Tuple> hotCard = null;
			List<Map<String, Object>> hotLs = new ArrayList<Map<String, Object>>();
			int days = 0;
			// int size = hotCardsSize;
			Map<String, Object> hotCardMap = new LinkedHashMap<String, Object>();
			int c = Integer.parseInt(DateUtil.format(new Date(), "HH")) / 6;
			int defaultDays = 1;
			if (c < 3) {
				defaultDays = 2;
			}
			while (hotLs.size() < hotCardsSize) {
				long ts = DateUtil.getTimeMillisBefore(days);
				for (int i = c; i >= 0; i--) {
					hotCardKey = BasicConstants.HOT_CARD_PREFIX + "_" + DateUtil.format(new Date(ts), "dd") + "_" + i;
					hotCard = redisCluster.zrevrangeWithScores(hotCardKey, 0, -1);
					logger.debug(hotCardKey + "---" + hotCard);
					hotLs.addAll(getNoReadItemsFromCardPool(userId, BasicConstants.HOT_CARD_READ, hotCard,
							hotCardsSize - hotLs.size()));
					if (hotLs.size() >= hotCardsSize) {
						break;
					}
				}
				c = 3;
				days++;
				if (days == defaultDays) {
					break;
				}
			}
			String hotCardReadJson = redisCluster.hget(BasicConstants.HOT_CARD_READ, userId);
			if (hotLs.size() > 0) {
				dataLen += hotLs.size();
				hotCardMap = transformCard(hotLs, BasicConstants.HOT_CARD_NAME);
				hotCardMap = refreshCacheData(hotCardMap, userId);
				// removeThread.add(new RemoveRecords(hotCardReadJson, userId,
				// BasicConstants.HOT_CARD_READ, 3));
				removeRecordsBeforeInReadForUser(hotCardReadJson, userId, BasicConstants.HOT_CARD_READ, 3);
			}
			rels = insertQueueAndMarkRead(userId, newsData, topicCard, adCard, starCard, themeCard, hotCardMap);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("", e);
		} finally {
			if (null != redisCluster) {
				redisCluster.close();
			}
		}

		// Utils.getThreadPool().execute(removeThread);
		logger.info(System.currentTimeMillis() - start + " timing for jumpQueue_3");
		return rels;
	}

	private String getGroupId() {
		return sdf.format(new Date()) + Utils.getRandomId_6();
	}

	private Map<String, Object> transformCard(List<Map<String, Object>> items, String name) {
		Map<String, Object> card = new HashMap<String, Object>();
		Map<String, Object> item = items.get(0);
		Object nameObj = item.get("name") == null ? item.get("star") : item.get("name");
		Object groupObj = item.get("group");
		Object groupId = item.get("group_id");
		card.put("data", items);
		// card.put("group", groupObj == null ? item.get("star") : groupObj);
		card.put("group", groupObj);
		card.put("group_id", groupId == null ? getGroupId() : groupId);
		// card.put("score", item.get("pub_time"));
		card.put("img", item.get("img"));// 其它卡片可为空
		card.put("style", item.get("style"));
		card.put("name", name == null ? nameObj : name);
		card.put("intro", item.get("intro") == null ? "" : item.get("intro"));
		card.put("badge", item.get("badge"));
		card.put("star_id", item.get("star_id")); // 其它卡片可为空
		// card.put("create_time", DateUtil.format(new
		// Date(Long.parseLong(item.get("pub_time").toString())) ,"yyyy-MM-dd
		// HH:mm:ss"));
		card.put("create_time", item.get("pub_time") == null ? "" : item.get("pub_time"));
		return card;
	}

	private Map<String, Object> transformStarCard(List<Map<String, Object>> items) {
		Map<String, Object> card = new HashMap<String, Object>();
		Map<String, Object> item = items.get(0);
		// Object nameObj = item.get("name");
		Object groupObj = item.get("group");
		card.put("data", items);
		card.put("group", groupObj == null ? "stars" : groupObj);
		card.put("group_id", getGroupId());
		// card.put("score", item.get("pub_time"));
		card.put("img", item.get("img"));
		card.put("style", item.get("style"));
		card.put("name", BasicConstants.STAR_CARD_NAME);
		card.put("intro", item.get("intro") == null ? "" : item.get("intro"));
		card.put("badge", item.get("badge"));
		card.put("star_id", item.get("star_id"));
		card.put("create_time", item.get("pub_time") == null ? "" : item.get("pub_time"));
		return card;
	}

	private List<Map<String, Long>> readJson2ReadMap(String json) {
		if (null == json) {
			return new ArrayList<>();
		}
		return Utils.getGson().fromJson(json, new TypeToken<List<Map<String, Long>>>() {
			private static final long serialVersionUID = 1L;
		}.getType());
	}

	private Map<String, Object> mapJson2Map(String json) {
		return Utils.getGson().fromJson(json, new TypeToken<Map<String, Object>>() {
			private static final long serialVersionUID = 1L;
		}.getType());
	}

	// 找不到要插队的数据 返回 0 size:取多少条
	private List<Map<String, Object>> getJumpData(Set<String> cardJson, String cardReadJson, int size,
			Map<String, Object> userAttr, boolean checkStrategy) {
		List<Map<String, Object>> jumpData = new ArrayList<Map<String, Object>>();
		List<Map<String, Long>> readMap = null;
		// 对所有满足条件的卡片咨询 进行那么 分组 属于同一那么 的卡片优先在一起
		Map<String, List<Map<String, Object>>> nameCount = new LinkedHashMap<String, List<Map<String, Object>>>();
		for (String item : cardJson) {
			try {
				Map<String, Object> itemMap = mapJson2Map(item);
				if (cardReadJson != null) {
					readMap = readJson2ReadMap(cardReadJson);
					// 校验是否读取 && 校验当前卡片信息是否满足规则
					if (isReaded(itemMap, readMap)) {
						logger.debug("this message is readed : [" + itemMap + "]");
						continue;
					}
				}
				String key = itemMap.get("name").toString();
				if (checkStrategy) {
					Object infoIdObj = itemMap.get("id");
					Object gp = itemMap.get("group");
					if (null != gp && null != infoIdObj
							&& !checkStrategy(itemMap.get("id").toString(), key, gp.toString(), userAttr)) {
						logger.warn("discard ,checkStrategy faild and info_id is " + infoIdObj);
						continue;
					} else {
						logger.warn("'group' may be empty ,card item is  " + item);
					}
				}
				jumpData = nameCount.get(key);
				if (null == jumpData) {
					jumpData = new ArrayList<Map<String, Object>>();
				}
				jumpData.add(itemMap);
				if (jumpData.size() >= size) {
					break;
				}
			} catch (Exception e) {
				logger.warn("card item is error [" + item + "] and cardReadJson is [" + cardReadJson + "]");
				logger.error(" ", e);
			}
		}
		if (jumpData.size() < size && nameCount != null) {
			int max = 0;
			for (String k : nameCount.keySet()) {
				List<Map<String, Object>> tmp = nameCount.get(k);
				if (max < tmp.size()) {
					max = tmp.size();
					jumpData = tmp;
				}
			}
		}
		return jumpData;
	}

	// 找不到要插队的数据 返回 0 size:取多少条
	private List<Map<String, Object>> getJumpDataForStarCard(Set<String> cardJson, String cardReadJson, int size,
			Map<String, Object> userAttr, boolean checkStrategy) {
		List<Map<String, Object>> jumpData = new ArrayList<Map<String, Object>>();
		List<Map<String, Long>> readMap = null;
		// 对所有满足条件的卡片咨询 进行那么 分组 属于同一那么 的卡片优先在一起
		try {
			for (String item : cardJson) {
				Map<String, Object> itemMap = mapJson2Map(item);
				if (cardReadJson != null) {
					readMap = readJson2ReadMap(cardReadJson);
					// 校验是否读取 && 校验当前卡片信息是否满足规则
					if (isReaded(itemMap, readMap)) {
						logger.debug("this message is readed : [" + itemMap + "]");
						continue;
					}
				}
				String key = itemMap.get("name").toString();
				if (checkStrategy) {
					Object infoIdObj = itemMap.get("id");
					if (null != infoIdObj && !checkStrategy(itemMap.get("id").toString(), key,
							itemMap.get("group").toString(), userAttr)) {
						logger.warn("discard ,checkStrategy faild and info_id is " + infoIdObj);
						continue;
					}
				}
				// jumpData = nameCount.get(key);
				if (null == jumpData) {
					jumpData = new ArrayList<Map<String, Object>>();
				}
				jumpData.add(itemMap);
				if (jumpData.size() >= size) {
					break;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jumpData;
	}

	// 策略是应用到 infoId > Name > group
	private boolean checkStrategy(String infoId, String name, String group, Map<String, Object> userAttr) {
		Map<String, Object> strategyMap = getStrategyById(infoId, name, group);
		if (null == strategyMap) {
			logger.info("not found any rules for [" + infoId + "," + name + "," + group + "]");
			return true;
		} else if (userAttr == null) {
			logger.info("user attribute is null for checkStrategy...");
			return false;
		}
		logger.info("userAttr:" + userAttr);
		logger.info("strategyMap:" + strategyMap);
		Regular regular = new Regular(strategyMap, userAttr);
		StatefulKnowledgeSession ks = KSessionManager.getKSession();
		ks.insert(regular);
		ks.fireAllRules();
		logger.info("checkStrategy result is [" + regular.isOk() + "] and " + regular.getDesc());
		return regular.isOk();
	}

	// 根据每条卡片数据的id 与操作时间 拿到策略json
	public Map<String, Object> getStrategyById(String infoId, String name, String group) {
		Map<String, Object> strateg = null;
	/*	String currentDate = DateUtil.getCurrentTime("yyyy-MM-dd HH:mm:ss");
		String sql = "select * from bk_config where state = '1' and info_id = '" + infoId + "' and '" + currentDate
				+ "' between time_range.s and time_range.e";
		QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
		logger.info(sql);
		if (queryResult.finalSuccess()) {
			List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, "bk_config");
			if (ls.size() > 0) {
				strateg = ls.get(0);
				logger.info("sql is [" + sql + "]");
			} else {
				sql = "select * from bk_config where c_name   = '" + name + "' and '" + currentDate
						+ "' between time_range.start and time_range.end";
				logger.info(sql);
				queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
				ls = CouchbaseUtil.resultToList(queryResult, "bk_config");
				if (ls.size() > 0) {
					strateg = ls.get(0);
					logger.info("sql is [" + sql + "]");
				} else {
					sql = "select * from bk_config where c_group  = '" + group + "' and '" + currentDate
							+ "' between time_range.start and time_range.end";
					logger.info(sql);
					queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd).query(Query.simple(sql));
					ls = CouchbaseUtil.resultToList(queryResult, "bk_config");
					if (ls.size() > 0) {
						strateg = ls.get(0);
						logger.info("sql is [" + sql + "]");
					}
				}
			}
		}*/
		return strateg;
	}

	// 根据用户ID 获取用户属性信息
	public Map<String, Object> getUserAttrById(String userId) {
		Map<String, Object> strateg = null;
		/*JsonDocument doc = CBaseConnectionPool.getBucket(BasicConstants.USER_ATTRIBUTE_BUCKET).get(userId);
		if (null != doc) {
			strateg = doc.content().toMap();
		}*/
		return strateg;
	}

	@SuppressWarnings("unchecked")
	private void markReadForUser(String userId, String readedChanel, Map<String, Object> card, Long time) {
		Jedis js = null;
		try {
			js = RedisConnectionPool.getCluster();
			String readLsJson = js.hget(readedChanel, userId);
			List<Map<String, Long>> readLs = null; // 用户已读列表模型 map_key = newsId
													// value = time
			if (null == readLsJson) {
				readLs = new ArrayList<Map<String, Long>>();
			} else {
				readLs = readJson2ReadMap(readLsJson);
			}

			// String newsId = card.get("id").toString();
			List<Map<String, Object>> items = (List<Map<String, Object>>) card.get("data");
			if (null == items) {
				return;
			}
			for (Map<String, Object> item : items) {
				boolean isContains = false;
				Object newsId = item.get("id");
				if (null == newsId) {
					continue;
				}
				for (Map<String, Long> m : readLs) {
					if (m.containsKey(newsId)) {
						isContains = true;
						break;
					}
				}
				if (!isContains) {
					Map<String, Long> m = new HashMap<String, Long>();
					m.put(newsId.toString(), time);
					readLs.add(m);
					String json = Utils.getGson().toJson(readLs);
					js.hset(readedChanel, userId, json);
					logger.debug("mark readed [" + json + "] for " + userId + " in " + readedChanel);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != js) {
				js.close();
			}
		}

	}

	private List<Map<String, Object>> insertQueueAndMarkRead(String userId, List<Map<String, Object>> data,
			Map<String, Object> topicCard, Map<String, Object> adCard, Map<String, Object> starCard,
			Map<String, Object> themeCard, Map<String, Object> hotCard) {
		// MarkReadThread markThread = new MarkReadThread();
		if (null != adCard && adCard.size() != 0 && data.size() > (adPosition - 1)) {
			data.add(adPosition, adCard);
			// markReadForUser(userId, BasicConstants.AD_CARD_READ, adCard,
			// markThread.add(new MarkRead(userId, BasicConstants.AD_CARD_READ,
			// adCard, System.currentTimeMillis()));
			markReadForUser(userId, BasicConstants.AD_CARD_READ, adCard, System.currentTimeMillis());
		} else {
			logger.debug("not show , position size is " + adPosition + " and adCard: " + adCard);
		}
		if (null != topicCard && topicCard.size() != 0 && data.size() > (topicPosition - 1)) {
			data.add(topicPosition, topicCard);
			// markReadForUser(userId, BasicConstants.TOPIC_CARD_READ,
			// topicCard, System.currentTimeMillis());
			// markThread.add(new MarkRead(userId,
			// BasicConstants.TOPIC_CARD_READ, topicCard,
			// System.currentTimeMillis()));
			markReadForUser(userId, BasicConstants.TOPIC_CARD_READ, topicCard, System.currentTimeMillis());
		} else {
			logger.debug("not show , position size is " + topicPosition + " and topicCard: " + topicCard);
		}

		if (null != starCard && starCard.size() != 0 && data.size() > (starPosition - 1)) {
			data.add(starPosition, starCard);
			// 明星直接删除
			// markReadForUser(userId, BasicConstants.STAR_CARD_READ, starCard,
			// System.currentTimeMillis());
			// markThread.add(new MarkRead(userId,
			// BasicConstants.STAR_CARD_READ, starCard,
			// System.currentTimeMillis()));
			markReadForUser(userId, BasicConstants.STAR_CARD_READ, starCard, System.currentTimeMillis());
		} else {
			logger.debug("not show , position size is " + starPosition + " and starCard: " + starCard);
		}
		if (null != themeCard && themeCard.size() != 0 && data.size() > (themPosition - 1)) {
			data.add(themPosition, themeCard);
			// markReadForUser(userId, BasicConstants.THEME_CARD_READ,
			// themeCard, System.currentTimeMillis());
			refreshCacheData(themeCard, userId);
			/*
			 * markThread .add(new MarkRead(userId,
			 * BasicConstants.THEME_CARD_READ, themeCard,
			 * System.currentTimeMillis()));
			 */
			markReadForUser(userId, BasicConstants.THEME_CARD_READ, themeCard, System.currentTimeMillis());
		} else {
			logger.debug("not show , position size is " + themPosition + " and themeCard: " + themeCard);
		}
		if (null != hotCard && hotCard.size() != 0 && data.size() > (hotPosition - 1)) {
			data.add(hotPosition, hotCard);
			// markReadForUser(userId, BasicConstants.HOT_CARD_READ, hotCard,
			// System.currentTimeMillis());
			refreshCacheData(hotCard, userId);
			// markThread.add(new MarkRead(userId, BasicConstants.HOT_CARD_READ,
			// hotCard, System.currentTimeMillis()));
			markReadForUser(userId, BasicConstants.HOT_CARD_READ, hotCard, System.currentTimeMillis());
		} else {
			logger.debug("not show , position size is " + hotPosition + " and hotCard: " + hotCard);
		}
		// Utils.getThreadPool().execute(markThread);
		return data;
	}

	// 移除readList 中很久没读取的数据

	// 话题、广告、人物组， 已读取列表放在 map<userId ,List<newsId>> 中，过期时间都是4天 (去重)，保证统一资源信息
	// 4天内不会重复读取
	private boolean isReaded(Map<String, Object> topicJson, List<Map<String, Long>> readIds) {
		try {
			// List<Map<String, Long>> readIds = ls.get(userId);
			Object newsId = topicJson.get("id");
			if (null == newsId) {
				logger.warn("discard ,not found newsId in card data , mark readed");
				return true;
			}
			if (null == readIds) {
				logger.debug("readed cache is null ");
				return false;
			}
			for (Map<String, Long> m : readIds) {
				if (m.containsKey(newsId.toString())) {
					return true;
				}
			}
			return false;
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			logger.error("isReaded", e);
			return false;
		}
	}
	/*
	 * private String getAll(Request request) { long start =
	 * System.currentTimeMillis(); ResultMsg resultObject = new ResultMsg();
	 * String userId = request.getParam("userId"); String channelId =
	 * request.getParam("channel"); if (userId == null || channelId == null) {
	 * resultObject.addReturnCode(CodeManager._CODE_ERROR);
	 * resultObject.addReturnDesc("缺失参数:" + request.getParams()); } else {
	 * String key = userId + ":" + channelId;
	 * resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
	 * resultObject.addResultData(RedisConnectionPool.getCluster().
	 * zrangeWithScores(key, 0, -1)); logger.info("执行成功 ,params is [" +
	 * request.getParams() + "]"); // app 查询readlist后 对readlist 内容进行清理
	 * RedisConnectionPool.getCluster().zremrangeByRank(key, 0, -1); }
	 * resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() -
	 * start); logger.info((System.currentTimeMillis() - start) + " timing");
	 * return resultObject.toJson(); }
	 */
}

/*
 * class RemoveCacheAndReadList implements Runnable { private static Logger
 * logger = LoggerFactory.getLogger(MarkReadThread.class); String readListKey =
 * ""; String appCacheKey = "";
 * 
 * RemoveCacheAndReadList(String readListKey, String appCacheKey) {
 * this.readListKey = readListKey; this.appCacheKey = appCacheKey; }
 * 
 * public void run() {
 * RedisConnectionPool.getCluster().zremrangeByRank(readListKey, 0, -1);
 * logger.info("清空 readList for " +readListKey);
 * RedisConnectionPool.getCluster().zremrangeByRank(appCacheKey, 0, -1);
 * logger.info("清空 appCache for " +appCacheKey); } }
 */
/*
 * class MarkReadThread1 implements Runnable { private static Logger logger =
 * LoggerFactory.getLogger(MarkReadThread.class); List<MarkRead> ls = new
 * LinkedList<MarkRead>(); Jedis js = null;
 * 
 * public void add(MarkRead m) { ls.add(m); }
 * 
 * public void run() { logger.info("start MarkReadThread "); try { js =
 * RedisConnectionPool.getCluster(); for (MarkRead m : ls) {
 * markReadForUser(m.getUserId(), m.getCardReadChanel(), m.getCard(),
 * m.getTime()); } } catch (Throwable e) { e.printStackTrace(); } finally { if
 * (null != js) { js.close(); } } }
 * 
 * @SuppressWarnings("unchecked") private void markReadForUser(String userId,
 * String readedChanel, Map<String, Object> card, Long time) {
 * 
 * String readLsJson = js.hget(readedChanel, userId); List<Map<String, Long>>
 * readLs = null; // 用户已读列表模型 map_key = newsId // value = time if (null ==
 * readLsJson) { readLs = new ArrayList<Map<String, Long>>(); } else { readLs =
 * readJson2ReadMap(readLsJson); }
 * 
 * // String newsId = card.get("id").toString(); List<Map<String, Object>> items
 * = (List<Map<String, Object>>) card.get("data"); if (null == items) { return;
 * } for (Map<String, Object> item : items) { boolean isContains = false; Object
 * newsId = item.get("id"); if (null == newsId) { continue; } for (Map<String,
 * Long> m : readLs) { if (m.containsKey(newsId)) { isContains = true; break; }
 * } if (!isContains) { Map<String, Long> m = new HashMap<String, Long>();
 * m.put(newsId.toString(), time); readLs.add(m); String json =
 * Utils.getGson().toJson(readLs); js.hset(readedChanel, userId, json);
 * logger.debug("mark readed [" + json + "] for " + userId + " in " +
 * readedChanel); } }
 * 
 * }
 * 
 * private List<Map<String, Long>> readJson2ReadMap(String json) { return
 * Utils.getGson().fromJson(json, new TypeToken<List<Map<String, Long>>>() {
 * private static final long serialVersionUID = 1L; }.getType()); } }
 * 
 * class RemoveRecordsThread1 implements Runnable { private static Logger logger
 * = LoggerFactory.getLogger(RemoveRecordsThread.class); List<RemoveRecords> ls
 * = new LinkedList<RemoveRecords>(); Jedis js = null;
 * 
 * public void add(RemoveRecords r) { ls.add(r); }
 * 
 * @Override public void run() { try { js = RedisConnectionPool.getCluster();
 * for (RemoveRecords r : ls) {
 * removeRecordsBeforeInReadForUser(r.getCardReadJson(), r.getUserId(),
 * r.getCardReadedChanel(), r.getSize()); } } catch (Throwable e) {
 * e.printStackTrace(); if (null != js) { js.close(); } } }
 * 
 * private void removeRecordsBeforeInReadForUser(String cardReadJson, String
 * userId, String channel, int days) { long start = System.currentTimeMillis();
 * List<Map<String, Long>> readLs = null; // 用户已读列表模型 map_key = newsId // value
 * = time if (null == cardReadJson) { readLs = new ArrayList<Map<String,
 * Long>>(); } else { readLs = Utils.getGson().fromJson(cardReadJson, new
 * TypeToken<List<Map<String, Long>>>() { private static final long
 * serialVersionUID = 1L; }.getType()); } long day_3 =
 * Utils.getTimeMillisBefore(days); Map<String, Long> tmp = new HashMap<String,
 * Long>(); for (Map<String, Long> m : readLs) { for (String k : m.keySet()) {
 * if (m.get(k) < day_3) { tmp.putAll(m); } } } if (tmp.keySet().size() == 0) {
 * return; } readLs.remove(tmp); // 从readLs 中移除已过期的数据 String json =
 * Utils.getGson().toJson(readLs);
 * 
 * js.hset(channel, userId, json);
 * 
 * logger.debug("remove [" + json + "] for " + userId);
 * logger.info(System.currentTimeMillis() - start +
 * " timing for removeRecordsBeforeInReadForUser "); } }
 */