package cn.qctt.httpserver.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.utils.KafkaProducer;
import cn.qctt.model.News;

@RequestMapping(BasicConstants.URL_PREFIX+"/flowTest")
public class FlowTestHandler implements Handler {
    private Logger logger = LoggerFactory.getLogger(FlowTestHandler.class);
    @Override
    public String invoke(Request request, Response response) {
    	long start = System.currentTimeMillis();
        Gson gson = new Gson();
        ResultMsg resultMsg = new ResultMsg();

        String json = new String(request.getContent());

        String id = null;
        News news;
        try {
            news = gson.fromJson(json, News.class);
            
            if(news != null){
            	id = news.getId();
            }
        } catch (Exception e) {
            logger.info("invoke" ,e);
            logger.info("error json " + json);
            resultMsg.addReturnCode(CodeManager.FLOWTEST);
            resultMsg.addReturnDesc(e.getMessage());
            return resultMsg.toJson();
        }

//        KafkaProducer producer = new KafkaProducer();
        String topic = ConfigInit.getValue("kafkaOutput");
        logger.debug(topic +" :kafka send recoder => "+ news);
        KafkaProducer.send(news, topic);
        logger.info("send "+topic +" "+news.getId());
        resultMsg.addReturnDesc("ok");
        resultMsg.addResultData(id);
        resultMsg.putHead("count",1);
//        resultMsg.addReturnCode(CodeManager._CODE_SUCCESS);
        resultMsg.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
        logger.info((System.currentTimeMillis() - start)+" timing");
        return resultMsg.toJson();
    }
}
