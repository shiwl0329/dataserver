package cn.qctt.httpserver.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.util.StringUtils;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import cn.qctt.httpserver.utils.DateUtil;
import cn.qctt.httpserver.utils.EmailUtil;
import cn.qctt.httpserver.utils.HBaseUtil;
import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;
import rx.Observable;
import rx.functions.Func1;

@RequestMapping(BasicConstants.URL_PREFIX + "/backManager")
public class BackManagerHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(BackManagerHandler.class);
	private static String upConfigUrl = BasicConstants.URL_PREFIX + "/backManager/upsertConfig";
	private static String getConfigUrlByIdUrl = BasicConstants.URL_PREFIX + "/backManager/getConfigUrlById";
	private static String commentSwitchUrl = BasicConstants.URL_PREFIX + "/backManager/commentSwitch";
	private static String pushMsgToReadListUrl = BasicConstants.URL_PREFIX + "/backManager/pushMsgToReadList";
//	private static String sendForNewUserUrl = BasicConstants.URL_PREFIX + "/backManager/sendForNewUser";
	private static String starHomeUrl = BasicConstants.URL_PREFIX + "/backManager/1starHome";
	private static String entityHomeListUrl = BasicConstants.URL_PREFIX + "/backManager/entityHomeList";
//	private static String starHomeNewListUrl = BasicConstants.URL_PREFIX + "/backManager/starHomeNewList";
	private static String getNewsByIdsWithoutContentUrl = BasicConstants.URL_PREFIX + "/backManager/getNewsByIdsWithoutContent";
	private static String starHomeLenUrl = BasicConstants.URL_PREFIX + "/backManager/1starHomeLen";
	private static String addCardUrl = BasicConstants.URL_PREFIX + "/backManager/addCard";
//	private static String addCardToEsUrl = BasicConstants.URL_PREFIX + "/backManager/addCardToEs";
//	private static String updateCardInEsUrl = BasicConstants.URL_PREFIX + "/backManager/updateCardInEs";
	private static String delCardUrl = BasicConstants.URL_PREFIX + "/backManager/1delCard";
//	private static String delCardInEsUrl = BasicConstants.URL_PREFIX + "/backManager/delCardInEs";
	private static String topNStarsUrl = BasicConstants.URL_PREFIX + "/backManager/1topNStars";
	private static String delHTableUrl = BasicConstants.URL_PREFIX + "/backManager/delHTable";
	private static String resetRedisUrl = BasicConstants.URL_PREFIX + "/backManager/rr";
	private static String newsListUrl = BasicConstants.URL_PREFIX + "/backManager/1newsList";
	private static String starDynamicUrl = BasicConstants.URL_PREFIX + "/backManager/1starDynamic";
	private static String playerDynamicUrl = BasicConstants.URL_PREFIX + "/backManager/playerDynamic";
	private static String cacheUserTagsUrl = BasicConstants.URL_PREFIX + "/backManager/cacheUserTags";
	private static String getThemesByIdUrl = BasicConstants.URL_PREFIX + "/backManager/getThemesById";
	
	// private static String getByIdUrl = "/backManager/getById";
	// private static String hqlUrl = "/backManager/hql";
//	private static JsonParser jsonParser = new JsonParser();
	private static String sourceOtherBucket = BasicConstants.SOURCE_OTHER_BUCKET;
//	private static String bkConfigBucket = BasicConstants.BK_CONFIG_BUCKET;
	private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
//	private static JdbcTemplate connectionUtil2 = ConnectionUtil.getMysqlConnect();
	
//	private static String INDEX_GENERALIZE = "generalize";
	private static String FILED_PREFIX = "refreshStarHome";
	
	
	/**
	 * 3.* 新版添加接口
	 */
	private static String deleteEntityIndexByIdsUrl = BasicConstants.URL_PREFIX + "/backManager/deleteEntityIndexByIds";
	private static String upsertEntityIndexUrl = BasicConstants.URL_PREFIX + "/backManager/upsertEntityIndex";
	private static String entityDynamicUrl = BasicConstants.URL_PREFIX + "/backManager/entityDynamic";
	private static String sendMailUrl = BasicConstants.URL_PREFIX + "/backManager/mail";
	private static String tbarHomeListUrl = BasicConstants.URL_PREFIX + "/backManager/tbarHomeList";
	
	
//	private JsonParser parser = new JsonParser();

	// private static String sourceOtherBucketPwd = "";
	// private static final int maxSendSize = 100;
	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(upConfigUrl)) {
//			returnStr = upConfig(request);
		} /*else if (urlPath.startsWith(addCardToEsUrl)) {
			returnStr = addCardToEs(request);
		} else if (urlPath.startsWith(updateCardInEsUrl)) {
			returnStr = updateCardInEs(request);
		} */else if (urlPath.startsWith(starDynamicUrl)) {
			returnStr = starDynamic(request);
		} else if (urlPath.startsWith(playerDynamicUrl)) {
			returnStr = playerDynamic(request);
		} else if (urlPath.startsWith(cacheUserTagsUrl)) {
			returnStr = cacheUserTags(request);
		}/* else if (urlPath.startsWith(starHomeNewListUrl)) {
			returnStr = starHomeNewList(request);
		} else if (urlPath.startsWith(delCardInEsUrl)) {
			returnStr = delCardInEs(request);
		}*/ else if (urlPath.startsWith(getConfigUrlByIdUrl)) {
//			returnStr = getConfigUrlById(request);
		} else if (urlPath.startsWith(commentSwitchUrl)) {
			returnStr = commentSwitch(request);
		} else if (urlPath.startsWith(pushMsgToReadListUrl)) {
			returnStr = pushMsgToReadList(request);
		} else if (urlPath.startsWith(topNStarsUrl)) {
			returnStr = topNStars(request);
		} /*else if (urlPath.startsWith(sendForNewUserUrl)) {
			returnStr = sendNews(request);
		}*/ else if (urlPath.startsWith(getThemesByIdUrl)) {
			returnStr = getThemesById(request);
		} else if (urlPath.startsWith(addCardUrl)) {
			// 添加卡片
			returnStr = addCards(request);
		} else if (urlPath.startsWith(delCardUrl)) {
			// del卡片
			returnStr = delCard(request);
		} else if (urlPath.startsWith(entityHomeListUrl)) {
			returnStr = entityHomeList(request);
		} else if (urlPath.startsWith(starHomeLenUrl)) {
			returnStr = starHomeLen(request);
		} else if (urlPath.startsWith(starHomeUrl)) {
			returnStr = getNewsByIdsWithoutContent(request);
		} else if (urlPath.startsWith(delHTableUrl)) {
			returnStr = delHTable(request);
		} else if (urlPath.startsWith(resetRedisUrl)) {
			returnStr = resetRedis(request);
		} else if (urlPath.startsWith(newsListUrl)) {
			returnStr = newsList(request);
		} else if (urlPath.startsWith(getNewsByIdsWithoutContentUrl)) {
			returnStr = getNewsByIdsWithoutContent(request);
		} else if (urlPath.startsWith(upsertEntityIndexUrl)) {
			returnStr = upsertEntityIndex(request);
		} else if (urlPath.startsWith(deleteEntityIndexByIdsUrl)) {
			returnStr = deleteEntityIndexByIds(request);
		} else if (urlPath.startsWith(entityDynamicUrl)) {
			returnStr = entityDynamic(request);
		} else if (urlPath.startsWith(sendMailUrl)) {
			returnStr = sendMail(request);
		} else if (urlPath.startsWith(tbarHomeListUrl)) {
			returnStr = tbarHomeList(request);
		} else {
			resultObject.addReturnCode(CodeManager.CODE_404);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}

	private String sendMail(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String title = request.getParam("title" ,"title");
		String content = request.getParam("content" ,"content");
		String to = request.getParam("to");
		EmailUtil.send(title, content, to);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getThemesById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String text = request.getParam("id");
		String page = request.getParam("page","1");
		String size = request.getParam("size" ,"10");
			if (StringUtils.isEmpty(text)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 text = "+text);
				logger.info("缺失 text = "+text);
			} else {
			List<Map<String, Object>> ls = new ArrayList<>();
			Map<String, Object> map = null;
			String bodyJson = "{  \"query\": {    \"constant_score\": {      \"filter\": {        \"term\": {          \"theme_ids\": \"$id\"        }      }    } },\"sort\": [    {      \"pub_time\": {        \"order\": \"desc\"      }    }  ] ,\"from\":$from,\"size\":$size}";
			int limit = Integer.parseInt(size);
			int from = (Integer.parseInt(page) - 1)*limit;
			bodyJson = bodyJson.replace("$id", text).replace("$from", from+"").replace("$size", limit+"");
			String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl")+"/nm*/_search", bodyJson, "utf-8");
			JsonParser paser = new JsonParser();
			com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
			if(obj != null){
					com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
					JsonElement orgUrl = null;
					JsonElement commentCount = null;
					JsonElement duration = null;
					String tableType = null;
					JsonElement total = hits.get("total");
					for(JsonElement starObj:hits.get("hits").getAsJsonArray()){
						com.google.gson.JsonObject job = starObj.getAsJsonObject();
						map = new HashMap<>();
						try {
							tableType = job.get("_type").getAsString();
							com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
							orgUrl = source.get("org_url");
							if(null != orgUrl){
								map.put("landing_param", orgUrl.getAsString());
							}
							commentCount = source.get("comment_count");
							if(null != commentCount){
								map.put("count", commentCount.getAsInt());
							}
							duration = source.get("duration");
							if(null != duration){
								map.put("duration", duration.getAsInt());
							}
							
							if(tableType.equals(BasicConstants.NM_WEIBO)){
								map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							}else{
								map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
							}
							map.put("news_title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsString());
							map.put("news_source", source.get("news_source").getAsString());
							map.put("list_images", source.get("list_images"));
							map.put("list_images_style", source.get("list_images_style").getAsInt());
	//							map.put("landing_param", source.get("org_url").getAsString());
	//							map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							map.put("look_relation", "");
	//							map.put("group", "video");
							ls.add(map);
						} catch (Exception e) {
							logger.error("",e);
						}
				}
				resultObject.putHead("total", total);
			}
			resultObject.putHead("count", ls.size());
			resultObject.addReturnDesc(ls);
		}
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String cacheUserTags(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		
		byte[] body = request.getContent();
		Jedis js = null;
		String content = "";
		try {
			if (StringUtils.isEmpty(userId) || body == null) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失字段");
				logger.info("缺失字段");
			} else {
				js = RedisConnectionPool.getCluster();
				String key = BasicConstants.USER_STATE_PREFIX + ":" + userId;
				content = new String(body);
				JsonElement json = Utils.getJsonParser().parse(content);
				if(null != json){
					Map<String,String> map = new HashMap<>();
					com.google.gson.JsonObject obj = json.getAsJsonObject();
					map.put("tags1", obj.get("tags1").getAsString());
					map.put("tags2", obj.get("tags2").getAsString());
					map.put("tags3", obj.get("tags3").getAsString());
					js.hmset(key, map);
				}
			}
		} catch (Exception e) {
			logger.error("cacheUserTags:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("cacheUserTags is error  content = "+content);
			logger.info(content);
		}finally{
			if(null != js){
				js.close();
			}
		} 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String playerDynamic(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		int page = Integer.parseInt(request.getParam("page" ,"1"));
		int size = Integer.parseInt(request.getParam("size","10"));
		try {
			if (StringUtils.isEmpty(userId) ) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失userId");
				logger.info("缺失 userId");
			} else {
				String bk = BasicConstants.USER_ENTITY_BUCKET;
				int offset = (page -1)*size;
				String sql = "select us.end_id as id,us.last_episode as episode from "+bk+" us where    us.relation='11' and us.start_id = '"
						+ userId +"'  and first_class_id='2'  order by us.update_time desc offset "+offset+" limit "+size;
				List<Map<String, Object>> tmpls = new ArrayList<>();
				QueryResult queryResult = CBaseConnectionPool.sql(bk ,sql);
				logger.info("sql is "+sql);
				String mysql = "select  p.name  ,p.id as pid,p.img_v,p.img,p.newest,v.episode  ,v.video_link_h5,tv_type ,p.total from  v_program p LEFT JOIN v_video v on p.max_episode=v.id where  p.id  in($pids '-1') ";
				StringBuffer pids = new StringBuffer();
				Map<String, String> map = new LinkedHashMap<>();
				if (queryResult.finalSuccess()) {
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						if (null != val) {
							String id = val.getString("id");
							pids.append("'"+id+"',");
							map.put(id, val.getString("episode"));
//							vids.append("'"+val.getString("episode")+"',");
						}
					}
				}
				if(pids.length() > 0){
					mysql = mysql.replace("$pids", pids.toString());
					logger.info("mysql is " + mysql);
					Map<String,Map<String, Object>> ls = connectionUtil.query(mysql, new ResultSetExtractor<Map<String,Map<String, Object>>>() {
						public Map<String,Map<String, Object>> extractData(ResultSet resultSet)
								throws SQLException, DataAccessException {
							Map<String, Object> map = null;
							Map<String,Map<String, Object>> ls = new HashMap<>();
							while (resultSet.next()) {
								map = new HashMap<>();
	//							map.put("rank", resultSet.getInt("rank"));
								map.put("title", resultSet.getString("name"));
	//							map.put("img", resultSet.getString("img"));
								map.put("img", resultSet.getString("img_v"));
								map.put("pid", resultSet.getString("pid"));
								map.put("newest", resultSet.getString("newest"));
								map.put("episode", resultSet.getString("episode"));
//								map.put("video_link", resultSet.getString("video_link"));
								map.put("video_link_h5", resultSet.getString("video_link_h5"));
								map.put("tv_type", resultSet.getString("tv_type"));
								map.put("total", resultSet.getString("total"));
							 
								ls.put(resultSet.getString("pid"), map);
							}
							return ls;
						}
					});
				/*	for(Map<String,Object> tmp:ls){
						String id = tmp.get("pid").toString();
						tmp.put("last_episode", map.get(id));
					}*/
					
					Map<String, Object> tmp = null;
					for(String s:map.keySet()){
						tmp = ls.get(s);
						if(tmp != null){
							tmp.put("last_episode",map.get(s));
							tmpls.add(tmp);
						}
					}
				}
				resultObject.putHead("count", tmpls.size());
				resultObject.addResultData(tmpls);
			}
		} catch (Exception e) {
			logger.error("playerDynamic:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("playerDynamic is error");
		} 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	/**
	 * 除影视类外的通用文件夹视图拉取接口
	 * @param request
	 * @return
	 */
	private String entityDynamic(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String firstClassId = request.getParam("firstClassId");
		Jedis redis = null;
		int page = Integer.parseInt(request.getParam("page" ,"1"));
		int size = Integer.parseInt(request.getParam("size","10"));
		try {
			if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(firstClassId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
				logger.info("缺失参数");
			} else {
				redis = RedisConnectionPool.getCluster();
				String bk = BasicConstants.USER_ENTITY_BUCKET ;
				int offset = (page -1)*size;
				String sql = "select end_id as entityId ,pin_time,`order` as sort from " + bk + " where  relation='11' and start_id = '"
						+ userId +"' and first_class_id = '"+firstClassId+"' order by pin_time desc, update_time desc offset "+offset+" limit "+size ;
				logger.info("sql is "+sql);
				QueryResult queryResult = CBaseConnectionPool.sql(bk, sql);
				List<Map<String, Object>> ls = new ArrayList<>();
				if (queryResult.finalSuccess()) {
					StringBuffer sb = new StringBuffer();
					Map<String,Object> map = null;
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						map = new HashMap<>();
						if (null != val) {
							String entityId = val.getString("entityId");
							Object order = val.get("sort");
							if(null != order){
								map.put("is_top", "1");
								map.put("pin_time", val.getLong("pin_time"));
							}
							String key = BasicConstants.ENTITY_HOME_PREFIX + ":" + entityId;
							String lenKey = BasicConstants.ENTITY_HOME_LEN_PREFIX + ":" + DateUtil.getDateFormat("MMdd").format(new Date())+":"+entityId;
							String count =  redis.get(lenKey);
//							System.out.println(lenKey+"==="+count);
							map.put("count",count==null?0:Integer.parseInt(count));
							Set<String> rowKeys = redis.zrevrange(key, 0, 1);
							for(String id : rowKeys){
								JsonDocument contentMap = CBaseConnectionPool.get(BasicConstants.SOURCE_BUCKET,id);
								map.put("desc", "");
								if(contentMap != null ){
									map.put("desc", contentMap.content().get("news_title"));
								} else{
									logger.info("",contentMap);
								}
								break;
							}
							
							map.put("entityId", entityId);
							sb.append(entityId+",");
							ls.add(map);
						}else{
							logger.info(queryResult.info().asJsonObject().toString());
						}
					}
					Map<String ,Map<String,Object>> entitysInfo = getEntitysInfo(sb.toString());
					Map<String, Object> tmp = null;
					List<Map<String, Object>> removing = new ArrayList<>();
					for(Map<String, Object> item:ls){
						tmp = entitysInfo.get(item.get("entityId").toString());
						if(null != tmp){
								item.putAll(tmp);
						}else{
							removing.add(item);
							logger.warn("",tmp);
						}
					}
					if(removing.size()>0){
						ls.removeAll(removing);
					}
				}
				resultObject.putHead("count", ls.size());
				resultObject.addResultData(ls);
			}
		} catch (Exception e) {
			logger.error("entityDynamic:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("entityDynamic is error");
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	private Map<String ,Map<String,Object>> getEntitysInfo(String ids){
		/*String mysql = "SELECT s.id,s.name,s.intro,s.img,s.badge,s.rank,GROUP_CONCAT(DISTINCT c.name) AS info_badge,GROUP_CONCAT(DISTINCT c2.name) AS classify_one FROM star AS s "
				+ "LEFT JOIN   star_classify AS sc ON s.id=sc.id LEFT JOIN classify  AS c ON sc.classify_id=c.id LEFT JOIN classify AS c2 ON c2.id=c.parent_id"
				+ " WHERE s.status=1 and s.id in(" + ids + " -1)" + " GROUP BY s.id ";*/
		
		String mysql = "SELECT s.id,s.name,s.intro,s.img,s.rank FROM entity AS s WHERE s.status=1 and s.id in(" + ids + " -1)";
		logger.info("mysql is " + mysql);
		Map<String ,Map<String,Object>> tmpMap = connectionUtil.query(mysql, new ResultSetExtractor<Map<String ,Map<String,Object>>>() {
			@Override
			public Map<String ,Map<String,Object>> extractData(ResultSet resultSet)
					throws SQLException, DataAccessException {
				Map<String, Object> map = null;
				Map<String ,Map<String,Object>> ls = new HashMap<>();
				while (resultSet.next()) {
					map = new HashMap<>();
					map.put("id", resultSet.getInt("id"));
					map.put("rank", resultSet.getDouble("rank"));
					map.put("name", resultSet.getString("name"));
					map.put("img", resultSet.getString("img"));
					map.put("intro", resultSet.getString("intro"));
					ls.put(resultSet.getInt("id")+"", map);
				}
				return ls;
			}
		});
		return tmpMap;
	}
	
	private String starDynamic(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		Jedis redis = null;
		int page = Integer.parseInt(request.getParam("page" ,"1"));
		int size = Integer.parseInt(request.getParam("size","10"));
		try {
			if (StringUtils.isEmpty(userId) ) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失userId");
				logger.info("缺失 userId");
			} else {
				redis = RedisConnectionPool.getCluster();
				int offset = (page -1)*size;
				String sql = "select end_id as id ,pin_time,`order` as sort from " + BasicConstants.USER_STAR_BUCKET + " where  relation='11'  and start_id = '"
						+ userId +"' and type is missing order by pin_time desc, update_time desc offset "+offset+" limit "+size ;
				logger.info("sql is "+sql);
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET).query(Query.simple(sql));
				List<Map<String, Object>> ls = new ArrayList<>();
				if (queryResult.finalSuccess()) {
					StringBuffer sb = new StringBuffer();
					Map<String,Object> map = null;
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						map = new HashMap<>();
						if (null != val) {
							String starId = val.getString("id");
							Object order = val.get("sort");
							if(null != order){
								map.put("is_top", "1");
								map.put("pin_time", val.getLong("pin_time"));
							}
							/*
							String lastTime = redis.hget(BasicConstants.USER_STATE_PREFIX+":"+userId, FILED_PREFIX+":"+starId);
							if(StringUtils.isEmpty(lastTime)){
								lastTime = System.currentTimeMillis()+"";
								redis.hset(BasicConstants.USER_STATE_PREFIX+":"+userId, FILED_PREFIX+":"+starId, lastTime);
								logger.info("lastRefreshStarHomeTime is null use system time");
							}
							lastTime = DateUtil.getCurrentDayMillis()+"";  //获取今日未读数
//							map.put("count", getDynamicMap(lastTime ,starId).get("count"));
							 * */
							String key = BasicConstants.ENTITY_HOME_PREFIX + ":" + starId;
							String lenKey = BasicConstants.ENTITY_HOME_LEN_PREFIX + ":" + DateUtil.getDateFormat("MMdd").format(new Date())+":"+starId;
							String count =  redis.get(lenKey);
//							System.out.println(lenKey+"==="+count);
							map.put("count",count==null?0:Integer.parseInt(count));
							Set<String> rowKeys = redis.zrevrange(key, 0, 1);
							for(String id : rowKeys){
								JsonDocument contentMap = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_BUCKET).get(id);
								map.put("desc", "");
								if(contentMap != null ){
									map.put("desc", contentMap.content().get("news_title"));
								} else{
									logger.info("",contentMap);
								}
								break;
							}
							
							map.put("starId", starId);
							sb.append(starId+",");
							ls.add(map);
						}else{
							logger.info(queryResult.info().asJsonObject().toString());
						}
					}
					Map<String ,Map<String,Object>> starsInfo = getStarsInfo(sb.toString());
					Map<String, Object> tmp = null;
					for(Map<String, Object> item:ls){
						tmp = starsInfo.get(item.get("starId").toString());
						if(null != tmp){
								item.putAll(tmp);
						}else{
							logger.warn("",tmp);
						}
					}
				}
				resultObject.putHead("count", ls.size());
				resultObject.addResultData(ls);
			}
		} catch (Exception e) {
			logger.error("starHomeDynamic:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("starHomeDynamic is error");
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	public String starDynamic2(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		Jedis redis = null;
		int page = Integer.parseInt(request.getParam("page" ,"1"));
		int size = Integer.parseInt(request.getParam("size","10"));
		try {
			if (StringUtils.isEmpty(userId) ) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失userId");
				logger.info("缺失 userId");
			} else {
				redis = RedisConnectionPool.getCluster();
				int offset = (page -1)*size;
				String sql = "select end_id as id ,pin_time,`order` as sort from " + BasicConstants.USER_STAR_BUCKET + " where  relation='11' and start_id = '"
						+ userId +"' and type is missing order by pin_time desc, update_time desc offset "+offset+" limit "+size ;
				logger.info("sql is "+sql);
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_STAR_BUCKET).query(Query.simple(sql));
				Map<String, Object> info = null;
				List<Map<String, Object>> ls = new ArrayList<>();
				if (queryResult.finalSuccess()) {
					StringBuffer sb = new StringBuffer();
					Map<String,Object> map = null;
					for (QueryRow row : queryResult.allRows()) {
						JsonObject val = row.value();
						map = new HashMap<>();
						if (null != val) {
							String starId = val.getString("id");
							Object order = val.get("sort");
							if(null != order){
								map.put("is_top", "1");
								map.put("pin_time", val.getLong("pin_time"));
							}
							/*
							String lastTime = redis.hget(BasicConstants.USER_STATE_PREFIX+":"+userId, FILED_PREFIX+":"+starId);
							if(StringUtils.isEmpty(lastTime)){
								lastTime = System.currentTimeMillis()+"";
								redis.hset(BasicConstants.USER_STATE_PREFIX+":"+userId, FILED_PREFIX+":"+starId, lastTime);
								logger.info("lastRefreshStarHomeTime is null use system time");
							}*/
							String lastTime = DateUtil.getCurrentDayMillis()+"";  //获取今日未读数
							info = getDynamicMap(lastTime ,starId);
							map.put("count", info.get("count"));
							map.put("desc",info.get("title"));
							/*String key = BasicConstants.STAR_HOME_PREFIX + ":" + starId;
							Set<String> rowKeys = redis.zrevrange(key, 0, 1);
							for(String id : rowKeys){
								JsonDocument contentMap = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_BUCKET).get(id);
								map.put("desc", "");
								if(contentMap != null ){
									map.put("desc", contentMap.content().get("news_title"));
								} else{
									logger.info("",contentMap);
								}
								break;
							}*/
							
							map.put("starId", starId);
							sb.append(starId+",");
							ls.add(map);
						}else{
							logger.info(queryResult.info().asJsonObject().toString());
						}
					}
					Map<String ,Map<String,Object>> starsInfo = getStarsInfo(sb.toString());
					Map<String, Object> tmp = null;
					for(Map<String, Object> item:ls){
						tmp = starsInfo.get(item.get("starId").toString());
						if(null != tmp){
							item.putAll(tmp);
						}else{
							logger.warn("",tmp);
						}
					}
				}
				resultObject.putHead("count", ls.size());
				resultObject.addResultData(ls);
			}
		} catch (Exception e) {
			logger.error("starHomeDynamic:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("starHomeDynamic is error");
		}finally{
			if(null != redis){
				redis.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	private Map<String ,Map<String,Object>> getStarsInfo(String ids){
		/*String mysql = "SELECT s.id,s.name,s.intro,s.img,s.badge,s.rank,GROUP_CONCAT(DISTINCT c.name) AS info_badge,GROUP_CONCAT(DISTINCT c2.name) AS classify_one FROM star AS s "
				+ "LEFT JOIN   star_classify AS sc ON s.id=sc.id LEFT JOIN classify  AS c ON sc.classify_id=c.id LEFT JOIN classify AS c2 ON c2.id=c.parent_id"
				+ " WHERE s.status=1 and s.id in(" + ids + " -1)" + " GROUP BY s.id ";*/
		
		String mysql = "SELECT s.id,s.name,s.intro,s.img,s.rank FROM star AS s WHERE s.status=1 and s.id in(" + ids + " -1)";
		logger.info("mysql is " + mysql);
		Map<String ,Map<String,Object>> tmpMap = connectionUtil.query(mysql, new ResultSetExtractor<Map<String ,Map<String,Object>>>() {
			@Override
			public Map<String ,Map<String,Object>> extractData(ResultSet resultSet)
					throws SQLException, DataAccessException {
				Map<String, Object> map = null;
				Map<String ,Map<String,Object>> ls = new HashMap<>();
				while (resultSet.next()) {
					map = new HashMap<>();
					map.put("id", resultSet.getInt("id"));
					map.put("rank", resultSet.getInt("rank")+"");
					map.put("name", resultSet.getString("name"));
					map.put("img", resultSet.getString("img"));
					map.put("intro", resultSet.getString("intro"));
					ls.put(resultSet.getInt("id")+"", map);
				}
				return ls;
			}
		});
		return tmpMap;
	}
	
	private Map<String,Object> getDynamicMap(String lastRefreshStarHomeTime ,String starId){
		
//		String lastRefreshStarHomeTime = redis.hget(BasicConstants.USER_STATE_PREFIX+":"+userId, FILED_PREFIX+":"+starId);
		String rangeQuery = " {  \"query\": {    \"bool\": {      \"filter\": {        \"range\": {          \"pub_time\": {            \"gte\": "+lastRefreshStarHomeTime+"          }        }      },      \"must\": [        {          \"term\": {            \"starIds\": \""+starId+"\"          }        }      ]    }  } ,\"size\":1} ";
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl")+"/nm_*/news,news_video,news_weibo/_search", rangeQuery, "utf-8");
		JsonParser paser = new JsonParser();
		com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		Map<String,Object> info = new HashMap<>();
		if (obj != null) {
			com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
			int total = hits.get("total").getAsInt();
			info.put("count", total );
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
					String title = source.get("title").getAsString();
					info.put("title", title);
			}
			}
		return info;
	}

	/*private String delCardInEs(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("id");
		String group = request.getParam("group");
		try {
			if (StringUtils.isEmpty(id) || StringUtils.isEmpty(group)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失id or group");
				logger.info("缺失 id");
			} else {
				String url = ConfigInit.getValue("esUrl")+"/"+INDEX_GENERALIZE+"/ad/"+id+"?routing="+group;
				String re = HttpUrlUtil.Delete(url);
				logger.info(re);
				if(!isSuccess(re)){
					resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				}
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addCardToEs:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addCardToEs is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
/*
	private String updateCardInEs(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("id");
		byte[] conetnt = request.getContent();
		String group = request.getParam("group");
		try {
			if (StringUtils.isEmpty(id) || StringUtils.isEmpty(group)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失id or group");
				logger.info("缺失 id");
			} else {
				String url = ConfigInit.getValue("esUrl")+"/"+INDEX_GENERALIZE+"/ad/"+id+"/_update?routing="+group;
				String json = new String(conetnt);
				logger.info("url="+url+" ,body="+json);
				JsonElement doc = parser.parse(json);
				Map<String,Object> map = new HashMap<>();
				map.put("doc", doc);
				String re = HttpClientUtil.sendPostRequest(url, Utils.getGson().toJson(map), "utf-8");
				logger.info(re);
				if(!isSuccess(re)){
					resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				}
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addCardToEs:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addCardToEs is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private boolean isSuccess(String json){
		com.google.gson.JsonObject root = parser.parse(json).getAsJsonObject();
		if(null != root){
			JsonElement error = root.get("error");
			if(error == null){
				return true;
			}
		}
		return false;
	}*/
	/*
	private String addCardToEs(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("id");
		byte[] conetnt = request.getContent();
		String group = request.getParam("group");
		try {
			if (StringUtils.isEmpty(id) || StringUtils.isEmpty(group)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失id or group");
				logger.info("缺失 id");
			} else {
				String url = ConfigInit.getValue("esUrl")+"/"+INDEX_GENERALIZE+"/ad/"+id+"?routing="+group;
				String json = new String(conetnt);
				logger.info("url="+url+" ,body="+json);
				String re = HttpClientUtil.sendPostRequest(url, json, "utf-8");
				logger.info(re);
				if(!isSuccess(re)){
					resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				}
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addCardToEs:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addCardToEs is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/

	private String resetRedis(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		resultObject.addReturnDesc(RedisConnectionPool.getClusterInfo());
		RedisConnectionPool.init();
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String delHTable(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String table = request.getParam("table");
		Jedis js = null;
		try {
			js = RedisConnectionPool.getCluster();
			if (StringUtils.isEmpty(table)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数table");
			} else {
				HBaseUtil.dropTable(table);
				resultObject.addReturnDesc("hbase, delete table " + table);
				logger.info("hbase delete table " + table);
			}
		} catch (Throwable e) {
			logger.error("starHomeLen:", e);
			logger.error("获取未读数据失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("获取未读数据失败  ,params is [" + request.getParams() + "]");
		} finally {
			js.close();
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String starHomeLen(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("starId");
		Jedis js = null;
		String prefix = request.getParam("type", BasicConstants.ENTITY_HOME_LEN_PREFIX);
		try {
			js = RedisConnectionPool.getCluster();
			if (StringUtils.isEmpty(id)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数starId");
			} else {
				String day = DateUtil.format(new Date(), "dd");
				String key = prefix + ":" + day + ":" + id;
				// resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
				String re = js.get(key);
				if (StringUtils.isEmpty(re)) {
					re = "0";
				}
				int count = Integer.parseInt(re);
				resultObject.addResultData(count + "");
				resultObject.addReturnDesc("get " + re + " ,really show " + count);
				resultObject.putHead("count", 1);
				logger.info("get " + re + " ,really show " + count);
			}
		} catch (Throwable e) {
			logger.error("starHomeLen:", e);
			logger.error("获取未读数据失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.USERACTION_GETNOREADCOUNT);
			resultObject.addReturnDesc("获取未读数据失败  ,params is [" + request.getParams() + "]");
		} finally {
			if (null != js) {
				js.close();
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String newsList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsIds = request.getParam("newsIds");
		if (StringUtils.isEmpty(newsIds)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数: newsIds = " + newsIds);
		} else {
			try {
				String[] ids = newsIds.split(",");
				String sql = "select default.link_type,default.org_url as landing_param,default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,default.comment_count  from  default  use keys [";
				String mysql = "SELECT pub_time,id,news_source,img_url as list_images,title as news_title,count,play_url as landing_param,star_id from star_information  where id in (";
				QueryResult queryResult = null;
				for (String n : ids) {
					sql += "'" + n + "',";
					mysql += "'" + n + "',";
				}
				sql += "''] ";
				mysql += "'')";
				queryResult = CBaseConnectionPool.getBucket(sourceOtherBucket).query(Query.simple(sql));
				logger.info("sql is " + sql);
				if (queryResult.finalSuccess()) {
					List<Map<String, Object>> ls = transform(CouchbaseUtil.resultToList(queryResult, "default"), null);
					ls.addAll(getVideos(mysql));
					logger.info("mysql is " + mysql);
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("", e);
			}
		}

		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public String starHomeNewList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String starId = request.getParam("starId");
		String userId = request.getParam("userId");
//		String starName = request.getParam("starName");
		long page = Long.parseLong(request.getParam("page", "1"));

		List<com.google.gson.JsonObject> ls = new ArrayList<>();
		long size = Long.parseLong(request.getParam("size", "16"));
		if (StringUtils.isEmpty(starId) || StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数: starId = " + starId + ",userId=" + userId);
			logger.warn("缺失参数: starId = " + starId + ",userId=" + userId);
		} else {
			Jedis cluster = null;
			try {
				cluster = RedisConnectionPool.getCluster();
				String key = BasicConstants.USER_STATE_PREFIX + ":" + userId;
				long from = (page - 1) * size;
				String bodyJson = ConfigInit.getValue("esGetPlayerQuery");
				bodyJson = bodyJson.replace("$text", starId);
				bodyJson = bodyJson.replace("$from", from+"");
				bodyJson = bodyJson.replace("$size", size+"");
				String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl")+"/nm*/"+BasicConstants.NEWS_WEIBO_VIDEO+"/_search", bodyJson, "utf-8");
				double minScore = 0;
				JsonParser paser = new JsonParser();
				com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
				if(obj != null){
					JsonElement hitsEle = obj.get("hits");
					if(null != hitsEle){
//						String sql = "select default.link_type,default.news_type as news_type,default.org_url as landing_param,default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,default.comment_count ,user_news.look_relation from  default  use keys [";
						String sql = "select look_relation,end_id as id from user_news use keys [";
						com.google.gson.JsonObject hits = hitsEle.getAsJsonObject();
						JsonElement maxScoreObj = hits.get("max_score");
						Map<String, com.google.gson.JsonObject> tmp = new HashMap<>();
						String tableType = "";
						if(maxScoreObj != null && ! maxScoreObj.isJsonNull()){
							minScore = maxScoreObj.getAsDouble() * 0.4;
							String newsId = "";
							for(JsonElement starObj:hits.get("hits").getAsJsonArray()){
								com.google.gson.JsonObject job = starObj.getAsJsonObject();
								if(job.get("_score").getAsDouble() < minScore){
									break;
								}else{
									com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
									tableType = job.get("_type").getAsString();
									source.addProperty("type", tableType);
									if(tableType.equals(BasicConstants.NM_WEIBO)){
										source.addProperty("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
									}else{
										source.addProperty("link_type", BasicConstants.NEWS_LINKED_TYPE);
									}
									newsId = source.get("id").getAsString();
									tmp.put(newsId, source);
									sql += "'" + userId +"_"+ newsId + "',";
									ls.add(source);
								}
							}
							sql += "''] ";
							/*sql += "'']   join source_other on keys default.id left outer join user_news on keys '" + userId
									+ "_' || default.id";*/
							QueryResult queryResult = CBaseConnectionPool.getBucket(sourceOtherBucket).query(Query.simple(sql));
							logger.info("sql is " + sql);
							if (queryResult.finalSuccess()) {
								Object relation = "";
								com.google.gson.JsonObject item = null;
								for (QueryRow row : queryResult.allRows()) {
									JsonObject val = row.value();
									if (null != val) {
										item = tmp.get(newsId);
										newsId = val.getString("id");
										relation = val.get("look_relation");
										if(null == relation || relation.toString().length() == 0){
											item.addProperty("look_relation", "");
										}else{
											item.addProperty("look_relation", relation.toString());
										}
									}
								}
								
								/*ls = transform(CouchbaseUtil.resultToList(queryResult, "default"),
										starId);*/
							}else{
								logger.error("query couchbase error");
							}
						}
					}
				}
				cluster.hset(key, FILED_PREFIX+":"+starId, System.currentTimeMillis()+"");
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("", e);
			} finally {
				if (null != cluster) {
					cluster.close();
				}
			}
		}

		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/*
	// 我方做缓存cacheKey，每次down操作清空用户明星页缓存
	private String starHomeList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String starId = request.getParam("starId");
		String userId = request.getParam("userId");
		long page = Long.parseLong(request.getParam("page", "1"));

		long size = Long.parseLong(request.getParam("size", "16"));
		if (StringUtils.isEmpty(starId) || StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数: starId = " + starId + ",userId=" + userId);
			logger.warn("缺失参数: starId = " + starId + ",userId=" + userId);
		} else {
			Jedis cluster = null;
			try {
				cluster = RedisConnectionPool.getCluster();
				Pipeline pipe = cluster.pipelined();
				String key = BasicConstants.STAR_HOME_PREFIX + ":" + starId;
				String cacheKey = BasicConstants.STAR_HOME_CACHE_PREFIX + ":" + starId + ":" + userId;
				// long begin = (page - 1) * size;
				if (page == 1) {
					Set<Tuple> src = cluster.zrangeWithScores(key, 0, -1);
					Map<String, Double> map = new LinkedHashMap<>();
					for (Tuple t : src) {
						map.put(t.getElement(), t.getScore());
					}
					if (map.size() > 0) {
						// cluster.del(key);
						
						 * String day = DateUtil.format(new Date(), "dd");
						 * String incrKey = BasicConstants.STAR_HOME_LEN_PREFIX
						 * + ":" +day+":"+ starId; long len =
						 * cluster.zcard(cacheKey); int todaySize =
						 * Integer.parseInt(cluster.get(incrKey)); if(len >
						 * BasicConstants.STAR_HOME_LEN_MAX && len > todaySize){
						 * cluster.zremrangeByRank(cacheKey, 0, len -
						 * todaySize); logger.info(
						 * "remove star home cache size is " +(len -
						 * todaySize)); }
						 
						pipe.del(cacheKey);
						pipe.zadd(cacheKey, map);
						pipe.expire(cacheKey, BasicConstants.REDIS_EXPIRE);
						pipe.sync();
					}

				}
				long begin = (page - 1) * size;
				long end = begin + size - 1;
				Set<String> ids = cluster.zrevrange(cacheKey, begin, end);
				// long len = cluster.zcard(cacheKey);
				// cluster.zremrangeByRank(cacheKey, len - ids.size() ,len);
				logger.info("starHomeList : begin[" + begin + "] ,end[" + end + "] and return size is " + ids.size());
				String sql = "select default.link_type,default.org_url as landing_param,default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,default.comment_count ,user_news.look_relation from  default  use keys [";
//				String mysql = "SELECT pub_time,id,news_source,img_url as list_images,title as news_title,count,play_url as landing_param,star_id from star_information  where id in (";
				String idstring = "";
				QueryResult queryResult = null;
				for (String n : ids) {
					sql += "'" + n + "',";
//					mysql += "'" + n + "',";
					idstring+=" \""+n+"\",";
				}
				idstring+="\"\"";
				sql += "'']   join source_other on keys default.id left outer join user_news on keys '" + userId
						+ "_' || default.id";
//				mysql += "'')";
				queryResult = CBaseConnectionPool.getBucket(sourceOtherBucket).query(Query.simple(sql));
				logger.info("sql is " + sql);
				if (queryResult.finalSuccess()) {
					List<Map<String, Object>> ls = transform(CouchbaseUtil.resultToList(queryResult, "default"),
							starId);
					ls.addAll(getVideos4Es(idstring, userId));
					Collections.sort(ls, new Comparator<Map<String, Object>>() {
						public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
							// 从大到小排序
							return arg1.get("pub_time").toString().compareTo(arg0.get("pub_time").toString());
						}
					});

					logger.info("starHomeList size: " + ls.size());
					resultObject.addResultData(ls);
					resultObject.putHead("count", ls.size());
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("", e);
			} finally {
				if (null != cluster) {
					cluster.close();
				}
			}
		}

		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
	
	private void addUserInfo2Bar(List<Map<String, Object>> post_bars) {
		if (post_bars.size() == 0) {
			return;
		}
		String mysql = "select * from  users_crawler u where u.user_id in (";
		String ids = "";
		for (Map<String, Object> bar : post_bars) {
			if (bar.get("post_from") == null || bar.get("post_from").toString().equals("1")) {
				ids += "'" + bar.get("owners") + "',";
			}
		}
		mysql += ids + "'')";
		logger.info("mysql is " + mysql);
		List<Map<String, String>> userInfos = connectionUtil.query(mysql,
				new ResultSetExtractor<List<Map<String, String>>>() {
					@Override
					public List<Map<String, String>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						List<Map<String, String>> userInfos = new ArrayList<>();
						while (resultSet.next()) {
							Map<String, String> m = new HashMap<String, String>();
							m.put("img", resultSet.getString("portrait"));
							m.put("user_id", resultSet.getString("user_id"));
							m.put("nick_name", resultSet.getString("nick_name"));
							userInfos.add(m);
						}
						return userInfos;
					}
				});
		if (userInfos.size() < post_bars.size()) {
			mysql = "select * from  users u where u.user_id in (";
			ids = "";
			for (Map<String, Object> bar : post_bars) {
				if (bar.get("post_from") != null && bar.get("post_from").toString().equals("0")) {
					ids += "'" + bar.get("owners") + "',";
				}
			}
			mysql += ids + "'')";
			logger.info("mysql is " + mysql);
			userInfos.addAll(connectionUtil.query(mysql, new ResultSetExtractor<List<Map<String, String>>>() {
				@Override
				public List<Map<String, String>> extractData(ResultSet resultSet)
						throws SQLException, DataAccessException {
					List<Map<String, String>> userInfos = new ArrayList<>();
					while (resultSet.next()) {
						Map<String, String> m = new HashMap<String, String>();
						m.put("img", resultSet.getString("portrait"));
						m.put("user_id", resultSet.getString("user_id"));
						m.put("nick_name", resultSet.getString("nick_name"));
						userInfos.add(m);
					}
					return userInfos;
				}
			}));
		}
		for (Map<String, Object> bar : post_bars) {
			Object owner = bar.get("owners");
			if (owner == null) {
				continue;
			}
			String userId = "";
			for (Map<String, String> map : userInfos) {
				userId = map.get("user_id");
				if (userId.equals(owner.toString())) {
					bar.put("author_img", map.get("img"));
					bar.put("post_author_name", map.get("nick_name"));
					break;
				}
			}
		}
	}
	
	private List<Map<String, Object>> queryPostBar(String entityId,
			Integer from, Integer size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = " {  \"query\": {    \"bool\": {      \"filter\": {        \"term\": {          \"entity_ids\": \"$entity_ids\"        }      },       \"should\": [        {          \"terms\": {            \"post_category\": [                            \"1\"            ]          }        }      ]    }  },  \"sort\": [    {      \"sort_order\": {        \"order\": \"desc\"      }    },    {      \"pub_time\": {        \"order\": \"desc\"      }    }  ],\"from\":$from ,\"size\":$size}";
		bodyJson = bodyJson.replace("$entity_ids", entityId);
		bodyJson = bodyJson.replace("$from", from + "");
		bodyJson = bodyJson.replace("$size", size + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/post_bar/_search",
				bodyJson, "utf-8");
		com.google.gson.JsonObject obj = Utils.getJsonParser().parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement postFrom = null;
			JsonElement commentCount = null;
			JsonElement upCount = null;
			JsonElement owners = null;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
						try {
							com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
							map.put("title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsLong());
							map.put("list_images", source.get("list_images").getAsString());
							map.put("list_images_style", source.get("list_images_style").getAsInt());
							map.put("entity_names", source.get("entity_names"));
							map.put("intro", source.get("intro"));
							map.put("content", source.get("content"));
							map.put("post_category", source.get("post_category"));
							// map.put("owners",
							// source.get("owners").getAsString());
							owners = (JsonElement) source.get("owners");
							if (null != owners) {
								map.put("owners", owners.getAsString());
							}
							commentCount = (JsonElement) source.get("comment_count");
							if (null != commentCount) {
								map.put("comment_count", commentCount.getAsInt());
							} else {
								map.put("comment_count", 0);
							}
							upCount = (JsonElement) source.get("up_count");
							if (null != upCount) {
								map.put("up_count", upCount.getAsInt());
							} else {
								map.put("up_count", 0);
							}
							postFrom = source.get("post_from");
							if (null == source.get("post_from")) {
								map.put("post_from", "1");
							} else {
								map.put("post_from", postFrom.getAsInt() + "");
							}

							 
							ls.add(map);
						} catch (Exception e) {
							logger.error("", e);
						}
				}
		}
		return ls;
	}
	
	private String tbarHomeList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String entityId = request.getParam("entityId");
		String page = request.getParam("page", "1");
		String size = request.getParam("size", "10");
		try {
			if (StringUtils.isEmpty(entityId) || StringUtils.isEmpty(userId)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失 entityId = " + entityId + " ,userId = " + userId);
				logger.info("缺失 entityId = " + entityId + " ,userId = " + userId);
			} else {
				List<Map<String, Object>> post_bar = null;
				int limit = Integer.parseInt(size);
				int from = (Integer.parseInt(page) - 1) * limit;
				post_bar = queryPostBar(entityId, from, limit);
				addUserInfo2Bar(post_bar);
				// data.put("post_bar", post_bar);
				resultObject.addResultData(post_bar);
				resultObject.putHead("count", post_bar.size());
			}
		} catch (Exception e) {
			logger.error("queryPostBar:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("queryPostBar is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	
	private String entityHomeList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String entityId = request.getParam("entityId");
		String userId = request.getParam("userId");
		long page = Long.parseLong(request.getParam("page", "1"));

		long size = Long.parseLong(request.getParam("size", "16"));
		String tables = request.getParam("newsTypes", BasicConstants.NEWS_WEIBO_VIDEO);
		
		if (StringUtils.isEmpty(entityId) || StringUtils.isEmpty(userId)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数: starId = " + entityId + ",userId=" + userId);
			logger.warn("缺失参数: starId = " + entityId + ",userId=" + userId);
		} else {
//			Jedis cluster = null;
			try {
			/*	cluster = RedisConnectionPool.getCluster();
				String key = BasicConstants.USER_STATE_PREFIX + ":" + userId;*/
				Collection<Map<String, Object>> ls = getNewsByStarId(userId,entityId,(page-1)*size,size ,tables);
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				//记录用户最好一次的刷新时间
//				cluster.hset(key, FILED_PREFIX+":"+starId, System.currentTimeMillis()+"");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("", e);
			}/*finally{
				if(cluster!=null){
					cluster.close();
				}
			}*/
		}

		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
    }
	private Collection<Map<String, Object>> getNewsByStarId(String userId ,String entityId ,long from ,long size ,String tables){
		Map<String,Map<String, Object>> tmpMap = new LinkedHashMap<>();
//		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = "{  \"query\": {    \"constant_score\": {      \"filter\": {        \"term\": {          \"entity_ids\": \"$entityId\"        }      }    } },\"sort\": [    {      \"pub_time\": {        \"order\": \"desc\"      }    }  ] ,\"from\":$from,\"size\":$size}";
		bodyJson = bodyJson.replace("$entityId", entityId).replace("$from", from+"").replace("$size", size+"");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl")+"/nm_*/"+tables+"/_search", bodyJson, "utf-8");
		JsonParser paser = new JsonParser();
		com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		StringBuffer newsIds = new StringBuffer();
		if(obj != null){
				com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
				JsonElement orgUrl = null;
				JsonElement commentCount = null;
				JsonElement duration = null;
				String tableType = null;
				String sql = "select id ,c_c as count from "+BasicConstants.SOURCE_OTHER_BUCKET+" use keys [ $newsIds] ";
//				logger.info("sql is  " + sql  );
				for(JsonElement starObj:hits.get("hits").getAsJsonArray()){
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					try {
						tableType = job.get("_type").getAsString();
						com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
						orgUrl = source.get("org_url");
						if(null != orgUrl){
							map.put("landing_param", orgUrl.getAsString());
						}
						commentCount = source.get("comment_count");
						if(null != commentCount){
							map.put("count", commentCount.getAsInt());
						}
						duration = source.get("duration");
						if(null != duration && tableType.equals(BasicConstants.NM_VIDEO)){
							map.put("duration", duration.getAsInt());
						}
						
							
						if(tableType.equals(BasicConstants.NM_VIDEO)){
							map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
							map.put("type", "video");
						}else if(tableType.equals(BasicConstants.NM_WEIBO)){
							map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							map.put("type", "weibo");
						}else{
							map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
							map.put("type", "news");
						}
						map.put("news_title", source.get("title").getAsString());
						map.put("id", source.get("id").getAsString());
						newsIds.append("'"+source.get("id").getAsString()+"',");
						map.put("pub_time", source.get("pub_time"));
						map.put("news_source", source.get("news_source").getAsString());
						map.put("list_images", source.get("list_images"));
						map.put("list_images_v", source.get("list_images_v"));
						map.put("list_images_style", source.get("list_images_style").getAsInt());
//							map.put("landing_param", source.get("org_url").getAsString());
//							map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
						map.put("look_relation", "");
//							map.put("group", "video");
						tmpMap.put(source.get("id").getAsString(),map);
						
					} catch (Exception e) {
						logger.error("",e);
					}
			}
				newsIds.append("''");
				sql = sql.replace(" $newsIds", newsIds.toString());
				logger.info("sql is "+sql);
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET).query(Query.simple(sql));
				if (queryResult.finalSuccess()) {
//					resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					String newsId = null;
					 for (QueryRow row : queryResult.allRows()) {
				        	JsonObject val = row.value().getObject(BasicConstants.SOURCE_OTHER_BUCKET);
				        	if(null == val){
				        		val = row.value();
				        	}
				        	newsId = val.getString("id");
				        	tmpMap.get(newsId).put("count", val.getInt("count"));
				        }
				}
		}
		
		StringBuffer sb = new StringBuffer();
		for (String key : tmpMap.keySet()) {
			sb.append("'" + userId + "_" + key + "',");
		}
		String ids = sb.toString();
		if (ids.length() > 0) {
			String sql = "select end_id as id ,look_relation from "+BasicConstants.USER_NEWS_BUCKET+" use keys[" + ids + "'']";
			QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.USER_NEWS_BUCKET, sql);
			logger.info("sql is " + sql);

			if (queryResult.finalSuccess()) {
				Map<String, Object> m = null;
				JsonObject val = null;
				String idKey = null;
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					idKey = val.getString("id");
					m = tmpMap.get(idKey);
					m.put("look_relation", val.getString("look_relation"));
//					ls.add(m);
//					tmpMap.remove(idKey);
				}
			}
		}
//		ls.addAll(tmpMap.values());
		return tmpMap.values();
	}
	
	private static Map<String, Double> redisSet2Map(Set<Tuple> src) {
		Map<String, Double> map = new HashMap<>();
		if (null == src) {
			return map;
		}
		Map<String, Object> tmp = null;
		for (Tuple t : src) {
			tmp = Utils.getGson().fromJson(t.getElement(), new TypeToken<Map<String, Object>>() {
				private static final long serialVersionUID = 1L;
			}.getType());
			map.put(tmp.get("id").toString(), t.getScore());
		}
		return map;
	}
	public static List<Map<String, Object>> getVideos4Es(String newsIds, String userId) {
		Map<String,Map<String, Object>> tmpMap = new HashMap<>();
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String bodyJson = "{  \"query\": {    \"terms\": {      \"id\": [ $newsIds ]    }  }}";
		bodyJson = bodyJson.replace("$newsIds", newsIds);
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl")+"/nm_*/news_video/_search", bodyJson, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if(obj != null){
			com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if(maxScoreObj != null && !maxScoreObj.isJsonNull()){
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				for(JsonElement starObj:hits.get("hits").getAsJsonArray()){
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if(job.get("_score").getAsDouble() < minScore){
						break;
					}else{
						try {
							com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
							map.put("news_title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsLong());
							map.put("news_source", source.get("news_source").getAsString());
							map.put("list_images", source.get("list_images").getAsString());
							map.put("list_images_style", source.get("list_images_style").getAsInt());
							map.put("landing_param", source.get("org_url").getAsString());
							map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							map.put("look_relation", "");
							map.put("type", "video");
							tmpMap.put(source.get("id").getAsString(),map);
							
						} catch (Exception e) {
							logger.error("",e);
						}
					}
				}
			}
		}
		
		StringBuffer sb = new StringBuffer();
		for (String key : tmpMap.keySet()) {
			sb.append("'" + userId + "_" + key + "',");
		}
		String ids = sb.toString();
		if (ids.length() > 0) {
			String sql = "select end_id as id ,look_relation from "+BasicConstants.USER_NEWS_BUCKET+" use keys[" + ids + "'']";
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET)
					.query(Query.simple(sql));
			logger.info("sql is " + sql);

			if (queryResult.finalSuccess()) {
				Map<String, Object> m = null;
				JsonObject val = null;
				String idKey = null;
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					idKey = val.getString("id");
					m = tmpMap.get(idKey);
					m.put("look_relation", val.getString("look_relation"));
					ls.add(m);
					tmpMap.remove(idKey);
				}
			}
		}
		ls.addAll(tmpMap.values());
		return ls;
	}
	
	public static List<Map<String, Object>> getVideos(String mysql, String userId) {
		List<Map<String, Object>> ls = new ArrayList<>();
		logger.info("mysql is " + mysql);
		Map<String, Map<String, Object>> tmpMap = connectionUtil.query(mysql,
				new ResultSetExtractor<Map<String, Map<String, Object>>>() {
					@Override
					public Map<String, Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						Map<String, Map<String, Object>> tmp = new HashMap<>();
						Map<String, Object> m = null;
						while (resultSet.next()) {
							m = new HashMap<>();
							String starId = resultSet.getString("star_id");
							if (null != starId) {
								m.put("star_id", starId);
							}
							m.put("pub_time", resultSet.getString("pub_time"));
							m.put("id", resultSet.getString("id"));
							m.put("news_source", resultSet.getString("news_source"));
							m.put("list_images", resultSet.getString("list_images"));
							m.put("list_images_style", 1);
							m.put("news_title", resultSet.getString("news_title"));
							m.put("landing_param", resultSet.getString("landing_param"));
							m.put("count", resultSet.getInt("count"));
							m.put("isHot", 0);
							m.put("type", "video");
							m.put("look_relation", "");
							m.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							tmp.put(resultSet.getString("id"), m);
						}
						return tmp;
					}
				});
		StringBuffer sb = new StringBuffer();
		for (String key : tmpMap.keySet()) {
			sb.append("'" + userId + "_" + key + "',");
		}
		String ids = sb.toString();
		if (ids.length() > 0) {
			String sql = "select end_id as id ,look_relation from "+BasicConstants.USER_NEWS_BUCKET+" use keys[" + ids + "'']";
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET)
					.query(Query.simple(sql));
			logger.info("sql is " + sql);

			if (queryResult.finalSuccess()) {
				Map<String, Object> m = null;
				JsonObject val = null;
				String idKey = null;
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					idKey = val.getString("id");
					m = tmpMap.get(idKey);
					m.put("look_relation", val.getString("look_relation"));
					ls.add(m);
					tmpMap.remove(idKey);
				}
			}
		}
		ls.addAll(tmpMap.values());
		return ls;
	}

	public static List<Map<String, Object>> getVideos(String mysql) {
		List<Map<String, Object>> ls = new ArrayList<>();
		logger.info("mysql is " + mysql);
		Map<String, Map<String, Object>> tmpMap = connectionUtil.query(mysql,
				new ResultSetExtractor<Map<String, Map<String, Object>>>() {
					@Override
					public Map<String, Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						Map<String, Map<String, Object>> tmp = new HashMap<>();
						Map<String, Object> m = null;
						while (resultSet.next()) {
							m = new HashMap<>();
							String starId = resultSet.getString("star_id");
							if (null != starId) {
								m.put("star_id", starId);
							}
							m.put("pub_time", resultSet.getString("pub_time"));
							m.put("id", resultSet.getString("id"));
							m.put("news_source", resultSet.getString("news_source"));
							m.put("list_images", resultSet.getString("list_images"));
							m.put("list_images_style", 1);
							m.put("news_title", resultSet.getString("news_title"));
							m.put("landing_param", resultSet.getString("landing_param"));
							m.put("count", resultSet.getInt("count"));
							m.put("isHot", 0);
							m.put("type", "video");
							m.put("look_relation", "");
							m.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
							tmp.put(resultSet.getString("id"), m);
						}
						return tmp;
					}
				});

		ls.addAll(tmpMap.values());
		return ls;
	}

	private String topNStars(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String date = request.getParam("date");
		String size = request.getParam("size", "20");
		try {
			if (null == date) {
				date = DateUtil.format(new Date(), "yyyyMMdd");
			}
			String sql = "select star,count(star) as count from source_other unnest source_other.stars star where  source_other.id > '"
					+ date + "' group by star order by count(star) desc limit " + size;
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				resultObject.addReturnCode(CodeManager.CODE_200);
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, "source_other");
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
				logger.info("sql is [" + sql + "]");
			} else {
				resultObject.addReturnCode(CodeManager.BM_TOPNSTARS);
				resultObject.addReturnDesc("查询失败  ,sql is [" + sql + "]");
			}
		} catch (Exception e) {
			logger.error("topNStars:", e);
			logger.error("topNStars 失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.BM_TOPNSTARS);
			resultObject.addReturnDesc("topNStars失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public String hotHome(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		long page = Long.parseLong(request.getParam("page", "1"));
		long size = Long.parseLong(request.getParam("size", "16"));
		String userId = request.getParam("userId");
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(size)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数: userId = " + userId + " ,size = " + size);
		} else {
			String userCacheKey = BasicConstants.HOT_CARD_PREFIX + ":" + userId;
			String readedKey = BasicConstants.HOT_READ_READED_PREFIX + ":" + userId;
			String hotCardKey = null;
			int days = 1;

			Jedis cluster = null;
			try {
				cluster = RedisConnectionPool.getCluster();
				Long len = cluster.zcard(userCacheKey);
				String readedStr = cluster.get(readedKey);
				Map<String, Long> readPosition = new HashMap<String, Long>();
				if (!StringUtils.isEmpty(readedStr)) {
					String d0 = DateUtil.format(new Date(), "dd");
					String d1 = DateUtil.format(new Date(DateUtil.getTimeMillisBefore(1)), "dd");
					String[] array = readedStr.split(",");
					for (String field : array) {
						String[] fs = field.split(":");
						if (fs[0].contains(d1) || fs[1].contains(d0)) {
							readPosition.put(fs[0], Long.parseLong(fs[1]));
						}
					}
				}

				long ts;
				Set<Tuple> hotSet = null;
				if (len == 0) {
					ts = DateUtil.getTimeMillisBefore(2);
					for (int i = 3; i >= 0; i--) {
						hotCardKey = BasicConstants.HOT_CARD_PREFIX + "_" + DateUtil.format(new Date(ts), "dd") + "_"
								+ i;
						hotSet = cluster.zrevrangeWithScores(hotCardKey, 0, -1);
						cluster.zadd(userCacheKey, redisSet2Map(hotSet));
					}
				} else if (len > 230) {
					cluster.zremrangeByRank(userCacheKey, -len, -150);
				}
				long downSize = 20;
				if (page == 1) {
					long startNum = 0;
					boolean isContinue = true;
					while (days < 2) {
						ts = DateUtil.getTimeMillisBefore(days);
						days--;
						for (int i = 3; i >= 0; i--) {
							hotCardKey = BasicConstants.HOT_CARD_PREFIX + "_" + DateUtil.format(new Date(ts), "dd")
									+ "_" + i;
							startNum = readPosition.get(hotCardKey) == null ? 0 : readPosition.get(hotCardKey);
							hotSet = cluster.zrangeWithScores(hotCardKey, startNum, startNum + downSize);
							if (hotSet != null && hotSet.size() > 5) {
								cluster.zadd(userCacheKey, redisSet2Map(hotSet));
								readPosition.put(hotCardKey, startNum + hotSet.size());
								isContinue = false;
								break;
							}
						}
						if (!isContinue) {
							break;
						}
					}
					if (!isContinue) {
						cluster.set(readedKey, readedMap2Str(readPosition));
					}
					cluster.expire(userCacheKey, BasicConstants.REDIS_EXPIRE);
					cluster.expire(readedKey, BasicConstants.REDIS_EXPIRE);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
				logger.error("", e);
			} finally {
				if (null != cluster) {
					cluster.close();
				}
			}
			long begin = (page - 1) * size;
			long end = begin + size - 1;
			Set<String> ids = cluster.zrevrange(userCacheKey, begin, end);
			String sql = "select default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,user_news.look_relation from  default  use keys [";
			QueryResult queryResult = null;
			for (String n : ids) {
				sql += "'" + n + "',";
			}
			sql += "'']  join source_other on keys default.id left outer join user_news on keys '" + userId
					+ "_' || default.id";
			queryResult = CBaseConnectionPool.getBucket(sourceOtherBucket).query(Query.simple(sql));
			logger.info("sql is " + sql);
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = transform(CouchbaseUtil.resultToList(queryResult, "default"), null);
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String readedMap2Str(Map<String, Long> map) {
		StringBuffer sb = new StringBuffer();
		for (String key : map.keySet()) {
			sb.append(key + ":" + map.get(key) + ",");
		}
		return sb.toString();
	}

	private String getNewsByIdsWithoutContent(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String ids = request.getParam("ids");
		if (StringUtils.isEmpty(ids)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数:" + request.getParams());
		} else {
			String[] idArray = ids.split(",");
			String sql = "select default.pub_time,default.duration ,default.type,default.org_url,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count from  "+BasicConstants.SOURCE_BUCKET+"  use keys [";
			QueryResult queryResult = null;
			for (String n : idArray) {
				sql += "'" + n + "',";
			}
			sql += "'']   join "+sourceOtherBucket+" on keys default.id order by default.create_time desc";
			queryResult = CBaseConnectionPool.sql(sourceOtherBucket, sql);
			logger.info("sql is " + sql);
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = transform(CouchbaseUtil.resultToList(queryResult, "default"), null);
				resultObject.addResultData(ls);
				resultObject.putHead("count", ls.size());
			}
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private List<Map<String, Object>> transform(List<Map<String, Object>> ls, String starId) {
		List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> job : ls) {
			String id = "";
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				Object title = job.get("news_title");
				if (title != null) {
					map.put("news_title", title);
					map.put("pub_time", Long.parseLong(job.get("pub_time")+""));
					map.put("id", job.get("id"));
					map.put("news_source", job.get("news_source"));
					map.put("list_images", job.get("list_images"));
					map.put("list_images_style", job.get("list_images_style"));
					Object lt = job.get("type");
					map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
					map.put("count", job.get("count"));
					map.put("duration", job.get("duration"));
					map.put("type", "news");
					if (lt != null && lt.toString().equals("3")) {
						map.put("type", "video");
						map.put("landing_param", job.get("org_url"));
						map.put("count", job.get("comment_count"));
					}else if(lt != null && lt.toString().equals("2")){
						map.put("type", BasicConstants.LINKED_TYPE_NEW_TAB);
						map.put("landing_param", job.get("org_url"));
					}
					Object lr = job.get("look_relation");
					map.put("look_relation", lr == null ? "" : lr);
//					map.put("group", "news");
					tmp.add(map);
				} 

			} catch (Throwable e) {
				e.printStackTrace();
				logger.error("newsId: " + id, e);
			}
		}
		return tmp;
	}

	/*
	 * private String sendForNewUser(Request request) { long start =
	 * System.currentTimeMillis(); ResultMsg resultObject = new ResultMsg();
	 * String userId = request.getParam("userId"); String ids =
	 * request.getParam("ids"); String days = request.getParam("beforeDays",
	 * "1"); String sizeStr = request.getParam("size", "6"); if
	 * (StringUtils.isEmpty(userId) || StringUtils.isEmpty(ids)) {
	 * resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
	 * resultObject.addReturnDesc("缺失参数[userId,ids]:" + request.getParams()); }
	 * else { Map<String, Map<String, Object>> map = getStarByIds(ids); //
	 * String[] names = ids.split(","); int size = Integer.parseInt(sizeStr);
	 * String beforeDays = Utils.getTimeMillisBefore(Integer.parseInt(days))+"";
	 * String sql =
	 * "select default.pub_time,default.id,default.news_source,default.list_images,default.news_title,default.title,star as name from  default join source_other on keys default.id unnest source_other.stars star where default.pub_time >'"
	 * + beforeDays + "' and "; List<Map<String,Object>> list = new
	 * ArrayList<Map<String,Object>>(); CountDownLatch tc = new
	 * CountDownLatch(map.size()); for (String n : map.keySet()) { String mysql
	 * = sql + " star='" + n + "'  limit " + size; logger.info("sql is " +
	 * mysql); Utils.getThreadPool().execute(new SendThread(tc, list, mysql));
	 * queryResult =
	 * CBaseConnectionPool.getBucket(BasicConstants.SOURCE_BUCKET).query(Query.
	 * simple(mysql)); //
	 * logger.info(queryResult.allRows().size()+""+queryResult.finalSuccess(),
	 * queryResult.allRows()); if (queryResult.finalSuccess()) {
	 * list.addAll(CouchbaseUtil.resultToList(queryResult ,"default")); }else{
	 * logger.error("",queryResult.info()); } } try { tc.await(); } catch
	 * (InterruptedException e) { logger.error("sendForNewUser",e); }
	 * resultObject.addReturnCode(CodeManager.CODE_200); Map<String, Double>
	 * items = transform(map, list); if (items.size() > 0) {
	 * RedisConnectionPool.getCluster().zadd(BasicConstants.
	 * READ_LIST_MAIN_CHANNEL + ":" + userId, items); } logger.info("send " +
	 * list.size() + " item success for user[" + userId + "]");
	 * resultObject.addReturnDesc("send " + items.size() + " item success for "
	 * + userId); } resultObject.putHead(BasicConstants.TIMING,
	 * System.currentTimeMillis() - start);
	 * logger.info((System.currentTimeMillis() - start) + " timing"); return
	 * resultObject.toJson(); }
	 */
	public String sendNews(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userId = request.getParam("userId");
		String ids = request.getParam("ids");
		String sizeStr = request.getParam("size", "6");
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(ids)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数[userId,ids]:" + request.getParams());
		} else {
			String[] idAarray = ids.split(",");
			Map<String, Map<String, Object>> map = getStarByIds(ids);
			Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				int size = Integer.parseInt(sizeStr);
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				String sql = "select default.pub_time,default.link_type ,default.org_url as landing_param,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,user_news.send_relation as isSend";
				// String sql2 = "SELECT pub_time,id,news_source,img_url as
				// list_images,title as news_title,count,play_url as
				// landing_param,star_id from star_information ";
				// select
				// default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c
				// as count
				CountDownLatch tc = new CountDownLatch(idAarray.length);
				// CountDownLatch tc2 = new CountDownLatch(idAarray.length);
				int begin = Utils.randomInt(6);
				for (String id : idAarray) {
					Set<String> nids = js.zrevrange(BasicConstants.ENTITY_HOME_PREFIX + ":" + id, begin, begin + 20 - 1);
					String innerSql = sql + ", '" + id + "' as star_id from  default use keys[";
					// String mysql2 = sql2+" where star_id="+id+" and id in(";
					for (String nid : nids) {
						innerSql += "'" + nid + "',";
						// mysql2 += "'"+nid+"',";
					}
					innerSql += "''] join source_other on keys default.id left outer join user_news on keys '" + userId
							+ "_'|| default.id where  user_news.send_relation is missing";
					// mysql2+="'')";
					logger.info("sql is " + innerSql);
					// logger.info("mySql is " + mysql2);
					Utils.getThreadPool().execute(new SendThread(tc, list, innerSql, userId, size));

					/*
					 * try { QueryResult queryResult =
					 * CBaseConnectionPool.getBucket(BasicConstants.
					 * SOURCE_OTHER_BUCKET).query(Query.simple(mysql)); if
					 * (queryResult.finalSuccess()) {
					 * list.addAll(CouchbaseUtil.resultToList(queryResult ,
					 * BasicConstants.SOURCE_BUCKET)); }else{ logger.error(
					 * "finalSuccess: ",queryResult.finalSuccess()); } } catch
					 * (Exception e) { logger.error("",e); }
					 */
					// Utils.getThreadPool().execute(new SendThreadForMysql(tc2,
					// list, mysql2));
				}

				tc.await();
				// tc2.await();
				resultObject.addReturnCode(CodeManager.CODE_200);
				Map<String, Double> items = transform(map, list);
				if (items.size() > 0) {
					logger.debug("sending readlist...");
					js.zadd(BasicConstants.READ_LIST_MAIN_CHANNEL + ":" + userId, items);
				}
				resultObject.addReturnDesc("send " + items.size() + " item success for " + userId);
				logger.info("send " + list.size() + " item success for user[" + userId + "]");
			} catch (Throwable e) {
				logger.error("sendForNewUser", e);
			} finally {
				if (null != js) {
					js.close();
				}
			}

		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private Map<String, Double> transform(Map<String, Map<String, Object>> stars, List<Map<String, Object>> ls) {
		Map<String, Double> tmp = new LinkedHashMap<String, Double>();

		for (Map<String, Object> job : ls) {
			Object id = null;
			Object g = null;
			String star_id = null;
			Object link_type = null;
			try {
				star_id = job.get("star_id") + "";
				Map<String, Object> map = stars.get(star_id);
				if (null == map) {
					logger.info("not fount propertites for star[" + star_id + "]");
					continue;
				}
				map.put("pub_time", job.get("pub_time"));
				id = job.get("id");
				g = job.get("group");
				map.put("id", id);
				map.put("news_source", job.get("news_source"));
				map.put("list_images", job.get("list_images"));
				map.put("list_images_style", job.get("list_images_style"));
				Object newsTitle = job.get("news_title");
				map.put("news_title", newsTitle);
				map.put("group", job.get("name"));
				link_type = job.get("link_type");
				map.put("link_type", link_type == null ? BasicConstants.NEWS_LINKED_TYPE : link_type);
				map.put("landing_param", job.get("landing_param"));
				map.put("star", job.get("name"));
				map.put("count", job.get("count"));
				map.put("group", g == null ? "sendNews" : g);
				map.put("isHot", 0);

				try {
					// 想要推送的新闻显示在列表页最前端
					tmp.put(Utils.getGson().toJson(map), Double.parseDouble(System.currentTimeMillis() + ""));
					// tmp.put(Utils.getGson().toJson(map),
					// Double.parseDouble(map.get("pub_time").toString()));
				} catch (Exception e) {
					logger.warn("pase score error ,use system time for pub_time");
					tmp.put(Utils.getGson().toJson(map), Double.parseDouble(System.currentTimeMillis() + ""));
				}
			} catch (Throwable e) {
				logger.error("star_id: " + star_id + " ,stars: " + stars + " ,info: " + job.toString(), e);
			}
		}
		return tmp;
	}

	private Map<String, Map<String, Object>> getStarByIds(String ids) {
		String mysql = "SELECT s.id,s.name,s.img,s.badge,s.intro,GROUP_CONCAT(DISTINCT c.name) AS info_badge FROM star AS s "
				+ " LEFT JOIN   star_classify AS sc ON s.id=sc.id LEFT JOIN classify  AS c ON sc.classify_id=c.id "
				+ " WHERE s.status=1 and s.id in(" + ids + ")" + " GROUP BY s.id ";
		logger.info("mysql is " + mysql);
		Map<String, Map<String, Object>> map = connectionUtil.query(mysql,
				new ResultSetExtractor<Map<String, Map<String, Object>>>() {
					@Override
					public Map<String, Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						Map<String, Map<String, Object>> tmp = new HashMap<>();
						while (resultSet.next()) {
							Map<String, Object> m = new HashMap<String, Object>();
							m.put("img", resultSet.getString("img"));
							m.put("info_badge", resultSet.getString("info_badge"));
							m.put("badge", resultSet.getString("badge"));
							m.put("name", resultSet.getString("name"));
							m.put("intro", resultSet.getString("intro"));
							m.put("star_id", resultSet.getString("id"));
							m.put("style", "1");
							tmp.put(resultSet.getString("id"), m);
						}
						return tmp;
					}
				});
		return map;
	}

	/*
	 * private Map<String, Map<String, Object>> getStarByIds(String ids) {
	 * String url = "http://" + ConfigInit.getValue("neo4jHost") + "/" +
	 * ConfigInit.getValue("getStarByIds") + "?ids=" + ids; logger.info(
	 * "getStarByIds url [" + url + "]"); Connection connection =
	 * Jsoup.connect(url); connection.timeout(2000); com.google.gson.JsonObject
	 * jsonObj = null; Map<String, Map<String, Object>> map = new
	 * HashMap<String, Map<String, Object>>(); try {
	 * connection.maxBodySize(Integer.MAX_VALUE);
	 * 
	 * Connection.Response response = connection.execute(); if
	 * (response.statusCode() != 200) { logger.error("服务器错误!!!!");
	 * 
	 * throw new IOException(); } jsonObj =
	 * Utils.getJsonParser().parse(response.body()).getAsJsonObject(); } catch
	 * (Exception e) { logger.error("getStarById", e); } if (null == jsonObj) {
	 * return null; } else { JsonArray jsonArray =
	 * jsonObj.get("response").getAsJsonObject().getAsJsonArray("data"); for
	 * (JsonElement json :jsonArray ) { com.google.gson.JsonObject jsonObject =
	 * json.getAsJsonObject(); Map<String, Object> m = new HashMap<String,
	 * Object>(); JsonElement img = jsonObject.get("img"); m.put("img", img ==
	 * null?"":img.getAsString()); String info_badge = ""; JsonArray ja =
	 * jsonObject.get("info_badge").getAsJsonArray(); for (JsonElement elm : ja)
	 * { info_badge += elm.getAsString() + ","; } if(info_badge.length() > 0){
	 * info_badge = info_badge.substring(0, info_badge.length() - 1); }
	 * m.put("info_badge", info_badge); m.put("badge",
	 * jsonObject.get("badge").getAsString()); m.put("name",
	 * jsonObject.get("name").getAsString()); m.put("intro",
	 * jsonObject.get("intro")); m.put("star_id", jsonObject.get("id")+"");
	 * m.put("style", "1"); map.put(jsonObject.get("id").getAsString(), m); }
	 * return map; } }
	 */
	private String delCard(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String channel = request.getParam("channel");
		String ids = request.getParam("ids");
		if (channel == null || ids == null) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数:" + request.getParams());
		} else {
			String key = channel;
			String[] idArray = ids.split(",");
			List<String> idList = Arrays.asList(idArray);
			Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				Set<Tuple> cards = js.zrangeWithScores(key, 0, -1);
				// Set<Tuple> tmp = new LinkedHashSet<Tuple>();
				Map<String, Double> tmp = new HashMap<>();
				for (Tuple t : cards) {
					Map<String, Object> card = Utils.getGson().fromJson(t.getElement(),
							new TypeToken<Map<String, Object>>() {
								private static final long serialVersionUID = 1L;
							}.getType());
					if (!idList.contains(card.get("id").toString())) {
						// tmp.add(t);
						tmp.put(t.getElement(), t.getScore());
					}
				}
				// cards.remove(tmp);
				js.zadd(key, tmp);
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} finally {
				if (null != js) {
					js.close();
				}
			}

			resultObject.addReturnCode(CodeManager.CODE_200);
			resultObject.addReturnDesc("delete sccuss for " + key);
			logger.info("delete " + ids + " for " + key);
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String addCards(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String channel = request.getParam("channel");
		byte[] msg = request.getContent();
		// String type = request.getParam("type");
		if (channel == null || msg == null) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数:" + request.getParams());
		} else {
			String key = channel;
			resultObject.addReturnCode(CodeManager.CODE_200);
			// resultObject.addResultData(RedisConnectionPool.getCluster().zrangeWithScores(key,
			// 0, -1));
			String content = new String(msg);
			logger.debug("content json is [" + content + "]");
			List<Map<String, Object>> cards = new ArrayList<Map<String, Object>>();
			try {
				cards = Utils.getGson().fromJson(content, new TypeToken<List<Map<String, Object>>>() {
					private static final long serialVersionUID = 1L;
				}.getType());
			} catch (JsonSyntaxException e) {
				Map<String, Object> card = Utils.getGson().fromJson(content, new TypeToken<Map<String, Object>>() {

					private static final long serialVersionUID = 1L;
				}.getType());
				cards.add(card);
			}
			Map<String, Double> tmp = new HashMap<String, Double>();
			for (Map<String, Object> m : cards) {
				double score = System.currentTimeMillis();
				try {
					score = Double.parseDouble(m.get("pub_time").toString());
				} catch (NumberFormatException e) {
					logger.error("", e);
				}
				tmp.put(Utils.getGson().toJson(m), score);
			}
			Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				if (tmp.size() > 0) {
					js.zadd(key, tmp);
					logger.info("批量添加卡片成功 ,params is [" + request.getParams() + "]");
					logger.info("add card for " + key);
					resultObject.addReturnDesc("add scuccess");
				} else {
					resultObject.addReturnDesc("not found data");
					logger.info("not found data for " + key);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("", e);
			} finally {
				if (js != null) {
					js.close();
				}
			}

		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String pushMsgToReadList(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String userIds = request.getParam("userIds");
		byte[] msg = request.getContent();
		String time = request.getParam("time");
		// String type = request.getParam("type");
		// String channel = "main";
		if (StringUtils.isEmpty(userIds) || msg == null) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("缺失参数");
		} else {
			String[] ids = userIds.split(",");
			Jedis js = null;
			try {
				js = RedisConnectionPool.getCluster();
				for (String userId : ids) {
					String key = BasicConstants.READ_LIST_MAIN_CHANNEL + ":" + userId;
					if (null == time || time == "") {
						time = System.currentTimeMillis() + "";
					}
					js.zadd(key, Long.parseLong(time), new String(msg));
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (null != js) {
					js.close();
				}
			}

			resultObject.addReturnCode(CodeManager.CODE_200);
			resultObject.addReturnDesc("send success");
			logger.info("执行成功 ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String commentSwitch(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String flag = request.getParam("switch");
		String newsId = request.getParam("newsId");
		try {
			if (null == flag || null == newsId) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
			} else {
				Bucket bk = CBaseConnectionPool.getBucket(sourceOtherBucket);
				JsonDocument doc = bk.get(newsId);
				if (null != doc && "1".equals(flag)) { // 关闭评论
					doc.content().put("c_switch", flag);
					bk.replace(doc);
					resultObject.addReturnCode(CodeManager.CODE_200);
					resultObject.addReturnDesc("关闭评论");
					logger.info("[" + newsId + "]已关闭评论");
				} else if ("0".equals(flag)) { // 正常状态 可评论
					doc.content().put("c_switch", flag);
					bk.replace(doc);
					resultObject.addReturnDesc("可评论");
					logger.info("[" + newsId + "]可评论");
				} else {
					resultObject.addReturnCode(CodeManager.BM_COMMENTSWITCH);
					resultObject.addReturnDesc("操作有误");
					logger.info("[" + newsId + "] switch ：" + flag);
				}
			}
		} catch (Exception e) {
			logger.error("commentSwitch:", e);
			logger.error("开关评论失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.BM_COMMENTSWITCH);
			resultObject.addReturnDesc("开关评论失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	
	
	/**
	 * 根据实体id 进行删除 ，ids为复数 ，英文逗号分隔
	 * @param request
	 * @return
	 */
	public String deleteEntityIndexByIds(Request request){
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();

		String ids = request.getParam("entityIds");
		String table = request.getParam("type" ,BasicConstants.ENTITY_INDEX_TABLE_OTHER);

		try {
			if (StringUtils.isEmpty(ids)) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数,ids="+ids);
				logger.info("缺失参数,ids="+ids);
			} else {
				String[] idArray = ids.split(",");
				Map<String, Object> doc = null;
				StringBuffer docs = new StringBuffer();
				for (String id : idArray) {
					doc = new HashMap<>();
					doc.put("_index", BasicConstants.ENTITY_INDEX_DB);
					doc.put("_type", table);
					doc.put("_id", id);
					docs.append("{\"delete\":" + Utils.getGson().toJson(doc) + "} \n");
				}
				String esUrl = ConfigInit.getValue("esUrl"); 
				logger.info("url=" + esUrl +" , body="+docs);
				String re = HttpClientUtil.sendPostRequest(esUrl + "/_bulk", docs.toString(), "utf-8");
				logger.info(re);
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	/**
	 * 添加或更新subscribe-other索引库下的实体索引 post请求类型，body中为实体的json数组
	 * @param request
	 * @return
	 */
	public String upsertEntityIndex(Request request){
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String upDateType = request.getParam("upDateType" ,"index");
		byte[] body = request.getContent();

		try {
			if (null == body) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数");
				logger.info("缺失参数");
			} else {
				String content = new String(body);
				logger.info(content);
				JsonElement datasElement = Utils.getJsonParser().parse(content);
				
				StringBuffer docs = new StringBuffer();
				if(datasElement.isJsonArray()){
					JsonArray datas = datasElement.getAsJsonArray();
					for (JsonElement element : datas) {
						packIndex(element,docs ,upDateType);
					}
				}else{
					packIndex(datasElement,docs ,upDateType);
				}
				String esUrl = ConfigInit.getValue("esUrl"); 
//				logger.info("url=" + esUrl +" , body="+docs);
				String re = HttpClientUtil.sendPostRequest(esUrl + "/_bulk", docs.toString(), "utf-8");
				logger.info(re);
				resultObject.addReturnDesc(re);
			}
		} catch (Exception e) {
			logger.error("addIndex:", e);
			resultObject.addReturnCode(CodeManager.ESINDEXHANDLER_SEARCH);
			resultObject.addReturnDesc("addIndex is error");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}
	
	private void packIndex(JsonElement element ,StringBuffer docs ,String upDateType){
		com.google.gson.JsonObject jsonObj = element.getAsJsonObject();
		String id = null;
		String name = null;
		Map<String, Object> doc = null;
		Map<String, Object> suMap = null;
		JsonElement idElement = jsonObj.get("id");
		if(idElement == null){
			logger.warn("not fount id in ["+element+"]");
			return;
		}else{
			id = idElement.getAsString();
		}
		JsonElement nameElement = jsonObj.get("name");
		JsonElement nicknamesElement = jsonObj.get("nicknames");
		if(nameElement == null){
			logger.warn("not fount name in ["+element+"]");
			return;
		}else{
			name = nameElement.getAsString();
		}

		doc = new HashMap<>();
		suMap = new HashMap<>();
		List<String> inputs = new ArrayList<>();
		inputs.add(name);
		if(nicknamesElement != null){
			inputs.addAll(Arrays.asList(nicknamesElement.getAsString().split(",")));
		}
		suMap.put("output", name);
		suMap.put("input", inputs);
		doc.put("_index", BasicConstants.ENTITY_INDEX_DB);
		doc.put("_type", BasicConstants.ENTITY_INDEX_TABLE_OTHER);
		doc.put("_id", id);
		jsonObj.add(BasicConstants.ENTITY_SUGGEST_FIELD, Utils.getJsonParser().parse(Utils.getGson().toJson(suMap)));
		docs.append("{\""+upDateType+"\":" + Utils.getGson().toJson(doc) + "} \n");
		docs.append(jsonObj.toString() +"\n");
	}
	
	public static void main(String[] args) {
		Map<String, Object> suMap = new HashMap<>();
		suMap.put("gyc", "aa");
		suMap.put("dsd",1);
		System.out.println(Utils.getJsonParser().parse(suMap.toString()));
	}
	
	/*private String getConfigUrlById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String id = request.getParam("id");
		try {
			if (null == id) {
				resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
				resultObject.addReturnDesc("缺失参数Id");
			} else {
				JsonDocument doc = CBaseConnectionPool.getBucket(bkConfigBucket).get(id);
				if (null == doc) {
					resultObject.addReturnDesc("无法获取配置信息, doc is [" + doc + "]");
					;
					// resultObject.putHead("count", 1);
					logger.info("无法获取配置信息, id is [" + id + "]");
				} else {
					resultObject.addResultData(doc.content().toMap());
					resultObject.putHead("count", 1);
					logger.info("获取配置成功  ,params is [" + request.getParams() + "]");
				}
			}
		} catch (Exception e) {
			logger.error("getConfigUrlById:", e);
			logger.error("获取配置失败 ,params is [" + request.getParams() + "]");
			resultObject.addReturnCode(CodeManager.BM_GETCONFIGURLBYID);
			resultObject.addReturnDesc("获取配置失败  ,params is [" + request.getParams() + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
/*
	private String upConfig(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String configInfo = request.getParam("configInfo");
		if (StringUtils.isEmpty(configInfo)) {
			resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
			resultObject.addReturnDesc("configInfo is empty...");
		}
		try {
			if (configInfo != null) {
				String id = jsonParser.parse(configInfo).getAsJsonObject().get("stra_id").getAsString();
				if (null == id) {
					resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
					resultObject.addReturnDesc("document中缺失参数stra_id");
					logger.info("document中缺失参数stra_id");
				} else {
					JsonObject job = JsonObject.fromJson(configInfo);
					JsonDocument doc = JsonDocument.create(id, job);
					CBaseConnectionPool.getBucket(bkConfigBucket).upsert(doc);
					// resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
					resultObject.addReturnDesc("成功下发配置");
					logger.info("成功下发配置  ,configInfo is [" + configInfo + "]");
				}
			} else {
				if (null == request.getContent()) {
					resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
					resultObject.addReturnDesc("post 请求无法获取文档信息");
					logger.info("post 请求无法获取文档信息");
				} else {
					configInfo = new String(request.getContent());
					String id = jsonParser.parse(configInfo).getAsJsonObject().get("stra_id").getAsString();
					if (null == id) {
						resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
						resultObject.addReturnDesc("document中缺失参数stra_id");
						logger.info("document中缺失参数stra_id");
					} else {
						JsonObject job = JsonObject.fromJson(configInfo);
						JsonDocument doc = JsonDocument.create(id, job);
						CBaseConnectionPool.getBucket(bkConfigBucket).upsert(doc);
						// resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
						resultObject.addReturnDesc("成功下发配置");
						logger.info("成功下发配置  ,configInfo is [" + configInfo + "]");
					}
				}
			}
		} catch (Exception e) {
			logger.error("upConfig:", e);
			logger.error("upConfig error ,configInfo is [" + configInfo + "]");
			resultObject.addReturnCode(CodeManager.BM_UPCONFIG);
			resultObject.addReturnDesc("upConfig error ,configInfo is [" + configInfo + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}*/
}

class SendThread implements Runnable {
	Logger logger = LoggerFactory.getLogger(SendThread.class);
	CountDownLatch c = null;
	List<Map<String, Object>> list = null;
	String sql = "";
	String userId = "";
	int maxSize = 6;

	SendThread(CountDownLatch c, List<Map<String, Object>> list, String sql, String userId, int max) {
		this.c = c;
		this.list = list;
		this.sql = sql;
		this.userId = userId;
		this.maxSize = max;
	}

	public void run() {
		try {
			QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_BUCKET)
					.query(Query.simple(sql));
			if (queryResult.finalSuccess()) {
				List<Map<String, Object>> ls = CouchbaseUtil.resultToList(queryResult, BasicConstants.SOURCE_BUCKET);
				Map<String, Object> tmp = new HashMap<>();
				String newsId = null;
				String key = null;
				List<JsonDocument> documents = new ArrayList<JsonDocument>();
				int count = 0;
				for (Map<String, Object> map : ls) {
					if (null == map.get("isSend")) {
						count++;
						list.add(map);
						newsId = map.get("id").toString();
						key = userId + "_" + newsId;
						JsonDocument data = CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET).get(key);
						tmp.put("start_id", userId);
						tmp.put("end_id", newsId);
						tmp.put("send_relation", "1");
						tmp.put("update_time", System.currentTimeMillis());
						if (null != data) {
							Map<String, Object> tmpMap = data.content().toMap();
							tmpMap.putAll(tmp);
							tmp = tmpMap;
						}
						JsonObject job = JsonObject.from(tmp);
						JsonDocument doc = JsonDocument.create(key, job);
						documents.add(doc);
						if (maxSize == count) {
							break;
						}
						// CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET).upsert(doc);
					}
				}
				if (documents.size() > 0) {
					Observable.from(documents).flatMap(new Func1<JsonDocument, Observable<JsonDocument>>() {
						@Override
						public Observable<JsonDocument> call(final JsonDocument docToInsert) {
							return CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET).async()
									.upsert(docToInsert);
						}
					}).last().toBlocking().single();
				}
				// list.addAll(CouchbaseUtil.resultToList(queryResult ,
				// BasicConstants.SOURCE_BUCKET));
			} else {
				logger.error("", queryResult.info());
			}
		} catch (Exception e) {
			logger.error("", e);
		} finally {
			c.countDown();
		}
	}
}
/*
 * class SendThreadForMysql implements Runnable { Logger logger =
 * LoggerFactory.getLogger(SendThread.class); CountDownLatch c = null;
 * List<Map<String,Object>> list = null; String sql = "";
 * SendThreadForMysql(CountDownLatch c,List<Map<String,Object>> list ,String
 * sql){ this.c = c; this.list = list; this.sql = sql; } public void run() { try
 * { list.addAll(BackManagerHandler.getVideos(sql)); } catch (Throwable e) {
 * logger.error("current thread count is "+c.getCount(),e); }finally{
 * c.countDown(); logger.info("SendThread current count is "+c.getCount()); } }
 * }
 */