package cn.qctt.httpserver.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.pool.RedisConnectionPool;
import cn.qctt.httpserver.utils.ConnectionUtil;
import cn.qctt.httpserver.utils.CouchbaseUtil;
import cn.qctt.httpserver.utils.HBaseUtil;
import cn.qctt.httpserver.utils.HttpClientUtil;
import cn.qctt.httpserver.utils.JsonToMap;
import cn.qctt.httpserver.utils.SerializationUtil;
import cn.qctt.httpserver.utils.Utils;
import redis.clients.jedis.Jedis;

@RequestMapping(BasicConstants.URL_PREFIX + "/news")
public class NewsInfoHandler implements Handler {
	private static Logger logger = LoggerFactory.getLogger(NewsInfoHandler.class);
	private static String getNewsByIdUrl = BasicConstants.URL_PREFIX + "/news/getNewsById"; // 根据新闻Id
	private static String getNewsLookedCountUrl = BasicConstants.URL_PREFIX + "/news/getNewsLookedCount"; // 根据新闻阅读数
	private static String getNewsRCUrl = BasicConstants.URL_PREFIX + "/news/getNewsRC"; // 根据新闻Id与用户获取推荐新闻
	private static String getNewsVideoRCURL = BasicConstants.URL_PREFIX + "/news/getNewsVideoRC"; // 获取短视频推荐列表
	// 获取新闻详情页
	private static String getContentByIdUrl = BasicConstants.URL_PREFIX + "/news/getContentById"; // 根据新闻Id获取新闻内容
	private static String getNewsByIdInHbaseUrl = BasicConstants.URL_PREFIX + "/news/getNewsByIdInHbase"; // 根据新闻Id获取新闻内容
																											// Hbase
	private static String getNewsProByIdUrl = BasicConstants.URL_PREFIX + "/news/getNewsProById"; // 根据新闻Id
	private static String scanWeiboUrl = BasicConstants.URL_PREFIX + "/news/scanWeibo"; // 根据新闻Id
	// 获取新闻详情页

	// 3.* 内容禁用 与 内容删除
	private static String disableUrl = BasicConstants.URL_PREFIX + "/news/disable";
	private static String delUrl = BasicConstants.URL_PREFIX + "/news/del";

	private static JdbcTemplate connectionUtil = ConnectionUtil.getMysqlConnect();
	private static String htableName = BasicConstants.NEWS_HTABLE;
	private static String sourceBucket = BasicConstants.SOURCE_BUCKET;
	private static String sourceBucketPwd = "";
	private static String sourceOtherBucket = BasicConstants.SOURCE_OTHER_BUCKET;
	private static String sourceOtherBucketPwd = "";
	private static final String contentFamily = "content";

	@Override
	public String invoke(Request request, Response response) {
		ResultMsg resultObject = new ResultMsg();
		String urlPath = request.getPath();
		String returnStr = "";
		if (urlPath.startsWith(getNewsByIdUrl)) {
			returnStr = getNewsById(request);
		} else if (urlPath.startsWith(getNewsProByIdUrl)) {
			returnStr = getNewsProById(request);
		} else if (urlPath.startsWith(getContentByIdUrl)) {
			returnStr = getContentById(request);
		} else if (urlPath.startsWith(getNewsByIdInHbaseUrl)) {
			returnStr = getNewsByIdInHbase(request);
		} else if (urlPath.startsWith(getNewsRCUrl)) {
			returnStr = getNewsRC(request);
		} else if (urlPath.startsWith(getNewsVideoRCURL)) {
			returnStr = getNewsVideoRC(request);
		} else if (urlPath.startsWith(getNewsLookedCountUrl)) {
			returnStr = getNewsLookedCount(request);
		} else if (urlPath.startsWith(scanWeiboUrl)) {
			returnStr = scanWeibo(request);
		} else if (urlPath.startsWith(disableUrl)) {
			returnStr = disable(request);
		} else if (urlPath.startsWith(delUrl)) {
			returnStr = del(request);
		} else {
			resultObject.addReturnCode(CodeManager.CODE_404);
			resultObject.addReturnDesc("not found :" + urlPath);
			returnStr = resultObject.toJson();
			logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
		}
		return returnStr;
	}

	//1:已删除 ；
	private String del(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String deleted = request.getParam("deleted" ,"1");
		try {
			String bk = BasicConstants.SOURCE_OTHER_BUCKET;
			if (null != newsId) {
				JsonDocument doc = CBaseConnectionPool.get(bk, newsId);
				if(doc != null){
					JsonObject source = doc.content();
					source.put("deleted", deleted); // source
					CBaseConnectionPool.upsert(bk, doc);
					resultObject.putHead("count", 1);
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Exception e) {
			logger.error("deleted:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	// 1：禁用 ；其它：正常
	private String disable(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String disable = request.getParam("disable" ,"1");
		try {
			String bk = BasicConstants.SOURCE_OTHER_BUCKET;
			if (null != newsId) {
				JsonDocument doc = CBaseConnectionPool.get(bk, newsId);
				if(doc != null){
					JsonObject source = doc.content();
					source.put("disable", disable); // source
					CBaseConnectionPool.upsert(bk, doc);
					resultObject.putHead("count", 1);
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Exception e) {
			logger.error("disable:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String scanWeibo(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		byte[] bt = request.getContent();
		String rootUrl = "http://m.weibo.cn/mblogwx?id=";
		try {
			if (bt != null) {
				String uri = new String(bt);
				int end = uri.lastIndexOf("?");
				if (end < 1) {
					end = uri.length();
				}
				String idPath = uri.substring(uri.lastIndexOf("/") + 1, end);
				String body = HttpClientUtil.sendGetRequest(rootUrl + idPath);
				end = body.lastIndexOf("<!doctype");
				if (end > 0) {
					body = body.substring(0, body.lastIndexOf("<!doctype"));
				}
				resultObject.addResultData(body);
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
				resultObject.addReturnDesc("缺失body");
				logger.info("缺失body");
			}
		} catch (Exception e) {
			logger.error("scanWeibo:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
			resultObject.addReturnDesc("scan weibo fail");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getNewsLookedCount(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		try {
			if (!StringUtils.isEmpty(newsId)) {
				String sql = "select cl_c +1 as count from " + BasicConstants.SOURCE_OTHER_BUCKET + " use keys ['"
						+ newsId + "']";
				QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.SOURCE_OTHER_BUCKET)
						.query(Query.simple(sql));
				logger.info("sql is " + sql);
				if (queryResult.finalSuccess()) {
					resultObject.addResultData(CouchbaseUtil.resultToList(queryResult));
					resultObject.putHead("count", 1);
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Exception e) {
			logger.error("getNewsProById:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getContentById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		try {
			if (!StringUtils.isEmpty(newsId)) {
				// resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				resultObject.addResultData(
						HBaseUtil.getResultByFamily(htableName, newsId, contentFamily).get(contentFamily));
				resultObject.putHead("count", 1);
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETCONTENTBYID);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Exception e) {
			logger.error("getContentByIdUrl:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETCONTENTBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getNewsProById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		try {
			if (null != newsId) {
				JsonDocument doc = CBaseConnectionPool.getBucket(sourceBucket, sourceBucketPwd).get(newsId);
				JsonDocument doc1 = CBaseConnectionPool.getBucket(sourceOtherBucket, sourceOtherBucketPwd).get(newsId);
				if (doc1 == null) {
					resultObject.addReturnDesc("没有找到对应的新闻，newsId is " + newsId);
					logger.info("没有找到对应的新闻，newsId is " + newsId);
				} else {
					JsonObject source = null;
					/*
					 * if (doc == null) { source =
					 * doc1.content().getObject(BasicConstants.
					 * SOURCE_COLUMN_NAME); } else {
					 */
					source = doc.content();
					// }
					doc1.content().put("n_s", source); // source
					resultObject.addResultData(doc1.content().toMap());
					resultObject.putHead("count", 1);
				}
				// resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Exception e) {
			logger.error("getNewsProById:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSPROBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private String getNewsById(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String userId = request.getParam("userId");

		try {
			if (!StringUtils.isBlank(newsId)) {
				JsonDocument doc = CBaseConnectionPool.getBucket(sourceBucket, sourceBucketPwd).get(newsId);
				JsonDocument doc1 = CBaseConnectionPool.getBucket(sourceOtherBucket, sourceOtherBucketPwd).get(newsId);
				// doc1.content().put("n_s", doc.content() == null ?
				// JsonObject.empty() : doc.content()); // source
				if (doc1 == null) {
					resultObject.addReturnDesc("没有找到对应的新闻，newsId is " + newsId);
					logger.info("没有找到对应的新闻，newsId is " + newsId);
				} else {
					JsonObject source = null;
					source = doc.content();
					String deleted = doc1.content().getString("deleted");
					if(!"1".equals(deleted)){
						doc1.content().put("n_s", source); // source
						doc1.content().put("n_c",
								HBaseUtil.getResultByFamily(htableName, newsId, contentFamily).get(contentFamily)); // news
						doc1.content().put("cl_c", doc1.content().getInt("cl_c") + 1);
						Map<String, Object> news = doc1.content().toMap();
						news.remove("k_w");
						resultObject.addResultData(news);
						resultObject.putHead("count", 1);
					}else{
						resultObject.addReturnCode(CodeManager.NEWS_IS_DELETED);
						resultObject.addReturnDesc("此新闻已删除");
					}
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYID);
				resultObject.addReturnDesc("缺失 newsId = " + newsId + " ,userId = " + userId);
				logger.info("缺失 newsId = " + newsId);
			}
		} catch (Throwable e) {
			logger.error("getNewsById:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	@SuppressWarnings("unchecked")
	private String getNewsRC(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String userId = request.getParam("userId");
		Jedis redis = null;
			if (!StringUtils.isBlank(newsId) && !StringUtils.isBlank(userId)) {
				JsonDocument doc1 = CBaseConnectionPool.getBucket(sourceOtherBucket, sourceOtherBucketPwd).get(newsId);
				if (doc1 == null) {
					resultObject.addReturnDesc("没有找到对应的新闻，newsId is " + newsId);
					logger.info("没有找到对应的新闻，newsId is " + newsId);
				} else {
					Map<String, Object> m = doc1.content().toMap();
					Object entityNames = m.get("entity_names");
					Object tl = m.get("tl");
					if (entityNames != null) {
						Set<String> classIds = null;
						try {
							redis = RedisConnectionPool.getCluster();
							classIds = redis.smembers(BasicConstants.DISABLED_CLASS_KEY);
						} catch (Exception e) {
							e.printStackTrace();
						}finally{
							if(null != redis){
								redis.close();
							}
						}
						String query = "  {  \"query\": {    \"function_score\": {      \"query\": {         \"bool\": {           \"should\": [             {               \"multi_match\": {                 \"query\": \"$text\",                 \"fields\": [\"name\",\"nicknames\"]               }             },            {               \"range\": {                 \"rank\": {                   \"gte\": $rankGTE,                   \"lte\": $rankLTE                 }             }             }           ]          }      },      \"functions\": [        {          \"field_value_factor\": {            \"field\": \"rank\",            \"modifier\": \"log1p\",            \"factor\": 0.5          }        }      ],      \"boost_mode\": \"sum\",      \"max_boost\": 2    }  },  \"from\": $from,   \"size\": $size}";
						List<String> array = (List<String>) entityNames;
						Map<String, Object> rcMap = getRcMap(array, tl.toString(),userId);
						Map<String, Object> rc = transform2RC(rcMap, userId);
						// rc.put("stars",stars);
						List<Map<String, Object>> entitys = popDataFromCache(BasicConstants.RC_ENTITY_PREFIX+":"+userId, 0, 0);
						if(null == entitys || entitys.size() == 0){
							entitys = getOtherEntitys(entityNames.toString(), query, 0, 35);
							addStatAndFlows4NoFlowed(entitys, userId, null); // 给明星添加关注关系
							filteredDisabledClass(entitys, classIds);
							pushDataToCache(BasicConstants.RC_ENTITY_PREFIX+":"+userId, entitys);
						}
						if (null != entitys && entitys.size() > 1) {
							entitys = entitys.subList(0, 1);
						}
						rc.put("rc_entitys", entitys);
						List<Map<String, Object>> players = popDataFromCache(BasicConstants.RC_PLAYER_PREFIX+":"+userId, 0, 0);
						if(null == players || players.size() == 0){
							players = addRcPlayer(userId, entityNames.toString() + " " + tl ,35);
							addStatAndFlows4NoFlowed(players, userId, "1");
							filteredDisabledClass(players, classIds);
							pushDataToCache(BasicConstants.RC_PLAYER_PREFIX+":"+userId, players);
						}
						if (null != players && players.size() > 1) {
							players = players.subList(0, 1);
						}
						rc.put("rc_players", players);
						resultObject.addResultData(rc);
						resultObject.putHead("count", 1);
					}
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYID);
				resultObject.addReturnDesc("缺失 newsId = " + newsId + " ,userId = " + userId);
				logger.info("缺失 newsId = " + newsId + " ,userId = " + userId);
			}
		 
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private void filteredDisabledClass(List<Map<String, Object>> items ,Set<String> classIds){
		List<Map<String, Object>> temps = new ArrayList<>();
		for(Map<String,Object> map:items){
			if(classIds != null && classIds.contains(map.get("first_class_ids"))){
				temps.add(map);
			}
		}
		items.removeAll(temps);
	} 
	@SuppressWarnings("unchecked")
	private String getNewsVideoRC(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");
		String userId = request.getParam("userId");
		int size = Integer.parseInt(request.getParam("size", "4"));

		try {
			if (!StringUtils.isBlank(newsId) && !StringUtils.isBlank(userId)) {
				JsonDocument doc1 = CBaseConnectionPool
						.getBucket(BasicConstants.SOURCE_OTHER_BUCKET, sourceOtherBucketPwd).get(newsId);
				if (doc1 == null) {
					resultObject.addReturnDesc("没有找到对应的新闻，newsId is " + newsId);
					logger.info("没有找到对应的新闻，newsId is " + newsId);
				} else {
					Map<String, Object> m = doc1.content().toMap();
					Object entityNames = m.get("entity_names");
					Object tl = m.get("tl");
					if (entityNames != null) {
						List<String> array = (List<String>) entityNames;
						List<Map<String, Object>> videos = popDataFromCache(BasicConstants.RC_VIDEO_PREFIX+":"+userId, 0, size - 1);
						if(videos == null || videos.size() < 3){
							videos = getRc4VideoMap(array, tl.toString() +" 第 花絮 剧 期  电影 电视剧 篮球 明星");
							videos = getNoReaded(videos, userId);
							pushDataToCache(BasicConstants.RC_VIDEO_PREFIX+":"+userId, videos);
						}
						/*if (videos.size() < 4) {
							videos.addAll(getRc4VideoMap(new ArrayList<String>(), "第 花絮 剧 期  宋仲基"));
							videos = getNoReaded(videos, userId);
						}*/
						if (null != videos && videos.size() >= size) {
							videos = videos.subList(0, size);
						}
						Map<String, Object> rc = new HashMap<>();
						rc.put("video", videos);
						resultObject.addResultData(rc);
						resultObject.putHead("count", 1);
					} else {
						logger.warn("not fount entity names");
					}
				}
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYID);
				resultObject.addReturnDesc("缺失 newsId = " + newsId + " ,userId = " + userId);
				logger.info("缺失 newsId = " + newsId + " ,userId = " + userId);
			}
		} catch (Throwable e) {
			logger.error("getNewsVideoRC:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYID);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	private List<Map<String, Object>> addRcPlayer(String userId, String text ,int size) {
		List<Map<String, Object>> rcPlayer = null;
		try {

			String queryRoot = " {  \"query\": {    \"bool\": {      \"should\": [        {          \"multi_match\": {            \"query\": \"$text\",            \"fields\": [                \"intro\",              \"title^3\",              \"entity_names^6\"            ]          }        }      ]    }  },\"size\": $size} ";
			String queryUrl = "";
			String queryJson = "";
			String tableType = "";
			queryJson = queryRoot.replace("$text", text);
			queryJson = queryJson.replace("$size", size+"");
			tableType = "player";
			queryUrl = ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/" + tableType
					+ "/_search";
			rcPlayer = getRcPlayer(queryUrl, queryJson);
		} catch (Exception e) {
			logger.error("", e);
		}
		return rcPlayer;
	}

	private List<Map<String, Object>> getObjFromJsonString(String jsonStr) {
		JsonParser paser = new JsonParser();
		com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		List<Map<String, Object>> jobs = new ArrayList<>();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return jobs;
			}
			com.google.gson.JsonObject hits = hitsEle.getAsJsonObject();
			// JsonElement maxScoreObj = hits.get("max_score");
			// if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
			com.google.gson.JsonObject item = null;
			Map<String, Object> map = null;
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
				item = starObj.getAsJsonObject().get("_source").getAsJsonObject();
				map = JsonToMap.toMap(item);
				map.put("entity_id", map.get("id"));
				jobs.add(map);
			}
		}
		return jobs;
	}

	private List<Map<String, Object>> getRcPlayer(String queryUrl, String queryJson) {
		List<Map<String, Object>> jsonObjs = null;
		String jsonStr = HttpClientUtil.sendPostRequest(queryUrl, queryJson, "utf-8");
		jsonObjs = getObjFromJsonString(jsonStr);
		return jsonObjs;
	}

	private void addStatAndFlows4NoFlowed(List<Map<String, Object>> items, String userId, String type) {
		if (items.size() == 0) {
			return;
		}
		String bk = BasicConstants.USER_ENTITY_BUCKET;
		String sql = "select end_id  from " + bk + " use keys[";
		String sql2 = "select end_id,count(end_id) count from " + bk + " where end_id in [";
		String ids1 = "";
		String ids = "";
		for (Map<String, Object> map : items) {
			ids += "'" + map.get("entity_id").toString().replace("\"", "") + "',";
			ids1 += "'" + userId + "_" + map.get("entity_id").toString().replace("\"", "") + "',";
		}
		sql += ids1 + "'']";
		sql2 += ids + " ''] and  relation='11' ";
		if (type != null) {
			sql2 += " and type='" + type + "' ";
		} else {
			sql2 += " and type is missing ";
		}
		sql2 += "group by end_id";
		QueryResult queryResult = CBaseConnectionPool.sql(bk, sql);
		logger.info("sql is " + sql);
		QueryResult queryResult2 = CBaseConnectionPool.sql(bk, sql2);
		logger.info("sql is " + sql2);
		if (queryResult.finalSuccess()) {
			String endId = "";
			List<Map<String, Object>> tmp = new ArrayList<>();
			for (Map<String, Object> map : items) {
				com.couchbase.client.java.document.json.JsonObject val = null;
				map.put("is_followed", "0");
				map.put("fans_count", "0");
				boolean flowed = false;
				for (QueryRow row : queryResult.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						String flowedId = map.get("entity_id").toString().replace("\"", "");
						if (flowedId.equals(endId)) {
							map.put("is_followed", "1");
							flowed = true;
							break;
						}
					}
				}
				if (flowed) {
					tmp.add(map);
					continue;
				}
				for (QueryRow row : queryResult2.allRows()) {
					val = row.value();
					if (null != val) {
						endId = val.getString("end_id");
						if (map.get("entity_id").toString().equals(endId)) {
							map.put("fans_count", val.getInt("count") + "");
							break;
						}
					}
				}
			}
			if (tmp.size() > 0) {
				items.removeAll(tmp);
			}
		}

	}

	private static List<Map<String, Object>> getOtherEntitys(String text, String query, Integer from, Integer size) {
		List<Map<String, Object>> ls = new ArrayList<>();
		Map<String, Object> map = null;
		String tag = "5566";
		boolean isContinue = false;
		if (!text.contains(tag)) {
			text += " " + tag;
			isContinue = true;
		}
		// String bodyJson = ConfigInit.getValue(queryKey).replace("$text",
		// text);
		query = query.replace("$text", text);
		query = query.replace("$from", from + "");
		query = query.replace("$size", size + "");
		query = query.replace("$rankGTE", "0.01");
		query = query.replace("$rankLTE", "1");
		String jsonStr = HttpClientUtil.sendPostRequest(
				ConfigInit.getValue("esUrl") + "/" + BasicConstants.ENTITY_INDEX_DB + "/other/_search", query, "utf-8");
		double minScore = 0;
		JsonParser paser = new JsonParser();
		com.google.gson.JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			com.google.gson.JsonObject hits = obj.get("hits").getAsJsonObject();
			JsonElement maxScoreObj = hits.get("max_score");
			if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
				minScore = maxScoreObj.getAsDouble() * BasicConstants.QUERY_SCORE_PERCENT;
				logger.info("min score is " + minScore);
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					if (job.get("_score").getAsDouble() < minScore) {
						break;
					} else if (job.get("_score").getAsDouble() > 0.2) {
						com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
						String name = source.get("name").getAsString();
						if (isContinue && name.contains(tag)) {
							continue;
						}
						map.put("name", name);
						map.put("entity_id", source.get("id").getAsString());
						map.put("intro", source.get("intro").getAsString());
						map.put("img", source.get("img").getAsString());
						map.put("badge", source.get("badge").getAsString());
						map.put("rank", source.get("rank").getAsString());
						map.put("first_class_ids", source.get("first_class_ids").getAsString());
						/*
						 * JsonElement info = source.get("info_badge");
						 * if(info!=null && !info.isJsonNull()){
						 * map.put("info_badge", info.getAsString()); }else{
						 * map.put("info_badge", ""); }
						 */

						ls.add(map);
					}
				}
			}
		}
		return ls;
	}

	private String set2String(Set<String> set) {
		return set2String(set, " ", "");
	}

	private String set2String(Set<String> set, String delim, String warp) {
		String s = "";
		if (null == set || set.size() == 0) {
			return s;
		}
		for (String str : set) {
			s += warp + str + warp + delim;
		}
		return s.substring(0, s.length() - 1);
	}

	private String getStarFilter(Set<String> stars) {
		String filter = "\"filter\": {        \"query\": {          \"multi_match\": {            \"query\": \"$query\",            \"fields\": [\"title\",\"content\"]          }       } } ,";
		String query = "";
		query = set2String(stars);
		if (query.length() == 0) {
			return "";
		}
		filter = filter.replace("$query", query);
		return filter;
	}

	private Map<String, Object> getRcMap(List<String> array, String currentTitle ,String userId) {
		int defaultRcCount = 20;
		Map<String, Object> reMap = new HashMap<>();
		List<Map<String, Object>> cls = null;
		List<Map<String, Object>> tls = null;
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		Set<String> stars = new HashSet<>(array);
		String filter = getStarFilter(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", set2String(stars) + " " + currentTitle);
		bodyJson = bodyJson.replace("$from", "0");
		bodyJson = bodyJson.replace("$size", "" + defaultRcCount);
		bodyJson = bodyJson.replace("$class", "");
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr =null ;
		
		cls = popDataFromCache(BasicConstants.RC_NEWS_PREFIX+":"+userId, 0, 0);
		com.google.gson.JsonObject obj = null;
		double minScore = 0;
		if(cls == null || cls.size() == 0){
			cls = new ArrayList<>();
			jsonStr = HttpClientUtil.sendPostRequest(
					ConfigInit.getValue("esUrl") + "/nm_*/news,news_video,news_weibo/_search", bodyJson, "utf-8");
			obj = Utils.getJsonParser().parse(jsonStr).getAsJsonObject();
			if (obj != null) {
				JsonElement hitsEle = obj.get("hits");
				if (null == hitsEle) {
					return reMap;
				}
				com.google.gson.JsonObject hits = hitsEle.getAsJsonObject();
				// JsonElement maxScoreObj = hits.get("max_score");
				// if(maxScoreObj != null && ! maxScoreObj.isJsonNull()){
				// minScore = maxScoreObj.getAsDouble() * 0.4;
				String tableType = "";
				JsonElement orgUrl = null;
				JsonElement commentCount = null;
				JsonElement duration = null;
				for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
					com.google.gson.JsonObject job = starObj.getAsJsonObject();
					map = new HashMap<>();
					// if(job.get("_score").getAsDouble() < minScore){
					// break;
					// }else{
					tableType = job.get("_type").getAsString();
					com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
					String title = source.get("title").getAsString();
					if (title.equals(currentTitle)) {
						continue;
					}
					orgUrl = source.get("org_url");
					if (null != orgUrl) {
						map.put("org_url", orgUrl.getAsString());
						map.put("landing_param", orgUrl.getAsString());
					}
					commentCount = (JsonElement) source.get("comment_count");
					if (null != commentCount) {
						map.put("count", commentCount.getAsInt());
					} else {
						map.put("count", 0);
					}
					duration = (JsonElement) source.get("duration");
					if (null != duration) {
						map.put("duration", duration.getAsInt());
					}
					// map.put("type", tableType);
	
					if (tableType.equals(BasicConstants.NM_VIDEO)) {
						map.put("type", "video");
						map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
					} else if (tableType.equals(BasicConstants.NM_WEIBO)) {
						map.put("type", "weibo");
						map.put("link_type", BasicConstants.LINKED_TYPE_NEW_TAB);
					} else {
						map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
						map.put("type", "news");
					}
					map.put("news_title", title);
					map.put("id", source.get("id").getAsString());
					map.put("pub_time", source.get("pub_time").getAsLong());
					map.put("news_source", source.get("news_source").getAsString());
					map.put("list_images", source.get("list_images")==null ?null:source.get("list_images").getAsString());
					map.put("list_images_style", source.get("list_images_style")==null?null:source.get("list_images_style").getAsString());
					map.put("entity_names", source.get("entity_names")==null?null:source.get("entity_names").getAsString());
	
					cls.add(map);
				}
				 
				if (cls != null) {
					cls = getNoReaded(cls, userId);
				}
				pushDataToCache(BasicConstants.RC_NEWS_PREFIX+":"+userId, cls);
			// }
			 }
		}
		
		reMap.put("info", cls);

		// 推荐贴吧
		tls = popDataFromCache(BasicConstants.RC_BAR_PREFIX+":"+userId, 0, 0);
		if(tls == null || tls.size() == 0){
			jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/post_bar/_search", bodyJson,
					"utf-8");
			obj = Utils.getJsonParser().parse(jsonStr).getAsJsonObject();
			if (obj != null) {
				tls = new ArrayList<>();
				JsonElement hitsEle = obj.get("hits");
				if (null == hitsEle) {
					return reMap;
				}
				com.google.gson.JsonObject hits = hitsEle.getAsJsonObject();
				JsonElement maxScoreObj = hits.get("max_score");
				if (maxScoreObj != null && !maxScoreObj.isJsonNull()) {
					minScore = maxScoreObj.getAsDouble() * 0.4;
					String type = "";
					JsonElement orgUrl = null;
					JsonElement commentCount = null;
					JsonElement postFrom = null;
					JsonElement upCount = null;
					for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
						com.google.gson.JsonObject job = starObj.getAsJsonObject();
						map = new HashMap<>();
						if (job.get("_score").getAsDouble() < minScore) {
							break;
						} else {
							type = job.get("_type").getAsString();
							com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
							orgUrl = source.get("org_url");
							if (null != orgUrl) {
								map.put("org_url", orgUrl.getAsString());
							}
							commentCount = (JsonElement) source.get("comment_count");
							if (null != commentCount) {
								map.put("count", commentCount.getAsInt());
							} else {
								map.put("count", 0);
							}
							map.put("type", type);
							map.put("title", source.get("title").getAsString());
							map.put("id", source.get("id").getAsString());
							map.put("pub_time", source.get("pub_time").getAsLong());
							map.put("list_images", source.get("list_images").getAsString());
							map.put("list_images_style", source.get("list_images_style").getAsInt());
							map.put("entity_names", source.get("entity_names").getAsString());
							map.put("owners", source.get("owners").getAsString());
							postFrom = (JsonElement) source.get("post_from");
							if (null != postFrom) {
								map.put("up_count", postFrom.getAsString());
							} else {
								map.put("up_count", "1");
							}
							upCount = (JsonElement) source.get("up_count");
							if (null != upCount) {
								map.put("up_count", upCount.getAsInt());
							} else {
								map.put("up_count", 0);
							}
	
							tls.add(map);
						}
					}
					if (tls != null) {
						tls = getNoReaded(tls, userId);
					}
					pushDataToCache(BasicConstants.RC_BAR_PREFIX+":"+userId, tls);
				}
			}
		}
		
		reMap.put("bar", tls);
		return reMap;
	}

	private List<Map<String, Object>> getRc4VideoMap(List<String> array, String currentTitle) {
		int defaultRcCount = 24;
		List<Map<String, Object>> cls = new ArrayList<>();
		Map<String,Map<String, Object>> maps = new HashMap<>();
		Map<String, Object> map = null;
		String bodyJson = ConfigInit.getValue("esGetNewsQuery");
		Set<String> stars = new HashSet<>(array);
		String filter = getStarFilter(stars);
		bodyJson = bodyJson.replace("$filter", filter);
		bodyJson = bodyJson.replace("$text", set2String(stars) + " " + currentTitle);
		bodyJson = bodyJson.replace("$from", "0");
		bodyJson = bodyJson.replace("$size", "" + defaultRcCount);
		bodyJson = bodyJson.replace("$class", "");
		bodyJson = bodyJson.replace("$currentTime", System.currentTimeMillis() + "");
		String jsonStr = HttpClientUtil.sendPostRequest(ConfigInit.getValue("esUrl") + "/nm_*/news_video/_search",
				bodyJson, "utf-8");
		com.google.gson.JsonObject obj = Utils.getJsonParser().parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return cls;
			}
			com.google.gson.JsonObject hits = hitsEle.getAsJsonObject();
			// JsonElement maxScoreObj = hits.get("max_score");
			// if(maxScoreObj != null && ! maxScoreObj.isJsonNull()){
			// minScore = maxScoreObj.getAsDouble() * 0.4;
			// String tableType = "";
			JsonElement orgUrl = null;
			// JsonElement commentCount = null;
			JsonElement duration = null;
			StringBuffer newIds = new StringBuffer("[");
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
				com.google.gson.JsonObject job = starObj.getAsJsonObject();
				map = new HashMap<>();
				// if(job.get("_score").getAsDouble() < minScore){
				// break;
				// }else{
				// tableType = job.get("_type").getAsString();
				com.google.gson.JsonObject source = job.get("_source").getAsJsonObject();
				String title = source.get("title").getAsString();
				if (title.equals(currentTitle)) {
					continue;
				}
				orgUrl = source.get("org_url");
				if (null != orgUrl) {
					map.put("landing_param", orgUrl.getAsString());
				}

				duration = (JsonElement) source.get("duration");
				if (null != duration) {
					map.put("duration", duration.getAsInt());
				}
				map.put("type", "video");
				map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
				/*
				 * if(tableType.equals(BasicConstants.NM_VIDEO)||tableType.
				 * equals(BasicConstants.NM_WEIBO)){ map.put("link_type",
				 * BasicConstants.LINKED_TYPE_NEW_TAB);
				 * if(tableType.equals(BasicConstants.NM_VIDEO)){
				 * map.put("type", "video"); }else{ map.put("group", "weibo"); }
				 * }else{ map.put("link_type", BasicConstants.NEWS_LINKED_TYPE);
				 * map.put("group", "news"); }
				 */
				map.put("news_title", title);
				map.put("id", source.get("id").getAsString());

				newIds.append("'"+source.get("id").getAsString()+"' ,");
			/*	JsonDocument c_c = CBaseConnectionPool.get(BasicConstants.SOURCE_OTHER_BUCKET,
						source.get("id").getAsString());
				if (null != c_c) {
					map.put("count", c_c.content().getInt("c_c"));
				} else {
					map.put("count", 0);
				}*/

				map.put("pub_time", source.get("pub_time").getAsLong() + "");
				map.put("news_source", source.get("news_source").getAsString());
				map.put("list_images", source.get("list_images").getAsString());
				map.put("list_images_style", source.get("list_images_style").getAsString());
				// map.put("en", source.get("stars"));

//				cls.add(map);
				maps.put(source.get("id").getAsString(), map);
			}
			// }
			// }
			newIds.append("'' ]");
			String sql = "select c_c ,id from "+BasicConstants.SOURCE_OTHER_BUCKET +" use keys " +newIds.toString();
			logger.info("sql is "+sql);
			QueryResult queryResult = CBaseConnectionPool.sql(BasicConstants.SOURCE_OTHER_BUCKET, sql);
			for (QueryRow row : queryResult.allRows()) {
				JsonObject val = row.value();
				if (null != val) {
					map = maps.get(val.getString("id")) ;
					map.put("count", val.getInt("c_c"));
					cls.add(map);
				}
			}
		}

		return cls;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> transform2RC(Map<String, Object> rc, String userId) {
		List<Map<String, Object>> cls = null;
		List<Map<String, Object>> ls = null;
		List<Map<String, Object>> tls = null;
		// Map<String, Object> rc = (Map<String, Object>) map.get("rc");
		cls = (List<Map<String, Object>>) rc.get("info"); // 类别 /新聞
		tls = (List<Map<String, Object>>) rc.get("bar"); // 明星 /帖子
/*		try {
			if (rc != null) {
				if (cls != null) {
					cls = getNoReaded(cls, userId);
				}
				if (tls != null) {
					tls = getNoReaded(tls, userId);
				}
			}
		} catch (Throwable e) {
			logger.warn("getNoReaded for recommend in user[" + userId + "]", e);
		}*/
		if (cls != null && cls.size() > 0) {
			ls = getNews(cls);
			int tsize = ls.size();
			if (tsize >= 1) {
				rc.put("t", ls.subList(0, 1));
				ls = ls.subList(1, ls.size());
			}
			// addPro4RC(tls);
			int csize = ls.size();
			if (csize >= 2) {
				rc.put("c", ls.subList(0, 2));
			}
		} /*else {
			logger.info("", cls);
		}*/

		if (cls != null && cls.size() > 0) {
			rc.put("info", cls.get(0));
		} else {
			rc.put("info", null);
		}
		if (tls != null && tls.size() > 0) {
			int seed = tls.size() - 1;
			int index = 0;
			if (seed > 0) {
				index = Utils.randomInt(seed); // 贴吧随机
			}
			rc.put("bar", addUserInfo(tls.get(index)));
		} else {
			rc.put("bar", null);
		}
		return rc;
	}

	private Map<String, Object> addUserInfo(Map<String, Object> map) {
		Object postFrom = map.get("post_from");
		String table = "users_crawler";
		if (postFrom != null && postFrom.toString().equals("0")) {
			table = "users";
		}
		String mysql = "select nick_name,portrait  from " + table + " where user_id = " + map.get("owners");
		logger.info("mysql is " + mysql);
		Map<String, Object> tmpMap = connectionUtil.query(mysql, new ResultSetExtractor<Map<String, Object>>() {
			@Override
			public Map<String, Object> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				Map<String, Object> map = null;
				while (resultSet.next()) {
					map = new HashMap<>();
					map.put("nick_name", resultSet.getString("nick_name"));
					map.put("portrait", resultSet.getString("portrait"));
				}
				return map;
			}
		});
		if (null == tmpMap) {
			tmpMap = new HashMap<>();
			tmpMap.put("nick_name", "");
			tmpMap.put("portrait", "");
		}
		map.putAll(tmpMap);
		return map;
	}

	private List<Map<String, Object>> getNews(List<Map<String, Object>> data) {
		List<Map<String, Object>> tmp = new ArrayList<>();
		for (Map<String, Object> map : data) {
			String type = map.get("type").toString();
			if (type.equals("news") || type.equals("weibo")) {
				tmp.add(map);
			}
		}
		return tmp;
	}

	/*
	 * @SuppressWarnings("unchecked") private Map<String, Object>
	 * transform2RC(Map<String, Object> map, String userId) { List<Map<String,
	 * Object>> cls = null; List<Map<String, Object>> tls = null; Map<String,
	 * Object> rc = (Map<String, Object>) map.get("rc"); try { if (rc != null) {
	 * cls = (List<Map<String, Object>>) rc.get("c"); // 类别 tls =
	 * (List<Map<String, Object>>) rc.get("t"); // 明星 if(cls != null){ cls =
	 * getNoReaded(cls, userId); } if(tls != null){ tls = getNoReaded(tls,
	 * userId); } } } catch (Throwable e) { logger.warn(
	 * "getNoReaded for recommend in user[" + userId + "]", e); } if (null !=
	 * tls ) { int tsize = tls.size(); if(tsize > 1){ tls = tls.subList(0, 1); }
	 * addPro4RC(tls); rc.put("t", tls); } if(null != cls){ int csize =
	 * cls.size(); if(csize > 2){ cls = cls.subList(0, 2); } addPro4RC(cls);
	 * rc.put("c", cls); } return rc; }
	 */
	public void addPro4RC(List<Map<String, Object>> ls) {
		String sql = "select default.pub_time,default.id,default.news_source,default.list_images,default.list_images_style,default.news_title,source_other.c_c as count ,default.link_type ,default.org_url,default.org_url as landing_param from  default  use keys [";
		for (Map<String, Object> m : ls) {
			sql += "'" + m.get("id") + "',";
		}
		sql += "'']   join source_other on keys default.id";
		QueryResult queryResult = CBaseConnectionPool.getBucket(sourceOtherBucket).query(Query.simple(sql));
		logger.info("sql is " + sql);
		if (queryResult.finalSuccess()) {
			List<Map<String, Object>> data = CouchbaseUtil.resultToList(queryResult, "default");
			List<Map<String, Object>> tmp = new ArrayList<>();
			for (Map<String, Object> m : ls) {
				String newsId = m.get("id").toString();
				boolean isRemove = true;
				for (Map<String, Object> map : data) {
					if (newsId.equals(map.get("id").toString())) {
						Object title = map.get("news_title");
						if (null == title) {
							tmp.add(m);
							continue;
						}
						m.put("pub_time", map.get("pub_time"));
						m.put("news_source", map.get("news_source"));
						m.put("news_title", map.get("news_title"));
						m.put("list_images", map.get("list_images"));
						m.put("list_images_style", map.get("list_images_style"));
						m.put("count", map.get("count"));
						Object link_type = map.get("link_type");
						m.put("link_type", link_type);
						if (link_type != null) {
							m.put("landing_param", map.get("landing_param"));
						}
						isRemove = false;
					}
				}
				if (isRemove) {
					tmp.add(m);
				}
			}
			if (tmp.size() > 0) {
				ls.removeAll(tmp);
			}
		} else {
			logger.warn("addPro4RC", queryResult.info());
			ls = null;
		}
	}

	private List<Map<String, Object>> getNoReaded(List<Map<String, Object>> ls, String userId) {
		List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
		StringBuffer sb = new StringBuffer();
		Object id = "";
		for (Map<String, Object> map : ls) {
			id = map.get("id");
			if (id == null) {
				logger.warn("not found field id in [" + map + "] ,continue");
				continue;
			}
			sb.append("'" + userId + "_" + id + "',");
		}

		// todo
		/*
		 * String url = "http://" + ConfigInit.getValue("neo4jHost") + "/" +
		 * ConfigInit.getValue("getLookById") + "?id=" + userId +
		 * "&newsIds="+sb.toString(); com.google.gson.JsonObject job =
		 * Utils.getByUrl(url);
		 */
		String sql = "select end_id as id from " + BasicConstants.USER_NEWS_BUCKET + " use keys [" + sb.toString()
				+ "''] where look_relation ='1'";
		QueryResult queryResult = CBaseConnectionPool.getBucket(BasicConstants.USER_NEWS_BUCKET)
				.query(Query.simple(sql));
		logger.info("sql is " + sql);
		if (!queryResult.finalSuccess()) {
			logger.warn("check is readed for user[" + userId + "] ,return null");
			return ls;
		}

		for (Map<String, Object> map : ls) {
			boolean isReaded = false;
			id = map.get("id").toString();
			for (QueryRow row : queryResult.allRows()) {
				JsonObject val = row.value();
				if (null != val) {
					if (id.equals(val.getString("id"))) {
						isReaded = true;
						break;
					}
				}
			}
			if (!isReaded) {
				tmp.add(map);
			}
		}
		/*
		 * Collections.sort(tmp, new Comparator<Map<String, Object>>() { public
		 * int compare(Map<String, Object> arg0, Map<String, Object> arg1) { //
		 * 从大到小排序 return new Double(arg1.get("score").toString())
		 * .compareTo(Double.parseDouble(arg0.get("score").toString())); } });
		 */
		return tmp;
	}

	/*
	 * private List<Map<String, Object>> getNoReaded(List<Map<String, Object>>
	 * ls, String userId) { List<Map<String, Object>> tmp = new
	 * ArrayList<Map<String, Object>>(); StringBuffer sb = new StringBuffer();
	 * Object id = ""; for (Map<String, Object> map : ls) { id = map.get("id");
	 * if(id == null){ logger.warn("not found field id in ["+map+"] ,continue");
	 * continue; } sb.append(id + ","); }
	 * 
	 * // todo String url = "http://" + ConfigInit.getValue("neo4jHost") + "/" +
	 * ConfigInit.getValue("getLookById") + "?id=" + userId +
	 * "&newsIds="+sb.toString(); com.google.gson.JsonObject job =
	 * Utils.getByUrl(url); if (null == job) { logger.warn(
	 * "check is readed for user[" + userId + "] ,return null"); return ls; }
	 * JsonArray ids =
	 * job.get("response").getAsJsonObject().getAsJsonArray("data"); for
	 * (Map<String, Object> map : ls) { id = map.get("id").toString(); boolean
	 * isNoReaded = true; if(ids != null){ for (JsonElement newsId : ids) { if
	 * (id.equals(newsId.getAsString())) { isNoReaded = false; break; } } } if
	 * (!isNoReaded) { tmp.add(map); } } Collections.sort(tmp, new
	 * Comparator<Map<String, Object>>() { public int compare(Map<String,
	 * Object> arg0, Map<String, Object> arg1) { // 从大到小排序 return new
	 * Double(arg1.get("score").toString())
	 * .compareTo(Double.parseDouble(arg0.get("score").toString())); } });
	 * return tmp; }
	 */
	private String getNewsByIdInHbase(Request request) {
		long start = System.currentTimeMillis();
		ResultMsg resultObject = new ResultMsg();
		String newsId = request.getParam("newsId");

		try {
			if (null != newsId) {
				// resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
				resultObject.addResultData(HBaseUtil.getByRowKey(htableName, newsId));
				resultObject.putHead("count", 1);
			} else {
				resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYIDINHBASE);
				resultObject.addReturnDesc("缺失 newsId");
				logger.info("缺失 newsId");
			}
		} catch (Throwable e) {
			logger.error("getNewsByIdInHbase:", e);
			resultObject.addReturnCode(CodeManager.NEWS_GETNEWSBYIDINHBASE);
			resultObject.addReturnDesc("get news faild ,newsId is [" + newsId + "]");
		}
		resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
		logger.info((System.currentTimeMillis() - start) + " timing");
		return resultObject.toJson();
	}

	public static void main(String[] args) {
		String url = "http://m.weibo.cn/mblogwx?id=3922805903251524";
		System.out.println(url.substring(url.lastIndexOf("/"), url.lastIndexOf("?")));
	}
	
	private void pushDataToCache(String key ,List<Map<String, Object>> ls){
		Jedis js = null;
//		SerializationUtils.
		try {
//			byte[] bkey = SerializationUtils.serialize(key);
			List<byte[]> arrayLs = serializerMapEncode(ls);
			if(arrayLs != null && arrayLs.size()>0){
				js = RedisConnectionPool.getCluster();
				js.lpush(key.getBytes(), arrayLs.toArray(new byte[][]{}));
				js.expire(key.getBytes(), 60 * 2);
			}
		} catch(Exception e){
		  e.printStackTrace();
		  logger.error("",e);
		}finally{
			if(null != js){
				js.close();
			}
		}
	}
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> popDataFromCache(String key ,long start ,long end){
		Jedis js = null;
//		SerializationUtils.
		List<Map<String, Object>> ls = new ArrayList<>();
		try {
			js = RedisConnectionPool.getCluster();
			if(end - start <= 1){
				byte[] topMap = js.lpop(key.getBytes());
				if(topMap != null){
					ls.add((Map<String, Object>) SerializationUtil.unserialize(topMap));
				}
			}else{
				ls = serializerMapDecode (js.lrange(key.getBytes(),start ,end));
				js.ltrim(key.getBytes(), ls.size(), -1);
			}
			if(ls !=null && ls.size()>0){
				logger.info("hit cache ,key is "+key);
			}
		} catch(Exception e){
		  e.printStackTrace();
		  logger.error("",e);
		}finally{
			if(null != js){
				js.close();
			}
		}
		return ls;
	}
	
	
	private List<byte[]> serializerMapEncode(List<Map<String, Object>> ls){
		List<byte[]> byteLs = new ArrayList<>();
		byte[] byteMap = null; 
		for(Map<String, Object> map:ls){
			byteMap = SerializationUtil.serialize(map);
			if(byteMap == null){
				continue;
			}
			byteLs.add(byteMap);
		}
		return byteLs;
	}
	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> serializerMapDecode(List<byte[]> ls){
		List<Map<String, Object>> byteLs = new ArrayList<>();
		if(ls != null && ls.size() > 0){
			for(byte[] bt:ls){
				byteLs.add((Map<String, Object>)SerializationUtil.unserialize(bt));
			}
		}
		return byteLs;
	}
}
