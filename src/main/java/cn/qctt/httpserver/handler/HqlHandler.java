package cn.qctt.httpserver.handler;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.query.Query;
import com.couchbase.client.java.query.QueryResult;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import cn.qctt.httpserver.utils.CouchbaseUtil;

@RequestMapping(BasicConstants.URL_PREFIX+"/hql")
public class HqlHandler implements Handler {
    private static final Logger logger = LoggerFactory.getLogger(HqlHandler.class);
    private static String bucketName = BasicConstants.SOURCE_BUCKET;
    private static String bucketPwd = "";

    @Override
    public String invoke(Request request, Response response) {
        // TODO Auto-generated method stub
        return getByHql(request);
    }

    private String getByHql(Request request) {
    	long start = System.currentTimeMillis();
        ResultMsg resultObject = new ResultMsg();
        String sql = request.getParam("sql");
        String tabName = request.getParam("tabName");
        try {
            if (null == sql) {
                resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
                resultObject.addReturnDesc("缺失参数sql");
            } else {
                QueryResult queryResult = CBaseConnectionPool.getBucket(bucketName, bucketPwd)
                        .query(Query.simple(sql));
                if (queryResult.finalSuccess()) {
//                    resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
                    List<Map<String,Object>> ls = CouchbaseUtil.resultToList(queryResult ,tabName);
                    resultObject.addResultData(ls);
                    resultObject.putHead("count", ls.size());
                    logger.info("sql is [" + sql + "]");
                } else {
                    resultObject.addReturnCode(CodeManager.HQL);
                    resultObject.addReturnDesc("查询失败  ,sql is [" + sql + "]");
                }
            }
        } catch (Exception e) {
            logger.error("getByHql:", e);
            logger.error("getByHql 失败 ,params is [" + request.getParams() + "]");
            resultObject.addReturnCode(CodeManager.HQL);
            resultObject.addReturnDesc("getByHql失败  ,params is [" + request.getParams() + "]");
        }
        resultObject.putHead(BasicConstants.TIMING, System.currentTimeMillis() - start);
        logger.info((System.currentTimeMillis() - start)+" timing");
        return resultObject.toJson();
    }
}
