package cn.qctt.httpserver.handler;

import java.net.InetSocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.utils.ByteBufToBytes;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;


/**
 * @author yanchao_guo
 */
public class HttpProtocolDecoder extends SimpleChannelInboundHandler<Object> {
    private static Logger LOGGER = LoggerFactory.getLogger(HttpProtocolDecoder.class);
    private Request request = null;
    private ByteBufToBytes reader;
 
    @Override
    public void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {
        Object obj = msg;
        String ip = "";
        boolean isContinue = true;
        if (msg instanceof HttpRequest) {
            HttpRequest req = (HttpRequest) msg;
            ip = getIp(req ,ctx);
            request = Request.builderRequest(req);
            request.setRequestIp(ip);
            if (HttpHeaders.isContentLengthSet(req)) {
                reader = new ByteBufToBytes((int) HttpHeaders.getContentLength(req));
                isContinue = false;
            }
        }

        if (msg instanceof HttpContent) {
            HttpContent httpContent = (HttpContent) msg;
            ByteBuf content = httpContent.content();
            if (null != reader) {
                reader.reading(content);
//				content.release();
                isContinue = false;
                if (reader.isEnd()) {
                    byte[] data = reader.readFull();
                    request.setContent(data);
                    isContinue = true;
                }
            } else {
                return;
            }
        }

        if (isContinue) {
            obj = request;
            ctx.fireChannelRead(obj);
        }
    }


	private String getIp(HttpRequest reqest ,ChannelHandlerContext ctx) {
    	String clientIP = reqest.headers().get("X-Forwarded-For");
		if (clientIP == null) {
			InetSocketAddress insocket = (InetSocketAddress) ctx.channel()
					.remoteAddress();
			clientIP = insocket.getAddress().getHostAddress();
		}
		return clientIP;
	}


	@Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        LOGGER.error("error:", cause);
        ResultMsg res = new ResultMsg();
        res.addReturnDesc(cause.getMessage());
        res.addReturnCode(CodeManager.CODE_500);
        ctx.fireChannelRead(res.toJson());
    }


}