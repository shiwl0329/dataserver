package cn.qctt.httpserver.handler;

import cn.qctt.httpserver.annotation.RequestMapping;
import cn.qctt.httpserver.bean.Request;
import cn.qctt.httpserver.bean.Response;
import cn.qctt.httpserver.bean.ResultMsg;
import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;
import cn.qctt.httpserver.manager.TokenManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequestMapping(BasicConstants.URL_PREFIX+"/token")
public class TokenHandler implements Handler {
    private static Logger logger = LoggerFactory.getLogger(TokenHandler.class);
    private static String getTokenUrl = BasicConstants.URL_PREFIX+"/token/getToken";

    @Override
    public String invoke(Request request, Response response) {
        ResultMsg resultObject = new ResultMsg();
        String urlPath = request.getPath();
        String returnStr = "";
        if (urlPath.startsWith(getTokenUrl)) {
            returnStr = doAction(request);

        } else {
            resultObject.addReturnCode(CodeManager.MISSING_PARAMETERS);
            resultObject.addReturnDesc("not found :" + urlPath);
            returnStr = resultObject.toJson();
            logger.info("无法获取访问连接地址  ,url is [" + request.getUri() + "]");
        }
        return returnStr;
    }

    private String doAction(Request request) {

        ResultMsg resultObject = new ResultMsg();
        String token_user = request.getParam("token_user");
        String token_pwd = request.getParam("token_pwd");

        try {
        	String expire = TokenManager.getExpire(token_user, token_pwd);
            if (null != expire) {
                String token = TokenManager.getToken(token_user, expire);
//                resultObject.addReturnCode(CodeManager._CODE_SUCCESS);
                resultObject.addResultData(token);
                resultObject.putHead("count", 1);
            } else {
                resultObject.addReturnCode(CodeManager.CODE_400);
                resultObject.addReturnDesc("获取token时安全认证失败");
                logger.info("获取token时安全认证失败  ,token_user is [" + token_user + "]");
            }
        } catch (Exception e) {
            logger.error("doAction:", e);
            logger.error("get token faild ,appid is [" + token_user + "]");
            resultObject.addReturnCode(CodeManager.CODE_400);
            resultObject.addReturnDesc("get token faild ,token_user is [" + token_user + "]");
        }
        return resultObject.toJson();
    }
}
