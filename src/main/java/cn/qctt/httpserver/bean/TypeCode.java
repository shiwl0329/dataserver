package cn.qctt.httpserver.bean;

public enum TypeCode {
	/**
     * 新闻
     */
    NEWS{
        public int getType() {
            return 1;
        }
    },
    /**
     * 微博
     */
    NEWS_WEIBO{
        public int getType() {
            return 2;
        }
    },
    /**
     * 视频
     */
    NEWS_VIDEO{
        public int getType() {
            return 3;
        }
    },
    /**
     * 贴吧
     */
    POST_BAR{
        public int getType() {
            return 4;
        }
    },
    /**
     * 明星
     */
    STARS{
        public int getType() {
            return 7;
        }
    },
    /**
     * 主题
     */
    THEME{
    	public int getType() {
    		return 11;
    	}
    },
    /**
     * 好看
     */
    ARDENLY{
    	public int getType() {
    		return 12;
    	}
    },
    /**
     * 话题
     */
    TOPIC_MODEL{
    	public int getType() {
    		return 13;
    	}
    },
    /**
     * 广告
     */
    AD{
    	public int getType() {
    		return 14;
    	}
    },
    /**
     * 活动
     */
    ACTIVITY{
    	public int getType() {
    		return 15;
    	}
    },
	/**
	 * 追剧
	 */
	PLAYER{
		public int getType() {
			return 10;
		}
	};

    public abstract int getType();
}
