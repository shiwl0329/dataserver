package cn.qctt.httpserver.bean;

import java.io.InputStream;

import cn.qctt.httpserver.utils.Utils;

public class CurlResponse{
	private int code = 200;
	private InputStream stream;
	
	public CurlResponse(int code ,InputStream stream){
		this.code = code;
		this.stream = stream;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public InputStream getStream() {
		return stream;
	}
	public void setStream(InputStream stream) {
		this.stream = stream;
	}
	
	public byte[] getContent(){
		byte[] b = null;
		try {
			b = Utils.readStream(stream);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}
}
