package cn.qctt.httpserver.bean;

import java.util.Map;

public class Regular {
	private Map<String, Object> strategy = null;
	private Map<String, Object> userAttribute = null;
	private boolean ok = true;
	private String desc = "success path";
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public Regular( Map<String, Object> strategy ,Map<String, Object> userAttribute){
		this.userAttribute = userAttribute;
		this.strategy = strategy;
	}

	public Map<String, Object> getUserAttribute() {
		return userAttribute;
	}

	public void setUserAttribute(Map<String, Object> userAttribute) {
		this.userAttribute = userAttribute;
	}

	public Map<String, Object> getStrategy() {
		return strategy;
	}

	public void setStrategy(Map<String, Object> strategy) {
		this.strategy = strategy;
	}
	
}
