package cn.qctt.httpserver.bean;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import cn.qctt.httpserver.core.BasicConstants;
import cn.qctt.httpserver.manager.CodeManager;

public class ResultMsg {

    private static Gson gson = new Gson();

//    private String statusCode = CodeManager.CODE_200;
    private Map<String, Object> result = new HashMap<String, Object>();
//    private Map<String, Object> head = new HashMap<String, Object>();
    private Map<String, Object> response = new HashMap<String, Object>();

    public ResultMsg() {
//    	head.put("count", 0);
    	response.put("message", "ok");
    	response.put("count", 0);
    	result.put("statusCode", CodeManager.CODE_200);
//    	response.put("data", result);
    }
    
    public void addReturnCode(Object code) {
    	result.put("statusCode", code);
    }

    public void addReturnDesc(Object desc) {
    	response.put("message", desc);
    }

    public void putHead(String key, Object val) {
    	response.put(key, val);
    }
    public void putCount(int count) {
    	response.put("count", count);
    }

 /*   public Object getByKeyFromHead(String key) {
        return result.get(key);
    }
*/
    public void addResultData(Object value) {
    	response.put("data", value);
    }

    public void putBody(String key, Object value) {
    	response.put(key, value);
    }

    public Object getByKeyFromBody(String key) {
        return response.get(key);
    }

    private void clear() {
//        head.clear();
    	response.clear();
        result.clear();
    }

    public String toJsonWithCallBack() {
        if (BasicConstants.JSONP_RETURN_CALL_METHOD.trim() == "") {
            return toJson();
        }
        return BasicConstants.JSONP_RETURN_CALL_METHOD + "(" + toJson() + ")";
    }

    public String toJson() {
//        result.put("statusCode", head);
        result.put("response", response);
        String json = gson.toJson(result);
        clear();
        return json;
    }

}