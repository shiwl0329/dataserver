package cn.qctt.httpserver.bean;

public class PlayerEntity {
	private String id = null;
	private String title = null;
	private Integer tv_type = null;
	private String fans_count = null;
	private String entity_names = null;
	private String img = null;
	private String img_v = null;
	private String entity_id = null;
	private String intro = null;
	private String news_source = null;
	private String list_images = null;
	private String list_images_v = null;
	private String is_followed = null;
	private String first_class_ids = null;
	private Long pub_time = null;
	private Double rank = null;
	private Integer list_images_style = null;
	private String district = null;
	
	private String c_play_id = null;
	private String c_play_url = null;
	private Long c_pub_time = null;
	
	
	public Double getRank() {
		return rank;
	}
	public void setRank(Double rank) {
		this.rank = rank;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getTv_type() {
		return tv_type;
	}
	public void setTv_type(Integer tv_type) {
		this.tv_type = tv_type;
	}
	public String getFans_count() {
		return fans_count;
	}
	public void setFans_count(String fans_count) {
		this.fans_count = fans_count;
	}
	public String getEntity_names() {
		return entity_names;
	}
	public void setEntity_names(String entity_names) {
		this.entity_names = entity_names;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getImg_v() {
		return img_v;
	}
	public void setImg_v(String img_v) {
		this.img_v = img_v;
	}
	public String getEntity_id() {
		return entity_id;
	}
	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getNews_source() {
		return news_source;
	}
	public void setNews_source(String news_source) {
		this.news_source = news_source;
	}
	public String getList_images() {
		return list_images;
	}
	public void setList_images(String list_images) {
		this.list_images = list_images;
	}
	public String getList_images_v() {
		return list_images_v;
	}
	public void setList_images_v(String list_images_v) {
		this.list_images_v = list_images_v;
	}
	public String getIs_followed() {
		return is_followed;
	}
	public void setIs_followed(String is_followed) {
		this.is_followed = is_followed;
	}
	public String getFirst_class_ids() {
		return first_class_ids;
	}
	public void setFirst_class_ids(String first_class_ids) {
		this.first_class_ids = first_class_ids;
	}
	public Long getPub_time() {
		return pub_time;
	}
	public void setPub_time(Long pub_time) {
		this.pub_time = pub_time;
	}
	public Integer getList_images_style() {
		return list_images_style;
	}
	public void setList_images_style(Integer list_images_style) {
		this.list_images_style = list_images_style;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getC_play_id() {
		return c_play_id;
	}
	public void setC_play_id(String c_play_id) {
		this.c_play_id = c_play_id;
	}
	public String getC_play_url() {
		return c_play_url;
	}
	public void setC_play_url(String c_play_url) {
		this.c_play_url = c_play_url;
	}
	public Long getC_pub_time() {
		return c_pub_time;
	}
	public void setC_pub_time(Long c_pub_time) {
		this.c_pub_time = c_pub_time;
	}
	
}
