package cn.qctt.httpserver.bean;

import java.util.LinkedHashMap;
import java.util.Map;

public class OtherEntity {
	private String id = null;
	private String name = null;
	private String first_class_ids = null;
	private String nicknames = null;
	private String img = null;
	private Double rank = null;
	private String badge = null;
	private String intro = null;
	private Map<String ,Object> name_su = new LinkedHashMap<>(); 
	
	
	public Map<String, Object> getName_su() {
		return name_su;
	}
	public void setName_su(Map<String, Object> name_su) {
		this.name_su = name_su;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirst_class_ids() {
		return first_class_ids;
	}
	public void setFirst_class_ids(String first_class_ids) {
		this.first_class_ids = first_class_ids;
	}
	
	public String getNicknames() {
		return nicknames;
	}
	public void setNicknames(String nicknames) {
		this.nicknames = nicknames;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public Double getRank() {
		return rank;
	}
	public void setRank(Double rank) {
		this.rank = rank;
	}
	public String getBadge() {
		return badge;
	}
	public void setBadge(String badge) {
		this.badge = badge;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{ id="+id+",")
			.append("name="+name+",")
			.append("first_class_ids="+first_class_ids+",")
			.append("nicknames="+nicknames+",")
			.append("img="+img+",")
			.append("rank="+rank+",")
			.append("badge="+badge+",")
			.append("intro="+intro+",")
			.append("name_su="+name_su+" }");
		return sb.toString();
	}
	
	
}
