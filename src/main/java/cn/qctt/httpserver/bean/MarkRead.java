package cn.qctt.httpserver.bean;

import java.util.Map;

public class MarkRead {
	String userId = "";
	String cardReadChanel = "";
	Map<String,Object> card = null;
	Long time = 0L;
	
	public MarkRead(String userId ,String cardReadChanel ,Map<String,Object> card ,Long time){
		this.userId = userId;
		this.cardReadChanel = cardReadChanel;
		this.card = card;
		this.time = time;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCardReadChanel() {
		return cardReadChanel;
	}
	public void setCardReadChanel(String cardReadChanel) {
		this.cardReadChanel = cardReadChanel;
	}
	public Map<String, Object> getCard() {
		return card;
	}
	public void setCard(Map<String, Object> card) {
		this.card = card;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	
	
}
