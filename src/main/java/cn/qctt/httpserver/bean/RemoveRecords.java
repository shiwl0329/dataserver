package cn.qctt.httpserver.bean;

public class RemoveRecords {
	String cardReadJson = "";
	String userId = "";
	String cardReadedChanel = "";
	int size = 3;
	public RemoveRecords(){}
	public RemoveRecords(String topicCardReadJson ,String userId ,String cardReadedChanel ,int size){
		this.cardReadJson = topicCardReadJson;
		this.userId = userId;
		this.cardReadedChanel = cardReadedChanel;
		this.size = size;
	}
	
	
	public String getCardReadJson() {
		return cardReadJson;
	}
	public void setCardReadJson(String cardReadJson) {
		this.cardReadJson = cardReadJson;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getCardReadedChanel() {
		return cardReadedChanel;
	}
	public void setCardReadedChanel(String cardReadedChanel) {
		this.cardReadedChanel = cardReadedChanel;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
}
