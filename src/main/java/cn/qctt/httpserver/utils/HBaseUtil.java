package cn.qctt.httpserver.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.pool.HbaseConnectionPool;

public class HBaseUtil {
    private static Logger logger = LoggerFactory.getLogger(HBaseUtil.class);

    public static Map<String, String> getByRowKey(String htableName, String rowKey) throws IOException, Exception {
        HTableInterface htable = HbaseConnectionPool.getConnection().getTable(htableName);
        HashMap<String, String> hm = new HashMap<String, String>();
        try {
            Get get = new Get(rowKey.getBytes());
            Result result = htable.get(get);
            String col = null;
            String key = null;
            for (Cell cell : result.listCells()) {
                col = Bytes.toString(CellUtil.cloneQualifier(cell));
                key = (col == null ? Bytes.toString(CellUtil.cloneFamily(cell)) : col);
                hm.put(key, Bytes.toString(CellUtil.cloneValue(cell)));
            }
//            System.err.println(hm.size());
//            System.err.println(hm.toString());
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(e);
        } finally {
            try {
                htable.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("getByRowKey", e);
            }
        }
        return hm;
    }

    public static void dropTable(String table) throws MasterNotRunningException, ZooKeeperConnectionException, IOException{
    	 HBaseAdmin admin = new HBaseAdmin(HbaseConnectionPool.getConfig());  
	     admin.disableTable(table);  
	     admin.deleteTable(table);  
	     admin.close();
    }
    
    public static Map<String, String> getResultByColumn(String htableName, String rowKey,
                                                        String familyName, String columnName) throws IOException, Exception {
        HTableInterface htable = HbaseConnectionPool.getConnection().getTable(htableName);
        HashMap<String, String> hm = new HashMap<String, String>();
        try {
            Get get = new Get(rowKey.getBytes());
            get.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(columnName));
            Result result = htable.get(get);
            String col = null;
            String key = null;
            for (Cell cell : result.listCells()) {
                col = Bytes.toString(CellUtil.cloneQualifier(cell));
               /* System.out.println("列  族：" + Bytes.toString(CellUtil.cloneFamily(cell)));
                System.out.println("列  名:" + col);
                System.out.println("列  值：" + Bytes.toString(CellUtil.cloneValue(cell)));
                System.out.println("时间戳：" + cell.getTimestamp());*/
             
                key = (col == null ? Bytes.toString(CellUtil.cloneFamily(cell)) : col);
                hm.put(key, Bytes.toString(CellUtil.cloneValue(cell)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                htable.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("getByRowKey", e);
            }
        }
        return hm;
    }

    public static Map<String, String> getResultByFamily(String htableName, String rowKey,
                                                        String familyName) throws IOException, Exception {
        HTableInterface htable = HbaseConnectionPool.getConnection().getTable(htableName);
        HashMap<String, String> hm = new HashMap<String, String>();
        try {
            Get get = new Get(rowKey.getBytes());
            get.addFamily(Bytes.toBytes(familyName));
            Result result = htable.get(get);
            String col = null;
            if(result.listCells() == null){
            	return hm;
            }
            for (Cell cell : result.listCells()) {
                col = Bytes.toString(CellUtil.cloneFamily(cell));
                hm.put(col, Bytes.toString(CellUtil.cloneValue(cell)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                htable.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("getByRowKey", e);
            }
        }
        return hm;
    }
}
