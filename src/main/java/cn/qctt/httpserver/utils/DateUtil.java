package cn.qctt.httpserver.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);
	private static final String DEFAUL_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String DEFAUL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static String getCurrentTime(){
		return getCurrentTime(DEFAUL_TIME_FORMAT);
	}
	public static String getCurrentTime(String format){
		return getDateFormat(format).format(new Date());
	}
	public static long getBeforeMonthTime(int num){
		Calendar c=Calendar.getInstance();
		c.setTime(new Date());
		if(DateUtil.getCurrentTime("MM").equals("01") || num >12){
			if(num<=12){
				c.roll(Calendar.YEAR, -1);
			}else{
				c.roll(Calendar.YEAR, -(num/12));
			}
		}
		c.roll(Calendar.MONTH, -num);
		return c.getTimeInMillis();
	}
	public static long getAfterMonthTime(int num){
		Calendar c=Calendar.getInstance();
		c.setTime(new Date());
		if(DateUtil.getCurrentTime("MM").equals("12") || num >12){
			if(num<=12){
				c.roll(Calendar.YEAR, 1);
			}else{
				c.roll(Calendar.YEAR, (num/12));
			}
		}
		c.roll(Calendar.MONTH, num);
		return c.getTimeInMillis();
	}
	
	public static String getCurrentDate(){
		return getCurrentTime(DEFAUL_DATE_FORMAT);
	}
	public static long getCurrentDayMillis(){
		long time = 0;
		try {
			time = getDateFormat("yyyyMMdd").parse(getDateFormat("yyyyMMdd").format(new Date())).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("",e);
		}
		return time;
	}
	
	public static Date addDay(int days){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, days);
		c.setTime(new Date());
		return c.getTime();
	}
	
	public static SimpleDateFormat getDateFormat(String format){
		return  new SimpleDateFormat(format);
	}
	
	public static Date parse(String dateStr,String format){
		try {
			return getDateFormat(format).parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return null;
	}
	public static String format(Date date,String format){
		try {
			return getDateFormat(format).format(date);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return null;
	}

	public static long getTimeMillisBefore(int days){
		return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(days);
	}
	public static void main(String[] args) {
		System.out.println(getDateFormat("yyyyMMdd HHmmss").format(new Date(1466817420000L)));
		System.out.println(getDateFormat("yyyyMMdd HHmmss").format(new Date(1465446660000L)));
		
	}
	
}
