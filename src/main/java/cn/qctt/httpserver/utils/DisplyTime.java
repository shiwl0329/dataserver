package cn.qctt.httpserver.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
public class DisplyTime {
	
	public static Long downRefresh(Long pub_time,Long last_refresh_time)
	{
		Long current_time=System.currentTimeMillis();
		Long interval = (current_time - last_refresh_time) / (1000 * 60);
		Random random=new Random();
		
		Long displyTime;
		if(interval<1)
		{
			int randomInt=random.nextInt(50)+9;
			displyTime=(current_time-1000*randomInt);
		}
		else if(interval>=1&&interval<5)
		{
			int randomInt=random.nextInt(5);
			displyTime=current_time-(1000 * 60)*randomInt;
		}
		else if(interval>=5&&interval<30)
		{
			int randomInt=random.nextInt(9);
			displyTime=current_time-(1000 * 60)-(1000 * 60)*randomInt;
		}
		else if(interval>=30&&interval<60)
		{
			int randomInt=random.nextInt(15);
			displyTime=current_time-(1000 * 60)*2-(1000 * 60)*randomInt;
		}
		else if(interval>=60&&interval<60*24)
		{
			int randomInt=random.nextInt(50);
			Long innerDisplayTime=current_time-(1000 * 60)*5-(1000 * 60)*randomInt;
			innerDisplayTime=pub_time<innerDisplayTime?innerDisplayTime:pub_time;
			displyTime=innerDisplayTime;
		}
		else 
		{
			int randomInt=random.nextInt(420);
			Long innerDisplayTime=current_time-(1000 * 60)*30-(1000 * 60)*randomInt;
			innerDisplayTime=pub_time<innerDisplayTime?innerDisplayTime:pub_time;
			displyTime=innerDisplayTime;
		}
		
		return displyTime;
	}
	
	
	public static Long upRefresh(Long pub_time,Long earliest_display_time)
	{
		Long current_time=System.currentTimeMillis();
		Long interval = (current_time - earliest_display_time) / (1000 * 60);
		Random random=new Random();
		Long displyTime;
		if(interval<10)
		{
			int randomInt=random.nextInt(2);
			displyTime=earliest_display_time-(1000 * 60)*randomInt;
			displyTime=pub_time<displyTime?displyTime:pub_time;
		}
		else if(interval>=10&&interval<120)
		{
			int randomInt=random.nextInt(5);
			displyTime=earliest_display_time-(1000 * 60)*randomInt;
			displyTime=pub_time<displyTime?displyTime:pub_time;
		}
		else if(interval>=60*2&&interval<60*24)
		{
			int randomInt=random.nextInt(10);
			displyTime=earliest_display_time-(1000 * 60)*randomInt;
			displyTime=pub_time<displyTime?displyTime:pub_time;
		}
		else 
		{
			int randomInt=random.nextInt(20);
			displyTime=earliest_display_time-(1000 * 60)*randomInt;
			displyTime=pub_time<displyTime?displyTime:pub_time;
		}
		
		return displyTime;
	}
	
	
	
	
	
	
	public static String getStrTime(Long cc_time) {  
		String re_StrTime = null;  
		  
		  
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒");  
		// 例如：cc_time=1291778220  
		long lcc_time = cc_time;  
		re_StrTime = sdf.format(new Date(lcc_time ));  
		  
		  
		return re_StrTime;  
		  
		  
		}   

}

