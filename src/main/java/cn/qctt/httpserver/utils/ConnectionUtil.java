package cn.qctt.httpserver.utils;

import com.alibaba.druid.pool.DruidDataSource;
import cn.qctt.httpserver.core.ConfigInit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import redis.clients.jedis.JedisCluster;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Administrator on 15-6-3. 连接池管理工具
 */
public class ConnectionUtil {
	private static final Logger logger = LoggerFactory.getLogger(ConnectionUtil.class);
	private static JedisCluster cluster;

	private static JdbcTemplate jdbcTemplate;
	private static JdbcTemplate jdbcTemplate2;

	public static JedisCluster getCluster() {
		return cluster;
	}

	public static void setCluster(JedisCluster cluster) {
		ConnectionUtil.cluster = cluster;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		ConnectionUtil.jdbcTemplate = jdbcTemplate;
	}

	//
	// mysql property
	private static final String mysqlDriver = "com.mysql.jdbc.Driver";
	private static final String mysqlUrl = ConfigInit.getValue("mySqlUrl");
	private static final String mysqlUser = ConfigInit.getValue("mySqlUser");
	private static final String mysqlPassword = ConfigInit.getValue("mySqlPasswd");
	private static final String mySqlMaxWait = ConfigInit.getValue("mySqlMaxWait");
	private static final String mySqlMaxActive = ConfigInit.getValue("mySqlMaxActive");
	private static final String mySqlInitialSize = ConfigInit.getValue("mySqlInitialSize");
	private static final String mySqlMinIdle = ConfigInit.getValue("mySqlMinIdle");
	private static final String mySqlRemoveAbandonedTimeout = ConfigInit.getValue("mySqlRemoveAbandonedTimeout");

	// //redis property
	// private static final String host = ConfigInit.getValue("redisHost");
	// private static final int port =
	// Integer.parseInt(ConfigInit.getValue("redisPort"));

	private ConnectionUtil() {
	}

	public synchronized static JdbcTemplate getMysqlConnect(String dbUrl,String user,String pwd) {
		return jdbcTemplate2 == null ? (jdbcTemplate2 = initMySql(dbUrl ,user ,pwd)) : jdbcTemplate2;
	}
	public synchronized static JdbcTemplate getMysqlConnect() {
		return jdbcTemplate == null ? (jdbcTemplate = initMySql(mysqlUrl,mysqlUser,mysqlPassword)) : jdbcTemplate;
	}

	// public synchronized static JedisCluster getRedisConnect() {
	// return cluster == null ? initRedis() : cluster;
	// }

	public static void main(String[] args) {
		JdbcTemplate tmp = initMySql("jdbc:mysql://thadoop3/nomiss" ,"next_tech" ,"00e4398aa6");
		String mysql = "SELECT 	star.id, 	star.rank, star.intro,	star.name,star.badge, 	star.img  , 	s.type AS relation FROM entity  as star 	LEFT JOIN star_relas as s ON s.end_id=star.id	left join star_code on star_code.type=s.type 	WHERE s.start_name='"
				+ "黄晓明' and star.status=1 GROUP BY star.id order by star_code.code desc limit " + 1;
		logger.info("mysql is " + mysql);
		List<Map<String, Object>> data = tmp.query(mysql,
				new ResultSetExtractor<List<Map<String, Object>>>() {
					@Override
					public List<Map<String, Object>> extractData(ResultSet resultSet)
							throws SQLException, DataAccessException {
						List<Map<String, Object>> tmp = new ArrayList<>();
						while (resultSet.next()) {
							Map<String, Object> m = new HashMap<String, Object>();
							m.put("img", resultSet.getString("img"));
							m.put("badge", resultSet.getString("badge"));
							m.put("name", resultSet.getString("name"));
							m.put("intro", resultSet.getString("intro"));
							m.put("star_id", resultSet.getString("id"));
							m.put("rank", resultSet.getString("rank"));
							// m.put("flows",
							// resultSet.getString("flows"));
							tmp.add(m);
						}
						return tmp;
					}
				});
		System.out.println(data);
	}
	
	private static JdbcTemplate initMySql(String mysqlUrl ,String user ,String pwd) {
		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setDriverClassName(mysqlDriver);
		dataSource.setUrl(mysqlUrl);
		dataSource.setUsername(user);
		dataSource.setPassword(pwd);
		dataSource.setRemoveAbandoned(true);
		dataSource.setConnectProperties(getProperties("characterEncoding=utf8"));
		try {
			if ("" != mySqlMaxWait)
				dataSource.setMaxWait(Long.parseLong(mySqlMaxWait));
			if ("" != mySqlMaxActive)
				dataSource.setMaxActive(Integer.parseInt(mySqlMaxActive));
			if ("" != mySqlInitialSize)
				dataSource.setInitialSize(Integer.parseInt(mySqlInitialSize));
			if ("" != mySqlMinIdle)
				dataSource.setMinIdle(Integer.parseInt(mySqlMinIdle));
			if ("" != mySqlRemoveAbandonedTimeout)
				dataSource.setRemoveAbandonedTimeout(Integer.parseInt(mySqlRemoveAbandonedTimeout));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error("mysql peoperties "+ConfigInit.properties);
			logger.error("initMySql", e);
		}

		return new JdbcTemplate(dataSource);
	}

	//
	public static Properties getProperties(String connectionProperties) {
		if (connectionProperties == null || connectionProperties.trim().length() == 0) {
			return null;
		}

		String[] entries = connectionProperties.split(";");
		Properties properties = new Properties();
		for (int i = 0; i < entries.length; i++) {
			String entry = entries[i];
			if (entry.length() > 0) {
				int index = entry.indexOf('=');
				if (index > 0) {
					String name = entry.substring(0, index);
					String value = entry.substring(index + 1);
					properties.setProperty(name, value);
				} else {
					properties.setProperty(entry, "");
				}
			}
		}
		return properties;
	}
	//
	// protected static JedisCluster initRedis() {
	// if (cluster == null) {
	// Set<HostAndPort> jedisNodes = new HashSet<HostAndPort>();
	//
	// jedisNodes.add(new HostAndPort(host, port));
	//
	// GenericObjectPoolConfig pool = new GenericObjectPoolConfig();
	//
	// pool.setMaxWaitMillis(Long.parseLong(ConfigInit.getValue("redisMaxWaitMillis")));
	// pool.setMinIdle(Integer.parseInt(ConfigInit.getValue("redisMinIdle")));
	// pool.setMaxTotal(Integer.parseInt(ConfigInit.getValue("redisMaxTotal")));
	//
	// cluster = new JedisCluster(jedisNodes, pool);
	// }
	//
	// return cluster;
	// }

}