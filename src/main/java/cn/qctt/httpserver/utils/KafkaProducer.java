package cn.qctt.httpserver.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cn.qctt.httpserver.bean.TypeCode;
import cn.qctt.httpserver.core.BasicConstants;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

public class KafkaProducer implements InitializingBean {
	private static Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

	private String kafkaList;
	private static String esHttpUrl = "http://";

	private static Producer<byte[], byte[]> producer = null;

	public String getKafkaList() {
		return kafkaList;
	}

	public void setKafkaList(String kafkaList) {
		this.kafkaList = kafkaList;
	}

	public KafkaProducer() {

	}

	/**
	 * @param obj
	 *            一条avro记录
	 * @param topic
	 *            写到kafka的哪个topic上
	 * @Title: sendMessage
	 */
	public static void send(Serializable obj, String topic) {
		byte[] data = SerializationUtils.serialize(obj);
		send(topic, data);
	}

	public static void send(String topic, byte[] recoder) {
		producer.send(new KeyedMessage<byte[], byte[]>(topic, recoder));
		logger.info("send msg for " + topic);
	}

	public static void send(String topic, String record) {
		send(topic, record.getBytes());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Properties props = new Properties();
		props.put("metadata.broker.list", kafkaList);
		props.put("serializer.class", "kafka.serializer.DefaultEncoder");
		props.put("request.required.acks", "0");
		props.put("compression.codec", "snappy");
		props.put("producer.type", "async");
		props.put("batch.num.messages", "200");

		ProducerConfig config = new ProducerConfig(props);
		logger.info("init kafkaProducer");
		try {
			producer = new Producer<byte[], byte[]>(config);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("", e);
		}

	}

	public static void main(String[] args) {
		esHttpUrl = "http://123.57.134.77:9200/_bulk";
		/*
		 * Properties props = new Properties();
		 * props.put("metadata.broker.list", "123.57.134.77:9092");
		 * props.put("serializer.class", "kafka.serializer.DefaultEncoder");
		 * props.put("request.required.acks", "0");
		 * props.put("compression.codec", "snappy"); props.put("producer.type",
		 * "async"); props.put("batch.num.messages", "200");
		 * 
		 * ProducerConfig config = new ProducerConfig(props); logger.info(
		 * "init kafkaProducer"); producer = new Producer<byte[],
		 * byte[]>(config); producer.send(new KeyedMessage<byte[],
		 * byte[]>("indices", "sssss".getBytes())); System.out.println("end");
		 */
		int from = 0;
		while (true) {
			List<String> ls = getPlayers(from + "", "100");
			if (ls.size() == 0 || from == 10) {
				return;
			}
			saveString(ls);
			from += 10;
		}
	}

	private static void mkJsonObj(String title, String delim, Set<String> ls) {
		StringTokenizer stoken = new StringTokenizer(title, delim);
		String next = "";
		while (stoken.hasMoreTokens()) {
			if (stoken.countTokens() == 0)
				break;
			next = stoken.nextToken();
			ls.add(next);
		}

	}

	public static void saveString(List<String> datas) {
//		long start = System.currentTimeMillis();
		JsonParser paser = new JsonParser();
		int type = 0;
		String id = null;
		StringBuffer docs = new StringBuffer();
		for (String en : datas) {
			JsonObject obj = paser.parse(en).getAsJsonObject();
			type = obj.get("type").getAsInt();
			id = obj.get("id").getAsString();
			obj.remove("type");
			if (TypeCode.NEWS.getType() == type || TypeCode.NEWS_WEIBO.getType() == type
					|| TypeCode.NEWS_VIDEO.getType() == type || TypeCode.POST_BAR.getType() == type) {
				/*
				 * index = getIndex(obj); String title =
				 * obj.get("title").getAsString(); JsonElement keywordsObj =
				 * obj.get("k_w"); Set<String> ls = new HashSet<>(); //只保留中文 英文
				 * 字符[^(a-zA-Z0-9\\u4e00-\\u9fa5)] title =
				 * title.replaceAll("[^(a-zA-Z\\u4e00-\\u9fa5)]", " ");
				 * mkJsonObj(title, " ", ls); Map<String, Object> su = new
				 * HashMap<>(); Map<String, Object> kw = new HashMap<>(); String
				 * pinyin = ""; // ni,hao,shi,jie String pinyinPrefix = "";
				 * Map<String, Object> doc = null; for(String input:ls){
				 * input=input.trim(); if(input.length()<4 ||
				 * input.length()>15){ continue; } su = new HashMap<>(); kw =
				 * new HashMap<>(); pinyin =
				 * PinyinHelper.convertToPinyinString(input, "",
				 * PinyinFormat.WITHOUT_TONE); pinyinPrefix =
				 * PinyinHelper.getShortPinyin(input); su.put("output",
				 * input.trim()); su.put("input", new
				 * String[]{pinyin,pinyinPrefix,input}); su.put("weight", 5);
				 * kw.put("kw_su", su); doc = new HashMap<>(); doc.put("_index",
				 * index); doc.put("_type", table); doc.put("_id", id);
				 * docs.append("{\"index\":"+Utils.getGson().toJson(doc)+"} \n"
				 * ); docs.append(Utils.getGson().toJson(kw) +"\n"); } if(null
				 * != keywordsObj && !keywordsObj.isJsonNull()){ String keywords
				 * = keywordsObj.getAsString(); String[] keywordsArray =
				 * keywords.split(","); for(String kword:keywordsArray){
				 * if(kword.length()<2 ){ continue; } su = new HashMap<>(); kw =
				 * new HashMap<>(); pinyin =
				 * PinyinHelper.convertToPinyinString(kword, " ",
				 * PinyinFormat.WITHOUT_TONE); pinyinPrefix =
				 * PinyinHelper.getShortPinyin(kword); su.put("output",
				 * kword.trim()); su.put("input", new
				 * String[]{pinyin,pinyinPrefix,kword,pinyin.replaceAll(" ",
				 * "")}); kw.put("kw_su", su);
				 * docs.append("{\"index\":"+Utils.getGson().toJson(doc)+"} \n"
				 * ); docs.append(Utils.getGson().toJson(kw) +"\n"); } }
				 */
			} else if (TypeCode.PLAYER.getType() == type) {
				String title = obj.get("title").getAsString();
				String intro = obj.get("intro").getAsString();
				String stars = obj.get("stars").getAsString();
				String[] starArray = stars.split(",");
				title += " " + intro;
				Set<String> ls = new HashSet<>();

				title = title.replaceAll("[^(a-zA-Z\\u4e00-\\u9fa5)]", " ");
				mkJsonObj(title, " ", ls);
				int c = 0;
				for (String name : starArray) {
					name = name.trim();
					if(name.length() < 2){
						continue;
					}
					ls.add(name);
					c++;
					if (c == 3) {
						break;
					}
				}
				Map<String, Object> su = new HashMap<>();
				Map<String, Object> kw = new HashMap<>();
				String pinyin = ""; // ni,hao,shi,jie
				String pinyinPrefix = "";
				Map<String, Object> doc = null;
				for (String input : ls) {
					input = input.trim();
					if (input.length() > 15) {
						continue;
					}
					su = new HashMap<>();
					kw = new HashMap<>();
					pinyin = PinyinHelper.convertToPinyinString(input, "", PinyinFormat.WITHOUT_TONE);
					pinyinPrefix = PinyinHelper.getShortPinyin(input);
					su.put("output", input.trim());
					su.put("input", new String[] { pinyin, pinyinPrefix, input });
					kw.put("title_su", su);
					doc = new HashMap<>();
					doc.put("_index", BasicConstants.INDEX_PLAYERS);
					doc.put("_type", "player");
					doc.put("_id", id);
					docs.append("{\"index\":" + Utils.getGson().toJson(doc) + "} \n");
					docs.append(Utils.getGson().toJson(kw) + "\n");
				}
			} else {
				System.out.println(type);
				System.out.println(en);
				return;
			}
			// LOGGER.info("index "+datas.size()+" use
			// "+(System.currentTimeMillis() - start) +" timing");
			String data = docs.toString();
			 HttpClientUtil.sendPostRequest(esHttpUrl, data, "utf-8");
			// logger.info(DateUtil.getCurrentDate()+": index " + datas.size() +
			// " use " + (System.currentTimeMillis() - start) + " timing");
			 System.out.println("url:"+esHttpUrl+" ; datas:"+data);
			// System.out.println("type:"+type+" ; id:"+id);
		}
	}

	private static List<String> getPlayers(String from, String size) {
		List<String> ls = new ArrayList<>();
		String bodyJson = " {  \"query\": {    \"match_all\": {}  },  \"from\": $from,  \"size\": $size}";
		bodyJson = bodyJson.replace("$from", from).replace("$size", size);
		String jsonStr = HttpClientUtil.sendPostRequest("http://123.57.134.77:9200/players/player/_search", bodyJson,
				"utf-8");
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null) {
			JsonElement hitsEle = obj.get("hits");
			if (null == hitsEle) {
				return ls;
			}
			JsonObject hits = hitsEle.getAsJsonObject();
			for (JsonElement starObj : hits.get("hits").getAsJsonArray()) {
				JsonObject job = starObj.getAsJsonObject();
				JsonObject source = job.get("_source").getAsJsonObject();
				 System.out.println(source.toString());
				source.addProperty("type", 10);
				ls.add(source.toString());
			}
		}
		return ls;
	}
}
