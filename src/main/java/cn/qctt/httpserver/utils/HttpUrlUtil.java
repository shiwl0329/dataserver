package cn.qctt.httpserver.utils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.qctt.httpserver.bean.CurlResponse;

public class HttpUrlUtil {
	private static final Logger logger = LoggerFactory.getLogger(HttpUrlUtil.class);
	
	public static String get(String uri){
		logger.info("get :"+uri);
		String re = "";
		try {
			byte[] bypes = get4Rep(uri).getContent();
			re = new String(bypes);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return re;
	}
	public static CurlResponse get4Rep(String uri){
		logger.info("get :"+uri);
		CurlResponse rep = null;
		try {
			URL url = new URL(uri);
			
			//打开restful链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				        
			// 提交模式
			conn.setRequestMethod("GET");//POST GET PUT DELETE
				        
			//设置访问提交模式，表单提交
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				        
			conn.setConnectTimeout(5000);//连接超时 单位毫秒
			conn.setReadTimeout(2000);//读取超时 单位毫秒
				        
//			conn.setDoOutput(true);// 是否输入参数

			/*StringBuffer params = new StringBuffer();
			// 表单参数与get形式一样
			params.append("customer").append("=").append(1);
			byte[] bypes = params.toString().getBytes();
			conn.getOutputStream().write(bypes);// 输入参数
*/			
			//读取请求返回值
			int code = conn.getResponseCode();
			InputStream inStream= null;
			if(code != 200){
				inStream = conn.getErrorStream();
			}else{
				inStream=conn.getInputStream();
			}
			rep = new CurlResponse(code, inStream);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return rep;
	}
	
	public static String Put(String uri ,String body){
		logger.info("put :"+uri+" ;body :"+body);
		String re = "";
		try {
			byte[] bypes = Put4Curl(uri, body).getContent();
			re = new String(bypes);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return re;
	}
	public static CurlResponse Put4Curl(String uri ,String body){
		logger.info("put :"+uri+" ;body :"+body);
		CurlResponse rep = null;
		try {
			URL url = new URL(uri);
			
			//打开restful链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			// 提交模式
			conn.setRequestMethod("PUT");//POST GET PUT DELETE
			
			//设置访问提交模式，表单提交
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			
			conn.setConnectTimeout(5000);//连接超时 单位毫秒
			conn.setReadTimeout(2000);//读取超时 单位毫秒
			
			conn.setDoOutput(true);// 是否输入参数
			
			/*StringBuffer params = new StringBuffer();
			// 表单参数与get形式一样
			params.append("customer").append("=").append(1);*/
			byte[] bypes = body.getBytes();
			conn.getOutputStream().write(bypes);// 输入参数
			
			//读取请求返回值
			int code = conn.getResponseCode();
			InputStream inStream = null;
			if(code != 200){
				inStream = conn.getErrorStream();
			}else{
				inStream=conn.getInputStream();
			}
			rep = new CurlResponse(code, inStream);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return rep;
	}
	
	public static String Delete(String uri){
		logger.info("url :"+uri);
		String re = "";
		try {
			byte[] bypes = Delete4Curl(uri).getContent();
			re = new String(bypes);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return re;
	}
	public static CurlResponse Delete4Curl(String uri){
		logger.info("delete :"+uri);
		CurlResponse rep = null;
		try {
			URL url = new URL(uri);
			
			//打开restful链接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			// 提交模式
			conn.setRequestMethod("DELETE");//POST GET PUT DELETE
			
			//设置访问提交模式，表单提交
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			
			conn.setConnectTimeout(20000);//连接超时 单位毫秒
			conn.setReadTimeout(15000);//读取超时 单位毫秒
			
			conn.setDoOutput(true);// 是否输入参数
			//读取请求返回值
			int code = conn.getResponseCode();
			InputStream inStream=null;
			if(code != 200){
				inStream = conn.getErrorStream();
			}else{
				inStream=conn.getInputStream();
			}
			rep = new CurlResponse(code, inStream);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("" ,e);
		}
		return rep;
	}
	
	public static void main(String[] args) {
//		System.out.println(DELETE("http://182.92.215.66:9200/nomiss_1512/new_tab/20151208022655656492?routing=other"));
//		String body = HttpClientUtil.sendGetRequest("http://m.weibo.cn/mblogwx?id=3922805903251524");
//		System.out.println(body.substring(0, body.lastIndexOf("<!doctype")));
		System.out.println(get4Rep("http://123.57.134.77:9200/suggest_1511"));
	}
	

}
