package cn.qctt.httpserver.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.pool.MySqlConnectionPool;

public abstract class MySqlUtil {
	private static final Logger logger = LoggerFactory.getLogger(MySqlUtil.class);

	public static List<Map<String, String>> query(String sql) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Connection connection = null;
		try {
			connection = MySqlConnectionPool.getConnection();
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			// 获得结构化的结果集对象包含表信息 ;
			ResultSetMetaData rsmd = rs.getMetaData();
			// 获取我所查询的表的总列数
			int columnCount = rsmd.getColumnCount();
			while (rs.next()) {
				Map<String, String> map = new HashMap<String, String>();
				for (int i = 1; i <= columnCount; i++) {
					String columnName = rsmd.getColumnLabel(i);
					String columnValue = rs.getString(columnName);
					map.put(columnName, columnValue);
				}
				list.add(map);
			}

		} catch (Exception exception) {
			logger.warn("查询失败，请检查sql语法 及格式 -- " + exception.getMessage());
		} finally {
			try {
				MySqlConnectionPool.closeConnection(connection);
			} catch (SQLException e) {
				logger.warn("query:", e);
			}
		}
		return list;
	}

	public static List<Map<String, Object>> resultSet2List(ResultSet rs) throws SQLException {
		List<Map<String, Object>> ls = new ArrayList<Map<String, Object>>();
		if (null == rs) {
			return null;
		} else {
			ResultSetMetaData rsmd = rs.getMetaData();
			// 获取我所查询的表的总列数
			int columnCount = rsmd.getColumnCount();
			while(rs.next()){
				Map<String, Object> m = new HashMap<String,Object>();
				for(int i =0 ; i<columnCount ;i++){
					String column = rsmd.getColumnLabel(i + 1);
					String value = rs.getString(column);
					m.put(column, value);
				}
				ls.add(m);
			}
		}
		return ls;
	}
}
