package cn.qctt.httpserver.utils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.qctt.httpserver.core.ConfigInit;

public class EmailUtil {
	private static final Logger logger = LoggerFactory.getLogger(EmailUtil.class);
	private static Properties properties = null;

	static {
		try {
			// 获取系统属性
			properties = System.getProperties();

			String host = "smtp.qq.com"; // QQ 邮件服务器

			// 设置邮件服务器
			properties.setProperty("mail.smtp.host", host);

			properties.put("mail.smtp.auth", "true");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void send(String subject, String msg) {
		 send(subject, msg , null);
	}
	
	public static void send(String subject, String msg ,String emailTo) {
		// 收件人电子邮箱
		String tos = emailTo;
		if( tos == null){
			tos = ConfigInit.getValue("mailTos");
		}
		// 发件人电子邮箱
		String	 from = ConfigInit.getValue("mailFrom");
		try {
			if (!StringUtils.isEmpty(tos)) {
				String[] toArray = tos.split(",");
				for (String to : toArray) {
					Session session = Session.getDefaultInstance(properties, new Authenticator() {
						public PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(ConfigInit.getValue("mailUser"), ConfigInit.getValue("mailPwd")); // 发件人邮件用户名、密码
						}
					});

					// 创建默认的 MimeMessage 对象
					MimeMessage message = new MimeMessage(session);

					// Set From: 头部头字段
					message.setFrom(new InternetAddress(from));

					// Set To: 头部头字段
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

					// Set Subject: 头部头字段
					message.setSubject(subject);

					// 设置消息体
					message.setText(msg);

					// 发送消息
					Transport.send(message);
				}
				logger.info("Sent message successfully....msg is ["+msg+"] ,tos is ["+tos+"]");
			} else {
				logger.info("not found toUser ,msg is [" + msg + "] ,title is [" + subject + "]");
			}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
