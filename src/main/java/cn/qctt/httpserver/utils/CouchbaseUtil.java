package cn.qctt.httpserver.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.QueryResult;
import com.couchbase.client.java.query.QueryRow;

public class CouchbaseUtil {
//	private static final Logger logger = LoggerFactory.getLogger(CouchbaseUtil.class);

    public static List<Map<String,Object>> resultToList(QueryResult result ,String tabName) {
    	if(StringUtils.isBlank(tabName)){
    		return resultToList(result);
    	}
        List<Map<String,Object>> ls = new ArrayList<Map<String,Object>>();
        for (QueryRow row : result.allRows()) {
        	JsonObject val = row.value().getObject(tabName);
        	if(null == val){
        		val = row.value();
        	}
            ls.add(val.toMap()); // ... adapt to your responses
        }
        return ls;
    }
    public static List<Map<String,Object>> resultToList(QueryResult result) {
    	List<Map<String,Object>> ls = new ArrayList<Map<String,Object>>();
    	for (QueryRow row : result.allRows()) {
    		ls.add(row.value().toMap()); // ... adapt to your responses
    	}
    	return ls;
    }
    public static Set<Map<String,Object>> resultToSet(QueryResult result ,String tabName) {
    	Set<Map<String,Object>> ls = new HashSet<Map<String,Object>>();
    	for (QueryRow row : result.allRows()) {
    		JsonObject val = row.value().getObject(tabName);
        	if(null == val){
        		val = row.value();
        	}
    		ls.add(val.toMap()); // ... adapt to your responses
    	}
    	return ls;
    }
}
