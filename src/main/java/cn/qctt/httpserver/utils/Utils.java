package cn.qctt.httpserver.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.document.JsonDocument;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cn.qctt.httpserver.core.ConfigInit;
import cn.qctt.httpserver.pool.CBaseConnectionPool;
import rx.Observable;
import rx.functions.Func1;

/**
 * @author yanchao_guo
 */
public class Utils {

	private static Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	private static ExecutorService pool = Executors.newCachedThreadPool();
//	private static long preUpTime = 0;
	private static Gson gson = new Gson();
	private static JsonParser jsonParser = new JsonParser();

	public static JsonParser getJsonParser() {
		return jsonParser;
	}

	public static double formatDouble(double num, int scale) {
		BigDecimal bd = new BigDecimal(num);  
		return bd.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	public static String moreCharToOne(String line, String c) {
		return line.replaceAll("(" + c + ")\\1+", "$1");
	}

	public static boolean getEsResultState(String json){
		JsonElement obj = Utils.getJsonParser().parse(json);
		if(null != obj){
			obj.getAsJsonObject().get("errors").getAsBoolean();
		}
		return false;
	}
	
	public static JsonArray fetchDataInES(String url ,String query){
		String jsonStr = HttpClientUtil.sendPostRequest(url,
				query, "utf-8");
		JsonParser paser = new JsonParser();
		JsonObject obj = paser.parse(jsonStr).getAsJsonObject();
		if (obj != null && obj.get("hits") != null) {
			JsonObject hits = obj.get("hits").getAsJsonObject();
			return hits.get("hits").getAsJsonArray();
		}
		return null;
	}
	
	public static String makeStr(Set<String> set,String split){
		if(null == set){
			return "";
		}
		StringBuffer str = new StringBuffer();
		Iterator<String> ite = set.iterator();
		int count = 0;
		for(int i = 0 ;i<set.size() ;i++){
			count++;
			if(count == set.size()){
				str.append(ite.next());
			}else{
				str.append(ite.next()+split);
			}
		}
		return str.toString();
	}
	/*public static JsonObject getByUrl(String url) {
		long start = System.currentTimeMillis();
//		String url = "http://" + ConfigInit.getValue("neo4jHost") + "/" + ConfigInit.getValue("getStarByIds") + "?ids="
		LOGGER.info("getByUrl url [" + url + "]");
		Connection connection = Jsoup.connect(url);
		connection.timeout(5000);
	    JsonObject jsonObj = null;
		try {
			connection.maxBodySize(Integer.MAX_VALUE);

			Connection.Response response = connection.execute();
			if (response.statusCode() != 200) {
				LOGGER.error("服务器错误!!!!");
				throw new IOException();
			}
			jsonObj = Utils.getJsonParser().parse(response.body()).getAsJsonObject();
		} catch (Exception e) {
			LOGGER.error("getByUrl", e);
		}
		LOGGER.info("getByUrl timing "+(System.currentTimeMillis() - start));
		return jsonObj;
	}*/
	
	public static InputStream getConfigFileStream(String fileName) {
		String path = null;
		try {
			path = getProjectPath() + File.separator + "conf" + File.separator + fileName;// jar外部配置路径
			File f = new File(path);
			LOGGER.info(path);
			if (!f.exists()) {
				path = getFilePathInClassPath(fileName);
				LOGGER.info("load " + path);
			}
			f = new File(path);
			/*if (preUpTime != f.lastModified()) {
				preUpTime = f.lastModified();*/
				return new FileInputStream(path);
			/*}
			return null;*/
		} catch (FileNotFoundException e) {
			LOGGER.error("getConfigFileStream error , maybe file <" + path + "> not found");
		}
		return null;
	}

	public static Gson getGson(){
		return gson;
	}
	public static String getFilePathInClassPath(String fileName) {
		return Utils.class.getResource("/conf/" + fileName).getPath();
	}

	public static String getConfigFilePath(String fileName) {
		String path = System.getProperty("user.dir") + File.separator + "conf" + File.separator + fileName;
		File f = new File(path);
		if (!f.exists()) {
			path = File.separator + "conf" + File.separator + fileName;
		}
		LOGGER.info("load file " + path);
		return path;
	}

	public static Integer paseInt(String str, Integer defaultInt) {
		if (null == str || str.trim().length() == 0) {
			return defaultInt;
		} else {
			return Integer.parseInt(str);
		}
	}

	public static long getFileCount(String filePath) {
		long n = 0;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(filePath)));
			while (br.readLine() != null) {
				n++;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info(e.getMessage());
			throw new RuntimeException(e.getMessage());
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return n;
	}

	/**
	 * 获取项目根目录
	 */
	public static String getProjectPath() {
		return System.getProperty("user.dir");
	}

	/**
	 * 在指定目录下遍历出指定文件的路径
	 */
	public static String getFilePath(File rootPath, String fileName) {
		String name = null;
		if (rootPath.isDirectory()) {
			File[] files = rootPath.listFiles();
			for (int i = 0; i < files.length; i++) {
				File f = files[i];
				if (!f.isDirectory() && f.getName().equalsIgnoreCase(fileName)) {
					name = f.getPath();
					break;
				} else {
					name = getFilePath(f, fileName);
					if (fileName.equalsIgnoreCase(name)) {
						break;
					}
				}
			}
		}
		return name;
	}

	public static String getHost(String url) {
		if (url == null || url.trim().equals("")) {
			return "";
		}
		String host = "";
		Pattern p = Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+");
		Matcher matcher = p.matcher(url);
		if (matcher.find()) {
			host = matcher.group();
		}
		return host;
	}

	public static ExecutorService getThreadPool() {
		if (null != pool && pool.isShutdown()) {
			pool = Executors.newCachedThreadPool();
		}
		return pool;
	}

	public static List<String> listPath(String path) {
		List<String> ls = new ArrayList<String>();
		File p = new File(path);
		ls.add(p.getPath().replaceAll("\\\\", "/"));
		while (p.getParent() != null) {
			p = new File(p.getParent());
			ls.add(p.getPath().replaceAll("\\\\", "/"));
		}
		Collections.reverse(ls);
		return ls;
	}

	public static String getNewsId() {
		return DateUtil.getDateFormat("yyyyMMddHHmmss").format(new Date())
				+ (new Random().nextInt(999999) % (999999 - 100000 + 1) + 100000);
	}
	
	public static String getRandomId_6(){
		return (new Random().nextInt(999999) % (999999 - 100000 + 1) + 100000) + "";
	}

	public static <T> Set<T>   subSet(Set<T> set ,int limit){
		Set<T> sets = new LinkedHashSet<>();
		int i = 0;
		for(T s:set){
			sets.add(s);
			i++;
			if(i==limit){
				break;
			}
		}
		return sets;
	}
	
	public static int getExpiry() {
		return ((int) (new Date().getTime() / 1000))
				+ (int) TimeUnit.DAYS.toSeconds(Long.parseLong(ConfigInit.getValue("expiry")));
	}
	
	public static long getTimeMillisBefore(int days){
		return System.currentTimeMillis() - TimeUnit.DAYS.toMillis(days);
	}
	public static long getTimeMillisBeforeH(int hour){
		return System.currentTimeMillis() - TimeUnit.HOURS.toMillis(hour);
	}
	
	public static Set<Map<String, Object>> setJson2Map(Set<String> sets){
		Set<Map<String, Object>> tmp = new LinkedHashSet<Map<String, Object>>();
		for(String json : sets){
			Map<String, Object> map = getGson().fromJson(json, new TypeToken<Map<String, Object>>() {
				private static final long serialVersionUID = 1L;
			}.getType());
			tmp.add(map);
		}
		return tmp;
	}
	public static int randomInt(int seed){
		return new Random().nextInt(seed);
	}
	public static int randomInt(int seed ,int from){
		return new Random().nextInt(seed) + from;
	}
	
	public static String toPlainString(String num){
		if(!StringUtils.isEmpty(num)){
			BigDecimal db = new BigDecimal(num);
			return db.toPlainString();
		}
		return num;
	}
	public static Long toPlainLong(String num){
		return Long.parseLong(toPlainString(num));
	}
	/**
	 * @功能 读取流
	 * @param inStream
	 * @return 字节数组
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception {
		int count = 0;
		while (count == 0) {
			count = inStream.available();
		}
		byte[] b = new byte[count];
		inStream.read(b);
		return b;
	}
	public static  List<JsonDocument> bulkGet(final List<String> ids ,final String bk) {
	    return Observable
	        .from(ids)
	        .flatMap(new Func1<String, Observable<JsonDocument>>() {
	            @Override
	            public Observable<JsonDocument> call(String id) {
	                return CBaseConnectionPool.getBucket(bk).async().get(id);
	            }
	        })
	        .toList()
	        .toBlocking()
	        .single();
	}
	public static void main(String[] args) throws InterruptedException {
		System.out.println(randomInt(1));
		System.out.println(randomInt(2));
		System.out.println(randomInt(2));
		System.out.println(randomInt(2));
		System.out.println(randomInt(1));
		System.out.println(randomInt(1));
	}
}
