package cn.qctt.httpserver.core;

public class RCodeManager {
	public static String userStarFollow = "01";
	public static String userStarSpecial = "02";
	public static String userStarReco = "03";
	public static String userSubPlayer = "04";
//	public static String userStarInterest = "05";

	public static String userNewsCollect = "11";
	public static String userNewsShare = "12";
	public static String userNewsComment = "13";
	public static String userNewsLook = "14";
	public static String userNewsThumb = "15";
	public static String userNewsCommentThumb = "16";

	public static String userBlogFollow = "21";
	public static String userBlogShare = "22";
	public static String userBlogComment = "23";
	public static String userBlogThumb = "24";
	public static String userBlogCommentThumb = "25";

	public static String userVideoCollect = "31";
	public static String userVideoShare = "32";
	public static String userVideoThumb = "33";

	public static String userMusicCollect = "41";
	public static String userMusicShare = "42";
	public static String userMusicThumb = "43";
	
	public static String userPlayCollect = "51";
	public static String userPlayShare = "52";
	public static String userPlayThumb = "53";
	
	public static String userPlayerLook = "61";
}
