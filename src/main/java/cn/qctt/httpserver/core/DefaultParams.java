package cn.qctt.httpserver.core;

public class DefaultParams {
    private String kafkaList = null;

    public String getKafkaList() {
        return kafkaList;
    }

    public void setKafkaList(String kafkaList) {
        this.kafkaList = kafkaList;
    }
}
