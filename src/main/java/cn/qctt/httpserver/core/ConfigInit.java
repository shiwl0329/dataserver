package cn.qctt.httpserver.core;

import java.util.Properties;

import org.springframework.beans.factory.InitializingBean;

/**
 * Created by zhou on 15-8-20.
 */
public class ConfigInit implements InitializingBean{
    public static Properties properties = new Properties();
//    private static Logger logger = LoggerFactory.getLogger(ConfigInit.class);
    private String kafkaOutput = "";
    private String encoder = "";
    private String mySqlUrl = "";
    private String mySqlVideoUrl = "";
    private String mySqlUser = "";
    private String mySqlPasswd = "";
    private String queryStarUrl = "";
    private String mySqlMaxWait = "60000";
    private String mySqlMaxActive = "15";
    private String mySqlInitialSize = "10";
    private String mySqlMinIdle = "8";
    private String mySqlRemoveAbandonedTimeout = "18000";
    private String expiry = "0";
    private String cardLifeAge = "0";
    private String newsCardsLength = "0";
    private String starsTopSize = "0";
    private String hotCardsSize = "0";
    private String queueLimit = "1";
    private String getStarByIds = "";
    private String neo4jHost = "";
    private String getStarByNames = "";
    private String getLookById = "";
    private String reqBlackIps = "";
    private String reqBlackUrls = "";
    private String mySqlVideoName = "";
    private String mySqlVideoPwd = "";
    private String maxLengthInGroup = "";
    private String getRelationByUserIdAndStarNames = "";

    private String adPosition = "";
    private String topicPosition = "";
    private String starPosition = "";
    private String themPosition = "";
    private String hotPosition = "";
    
    private String esUrl = "";
    private String esGetStars2 = "";
    private String esGetStars = "";
    private String esGetNewsQuery = "";
    private String esGetPlayerQuery = "";
    private String esGetNewsWeiBoAndVideoQuery = "";
    private String esGetOtherCardQuery = "";
    private String esStarHomeList = "";
    private String extendStars = "";
    
    private String countFlag = "";
    private String esIndexDel = "";
    
    private String mailPwd = "";
    private String mailUser = "";
    private String mailFrom = "";
    private String mailTos = "";
    
//    private String ioPoolSize = "";
//    private String computationPoolSize = "";
    
	public void setEsGetStars2(String esGetStars2) {
		this.esGetStars2 = esGetStars2;
	}


	public void setCountFlag(String countFlag) {
		this.countFlag = countFlag;
	}


	public void setAdPosition(String adPosition) {
		this.adPosition = adPosition;
	}


	public void setTopicPosition(String topicPosition) {
		this.topicPosition = topicPosition;
	}


	public void setEsGetOtherCardQuery(String esGetOtherCardQuery) {
		this.esGetOtherCardQuery = esGetOtherCardQuery;
	}


	public void setExtendStars(String extendStars) {
		this.extendStars = extendStars;
	}


	public void setStarPosition(String starPosition) {
		this.starPosition = starPosition;
	}


	public void setThemPosition(String themPosition) {
		this.themPosition = themPosition;
	}


	public void setEsStarHomeList(String esStarHomeList) {
		this.esStarHomeList = esStarHomeList;
	}


	public void setHotPosition(String hotPosition) {
		this.hotPosition = hotPosition;
	}


	public void setMaxLengthInGroup(String maxLengthInGroup) {
		this.maxLengthInGroup = maxLengthInGroup;
	}


	public void setMySqlVideoName(String mySqlVideoName) {
		this.mySqlVideoName = mySqlVideoName;
	}


	public void setMySqlVideoPwd(String mySqlVideoPwd) {
		this.mySqlVideoPwd = mySqlVideoPwd;
	}


	public void setMySqlVideoUrl(String mySqlVideoUrl) {
		this.mySqlVideoUrl = mySqlVideoUrl;
	}


	public void setReqBlackIps(String reqBlackIps) {
		this.reqBlackIps = reqBlackIps;
	}


	public void setReqBlackUrls(String reqBlackUrls) {
		this.reqBlackUrls = reqBlackUrls;
	}


	public void setGetRelationByUserIdAndStarNames(String getRelationByUserIdAndStarNames) {
		this.getRelationByUserIdAndStarNames = getRelationByUserIdAndStarNames;
	}

	public void setGetLookById(String getLookById) {
		this.getLookById = getLookById;
	}


	public void setGetStarByNames(String getStarByNames) {
		this.getStarByNames = getStarByNames;
	}


	public void setGetStarByIds(String getStarByIds) {
		this.getStarByIds = getStarByIds;
	}


	public void setNeo4jHost(String neo4jHost) {
		this.neo4jHost = neo4jHost;
	}

	public void setQueueLimit(String queueLimit) {
		this.queueLimit = queueLimit;
	}

	public static void setProperties(Properties properties) {
		ConfigInit.properties = properties;
	}

	public void setKafkaOutput(String kafkaOutput) {
		this.kafkaOutput = kafkaOutput;
	}

	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}

	public void setMySqlUrl(String mySqlUrl) {
		this.mySqlUrl = mySqlUrl;
	}

	public void setMySqlUser(String mySqlUser) {
		this.mySqlUser = mySqlUser;
	}

	public void setMySqlPasswd(String mySqlPasswd) {
		this.mySqlPasswd = mySqlPasswd;
	}

	public void setQueryStarUrl(String queryStarUrl) {
		this.queryStarUrl = queryStarUrl;
	}

	public void setMySqlMaxWait(String mySqlMaxWait) {
		this.mySqlMaxWait = mySqlMaxWait;
	}

	public void setMySqlMaxActive(String mySqlMaxActive) {
		this.mySqlMaxActive = mySqlMaxActive;
	}

	public void setMySqlInitialSize(String mySqlInitialSize) {
		this.mySqlInitialSize = mySqlInitialSize;
	}

	public void setMySqlMinIdle(String mySqlMinIdle) {
		this.mySqlMinIdle = mySqlMinIdle;
	}

	public void setMySqlRemoveAbandonedTimeout(String mySqlRemoveAbandonedTimeout) {
		this.mySqlRemoveAbandonedTimeout = mySqlRemoveAbandonedTimeout;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public void setCardLifeAge(String cardLifeAge) {
		this.cardLifeAge = cardLifeAge;
	}

	public void setNewsCardsLength(String newsCardsLength) {
		this.newsCardsLength = newsCardsLength;
	}

	public void setStarsTopSize(String starsTopSize) {
		this.starsTopSize = starsTopSize;
	}

	public void setHotCardsSize(String hotCardsSize) {
		this.hotCardsSize = hotCardsSize;
	}

	
	public void setEsUrl(String esUrl) {
		this.esUrl = esUrl;
	}


	public void setEsGetStars(String esGetStars) {
		this.esGetStars = esGetStars;
	}


	public void setEsGetNewsQuery(String esGetNewsQuery) {
		this.esGetNewsQuery = esGetNewsQuery;
	}


	public void setEsGetNewsWeiBoAndVideoQuery(String esGetNewsWeiBoAndVideoQuery) {
		this.esGetNewsWeiBoAndVideoQuery = esGetNewsWeiBoAndVideoQuery;
	}


	public void setEsIndexDel(String esIndexDel) {
		this.esIndexDel = esIndexDel;
	}
	
	public void setMailPwd(String mailPwd) {
		this.mailPwd = mailPwd;
	}


	public void setMailUser(String mailUser) {
		this.mailUser = mailUser;
	}


	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}


	public void setMailTos(String mailTos) {
		this.mailTos = mailTos;
	}

	public void setEsGetPlayerQuery(String esGetPlayerQuery) {
		this.esGetPlayerQuery = esGetPlayerQuery;
	}


	private ConfigInit() {
    }

    public synchronized static String getValue(String key) {
        return properties.getProperty(key, "").trim();
    }

	@Override
	public void afterPropertiesSet() throws Exception {
		properties.put("kafkaOutput", kafkaOutput);
		properties.put("encoder", encoder);
		
		properties.put("mySqlUrl", mySqlUrl);
		properties.put("mySqlUser", mySqlUser);
		properties.put("mySqlPasswd", mySqlPasswd);
		properties.put("mySqlMaxWait", mySqlMaxWait);
		properties.put("mySqlMaxActive", mySqlMaxActive);
		properties.put("mySqlInitialSize", mySqlInitialSize);
		properties.put("mySqlMinIdle", mySqlMinIdle);
		properties.put("mySqlRemoveAbandonedTimeout", mySqlRemoveAbandonedTimeout);
		properties.put("expiry", expiry);
		properties.put("queryStarUrl", queryStarUrl);
		properties.put("starsTopSize", starsTopSize);
		properties.put("cardLifeAge", cardLifeAge);
		properties.put("newsCardsLength", newsCardsLength);
		properties.put("hotCardsSize", hotCardsSize);
		properties.put("queueLimit", queueLimit);
		properties.put("getStarByIds", getStarByIds);
		properties.put("neo4jHost", neo4jHost);
		properties.put("getStarByNames", getStarByNames);
		properties.put("reqBlackIps", reqBlackIps);
		properties.put("getLookById", getLookById);
		properties.put("reqBlackUrls", reqBlackUrls);
		properties.put("getRelationByUserIdAndStarNames", getRelationByUserIdAndStarNames);
		properties.put("mySqlVideoName", mySqlVideoName);
		properties.put("mySqlVideoPwd", mySqlVideoPwd);
		properties.put("mySqlVideoUrl", mySqlVideoUrl);
		properties.put("maxLengthInGroup", maxLengthInGroup);
		
		properties.put("adPosition", adPosition);
		properties.put("topicPosition", topicPosition);
		properties.put("themPosition", themPosition);
		properties.put("starPosition", starPosition);
		properties.put("hotPosition", hotPosition);
		
		properties.put("esGetNewsWeiBoAndVideoQuery", esGetNewsWeiBoAndVideoQuery);
		properties.put("esGetNewsQuery", esGetNewsQuery);
		properties.put("esUrl", esUrl);
		properties.put("esGetStars2", esGetStars2);
		properties.put("esGetPlayerQuery", esGetPlayerQuery);
		properties.put("esGetStars", esGetStars);
		properties.put("esStarHomeList", esStarHomeList);
		properties.put("esGetOtherCardQuery", esGetOtherCardQuery);
		properties.put("extendStars", extendStars);
		
		properties.put("countFlag", countFlag);
		properties.put("esIndexDel", esIndexDel);
		
		properties.put("mailPwd", mailPwd);
		properties.put("mailUser", mailUser);
		properties.put("mailFrom", mailFrom);
		properties.put("mailTos", mailTos);
		
//		properties.put("ioPoolSize", ioPoolSize);
//		properties.put("computationPoolSize", computationPoolSize);
	}
}
