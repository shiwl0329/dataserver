package cn.qctt.httpserver.core;

public class BasicConstants {
    public static String JSONP_RETURN_CALL_METHOD = "callback";
    public final static String HTTP_SERVER_URL_CHARCODE_UTF_8 = "UTF-8";
//
//    public final static int RETURN_CODE_SUCCESS = 0;
//    public final static int RETURN_CODE_ERROR = 1;

    public final static String ADMIN_TOKEN = "NeXT";
//    public final static String LAST_REFRESH_PREFIX = "last_refresh";
    
    public final static String SOURCE_COLUMN_NAME = "source";
    
    public final static String ENTITY_HOME_PREFIX = "entityHome";
    public final static String USER_STATE_PREFIX = "state";
    public final static String USER_SHORT_PROFILE = "shortProfile";
    public final static String STAR_HOME_CACHE_PREFIX = "starHomeCache";
//    public final static String STAR_HOME_SCORE_PREFIX = "starHomeScore";
    public final static String ENTITY_HOME_LEN_PREFIX = "incr";
    public final static int STAR_HOME_LEN_MAX = 200;
    
    public final static String TIMING = "timing";
  
    public final static String NEWS_LINKED_TYPE = "native";
    public final static String LINKED_TYPE_NEW_TAB = "new_tab";
    
    public final static String SOURCE_OTHER_BUCKET = "source_other";
    public final static String COMMENTS_BUCKET = "comments";
    public final static String SOURCE_BUCKET = "default";
//    public final static String BK_CONFIG_BUCKET = "bk_config";
    public final static String NEWS_HTABLE = "news";
//    public final static String USER_ATTRIBUTE_BUCKET = "user_attribute";
    
    public final static int ENTITY_MAX_PASS_NEWS_SIZE = 2;
    /**
     * redis db key
     */
    public final static String DISABLED_CLASS_KEY = "disabled_class";
    
    public final static String RC_VIDEO_PREFIX = "rcVideo";
    public final static String RC_NEWS_PREFIX = "rcNews";
    public final static String RC_PLAYER_PREFIX = "rcPlayer";
    public final static String RC_ENTITY_PREFIX = "rcEntity";
    public final static String RC_BAR_PREFIX = "rcBar";
    
    /**
     * couchbase 表
     */
    public final static String USER_STAR_BUCKET = "user_star";
    public final static String USER_NEWS_BUCKET = "user_news";
    public final static String STAR_STAR_BUCKET = "star_star";
    public final static String USER_ENTITY_BUCKET = "user_entity";
    /**
     * elasticsearch 相关
     */
    public final static String ENTITY_INDEX_DB = "entity";
    public final static String ENTITY_INDEX_TABLE_OTHER = "other";
    public final static String ENTITY_INDEX_TABLE_PLAYER = "player";
    public final static String ENTITY_SUGGEST_FIELD = "name_su";
    /**
     * mysql 相关
     */
    public final static String MYSQL_ENTITY_TABLE = "entity";
    public final static String MYSQL_ENTITY_RELAS_TABLE = "entity_relas";
    public final static String MYSQL_ENTITY_CODE_TABLE = "entity_code";
    public final static String MYSQL_ENTITY_CLASS_TABLE = "entity_class";
    
    /**
     * nomiss 3.0 相关
     */
    public final static String REDIS_USER_ENTITIES_PREFIX = "user_entities";
    public final static String MY_LOG_PREFIX = "=|";
    public final static String MY_LOG_SPLIT = "|";
    
    
    
    public final static String URL_PREFIX = "/dataServer/V3.0";
    
    /**
     * relation code
     */
    public final static String SUB_ENTITY_RELATION_CODE = "11";
    public final static String UNSUB_ENTITY_RELATION_CODE = "10";
    public final static String UNINTERESTED_ENTITY_RELATION_CODE = "30";
    
    //redis 中容器过期时间
    public final static int REDIS_EXPIRE = 7 * 3600 * 24;
    
    public final static String READ_LIST_MAIN_CHANNEL = "main";
    //card channel and app channel
    public final static String APP_CACHE_CHANNEL = "cache";
    public final static String USER_FEED = "distribute";
    public final static String HOT_CARD_PREFIX = "hot";
    public final static String HOT_READ_READED_PREFIX = "hot_readed";
    public final static String HOT_CARD_READ = "hot_card_read";
    public final static String THEME_CARD_CHANNEL = "theme_card";
    public final static String THEME_CARD_READ = "theme_card_read";
    public final static String AD_CARD_CHANNEL = "ad_card";
    public final static String AD_CARD_READ = "ad_card_read";
    public final static String TOPIC_CARD_CHANNEL = "topic_card";
    public final static String TOPIC_CARD_READ = "topic_card_read";
    public final static String STAR_CARD_PREFIX = "hot_stars";
    public final static String STAR_CARD_READ = "star_card_read";
    
    public final static String STAR_CARD_NAME = "推荐关注";
    public final static String HOT_CARD_NAME = "娱乐热点";
    
    //card type
    public final static String HOT = "hot";
    public final static String THEME = "theme";
    public final static String AD = "ad";
    public final static String STAR = "star";
    public final static String TOPIC = "topic";
    //position
    public final static int AD_POSITION = 1;
    public final static int TOPIC_POSITION = 5;
    public final static int STAR_POSITION = 9;
    public final static int THEME_POSITION = 3;
    public final static int ACTIVITY_POSITION = 7;
    public final static int HOT_POSITION1 = 2;
//    public final static int HOT_POSITION2 = 14;
    
    public final static String RULENAME = "rule.drl";
//    public final static int MAX_QUERY_LENGTH_FOR_JUMP = 6;
    public final static int KSESSION_REFRESH_INTERVAL = 5;
    public final static int REDIS_POOL_REFRESH_INTERVAL = 60*1;
    
    public final static String STAR_USERS_PREFIX = "star_users";
    public final static String USER_STARS_PREFIX = "user_stars";
    public final static String PLAYER_USERS_PREFIX = "player_users";
    public final static String USER_PLAYERS_PREFIX = "user_players";
    
    public final static String ES_KAFKA_TOPIC = "indices";
    public final static String INDEX_PREFIX = "nm_";
    
    public final static String REDIS_COMMENT_COUNT = "comment_count";
    public final static String REDIS_CLICK_COUNT = "click_count";
    
    public final static String ES_QUERY_STARS2 = "esGetStars2";
    public final static String ES_QUERY_STARS = "esGetStars";
    
    public final static double QUERY_SCORE_PERCENT = 0.5;
    
    
    public final static int STAR_MAP_TOP_NUM = 6;
    
    
    
    public static final String NEWS_WEIBO_VIDEO = "news,news_video,news_weibo";
    public static final String NEWS_AND_WEIBO = "news,news_weibo";
    public static final String NM_NEWS = "news";
    public static final String NM_WEIBO = "news_weibo";
    public static final String NM_VIDEO = "news_video";
    public static final String NM_POST = "post_bar";
    public static final String CARD_AD = "ad";
    public static final String CARD_TOPIC = "topic";
    public static final String CARD_THEME = "theme";
    public static final String CARD_ARDENLY = "ardenly";
    public static final String CARD_ACTIVITY = "activity";
    public static final String INDEX_STARS = "stars";
    public static final String INDEX_PLAYERS = "players";
    public static final String INDEX_OTHER_CARD = "other_card";
    
}
