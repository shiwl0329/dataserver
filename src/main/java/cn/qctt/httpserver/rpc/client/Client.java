package cn.qctt.httpserver.rpc.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.qctt.httpserver.rpc.server.FooService;

public class Client {
	 public static void main(String[] args) throws InterruptedException {
	        @SuppressWarnings("resource")
			ApplicationContext ctx = new ClassPathXmlApplicationContext("spring/motan_client.xml");
	        FooService service = (FooService) ctx.getBean("remoteService");
	        System.err.println(service.hello("motan"));
	    }
}
