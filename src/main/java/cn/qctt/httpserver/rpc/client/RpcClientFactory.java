package cn.qctt.httpserver.rpc.client;

import org.apache.hadoop.yarn.factories.RpcServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.allydata.VideoProcessService;

public class RpcClientFactory {
	private static ApplicationContext videoCtx = null;
	private static VideoProcessService videoService = null;

	static{
		if(null == videoService){
			synchronized (RpcServerFactory.class) {
				if(null == videoCtx){
					videoCtx = new ClassPathXmlApplicationContext("spring/video_client.xml");
				}
			}
		}
	}
	
	public static VideoProcessService getVideoProcessService(){
		videoService = (VideoProcessService) videoCtx.getBean("videoProcessService");
		return videoService;
	} 
	
	
}
