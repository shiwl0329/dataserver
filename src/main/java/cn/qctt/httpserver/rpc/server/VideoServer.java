package cn.qctt.httpserver.rpc.server;

import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.qctt.httpserver.rpc.client.RpcClientFactory;

public class VideoServer {
	public static List<Map<String, Object>> getVideoStream(String h5){
		return RpcClientFactory.getVideoProcessService().getVideoStreamUrl(h5);
	}
	public static Set<String> getAllProcess(){
		return RpcClientFactory.getVideoProcessService().getAllProcess();
	}
}
